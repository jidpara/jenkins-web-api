package com.gk337;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.gk337.common.core.AppConfig;

import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

@SpringBootApplication
@EnableFeignClients(basePackages = "com.gk337")
@EnableTransactionManagement
@Slf4j
public class JGWApiServiceApplication implements CommandLineRunner {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(JGWApiServiceApplication.class);

	@Autowired(required=true)
	private AppConfig config;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;

	public static void main(String[] args) {
		SpringApplication.run(JGWApiServiceApplication.class, args);
	}

	@PostConstruct
	public void started() {
		System.out.println("Default 현재시각 : " + new Date());
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Seoul"));
		System.out.println("Asia/Seoul 현재시각 : " + new Date());
	}
	
	@Override
	public void run(String... args) throws Exception {
		log.debug(bcryptEncoder.encode("1234"));
		log.debug("run active ... {}", config.getActive());
		log.debug("run appName ... {}", config.getAppName());
		
	}

}
