package com.gk337.common.exception;

public class JGWCommunicationException extends Exception {
    public JGWCommunicationException(String message){
        super(message);
    }
}
