package com.gk337.common.exception;

import com.gk337.common.core.exception.BizException;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 공통모듈
 * 프로그램명 : JGWServiceException.java
 *
 *  - RestAPI 관련 공통 Exception 클래스 -
 *
 * </PRE>
 *
 * @since 2020.01.26
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

public class JGWServiceException extends BizException {
	
	private static final long serialVersionUID = -7814830180580932023L;

	public JGWServiceException(String exceptionCode) {
		super(exceptionCode);
	}

	public JGWServiceException(String exceptionCode, Throwable cause) {
		super(exceptionCode, cause);
	}

	public JGWServiceException(String exceptionCode, String... messageArgs) {
		super(exceptionCode, messageArgs);
	}

	public JGWServiceException(String exceptionCode, Object... messageArgs) {
		super(exceptionCode, messageArgs);
	}
}
