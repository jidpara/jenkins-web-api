package com.gk337.common.util;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;

import java.util.List;

public class CollectionUtils {

    public static <L, T> List<L> convert(List<T> list, Builder<L> builder) {
        return Lists.transform(list, new Function<T, L>() {

            public L apply(T from) {

                L magic = builder.build();
                BeanUtils.copyProperties(from, magic, BeanUtilsExt.getNullPropertyNames(from));

                return magic;
            }
        });
    }
}
