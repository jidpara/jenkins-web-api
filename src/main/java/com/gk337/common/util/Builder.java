package com.gk337.common.util;

public interface Builder<T> {
  public T build();
}