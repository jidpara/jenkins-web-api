package com.gk337.common.util;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageUtils {

    public static File convertFile(MultipartFile mfile) {
        File file = new File(mfile.getOriginalFilename());
        try {
            file.createNewFile();

        FileOutputStream fos = new FileOutputStream(file);
        fos.write(mfile.getBytes());
        fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static BufferedImage checkImage(File file, int orientation) throws Exception {
        BufferedImage bi = ImageIO.read(file);
        if (orientation == 1) { // 정위치
            return bi;
        } else if (orientation == 6) {
            return rotateImage(bi, 90);
        } else if (orientation == 3) {
            return rotateImage(bi, 180);
        } else if (orientation == 8) {
            return rotateImage(bi, 270);
        } else{
            return bi;
        }
    }

    public static int getOrientation(File file) throws Exception {
        int orientation = 1;
        Metadata metadata = ImageMetadataReader.readMetadata(file);
        Directory directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
        if (directory != null) {
            orientation = directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
        }
        return orientation;
    }

    private static BufferedImage rotateImage (BufferedImage orgImage, int radians) {
        BufferedImage newImage;
        if(radians == 90 || radians == 270) {
            newImage = new BufferedImage(orgImage.getHeight(),orgImage.getWidth(),orgImage.getType());
        } else if (radians==180){
            newImage = new BufferedImage(orgImage.getWidth(),orgImage.getHeight(),orgImage.getType());
        } else{
            return orgImage;
        }
        Graphics2D graphics = (Graphics2D) newImage.getGraphics();
        graphics.rotate(Math.toRadians(radians), newImage.getWidth() / 2, newImage.getHeight() / 2);
        graphics.translate((newImage.getWidth() - orgImage.getWidth()) / 2, (newImage.getHeight() - orgImage.getHeight()) / 2);
        graphics.drawImage(orgImage, 0, 0, orgImage.getWidth(), orgImage.getHeight(), null);

        return newImage;
    }
}
