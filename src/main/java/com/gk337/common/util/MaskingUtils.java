package com.gk337.common.util;

public class MaskingUtils {
    public static String maskCardNumber(String cardNumber, String mask) {

    	if(cardNumber == null || "".equals(cardNumber)) {
    		return "";
		}
	    // format the number
	    int index = 0;
	    StringBuilder maskedNumber = new StringBuilder();
	    for (int i = 0; i < mask.length(); i++) {
	        char c = mask.charAt(i);
	        if (c == '#') {
	            maskedNumber.append(cardNumber.charAt(index));
	            index++;
	        } else if (c == 'x') {
	            maskedNumber.append(c);
	            index++;
	        } else {
	            maskedNumber.append(c);
	        }
	    }

	    // return the masked number
	    return maskedNumber.toString();
	}

	public static void main(String[] args) {
		System.out.println(maskCardNumber("6210038058377055", "##**********####"));
	}
}
