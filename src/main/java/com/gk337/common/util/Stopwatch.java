package com.gk337.common.util;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 공통 유틸
 * 프로그램명 : Stopwatch.java
 *
 *  - 수행 시간 측정용 유틸 클래스 -
 *
 * </PRE>
 *
 * @since   2020. 01. 26
 * @author  sejoonhwang
 * @version 1.0
 * @see
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

public class Stopwatch {

    private long start;

    public Stopwatch() {
        reset();
    }

    public void reset() {
        this.start = System.currentTimeMillis();
    }

    public long elapsed() {
        long elapsed = System.currentTimeMillis() - this.start;
        reset();
        return elapsed;
    }

    /*public static void main(String[] args) {
        System.out.println("stopwatch start");
        Stopwatch stopwatch = new Stopwatch();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) { }

        System.out.println(stopwatch.elapsed());

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) { }

        System.out.println(stopwatch.elapsed());
    }*/
}