package com.gk337.common.util;

public class BizUtils {

	public static boolean checkBizNum(String bizNum) {
		
		String[] strs = bizNum.split("");
        if (strs.length != 10) return false;

        int[] ints = new int[10];
        for(int i=0; i<10; i++) {
            ints[i] = Integer.valueOf(strs[i]);
        }
        
        int sum = 0;
        int[] indexs = new int[] { 1, 3, 7, 1, 3, 7, 1, 3 };
        for (int i = 0; i < 8; i++) {
            sum += ints[i] * indexs[i];
        }
        int num = ints[8] * 5;

        sum += (num / 10) + (num % 10);
        sum = 10 - (sum % 10);
        if(sum== 10){
             return 0 == ints[9] ? true : false;
        }else{
             return sum == ints[9] ? true : false;
        }
	}
	
	public static void main(String[] args) {
		System.out.println( BizUtils.checkBizNum("1231254321") );
	}
}
