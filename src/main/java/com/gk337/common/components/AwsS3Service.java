package com.gk337.common.components;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 공통모듈
 * 프로그램명 : AwsS3Service.java
 *
 *  - AWS S3 관련 서비스 클래스 -
 *
 * </PRE>
 *
 * @since 2020.02.20
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */
@Slf4j
@Service
public class AwsS3Service {

    @Autowired
    private AmazonS3 s3Client;

    @Value("${aws.bucket}")
    private String bucketName;

    @Value("${aws.end-point-url}")
    private String endPointUrl;

    /**
     * AWS S3 에 파일업로드 후 Cloud Front EndPoint URL을 리턴
     *
     * @param multipartFile
     * @param assetPath
     * @param fileChgName
     * @return String - Cloud Front End-Point URL
     * @throws IOException
     */
    public String uploadObject(MultipartFile multipartFile, String assetPath, String fileChgName) throws IOException {

        ObjectMetadata omd = new ObjectMetadata();
        omd.setContentType(multipartFile.getContentType());
        omd.setContentLength(multipartFile.getSize());
        omd.setHeader("filename", fileChgName);

        log.info("[AWS_S3] uploadObject");
        log.info("[AWS_S3] bucketName : " + bucketName + assetPath);
        log.info("[AWS_S3] fileName : " + multipartFile.getOriginalFilename());
        log.info("[AWS_S3] fileChgName : " + fileChgName);
        log.info("[AWS_S3] Cloud Front EndPoint URL : " + bucketName + assetPath + "/" + fileChgName);
        // Copy file to the target location (Replacing existing file with the same name)
        // 주의 - bucket name 마지막에 "/" 들어가면 파일 정상 생성 되지 않고 성공함.
        PutObjectResult result = s3Client.putObject(new PutObjectRequest(bucketName + assetPath, fileChgName,
                multipartFile.getInputStream(), omd));

        log.info("[AWS_S3] result : " + result.getContentMd5());

        return endPointUrl + assetPath + "/" + fileChgName;

    }

    /**
     * AWS S3 에 파일업로드 후 Cloud Front EndPoint URL을 리턴
     *
     * @param multipartFile
     * @param assetPath
     * @param fileChgName
     * @return String - Cloud Front End-Point URL
     * @throws IOException
     */
    public String uploadObject(File file, String assetPath, String fileChgName) throws IOException {

        log.info("[AWS_S3] uploadObject");
        log.info("[AWS_S3] bucketName : " + bucketName + assetPath);
        log.info("[AWS_S3] fileName : " + file.getName());
        log.info("[AWS_S3] fileChgName : " + fileChgName);
        log.info("[AWS_S3] Cloud Front EndPoint URL : " + bucketName + assetPath + "/" + fileChgName);
        // Copy file to the target location (Replacing existing file with the same name)
        // 주의 - bucket name 마지막에 "/" 들어가면 파일 정상 생성 되지 않고 성공함.
        PutObjectResult result = s3Client.putObject(new PutObjectRequest(bucketName + assetPath, fileChgName, file));

        log.info("[AWS_S3] result : " + result.getContentMd5());

        return endPointUrl + assetPath + "/" + fileChgName;

    }

    // 미사용
    public void deleteObject(String date, String storedFileName) throws AmazonServiceException {

        s3Client.deleteObject(new DeleteObjectRequest(bucketName + "/" + date, storedFileName));

    }

    // 미사용
    public Resource getObject(String date, String storedFileName) throws IOException {
        S3Object o = s3Client.getObject(new GetObjectRequest(bucketName + "/" + date, storedFileName));
        S3ObjectInputStream objectInputStream = o.getObjectContent();
        byte[] bytes = IOUtils.toByteArray(objectInputStream);

        Resource resource = new ByteArrayResource(bytes);
        return resource;
    }

}