package com.gk337.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 공통모듈
 * 프로그램명 : LogDuration.java
 * 
 *  - Spring AOP 기능을 구현한 수행시간 측정용 어노테이션 클래스 -
 *  
 *  Spring Component 스캔 범위 내의 클래스/메소드레벨에 적용
 *
 * </PRE> 
 * 
 * @since 2020.01.25
 * @author sejoonhwang
 * @version 1.0
 * @see com.gk337.common.aop.JWGAspectJ
 * @see
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE> 
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface LogDuration {

	// 수행시간 측정 명
	String value() default "duration";	
}
