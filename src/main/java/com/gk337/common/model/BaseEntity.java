package com.gk337.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gk337.common.core.exception.ErrorVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.net.UnknownHostException;
import java.time.LocalDateTime;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity {

	@CreatedDate
	@JsonIgnore
	@Column(name="reg_date", nullable=false, updatable=false)
	@ApiModelProperty(notes = "등록일")
	@JsonProperty(value = "reg_date", access = JsonProperty.Access.WRITE_ONLY)
	private LocalDateTime regDate;
	
	@JsonIgnore
	@LastModifiedDate
	@Column(name="chg_date", nullable=false)
	@ApiModelProperty(notes = "수정일자")
	@JsonProperty(value = "chg_date", access = JsonProperty.Access.WRITE_ONLY)
	private LocalDateTime chgDate;
	

	// 로그인정책 결정된 후 세션의 로그인 아이디로 변경할 예정임.
	@PrePersist
	public void prePersist() throws UnknownHostException {
		this.preUpdate();
	}

	// 로그인정책 결정된 후 세션의 로그인 아이디로 변경할 예정임.
	@PreUpdate
	public void preUpdate() throws UnknownHostException {
	}
	
	@JsonProperty("error")
	@Transient
	private ErrorVo errorVo = new ErrorVo();
	
}