package com.gk337.common.model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gk337.common.core.exception.ErrorVo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class AuditEntity {

	@CreatedBy
	@JsonIgnore
	@Column(name="reg_id", nullable=false, length=20, updatable=false)
	@ApiModelProperty(notes = "등록자")
	@JsonProperty(value = "reg_id", access = JsonProperty.Access.WRITE_ONLY)
	private String regId;
	
	@CreatedDate
	@JsonIgnore
	@Column(name="reg_date", nullable=false, updatable=false)
	@ApiModelProperty(notes = "등록일")
	@JsonProperty(value = "reg_date", access = JsonProperty.Access.WRITE_ONLY)
	private LocalDateTime regDate;
	
	@JsonIgnore
	@Column(name="reg_ip", nullable=false, updatable=false)
	@ApiModelProperty(notes = "등록자 아이피")
	@JsonProperty(value = "reg_ip", access = JsonProperty.Access.WRITE_ONLY)
	private String regIp;
	
	@JsonIgnore
	@LastModifiedBy
	@Column(name="chg_id", nullable=false, length=20)
	@ApiModelProperty(notes = "수정자")
	@JsonProperty(value = "chg_id", access = JsonProperty.Access.WRITE_ONLY)
	private String chgId;
	
	@JsonIgnore
	@LastModifiedDate
	@Column(name="chg_date", nullable=false)
	@ApiModelProperty(notes = "수정일자")
	@JsonProperty(value = "chg_date", access = JsonProperty.Access.WRITE_ONLY)
	private LocalDateTime chgDate;
	
	@JsonIgnore
	@Column(name="chg_ip", nullable=false)
	@ApiModelProperty(notes = "수정자 아이피")
	@JsonProperty(value = "chg_ip", access = JsonProperty.Access.WRITE_ONLY)
	private String chgIp;
	
	@Transient
	@JsonIgnore
	public String ip;

	// 로그인정책 결정된 후 세션의 로그인 아이디로 변경할 예정임.
	@PrePersist
	public void prePersist() throws UnknownHostException {
		this.regId = this.regId == null ? "admin" : this.regId;
		this.regIp = this.getIp();
		this.preUpdate();
	}

	// 로그인정책 결정된 후 세션의 로그인 아이디로 변경할 예정임.
	@PreUpdate
	public void preUpdate() throws UnknownHostException {
		this.chgId = this.chgId == null ? "admin" : this.chgId;
		this.chgIp = this.getIp();
	}
	
	public String getIp() throws UnknownHostException {
		InetAddress local = InetAddress.getLocalHost();
		return local.getHostAddress(); 
	}
	
	@JsonProperty("error")
	@Transient
	private ErrorVo errorVo = new ErrorVo();
	
}