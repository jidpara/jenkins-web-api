package com.gk337.common.aop;

import com.gk337.api.base.entity.AccessCountEntity;
import com.gk337.api.base.repository.AccessCountEntityRepository;
import com.gk337.common.annotation.AccessLog;
import com.gk337.common.annotation.LogDuration;
import com.gk337.common.core.aop.JGWApiAspect;
import com.gk337.common.util.Stopwatch;
import org.apache.commons.lang.time.StopWatch;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 공통모듈
 * 프로그램명 : JWGAspectJ.java
 * 
 *  - AOP 클래스 -
 *
 * Component scan 범위 내의 클래스/메소드레벨에 적용 가능.
 *
 * </PRE> 
 * 
 * @since 2020.01.25
 * @author sejoonhwang
 * @version 1.0
 * @see com.gk337.common.annotation.AccessLog
 * @see com.gk337.common.annotation.LogDuration
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE> 
 */

@Component
@Aspect
public class JWGAspectJ {

	private static final Logger LOGGER = LoggerFactory.getLogger(JWGAspectJ.class);

	@Autowired
	private AccessCountEntityRepository accessCountEntityRepository;

	/**
	 * 사용이력 AccessLog aop 설정
	 * '@AccessLog' annotation을 설정한 모든 리턴값 모든 함수 모든 인자값에 대해서 동작
	 * 
	 * @param joinPoint
	 * @param accessLogLogAnnotation - 함수에 마킹 된 AccessLog Annotation 객체
	 * @return
	 * @throws Throwable
	 */
	@Around("execution(@com.gk337.common.annotation.AccessLog * *(..)) && @annotation(accessLogLogAnnotation)")
	public Object auditLog(ProceedingJoinPoint joinPoint, AccessLog accessLogLogAnnotation)
		throws Throwable {

		Stopwatch stopwatch = new Stopwatch();

		String name = accessLogLogAnnotation.name();
		LOGGER.info("[ACCESS] " + name + " Start. ");

		Object result = null;
		try{
			// 실행 메소드 결과를 얻는다.
			result = joinPoint.proceed();
		}catch (Exception ex) {
			long elapsed = stopwatch.elapsed();
			LOGGER.error("[ACCESS] " + name + " End. : 수행시간 : " + elapsed + " ms");
		}finally {
			// 수행시간 기록
			long elapsed = stopwatch.elapsed();
			// 일단은 로그만 기록하도록 함
			// TODO : Mongodb 에 API 조회 이력 insert
			LOGGER.info("[ACCESS] " + name + " End. : 수행시간 : " + elapsed + " ms");

		}

		return result;
	}
	
	
	/**
	 * 수행시간 체크  pointCut
	 */
	@Pointcut("@annotation(com.gk337.common.annotation.LogDuration)")
	public void targetMethod() {}

	/**
	 * 수행시간 로그 around 설정
	 * '@LogDuration' annotation을 설정한 모든 리턴값 모든 함수 모든 인자값에 대해서 동작
	 * 
	 * @param joinPoint
	 * @param logDurationAnnotation - 함수에 마킹 된 LogDuration Annotation 객체
	 * @see LogDuration
	 * @return
	 * @throws Throwable
	 */
	@Around("targetMethod() && @annotation(logDurationAnnotation)")
	private Object methodDuration(ProceedingJoinPoint joinPoint, LogDuration logDurationAnnotation) throws Throwable {

		Stopwatch stopwatch = new Stopwatch();
		// 실행 메소드 결과를 얻는다.
		Object result = joinPoint.proceed();
		// 수행시간 기록
		LOGGER.info("[Duration] "+ logDurationAnnotation.value() + " : " + stopwatch.elapsed() + " ms ");
		
		return result;
	}


	/**
	 * 유입되는 Api 로깅 Aop
	 */
//	@Pointcut("execution(* com.gk337.api.*.controller.*Controller.*(..))")
	@Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)"
			+ " || @annotation(org.springframework.web.bind.annotation.PostMapping)"
			+ " || @annotation(org.springframework.web.bind.annotation.PutMapping)"
			+ " || @annotation(org.springframework.web.bind.annotation.DeleteMapping)")
	public void jgwApiAspect() {
		//
	}

	@Before("jgwApiAspect() && @annotation(requestMappingAnnotaion)")
	public void jgwApiBefore(final JoinPoint joinPoint, RequestMapping requestMappingAnnotaion) throws Exception {

		HttpMethod httpMethod  = null;
		String fullUrl = "";
		String url = "";

		final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		final Method method = signature.getMethod();

		if(!method.getDeclaringClass().isAnnotationPresent(FeignClient.class)) {

			if(method.getDeclaringClass().isAnnotationPresent(RequestMapping.class)) {
				url = String.join("", method.getDeclaringClass().getAnnotation(RequestMapping.class).value());
				httpMethod = HttpMethod.resolve(requestMappingAnnotaion.method()[0].name());
			}

			if (method.isAnnotationPresent(GetMapping.class)) {
				httpMethod = HttpMethod.GET;
				fullUrl = url + String.join("", method.getAnnotation(GetMapping.class).value());
			} else if (method.isAnnotationPresent(PostMapping.class)) {
				httpMethod = HttpMethod.POST;
				fullUrl = url + String.join("", method.getAnnotation(PostMapping.class).value());
			} else if (method.isAnnotationPresent(PutMapping.class)) {
				httpMethod = HttpMethod.PUT;
				fullUrl = url + String.join("", method.getAnnotation(PutMapping.class).value());
			} else if (method.isAnnotationPresent(DeleteMapping.class)) {
				httpMethod = HttpMethod.DELETE;
				fullUrl = url + String.join("", method.getAnnotation(DeleteMapping.class).value());
			}

			if(httpMethod != null) {
				AccessCountEntity accessCountEntity = accessCountEntityRepository.findByMethodAndUri(httpMethod.name(), url);
				if(accessCountEntity == null){
					accessCountEntity = new AccessCountEntity();
					accessCountEntity.setMethod(httpMethod.name());
					accessCountEntity.setUri(url);
					accessCountEntityRepository.save(accessCountEntity);
				}
				accessCountEntityRepository.updateCount(httpMethod.name(), url);
			}
		}
	}

}