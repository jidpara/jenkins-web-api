package com.gk337.v2api.point.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Builder
@Data
@Entity
@Table(name = "jw_point_detail")
public class PointDetailEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, name = "seq", nullable = false)
    private Integer seq;

    /**
     * 요청서 시퀀스
     */
    @Column(name = "req_seq")
    private Integer reqSeq;

    /**
     * [PT002] 포인트종류 (GMIDAS:지마이다스 BASIC:기본)
     */
    @Column(name = "point_type")
    private String pointType;

    /**
     * [PT001] 포인트상태 (A:추가 U:사용)
     */
    @Column(name = "point_status")
    private String pointStatus;

    /**
     * 포인트 소유자 시퀀스
     */
    @Column(name = "mem_seq")
    private Integer memSeq;

    /**
     * 회원 포인트
     */
    @Column(name = "point")
    private Integer point;

    /**
     * 판매자 시퀀스
     */
    @Column(name = "sell_mem_seq")
    private Integer sellMemSeq;

    /**
     * 포인트 신청일자
     */
    @Column(name = "point_req_date")
    private LocalDateTime pointReqDate;

    /**
     * 포인트 사용일자
     */
    @Column(name = "point_use_date")
    private LocalDateTime pointUseDate;

    /**
     * 원본seq
     */
    @Column(name = "org_seq")
    private Integer orgSeq;

    /**
     * 적립대상seq
     */
    @Column(name = "save_seq")
    private Integer saveSeq;

    /**
     * 취소대상 seq
     */
    @Column(name = "cancel_seq")
    private Integer cancelSeq;

    /**
     * 포인트 만료일자
     */
    @Column(name = "point_expire_date")
    private LocalDateTime pointExpireDate;

    
}