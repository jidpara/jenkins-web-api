package com.gk337.v2api.point.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gk337.common.model.AuditEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

@Builder
@Entity
@Table(name = "jw_point_master")
@Data
public class PointMasterEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, name = "seq", nullable = false)
    private Integer seq;

    /**
     * 포인트 대상 회원 시퀀스
     */
    @Column(name = "mem_seq")
    private Integer memSeq;

    /**
     * [PT002] 포인트 종류(GMIDAS:지마이다스,BASIC:기본)
     */
    @Column(name = "point_type")
    private String pointType;

    /**
     * [PT001] 포인트 상태(A:추가,U:사용)
     */
    @Column(name = "point_status")
    private String pointStatus;

    /**
     * 포인트
     */
    @Column(name = "point")
    private Integer point;

    /**
     * 원본 seq
     */
    @Column(name = "org_seq")
    private Integer orgSeq;

    /**
     * 포인트 요청일
     */
    @CreatedDate
	@Column(name="req_date", nullable=false, updatable=false)
	private LocalDateTime reqDate;

    /**
     * 포인트 만료일자
     */
    @Column(name = "point_expire_date")
    private LocalDateTime pointExpireDate;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="org_seq")
	private Collection<PointDetailEntity> pointDetailEntities;

    public void addPointDetailEntity(PointDetailEntity p) {
        if (pointDetailEntities == null) {
            pointDetailEntities = new ArrayList<PointDetailEntity>();
        }
        pointDetailEntities.add(p);
    }
}