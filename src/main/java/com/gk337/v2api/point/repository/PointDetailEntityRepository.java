package com.gk337.v2api.point.repository;

import com.gk337.v2api.point.entity.PointDetailEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PointDetailEntityRepository extends JpaRepository<PointDetailEntity, Integer>, JpaSpecificationExecutor<PointDetailEntity> {

}