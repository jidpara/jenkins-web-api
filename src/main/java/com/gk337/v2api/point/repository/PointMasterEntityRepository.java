package com.gk337.v2api.point.repository;

import com.gk337.v2api.point.entity.PointMasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Map;

public interface PointMasterEntityRepository extends JpaRepository<PointMasterEntity, Integer>, JpaSpecificationExecutor<PointMasterEntity> {

    @Query(value=" select mem_seq, SUM(point) as point from jw_point_master p " +
				"where point_expire_date > now() " +
				"and mem_seq = :memSeq " +
				"group by mem_seq ", nativeQuery = true)
    Map<String, Object> getMyPoint(@Param("memSeq") Integer memSeq) ;
}