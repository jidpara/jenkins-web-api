package com.gk337.v2api.point.service;

import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.util.BeanUtilsExt;
import com.gk337.v2api.point.entity.PointDetailEntity;
import com.gk337.v2api.point.entity.PointMasterEntity;
import com.gk337.v2api.point.repository.PointDetailEntityRepository;
import com.gk337.v2api.point.repository.PointMasterEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class PointService {

    @Autowired
    private PointMasterEntityRepository pointMasterEntityRepository;

    @Autowired
    private PointDetailEntityRepository pointDetailEntityRepository;

    /**
     * 포인트 차감 & 적립 등록
     * @param pointDetailEntity
     * @return
     */
	@Transactional
	public ErrorVo insertPointDetailWithMaster(PointDetailEntity pointDetailEntity) {

		ErrorVo result = new ErrorVo();
        PointMasterEntity master = PointMasterEntity.builder().build();
		BeanUtilsExt.copyNonNullProperties(pointDetailEntity, master);

		pointMasterEntityRepository.save(master);
		pointDetailEntityRepository.save(pointDetailEntity);

		return result;
	}

	    /**
     * 포인트 차감 & 적립 등록
     * @param pointMasterEntity
     * @return
     */
	@Transactional
	public PointMasterEntity insertPointMaster(PointMasterEntity pointMasterEntity) {

		PointMasterEntity master = pointMasterEntityRepository.save(pointMasterEntity);

		return master;
	}
}
