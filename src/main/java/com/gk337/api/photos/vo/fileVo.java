package com.gk337.api.photos.vo;

import org.springframework.web.multipart.MultipartFile;

import com.gk337.common.core.mvc.model.BaseVo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class fileVo {
	
	private String filename;
	private MultipartFile file;

}
