package com.gk337.api.photos.service;

import java.util.ArrayList;
import java.util.List;

import com.gk337.api.photos.model.Comment;
import com.gk337.api.photos.model.Post;
import com.gk337.api.photos.repository.PhotosCommentRepository;
import com.gk337.api.photos.repository.PhotosPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.gk337.api.photos.model.Tag;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.util.FileUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PhotosService {
	
	@Autowired
	private PhotosPostRepository postRepository;
	
	@Autowired
	private PhotosCommentRepository commentRepository;
	
	@Value("${file.upload.directory}")
	private String dir;
	
	@Transactional
	public ErrorVo savePost(String userId,
			String content,
			String[] tags,
			MultipartFile multipartFile) {
		log.debug("InstaService savePost() =============== " );
		try {
			
			FileUtil.fileUpload(dir+"articles/", multipartFile);
			
			Post post = Post.builder()
					.fileName(multipartFile.getOriginalFilename())
					.imagePath(dir)
					.content(content)
					.uid(userId)
					.build();
			postRepository.save(post);
			
			List<Tag> tagList = new ArrayList<Tag>();
			for(String t: tags) {
				Tag tag = Tag.builder()
						.postId(post.getPostId())
						.tag(t)
						.build();
				tagList.add(tag);
			}
			post.setTag(tagList);
			postRepository.save(post);
			
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("INFO.COUNSEL.MESSAGE.0001");
		}
		return new ErrorVo();
	}
	
	public ListVo<Post> findPostByUid(String userId) {
		ListVo<Post> listVo = null;
		try {
			listVo = new ListVo<Post>(postRepository.findByUid(userId));
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("INFO.COUNSEL.MESSAGE.0001");
		}
		return listVo;
	}
	
	public Post findPostByPostId(Long postId) {
		Post post = null;
		try {
			post = postRepository.findByPostId(postId);
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			throw new BizException("INFO.COUNSEL.MESSAGE.0001");
		}
		return post;
	}
	
	
	public Page<Post> findAllByPaging(Pageable pageable) {
		Page<Post> pageList = null;
		try {
			pageList = postRepository.findAll(pageable);
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("INFO.COUNSEL.MESSAGE.0001");
		}
		return pageList;
	}
	
	@Transactional
	public Comment saveComment(Long postId, String userId, String comment) {
		log.debug("InstaService savePost() =============== " );
		Comment entity = new Comment();
		try {
			entity = Comment.builder()
					.postId(postId)
					.userId(userId)
					.comment(comment)
					.build();
			
			commentRepository.save(entity);
			
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("INFO.COUNSEL.MESSAGE.0001");
		}
		return entity;
	}
	
	@Transactional
	public ErrorVo deletePost(Long postId) {
		try {
			postRepository.deleteById(postId);
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("INFO.COUNSEL.MESSAGE.0001");
		}
		return new ErrorVo();
	}
	
}
