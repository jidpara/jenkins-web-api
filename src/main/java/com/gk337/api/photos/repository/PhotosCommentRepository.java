package com.gk337.api.photos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gk337.api.photos.model.Comment;

public interface PhotosCommentRepository extends PagingAndSortingRepository<Comment, Long>, JpaRepository<Comment, Long>{

}
