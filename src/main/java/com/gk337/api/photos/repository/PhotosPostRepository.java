package com.gk337.api.photos.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gk337.api.photos.model.Post;

public interface PhotosPostRepository extends PagingAndSortingRepository<Post, Long>, JpaRepository<Post, Long>{
	
	List<Post> findByUid(String Uid);
	
	Post findByPostId(Long postId);
	
	Page<Post> findAll(Pageable pageable);

}
