package com.gk337.api.photos.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.gk337.api.photos.model.Comment;
import com.gk337.api.photos.model.Post;
import com.gk337.api.photos.service.PhotosService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@CrossOrigin("*")
@RestController
@RequestMapping("/api/photos")
@Api(tags= {"[샘플] 블로그 API"}, protocols="http", produces="application/json", consumes="application/json")
public class PhotosRestController {
//
//	@Autowired
//	private PhotosService photosService;
//	
//	@ApiImplicitParams({
//        @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", paramType = "header")
//	})
//	@ApiOperation(value = "포스트저장", notes = "로그인 계정의 게시물 저장")
//	@RequestMapping(method = RequestMethod.POST, path="/post", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	public ResponseEntity<String> imageUpload(
//			@RequestHeader(value="X-AUTH-TOKEN") String xAuthToken,
//			@RequestParam(value="user_id") String userId,
//			@RequestParam(value="content") String content,
//			@RequestParam(value="tags") String[] tags,
//			@RequestParam(value="file") MultipartFile multipartFile
//			) {
//		log.debug("token ::: {} ", xAuthToken);
//		return BaseResponse.ok(photosService.savePost(userId, content, tags, multipartFile));
//	}
//	
//	@ApiOperation(value = "포스트조회[user_id]", notes="로그인 계정의 게시물을 조회한다.", position=1)
//    @RequestMapping(method = RequestMethod.GET, path="/post", produces = "application/json")
//	public ResponseEntity<List<Post>> findByUserId(
//			@ApiParam(value="유저아이디", required=true, defaultValue = "ohdr1111@gmail.com") @RequestParam(value = "user_id", required = true) String userId) {
//		return BaseResponse.ok(photosService.findPostByUid(userId) );
//	}
//	
//	@ApiOperation(value = "포스트조회[post_id]", notes="포스트번호 해당하는 게시물을 조회한다.", position=1)
//    @RequestMapping(method = RequestMethod.GET, path="/post/{post_id}", produces = "application/json")
//	public ResponseEntity<List<Post>> findByPostId(
//			@ApiParam(value="포스트아이디", required=true, defaultValue = "13") @PathVariable(value = "post_id", required = true) Long postId) {
//		return BaseResponse.ok(photosService.findPostByPostId(postId) );
//	}
//
//	@SuppressWarnings("deprecation")
//	@ApiOperation(value = "포스트조회[페이징]", notes="게시물 전체 리스트를 페이징으로 조회한다.", position=1)
//    @RequestMapping(method = RequestMethod.GET, path="/posts/{page_no}/{page_size}/{sort}", produces = "application/json")
//	public Page<Post> findBysmplnrStrdGrpIdAndDelYnAndEffDt(
//			@ApiParam(value="페이지번호", required=true) @PathVariable(value = "page_no", required = true) int pNo,
//			@ApiParam(value="사이즈", required=true) @PathVariable(value = "page_size", required = true) int size,
//			@ApiParam(value="정렬키", required=false, defaultValue="auditDtm") @PathVariable(value = "sort", required = false) String sort
//			) {
//		Direction dir = Sort.Direction.DESC;
//		PageRequest pageable = new PageRequest(pNo-1, size, dir, sort);
//		return photosService.findAllByPaging(pageable);
//	}
//	
//	
//	@ApiOperation(value = "코맨트저장", notes = "코맨트저장")
//	@RequestMapping(method = RequestMethod.POST, path="/comment", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	public ResponseEntity<Comment> saveComment(
//			//@RequestHeader(value="X-AUTH-TOKEN") String xAuthToken,
//			@RequestParam(value="post_id") Long postId,
//			@RequestParam(value="user_id") String userId,
//			@RequestParam(value="comment") String comment
//			) {
//		//log.debug("token ::: {} ", xAuthToken);
//		return BaseResponse.ok(photosService.saveComment(postId, userId, comment));
//	}
//	
//	@ApiOperation(value = "포스트삭제", notes = "코맨트삭제")
//	@RequestMapping(method = RequestMethod.DELETE, path="/{post_id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	public ResponseEntity<ErrorVo> deletePost(
//			//@RequestHeader(value="X-AUTH-TOKEN") String xAuthToken,
//			@PathVariable(value="post_id") Long postId
//			) {
//		//log.debug("token ::: {} ", xAuthToken);
//		return BaseResponse.ok(photosService.deletePost(postId));
//	}
}
