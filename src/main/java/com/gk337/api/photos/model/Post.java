package com.gk337.api.photos.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gk337.api.user.model.User;
import com.gk337.common.core.domain.AggregateRoot;
import com.gk337.common.core.domain.AggregateRootEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("deprecation")
@Builder // builder를 사용할수 있게 합니다.
@Entity // jpa entity임을 알립니다.
@Getter // user 필드값의 getter를 자동으로 생성합니다.
@Setter
@NoArgsConstructor // 인자없는 생성자를 자동으로 생성합니다.
@AllArgsConstructor // 인자를 모두 갖춘 생성자를 자동으로 생성합니다.
@Table(name = "POST") // 'user' 테이블과 매핑됨을 명시
@ApiModel(description = "게시물")
@JsonNaming(PropertyNamingStrategy.LowerCaseWithUnderscoresStrategy.class)
public class Post  extends AggregateRootEntity implements AggregateRoot {

	@Id // pk
    @GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "ID")
	private Long postId;

	@Column(nullable = false, length = 100)
	@ApiModelProperty(notes = "UID")
	private String uid;

	@Column(nullable = false, length = 400)
	@ApiModelProperty(notes = "CONTENT")
	private String content;

	@Column(nullable = false, length = 100)
	@ApiModelProperty(notes = "IMAGE_PATH")
	private String imagePath;
	
	@Column(nullable = false, length = 100)
	@ApiModelProperty(notes = "FILE_NAME")
	private String fileName;
	
	//@OrderBy("sortSeq ASC")
	@JoinColumn(name = "postId", insertable = false, updatable = false)
	@NotFound(action = NotFoundAction.IGNORE)
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Tag> tag;   
	
	@JoinColumn(name = "postId", insertable = false, updatable = false)
	@NotFound(action = NotFoundAction.IGNORE)
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Comment> comment; 
	
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JoinColumn(
	    name = "uid", 
	    referencedColumnName = "uid",
	    insertable = false, 
	    updatable = false
	)
	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY/* , cascade = CascadeType.ALL */)
	private User user;
}