package com.gk337.api.photos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gk337.common.core.domain.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@SuppressWarnings("deprecation")
@Builder // builder를 사용할수 있게 합니다.
@Entity // jpa entity임을 알립니다.
@Getter // user 필드값의 getter를 자동으로 생성합니다.
@NoArgsConstructor // 인자없는 생성자를 자동으로 생성합니다.
@AllArgsConstructor // 인자를 모두 갖춘 생성자를 자동으로 생성합니다.
@Table(name = "TAG") // 'user' 테이블과 매핑됨을 명시
@ApiModel(description = "태그")
@JsonNaming(PropertyNamingStrategy.LowerCaseWithUnderscoresStrategy.class)
public class Tag extends BaseEntity {
	
	// tag_id 
	@Id // pk
    @GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "ID")
    private Long tagId;

    // post_id 
	@Column(nullable = false, length = 400)
	@ApiModelProperty(notes = "POST_ID")
    private Long postId;

    // tag 
	@Column(nullable = false, length = 400)
	@ApiModelProperty(notes = "태그")
    private String tag;

}
