package com.gk337.api.photos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gk337.api.user.model.User;
import com.gk337.common.core.domain.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@SuppressWarnings("deprecation")
@Builder // builder를 사용할수 있게 합니다.
@Entity // jpa entity임을 알립니다.
@Getter // user 필드값의 getter를 자동으로 생성합니다.
@NoArgsConstructor // 인자없는 생성자를 자동으로 생성합니다.
@AllArgsConstructor // 인자를 모두 갖춘 생성자를 자동으로 생성합니다.
@Table(name = "COMMENT") // 'user' 테이블과 매핑됨을 명시
@ApiModel(description = "태그")
@JsonNaming(PropertyNamingStrategy.LowerCaseWithUnderscoresStrategy.class)
public class Comment extends BaseEntity {
	
	// id 
	@Id // pk
    @GeneratedValue(strategy = GenerationType.AUTO)
	@ApiModelProperty(notes = "ID")
    private Long commentId;

    // post_id 
	@Column(nullable = false, columnDefinition = "int(11)")
	@ApiModelProperty(notes = "POST_ID")
    private Long postId;

    // comment 
	@Column(nullable = false, length = 400)
	@ApiModelProperty(notes = "태그")
    private String comment;
	
	// tag 
	@Column(nullable = false, length = 30)
	@ApiModelProperty(notes = "유저아이디")
    private String userId;
	
	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@JoinColumn(
	    name = "userId", 
	    referencedColumnName = "uid",
	    insertable = false, 
	    updatable = false
	)
	@NotFound(action = NotFoundAction.IGNORE)
	@ManyToOne(fetch = FetchType.LAZY /* , cascade = CascadeType.ALL */)
	private User user;

	public void setPostId(Long postId) {
		this.postId = postId;
	}
	
	

}
