package com.gk337.api.payment.mapper;

import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface PackageMapper {

	public void clearPackageInfo(Integer shopSeq);

	public Integer insertShopAreaMatchFromShopAreaGroup(Integer shopSeq);

	public Integer insertShopAreaMatchFromPaymentAreaGroup(String paymentCode, Integer memSeq, Integer shopSeq);

}
