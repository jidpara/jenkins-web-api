package com.gk337.api.payment.mapper;

import com.gk337.api.payment.entity.PaymentHistoryResponse;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


@Mapper
public interface PaymentMapper {

	/**
	 * 결제 내역 목록 카운트
	 * @param map Integer
	 * @return
	 */
	public Integer selectPaymentHistoryListCount(Map map);

		/**
	 * 결제 내역 목록
	 * @param map
	 * @return
	 */
	public List<PaymentHistoryResponse> selectPaymentHistoryList(Map map);

	/**
	 * 결제 상세정보
	 * @param map
	 * @return PaymentHistoryResponse
	 */
	public PaymentHistoryResponse selectPaymentHistoryDetail(Map map);

	/**
	 * 결제정보 - 지역
	 * @param Map request
	 * @return Map
	 */
	public List<Map<String, Object>> selectPaymentAreaInfo(Map request);

	/**
	 * 결제정보 - 카테고리
	 * @param Map request
	 * @return Map
	 */
	public Map<String, String> selectPaymentCategoryInfo(Map request);
}
