package com.gk337.api.payment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import java.util.HashMap;
import java.util.List;

import com.gk337.api.payment.entity.PaymentHistoryResponse;
import com.gk337.api.payment.mapper.PaymentMapper;
import com.gk337.api.payment.repository.PaymentHistoryEntityRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.exception.JGWServiceException;

@Slf4j
@Service
public class PaymentService {

	@Autowired
	PaymentHistoryEntityRepository paymentHistoryEntityRepository;

	@Autowired
	PaymentMapper paymentMapper;

	/**
	 * 결제내역 - 목록조회(페이징처리)
	 * @param pageNo
	 * @param pageSize
	 * @param memSeq
	 * @return PageInfo<PaymentHistoryResponse>
	 */
	public PageInfo<PaymentHistoryResponse> getPaymentHistoryListPaging(Integer pageNo, Integer pageSize, String period, Integer memSeq) {
		PageInfo<PaymentHistoryResponse> pageInfo = new PageInfo<PaymentHistoryResponse>();
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("memSeq", memSeq);
			hashMap.put("period", period);

			Integer totalSize = paymentMapper.selectPaymentHistoryListCount(hashMap);
			PageHelper.startPage(pageNo, pageSize);
			List<PaymentHistoryResponse> list = paymentMapper.selectPaymentHistoryList(hashMap);
			pageInfo = new PageInfo<PaymentHistoryResponse>(list);
			pageInfo.setTotal(totalSize);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PAYMENT.0001", "목록조회");
		}
		return pageInfo;
	}

	/**
	 * 결제내역 - 상세조회
	 * @param memSeq
	 * @param paymentCode
	 * @return PaymentHistoryResponse
	 */
	public PaymentHistoryResponse getPaymentHistory(Integer memSeq, String paymentCode) {
		PaymentHistoryResponse data = null;
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("memSeq", memSeq);
			hashMap.put("paymentCode", paymentCode);

			data = paymentMapper.selectPaymentHistoryDetail(hashMap);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PAYMENT.0001", "상세조회");
		}
		return data;
	}

}


