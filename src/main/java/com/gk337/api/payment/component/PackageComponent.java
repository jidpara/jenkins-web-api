package com.gk337.api.payment.component;

import com.gk337.api.area.repository.ShopAreaMatchRepository;
import com.gk337.api.area.repository.ShopAreaRepository;
import com.gk337.api.payment.entity.*;
import com.gk337.api.payment.mapper.PackageMapper;
import com.gk337.api.payment.repository.*;
import com.gk337.api.request.entity.*;
import com.gk337.api.request.service.RequestService;
import com.gk337.api.shop.entity.ShopAreaGroupEntity;
import com.gk337.api.shop.entity.ShopCategoryEntity;
import com.gk337.api.shop.entity.ShopCategoryMatchEntity;
import com.gk337.api.shop.entity.ShopInfoEntity;
import com.gk337.api.shop.repository.ShopAreaGroupEntityRepository;
import com.gk337.api.shop.repository.ShopCategoryMatchEntityRepository;
import com.gk337.api.shop.repository.ShopCategoryRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.BeanUtilsExt;
import com.gk337.common.util.CollectionUtils;
import com.gk337.common.util.MessageUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.beans.Beans;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class PackageComponent {

	@Autowired
	private RequestService requestService;

	@Autowired
	private PackageEntityRepository packageEntityRepository;

	@Autowired
	private PaymentResultEntityRepository paymentResultEntityRepository;

	@Autowired
	private PaymentStatusEntityRepository paymentStatusEntityRepository;

	@Autowired
	private ShopAreaRepository shopAreaRepository;

	@Autowired
	private ShopAreaGroupEntityRepository shopAreaGroupEntityRepository;

	@Autowired
	private ShopAreaMatchRepository shopAreaMatchRepository;

	@Autowired
	private ShopCategoryRepository shopCategoryRepository;

	@Autowired
	private ShopCategoryMatchEntityRepository shopCategoryMatchEntityRepository;

	@Autowired
	private IfImportPayRequestEntityRepository ifImportPayRequestEntityRepository;

	@Autowired
	private PaymentHistoryEntityRepository paymentHistoryEntityRepository;

	@Autowired
	private PaymentAreaEntityRepository paymentAreaEntityRepository;

	@Autowired
	private PaymentAreaGroupEntityRepository paymentAreaGroupEntityRepository;

	@Autowired
	private PaymentCategoryEntityRepository paymentCategoryEntityRepository;

	@Autowired
	private PackageMapper packageMapper;


	/**
	 *
	 * 딜러 패키지 결제정보 등록 또는 수정.
	 * @param request
	 * @return
	 */
	public PackageResponse processPaymentAll(PackageRequest request) {
		SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");
		UUID paymentCode = UUID.randomUUID();
		LocalDateTime startDate = LocalDateTime.parse("2020-04-01 00:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")); // 2008-10-04
		LocalDateTime endDate = startDate.plusMonths(1);

		// 개발 및 용 임시 delete & insert
		packageMapper.clearPackageInfo(request.getShopSeq());

		PackageResponse packageResponse = new PackageResponse();

		try {

			// 결제 결과 및 상태 정보 등록
			// 1차 - 기본정보 대거 하드코딩
			PaymentResultEntity paymentResultEntity = new PaymentResultEntity();

			paymentResultEntity.setPackageSeq(request.getPackageSeq());
			paymentResultEntity.setPrice(0); // 1차 - 한달 무료로 0원 설정
			paymentResultEntity.setShopSeq(request.getShopSeq());
			paymentResultEntity.setPaymentType("MA"); // 1차 - 한달 무료로 MA고정
			paymentResultEntity.setPaymentCode(paymentCode.toString());
			paymentResultEntity.setPaymentRequestSeq(0); // 2차 - PG사 결제 후 실제값 적용
			paymentResultEntity.setPaymentRequestSeq(0); // 2차 - PG사 결제 후 실제값 적용
			paymentResultEntity.setStartDate(startDate);
			paymentResultEntity.setEndDate(endDate);

			paymentResultEntity = paymentResultEntityRepository.save(paymentResultEntity);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			// TODO : 메세지 처리
			throw new JGWServiceException("결제정보 등록 중 오류 발생", "결제정보 등록 중 오류 발생");
		}

		try {

			// V2
			// 딜러의 상점정보 중 지역정보 저장
//			List<ShopAreaEntity> shopAreaList = CollectionUtils.convert(request.getShopAreaList(), ShopAreaEntity::new);
//			shopAreaList = shopAreaRepository.saveAll(shopAreaList);

			// 같은 데이터 매칭 테이블에 저장
//			List<ShopAreaMatchEntity> shopAreaMatchEntityList = CollectionUtils.convert(request.getShopAreaList(), ShopAreaMatchEntity::new);
//			shopAreaMatchEntityList = shopAreaMatchRepository.saveAll(shopAreaMatchEntityList);


			// V1
			// 딜러의 상점정보 중 지역정보 저장
			List<ShopAreaGroupEntity> shopAreaGroupList = CollectionUtils.convert(request.getShopAreaList(), ShopAreaGroupEntity::new);
			shopAreaGroupList = shopAreaGroupEntityRepository.saveAll(shopAreaGroupList);

			// 지역 데이터 매칭 테이블에 저장 ( 지역그룹 데이터를 기반으로 insert select 처리 )
			packageMapper.insertShopAreaMatchFromShopAreaGroup(request.getShopSeq());
//			List<ShopAreaMatchEntity> shopAreaMatchEntityList = CollectionUtils.convert(request.getShopAreaList(), ShopAreaMatchEntity::new);
//			shopAreaMatchEntityList = shopAreaMatchRepository.saveAll(shopAreaMatchEntityList);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			// TODO : 메세지 처리
			throw new JGWServiceException("딜러의 상점지역 저장 중 오류 발생", "딜러의 상점지역 저장 중 오류 발생");
		}

		try {
			// 딜러의 상점정보 중 카테고리 정보 저장
			List<ShopCategoryEntity> shopCategoryList = CollectionUtils.convert(request.getShopCategoryList(), ShopCategoryEntity::new);
			shopCategoryList = shopCategoryRepository.saveAll(shopCategoryList);

			// 같은 데이터 매칭 테이블에 저장
			List<ShopCategoryMatchEntity> shopCategoryMatchEntityList = CollectionUtils.convert(request.getShopCategoryList(), ShopCategoryMatchEntity::new);
			shopCategoryMatchEntityList = shopCategoryMatchEntityRepository.saveAll(shopCategoryMatchEntityList);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			// TODO : 메세지 처리
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}

		try {

			// 1차 - 결제 상태 저장
			PaymentStatusEntity paymentStatusEntity = new PaymentStatusEntity();
			paymentStatusEntity.setCstatus("Y");
			paymentStatusEntity.setPaymentStatus("N");  // TODO : N : 일반...  최선인가
			paymentStatusEntity.setShopSeq(request.getShopSeq());
			paymentStatusEntity.setStartDate(startDate);
			paymentStatusEntity.setEndDate(endDate);

			paymentStatusEntity = paymentStatusEntityRepository.save(paymentStatusEntity);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			// TODO : 메세지 처리
			throw new JGWServiceException("결제 상태 저장", "결제 상태 저장");
		}

		BeanUtils.copyProperties(request, packageResponse);

		return packageResponse;

	}

	/**
	 *
	 * 딜러 패키지 결제정보 등록 또는 수정.
	 * @param request
	 * @return
	 */
	public PackageResponse processPaymentAllV2(PackageRequest request) {

		final Integer memSeq;
		Date today = new Date();
		SimpleDateFormat paymentCodeFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		// 현재 시점으로 부터 1개월
		LocalDateTime startDate = LocalDateTime.parse(dateFormat.format(today), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
		LocalDateTime endDate = startDate.plusMonths(1);

		String paymentCode = String.format("%s-%s", paymentCodeFormat.format(today), request.getShopSeq());
		System.out.println("paymentCode = " +  paymentCode);
		// 개발 및 용 임시 delete & insert
		packageMapper.clearPackageInfo(request.getShopSeq());

		PackageResponse packageResponse = new PackageResponse();

		try {
			// 결제 결과 및 상태 정보 등록

			// 아임포트 IF 테이블 조회
			Optional<IfImportPayRequestEntity> ifImportPayRequest =  ifImportPayRequestEntityRepository.findById(request.getMerchantUid());
			if(ifImportPayRequest.isPresent()) {

				IfImportPayRequestEntity ifImportPayRequestEntity = ifImportPayRequest.get();
				memSeq = Integer.parseInt(ifImportPayRequestEntity.getUserId()); // user_id 에 mem_seq 가 넘어옴 ㅈㅅ

				PaymentHistoryEntity paymentHistoryEntity = new PaymentHistoryEntity();
				BeanUtilsExt.copyNonNullProperties(ifImportPayRequestEntity, paymentHistoryEntity);
				paymentHistoryEntity.setPaymentCode(paymentCode);
				paymentHistoryEntity.setPaymentStatus(ifImportPayRequestEntity.getStatus());
				paymentHistoryEntity.setPaymentMethod(ifImportPayRequestEntity.getPayMethod());
				paymentHistoryEntity.setAmount(Integer.parseInt(ifImportPayRequestEntity.getAmount()));
				paymentHistoryEntity.setMemSeq(memSeq);
				paymentHistoryEntity.setShopSeq(request.getShopSeq());
				paymentHistoryEntity.setStartDate(startDate);
				paymentHistoryEntity.setEndDate(endDate);

				paymentHistoryEntityRepository.save(paymentHistoryEntity);
			} else {
				throw new BizException("결제정보 등록 중 오류 발생", "결제정보 등록 중 오류 발생");
			}

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			// TODO : 메세지 처리
			throw new JGWServiceException("결제정보 등록 중 오류 발생", "결제정보 등록 중 오류 발생");
		}

		try {

			// 결제정보 중 지역정보 저장
			List<PaymentAreaGroupEntity> paymentAreaGroupList = request.getShopAreaList().stream()
					.map(shopArea -> {
						PaymentAreaGroupEntity paymentAreaGroupEntity = new PaymentAreaGroupEntity();
						BeanUtilsExt.copyNonNullProperties(shopArea, paymentAreaGroupEntity);
						paymentAreaGroupEntity.setPaymentCode(paymentCode);
						paymentAreaGroupEntity.setMemSeq(memSeq);
						return paymentAreaGroupEntity;
					})
					.collect(Collectors.toList());
			paymentAreaGroupList = paymentAreaGroupEntityRepository.saveAll(paymentAreaGroupList);

			// 딜러의 상점정보 중 지역정보 저장
			List<ShopAreaGroupEntity> shopAreaGroupList = CollectionUtils.convert(request.getShopAreaList(), ShopAreaGroupEntity::new);
			shopAreaGroupList = shopAreaGroupEntityRepository.saveAll(shopAreaGroupList);


			// 결제 지역정보 저장 ( 지역그룹 데이터를 기반으로 insert select 처리 )
			packageMapper.insertShopAreaMatchFromPaymentAreaGroup(paymentCode, memSeq, request.getShopSeq());
			// 지역 데이터 매칭 테이블에 저장 ( 지역그룹 데이터를 기반으로 insert select 처리 )
			packageMapper.insertShopAreaMatchFromShopAreaGroup(request.getShopSeq());

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			// TODO : 메세지 처리
			throw new JGWServiceException("딜러의 상점지역 저장 중 오류 발생", "딜러의 상점지역 저장 중 오류 발생");
		}

		try {

			// 결제정보 중 카테고리 저장
			List<PaymentCategoryEntity> paymentCategoryList = request.getShopCategoryList().stream()
					.map(shopCategory -> {
						PaymentCategoryEntity paymentCategoryEntity = new PaymentCategoryEntity();
						BeanUtilsExt.copyNonNullProperties(shopCategory, paymentCategoryEntity);
						paymentCategoryEntity.setPaymentCode(paymentCode);
						paymentCategoryEntity.setMemSeq(memSeq);
						return paymentCategoryEntity;
					})
					.collect(Collectors.toList());
			paymentCategoryList = paymentCategoryEntityRepository.saveAll(paymentCategoryList);

			// 딜러의 상점정보 중 카테고리 정보 저장
			List<ShopCategoryEntity> shopCategoryList = CollectionUtils.convert(request.getShopCategoryList(), ShopCategoryEntity::new);
			shopCategoryList = shopCategoryRepository.saveAll(shopCategoryList);

			// 같은 데이터 매칭 테이블에 저장
			List<ShopCategoryMatchEntity> shopCategoryMatchEntityList = CollectionUtils.convert(request.getShopCategoryList(), ShopCategoryMatchEntity::new);
			shopCategoryMatchEntityList = shopCategoryMatchEntityRepository.saveAll(shopCategoryMatchEntityList);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			// TODO : 메세지 처리
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}

		try {

			// 1차 - 결제 상태 저장
			PaymentStatusEntity paymentStatusEntity = new PaymentStatusEntity();
			paymentStatusEntity.setCstatus("Y");
			paymentStatusEntity.setPaymentStatus("N");  // TODO : N : 일반...  최선인가
			paymentStatusEntity.setShopSeq(request.getShopSeq());
			paymentStatusEntity.setStartDate(startDate);
			paymentStatusEntity.setEndDate(endDate);

			paymentStatusEntity = paymentStatusEntityRepository.save(paymentStatusEntity);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			// TODO : 메세지 처리
			throw new JGWServiceException("결제 상태 저장", "결제 상태 저장");
		}

		BeanUtils.copyProperties(request, packageResponse);
		packageResponse.setPaymentCode(paymentCode);
		packageResponse.setPaymentDate(startDate);
		packageResponse.setPaymentDateStr(startDate.format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss")));
		packageResponse.setStartDate(startDate.format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss")));
		packageResponse.setEndDate(endDate.format(DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss")));

		return packageResponse;

	}
}
