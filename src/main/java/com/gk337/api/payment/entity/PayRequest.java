package com.gk337.api.payment.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Data
@Entity
@Table(name = "if_import_pay_request", catalog = "")
public class PayRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "merchant_uid", insertable = false, nullable = false)
    private String merchantUid;
    @Column(name = "app", nullable = false)
    private String app;
    @Column(name = "order_name")
    private String orderName = "NULL";
    @Column(name = "amount", nullable = false)
    private String amount;
    @Column(name = "product_id", nullable = false)
    private String productId;
    @Column(name = "user_id", nullable = false)
    private String userId;
    @Column(name = "imp_uid")
    private String impUid = "NULL";
    @Column(name = "status")
    private String status = "NULL";
    @Column(name = "message")
    private String message = "NULL";
    private String payMethod;
    private String channel;
    private String pgProvider;
    private String pgTid;
    private String pgId;
    private Byte escrow;
    private String applyNum;
    private String bankCode;
    private String bankName;
    private String cardCode;
    private String cardName;
    private Integer cardQuota;
    private String cardNumber;
    private String cardType;
    private String vbankCode;
    private String vbankName;
    private String vbankNum;
    private String vbankHolder;
    private Integer vbankDate;
    private Integer vbankIssuedAt;
    private String name;
    private Integer cancelAmount;
    private String currency;
    private String buyerName;
    private String buyerEmail;
    private String buyerTel;
    private String buyerAddr;
    private String buyerPostcode;
    private String customData;
    private String userAgent;
    private Integer startedAt;
    private Integer paidAt;
    private Integer failedAt;
    private Integer cancelledAt;
    private String failReason;
    private String cancelReason;
    private String receiptUrl;
    private String cancelReceiptUrls;
    private Byte cashReceiptIssued;
    private String customerUid;

    @Basic
    @Column(name = "customer_uid", nullable = true, length = 100)
    public String getCustomerUid() {
        return customerUid;
    }

    public void setCustomerUid(String customerUid) {
        this.customerUid = customerUid;
    }

    @Id
    @Column(name = "merchant_uid", nullable = false, length = 80)
    public String getMerchantUid() {
        return merchantUid;
    }

    public void setMerchantUid(String merchantUid) {
        this.merchantUid = merchantUid;
    }

    @Basic
    @Column(name = "app", nullable = false, length = 10)
    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    @Basic
    @Column(name = "order_name", nullable = true, length = 200)
    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    @Basic
    @Column(name = "amount", nullable = false, length = 40)
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "product_id", nullable = false, length = 50)
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "user_id", nullable = false, length = 50)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "imp_uid", nullable = true, length = 40)
    public String getImpUid() {
        return impUid;
    }

    public void setImpUid(String impUid) {
        this.impUid = impUid;
    }

    @Basic
    @Column(name = "status", nullable = true, length = 20)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "message", nullable = true, length = 200)
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PayRequest that = (PayRequest) o;
        return Objects.equals(merchantUid, that.merchantUid) &&
                Objects.equals(app, that.app) &&
                Objects.equals(orderName, that.orderName) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(productId, that.productId) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(impUid, that.impUid) &&
                Objects.equals(status, that.status) &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(merchantUid, app, orderName, amount, productId, userId, impUid, status, message);
    }

    @Basic
    @Column(name = "pay_method", nullable = true, length = 20)
    public String getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }

    @Basic
    @Column(name = "channel", nullable = true, length = 10)
    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Basic
    @Column(name = "pg_provider", nullable = true, length = 10)
    public String getPgProvider() {
        return pgProvider;
    }

    public void setPgProvider(String pgProvider) {
        this.pgProvider = pgProvider;
    }

    @Basic
    @Column(name = "pg_tid", nullable = true, length = 20)
    public String getPgTid() {
        return pgTid;
    }

    public void setPgTid(String pgTid) {
        this.pgTid = pgTid;
    }

    @Basic
    @Column(name = "pg_id", nullable = true, length = 20)
    public String getPgId() {
        return pgId;
    }

    public void setPgId(String pgId) {
        this.pgId = pgId;
    }

    @Basic
    @Column(name = "escrow", nullable = true)
    public Byte getEscrow() {
        return escrow;
    }

    public void setEscrow(Byte escrow) {
        this.escrow = escrow;
    }

    @Basic
    @Column(name = "apply_num", nullable = true, length = 20)
    public String getApplyNum() {
        return applyNum;
    }

    public void setApplyNum(String applyNum) {
        this.applyNum = applyNum;
    }

    @Basic
    @Column(name = "bank_code", nullable = true, length = 20)
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "bank_name", nullable = true, length = 20)
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Basic
    @Column(name = "card_code", nullable = true, length = 20)
    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    @Basic
    @Column(name = "card_name", nullable = true, length = 20)
    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    @Basic
    @Column(name = "card_quota", nullable = true)
    public Integer getCardQuota() {
        return cardQuota;
    }

    public void setCardQuota(Integer cardQuota) {
        this.cardQuota = cardQuota;
    }

    @Basic
    @Column(name = "card_number", nullable = true, length = 20)
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "card_type", nullable = true, length = 20)
    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    @Basic
    @Column(name = "vbank_code", nullable = true, length = 20)
    public String getVbankCode() {
        return vbankCode;
    }

    public void setVbankCode(String vbankCode) {
        this.vbankCode = vbankCode;
    }

    @Basic
    @Column(name = "vbank_name", nullable = true, length = 20)
    public String getVbankName() {
        return vbankName;
    }

    public void setVbankName(String vbankName) {
        this.vbankName = vbankName;
    }

    @Basic
    @Column(name = "vbank_num", nullable = true, length = 20)
    public String getVbankNum() {
        return vbankNum;
    }

    public void setVbankNum(String vbankNum) {
        this.vbankNum = vbankNum;
    }

    @Basic
    @Column(name = "vbank_holder", nullable = true, length = 20)
    public String getVbankHolder() {
        return vbankHolder;
    }

    public void setVbankHolder(String vbankHolder) {
        this.vbankHolder = vbankHolder;
    }

    @Basic
    @Column(name = "vbank_date", nullable = true)
    public Integer getVbankDate() {
        return vbankDate;
    }

    public void setVbankDate(Integer vbankDate) {
        this.vbankDate = vbankDate;
    }

    @Basic
    @Column(name = "vbank_issued_at", nullable = true)
    public Integer getVbankIssuedAt() {
        return vbankIssuedAt;
    }

    public void setVbankIssuedAt(Integer vbankIssuedAt) {
        this.vbankIssuedAt = vbankIssuedAt;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "cancel_amount", nullable = true)
    public Integer getCancelAmount() {
        return cancelAmount;
    }

    public void setCancelAmount(Integer cancelAmount) {
        this.cancelAmount = cancelAmount;
    }

    @Basic
    @Column(name = "currency", nullable = true, length = 20)
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Basic
    @Column(name = "buyer_name", nullable = true, length = 20)
    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    @Basic
    @Column(name = "buyer_email", nullable = true, length = 20)
    public String getBuyerEmail() {
        return buyerEmail;
    }

    public void setBuyerEmail(String buyerEmail) {
        this.buyerEmail = buyerEmail;
    }

    @Basic
    @Column(name = "buyer_tel", nullable = true, length = 20)
    public String getBuyerTel() {
        return buyerTel;
    }

    public void setBuyerTel(String buyerTel) {
        this.buyerTel = buyerTel;
    }

    @Basic
    @Column(name = "buyer_addr", nullable = true, length = 100)
    public String getBuyerAddr() {
        return buyerAddr;
    }

    public void setBuyerAddr(String buyerAddr) {
        this.buyerAddr = buyerAddr;
    }

    @Basic
    @Column(name = "buyer_postcode", nullable = true, length = 10)
    public String getBuyerPostcode() {
        return buyerPostcode;
    }

    public void setBuyerPostcode(String buyerPostcode) {
        this.buyerPostcode = buyerPostcode;
    }

    @Basic
    @Column(name = "custom_data", nullable = true, length = 50)
    public String getCustomData() {
        return customData;
    }

    public void setCustomData(String customData) {
        this.customData = customData;
    }

    @Basic
    @Column(name = "user_agent", nullable = true, length = 20)
    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @Basic
    @Column(name = "started_at", nullable = true)
    public Integer getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Integer startedAt) {
        this.startedAt = startedAt;
    }

    @Basic
    @Column(name = "paid_at", nullable = true)
    public Integer getPaidAt() {
        return paidAt;
    }

    public void setPaidAt(Integer paidAt) {
        this.paidAt = paidAt;
    }

    @Basic
    @Column(name = "failed_at", nullable = true)
    public Integer getFailedAt() {
        return failedAt;
    }

    public void setFailedAt(Integer failedAt) {
        this.failedAt = failedAt;
    }

    @Basic
    @Column(name = "cancelled_at", nullable = true)
    public Integer getCancelledAt() {
        return cancelledAt;
    }

    public void setCancelledAt(Integer cancelledAt) {
        this.cancelledAt = cancelledAt;
    }

    @Basic
    @Column(name = "fail_reason", nullable = true, length = 100)
    public String getFailReason() {
        return failReason;
    }

    public void setFailReason(String failReason) {
        this.failReason = failReason;
    }

    @Basic
    @Column(name = "cancel_reason", nullable = true, length = 100)
    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    @Basic
    @Column(name = "receipt_url", nullable = true, length = 100)
    public String getReceiptUrl() {
        return receiptUrl;
    }

    public void setReceiptUrl(String receiptUrl) {
        this.receiptUrl = receiptUrl;
    }

    @Basic
    @Column(name = "cancel_receipt_urls", nullable = true, length = 100)
    public String getCancelReceiptUrls() {
        return cancelReceiptUrls;
    }

    public void setCancelReceiptUrls(String cancelReceiptUrls) {
        this.cancelReceiptUrls = cancelReceiptUrls;
    }

    @Basic
    @Column(name = "cash_receipt_issued", nullable = true)
    public Byte getCashReceiptIssued() {
        return cashReceiptIssued;
    }

    public void setCashReceiptIssued(Byte cashReceiptIssued) {
        this.cashReceiptIssued = cashReceiptIssued;
    }
}
