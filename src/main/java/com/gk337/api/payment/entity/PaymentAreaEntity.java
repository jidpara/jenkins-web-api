package com.gk337.api.payment.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 결제 지역정보
 */
@Data
@Entity
@IdClass(PaymentAreaEntityPK.class)
@Table(name = "jw_payment_area")
public class PaymentAreaEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 주문번호
     */
    @Id
    @Column(name = "payment_code", insertable = false, nullable = false)
    private String paymentCode;

    /**
     * 지역시퀀스
     */
    @Id
    @Column(name = "area_seq", insertable = false, nullable = false)
    private Integer areaSeq;

    /**
     * 지역그룹시퀀스
     */
    @Id
    @Column(insertable = false, name = "area_group_seq", nullable = false)
    private Integer areaGroupSeq;

    /**
     * 회원시퀀스
     */
    @Column(name = "mem_seq", nullable = false)
    private Integer memSeq;

    /**
     * 상점시퀀스
     */
    @Column(name = "shop_seq", nullable = false)
    private Integer shopSeq;

    /**
     * 등록ID
     */
    @Column(name = "reg_id")
    private String regId = "admin";

    /**
     * 등록IP
     */
    @Column(name = "reg_ip")
    private String regIp = "127.0.0.1";

    /**
     * 등록일자
     */
    @Column(name = "reg_date")
    private LocalDateTime regDate;

    @Column(name = "chg_id")
    private String chgId = "admin";

    @Column(name = "chg_ip")
    private String chgIp = "127.0.0.1";

    /**
     * 수정일자
     */
    @Column(name = "chg_date")
    private LocalDateTime chgDate;

    
}