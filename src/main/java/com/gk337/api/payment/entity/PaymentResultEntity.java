package com.gk337.api.payment.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 결제결과
 */
@Table(name = "jw_payment_result")
@Entity
@Data
@IdClass(PaymentResultEntityPK.class)
public class PaymentResultEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 상점시퀀스
     */
    @Id
    @Column(name = "shop_seq", insertable = false, nullable = false)
    private Integer shopSeq;

    /**
     * 패키지시퀀스
     */
    @Id
    @Column(name = "package_seq", insertable = false, nullable = false)
    private Integer packageSeq;

    /**
     * 1차에선 UUID값 적용
     */
    @Id
    @Column(name = "payment_code", insertable = false, nullable = false)
    private String paymentCode;

    /**
     * 1차 미사용 필드
     */
    @Column(name = "payment_request_seq")
    private Integer paymentRequestSeq;

    /**
     * 1차 미사용 필드
     */
    @Column(name = "payment_response_seq")
    private Integer paymentResponseSeq;

    /**
     * 결제타입 (PG:PG사, BK:계좌이체, CC: 신용카드, MA: 메뉴얼(무료이벤트|관리자) )
     */
    @Column(name = "payment_type", nullable = false)
    private String paymentType;

    /**
     * 가격 - 입력값 혹은 최종 계산값
     */
    @Column(name = "price", nullable = false)
    private Integer price;

    /**
     * 시작일
     */
    @Column(name = "start_date", nullable = false)
    private LocalDateTime startDate;

    /**
     * 만료일
     */
    @Column(name = "end_date", nullable = false)
    private LocalDateTime endDate;

}