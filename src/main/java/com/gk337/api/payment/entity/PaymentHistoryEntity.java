package com.gk337.api.payment.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gk337.common.model.AuditEntity;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

/**
 * 결제결과 이력정보
 */
@Entity
@Table(name = "jw_payment_history")
@Data
public class PaymentHistoryEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 주문번호
     */
    @Id
    @Column(name = "payment_code", insertable = false, nullable = false)
    private String paymentCode;

    /**
     * 아임포트TR고유아이디
     */
    @Column(name = "merchant_uid")
    private String merchantUid = "";

    /**
     * 결제종류코드(PH002) - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌
     */
    @Column(name = "payment_method", nullable = false)
    private String paymentMethod;

    /**
     * 가격 - 입력값 혹은 최종 계산값
     */
    @Column(name = "amount", nullable = false)
    private Integer amount;

    /**
     * 결제상태(PH001) - ready:미결제, paid:결제완료, cancelled:결제취소, failed:결제실패
     */
    @Column(name = "payment_status")
    private String paymentStatus = "";

    /**
     * 결제 시작일
     */
    @Column(name = "start_date", nullable = false)
    private LocalDateTime startDate;

    /**
     * 결제 종료일
     */
    @Column(name = "end_date", nullable = false)
    private LocalDateTime endDate;

    /**
     * 상점시퀀스
     */
    @Column(name = "shop_seq", nullable = false)
    private Integer shopSeq;

    /**
     * 회원시퀀스
     */
    @Column(name = "mem_seq", nullable = false)
    private Integer memSeq;

    /**
     * 승인일시(PG)
     */
    @CreatedDate
    @Column(name = "apply_date")
    private LocalDateTime applyDate;

    /**
     * 취소일시(PG)
     */
    @Column(name = "cancel_date")
    private LocalDateTime cancelDate;

    /**
     * 승인번호(PG)
     */
    @Column(name = "pg_tid")
    private String pgTid = "";

    /**
     * 카드사 코드번호(금융결제원 표준코드번호) = ['361(BC카드)', '364(광주카드)', '365(삼성카드)', '366(신한카드)', '367(현대카드)', '368(롯데카드)', '369(수협카드)', '370(씨티카드)', '371(NH카드)', '372(전북카드)', '373(제주카드)', '374(하나SK카드)', '381(KB국민카드)', '041(우리카드)', '071(우체국)']"
     */
    @Column(name = "card_code")
    private String cardCode = "";

    @Column(name = "card_name")
    private String cardName = "";

    /**
     * 할부개월 수(0이면 일시불)
     */
    @Column(name = "card_quota")
    private Integer cardQuota = 0;

    /**
     * 결제에 사용된 마스킹된 카드번호. 7~12번째 자리를 마스킹하는 것이 일반적이지만, PG사의 정책/설정에 따라 다소 차이가 있을 수 있음
     */
    @Column(name = "card_number")
    private String cardNumber = "";

    /**
     * 영수증URL
     */
    @Column(name = "receipt_url")
    private String receiptUrl = "";

    /**
     * 현금영수증발급 여부
     */
    @Column(name = "cash_receipt_issued")
    private Integer cashReceiptIssued = 0;

}