package com.gk337.api.payment.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "패키지 상점카테고리 등록 Request Vo")
public class PackageShopCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "상점시퀀스",  example = "26")
    private Integer shopSeq;

    @ApiModelProperty(notes = "카테고리시퀀스(1depth)",  example = "1")
    private Integer cateSeq;

    
}