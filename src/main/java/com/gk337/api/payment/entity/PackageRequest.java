package com.gk337.api.payment.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.api.area.entity.ShopAreaEntity;
import com.gk337.api.request.entity.RequestProductAtfilRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "패키지 등록 Request Vo")
public class PackageRequest {

    @ApiModelProperty(notes = "상점 시퀀스", example = "26")
    private Integer shopSeq;

    // 1차에서는 패키지 선택 없음 JsonIgnore 설정 후 DEFAULT 값 적용
    @JsonIgnore
    @ApiModelProperty(notes = "패키지 시퀀스", example = "9000000")
    private Integer packageSeq = 9000000;

    @ApiModelProperty(notes = "아임포트결제고유ID", example = "078831d6b187b2bd23157b3898a142816793fb39")
    private String merchantUid;

    @ApiModelProperty(notes = "딜러 상점지역 리스트")
    private List<PackpageShopArea> shopAreaList;

    @ApiModelProperty(notes = "딜러 상점카테고리 리스트")
    private List<PackageShopCategory> shopCategoryList;


}
