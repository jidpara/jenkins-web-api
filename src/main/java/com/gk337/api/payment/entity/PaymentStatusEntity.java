package com.gk337.api.payment.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 결제상태
 */
@Data
@Table(name = "jw_payment_status")
@Entity
public class PaymentStatusEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 상점시퀀스
     */
    @Id
    @Column(name = "shop_seq", nullable = false)
    private Integer shopSeq;

    /**
     * 상태값 (Y:정상, N:비노출, D:삭제)
     */
    @Column(name = "cstatus", nullable = false)
    private String cstatus;

    /**
     * 결제상태(N:일반, F: 무료, E:기타 )
     */
    @Column(name = "payment_status", nullable = false)
    private String paymentStatus;

    /**
     * 시작일
     */
    @Column(name = "start_date", nullable = false)
    private LocalDateTime startDate;

    /**
     * 만료일
     */
    @Column(name = "end_date", nullable = false)
    private LocalDateTime endDate;

}