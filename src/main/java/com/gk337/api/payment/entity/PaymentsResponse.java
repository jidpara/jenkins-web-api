package com.gk337.api.payment.entity;

import com.gk337.api.request.entity.RequestProductAtfilResponse;
import com.gk337.common.core.mvc.model.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "결제내역상세 Response Vo")
public class PaymentsResponse extends BaseVo {

    @ApiModelProperty(notes = "결제아이디", example = "f58484ac6f9aa39cf41f11f17180f2bbb546ffe7c312842bafe9135906a0b99d")
    private String merchantUid = "f58484ac6f9aa39cf41f11f17180f2bbb546ffe7c312842bafe9135906a0b99d";

	@ApiModelProperty(notes = "상점시퀀스", example = "1")
    private Integer shopSeq;

	@ApiModelProperty(notes = "결제종류코드 - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌", example = "1")
    private String paymentMethod;

	@ApiModelProperty(notes = "결제종류이름", example = "신용카드")
    private String paymentMethodName;

    @ApiModelProperty(notes = "금액", example = "10000")
    private Integer amount;

	@ApiModelProperty(notes = "결제상태. ready:미결제, paid:결제완료, cancelled:결제취소, failed:결제실패 = ['ready', 'paid', 'cancelled', 'failed']", example = "paid")
    private String paymentStatus;

	@ApiModelProperty(notes = "결제상태명", example = "결제완료")
    private String paymentStatusName;

    @ApiModelProperty(notes = "결제일", example = "2020-06-25 22:15:00")
    private LocalDateTime paymentDate;

    @ApiModelProperty(notes = "결제일 포멧팅", example = "06.25 22:15")
    private String paymentDateStr;
}
