package com.gk337.api.payment.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 패키지
 */
@Table(name = "jw_package")
@Entity
@Data
public class PackageEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 시퀀스
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "seq", nullable = false)
    private Integer seq;

    /**
     * 상태값 (Y:정상, N:비노출, D:삭제)
     */
    @Column(name = "cstatus", nullable = false)
    private String cstatus;

    /**
     * 패키지명
     */
    @Column(name = "package_name", nullable = false)
    private String packageName;

    /**
     * 가격
     */
    @Column(name = "price", nullable = false)
    private Integer price;

    /**
     * 등록ID
     */
    @Column(name = "reg_id", nullable = false)
    private String regId;

    /**
     * 등록IP
     */
    @Column(name = "reg_ip", nullable = false)
    private String regIp;

    /**
     * 등록일자
     */
    @Column(name = "reg_date", nullable = false)
    private LocalDateTime regDate;

    /**
     * 최종수정ID
     */
    @Column(name = "chg_id")
    private String chgId = "NULL";

    /**
     * 최종수정IP
     */
    @Column(name = "chg_ip")
    private String chgIp = "NULL";

    /**
     * 최종수정일자
     */
    @Column(name = "chg_date")
    private LocalDateTime chgDate;

    
}