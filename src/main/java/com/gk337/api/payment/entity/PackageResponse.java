package com.gk337.api.payment.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.api.request.entity.RequestProductAtfilResponse;
import com.gk337.common.core.mvc.model.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "결제 패키지 등록 Response Vo")
public class PackageResponse extends BaseVo {

	@ApiModelProperty(notes = "주문번호", example = "20200627-123456-1123")
    private String paymentCode = "20200627-123456-1123";

	@ApiModelProperty(notes = "상점 시퀀스", example = "22")
	private Integer shopSeq;

	@ApiModelProperty(notes = "결제일", example = "2020-06-25 22:15:00")
    private LocalDateTime paymentDate;

    @ApiModelProperty(notes = "결제일 포멧팅", example = "06.25 22:15")
    private String paymentDateStr;

    @ApiModelProperty(notes = "서비스기간 시작일", example = "2020-06-27")
    String startDate;

    @ApiModelProperty(notes = "서비스기간 종료일", example = "2020-07-27")
    String endDate;

	// 1차에서는 패키지 선택 없음 JsonIgnore 설정 후 DEFAULT 값 적용
	@JsonIgnore
	@ApiModelProperty(notes = "패키지 시퀀스", example = "9000000")
	private Integer packageSeq = 9000000;

	@ApiModelProperty(notes = "딜러 상점지역 리스트")
	private List<PackpageShopArea> shopAreaList;

	@ApiModelProperty(notes = "딜러 상점카테고리 리스트")
	private List<PackageShopCategory> shopCategoryList;



}
