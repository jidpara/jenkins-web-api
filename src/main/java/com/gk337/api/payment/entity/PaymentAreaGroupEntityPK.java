package com.gk337.api.payment.entity;

import com.gk337.common.model.AuditEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * 결제결과
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class PaymentAreaGroupEntityPK extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EqualsAndHashCode.Include
    private String paymentCode;

    @EqualsAndHashCode.Include
    private Integer areaGroupSeq;

}