package com.gk337.api.payment.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 결제 카테고리정보
 */
@Table(name = "jw_payment_category")
@Entity
@Data
@IdClass(PaymentCategoryEntityPK.class)
public class PaymentCategoryEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 주문번호
     */
    @Id
    @Column(name = "payment_code", nullable = false)
    private String paymentCode;

    /**
     * 카테고리시퀀스
     */
    @Id
    @Column(name = "cate_seq", nullable = false)
    private Integer cateSeq;

    /**
     * 상점시퀀스
     */
    @Column(name = "shop_seq", nullable = false)
    private Integer shopSeq;

    /**
     * 회원시퀀스
     */
    @Column(name = "mem_seq", nullable = false)
    private Integer memSeq;

}