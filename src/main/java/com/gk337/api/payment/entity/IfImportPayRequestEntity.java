package com.gk337.api.payment.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * IF 아임포트 결재 내역
 */
@Entity
@Table(name = "if_import_pay_request")
@Data
public class IfImportPayRequestEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "app", nullable = false)
    private String app;

    @Id
    @Column(name = "merchant_uid", insertable = false, nullable = false)
    private String merchantUid;

    @Column(name = "imp_uid", nullable = false)
    private String impUid = "''";

    @Column(name = "order_name")
    private String orderName = "NULL";

    @Column(name = "amount", nullable = false)
    private String amount;

    @Column(name = "product_id", nullable = false)
    private String productId;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "status")
    private String status = "''";

    @Column(name = "message")
    private String message = "''";

    @Column(name = "pay_method")
    private String payMethod = "NULL";

    @Column(name = "channel")
    private String channel = "NULL";

    @Column(name = "pg_provider")
    private String pgProvider = "NULL";

    @Column(name = "pg_tid")
    private String pgTid = "NULL";

    @Column(name = "pg_id")
    private String pgId = "NULL";

    @Column(name = "escrow")
    private Integer escrow = 1;

    @Column(name = "apply_num")
    private String applyNum = "NULL";

    @Column(name = "bank_code")
    private String bankCode = "NULL";

    @Column(name = "bank_name")
    private String bankName = "NULL";

    @Column(name = "card_code")
    private String cardCode = "NULL";

    @Column(name = "card_name")
    private String cardName = "NULL";

    @Column(name = "card_quota")
    private Integer cardQuota = 0;

    @Column(name = "card_number")
    private String cardNumber = "NULL";

    @Column(name = "card_type")
    private String cardType = "NULL";

    @Column(name = "vbank_code")
    private String vbankCode = "NULL";

    @Column(name = "vbank_name")
    private String vbankName = "NULL";

    @Column(name = "vbank_num")
    private String vbankNum = "NULL";

    @Column(name = "vbank_holder")
    private String vbankHolder = "NULL";

    @Column(name = "vbank_date")
    private Integer vbankDate = 0;

    @Column(name = "vbank_issued_at")
    private Integer vbankIssuedAt = 0;

    @Column(name = "name")
    private String name = "NULL";

    @Column(name = "cancel_amount")
    private Integer cancelAmount = 0;

    @Column(name = "currency")
    private String currency = "NULL";

    @Column(name = "buyer_name")
    private String buyerName = "NULL";

    @Column(name = "buyer_email")
    private String buyerEmail = "NULL";

    @Column(name = "buyer_tel")
    private String buyerTel = "NULL";

    @Column(name = "buyer_addr")
    private String buyerAddr = "NULL";

    @Column(name = "buyer_postcode")
    private String buyerPostcode = "NULL";

    @Column(name = "custom_data")
    private String customData = "NULL";

    @Column(name = "user_agent")
    private String userAgent = "NULL";

    @Column(name = "started_at")
    private Integer startedAt = 0;

    @Column(name = "paid_at")
    private Integer paidAt = 0;

    @Column(name = "failed_at")
    private Integer failedAt = 0;

    @Column(name = "cancelled_at")
    private Integer cancelledAt = 0;

    @Column(name = "fail_reason")
    private String failReason = "NULL";

    @Column(name = "cancel_reason")
    private String cancelReason = "NULL";

    @Column(name = "receipt_url")
    private String receiptUrl = "NULL";

    @Column(name = "cancel_receipt_urls")
    private String cancelReceiptUrls = "NULL";

    @Column(name = "cash_receipt_issued")
    private Integer cashReceiptIssued = 1;

    @Column(name = "customer_uid")
    private String customerUid = "NULL";

    
}