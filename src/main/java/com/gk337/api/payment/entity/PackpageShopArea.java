package com.gk337.api.payment.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.api.area.entity.ShopAreaEntity;
import com.gk337.api.request.entity.RequestProductAtfilRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "패키지 상점지역정보 등록 Request Vo")
public class PackpageShopArea {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "상점시퀀스", example = "26")
    private Integer shopSeq;

    @ApiModelProperty(notes = "지역그룹시퀀스", example = "1")
    private Integer areaGroupSeq;
}
