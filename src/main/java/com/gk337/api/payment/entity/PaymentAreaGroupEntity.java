package com.gk337.api.payment.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 결제지역그룹 정보
 */
@Entity
@Data
@IdClass(PaymentAreaGroupEntityPK.class)
@Table(name = "jw_payment_area_group")
public class PaymentAreaGroupEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 주문번호
     */
    @Id
    @Column(name = "payment_code", nullable = false)
    private String paymentCode;

    /**
     * 지역그룹시퀀스
     */
    @Id
    @Column(name = "area_group_seq", nullable = false)
    private Integer areaGroupSeq;

    /**
     * 상태값 (Y:정상, N:비노출, D:삭제)
     */
    @Column(name = "cstatus", nullable = false)
    private String cstatus = "Y";

    /**
     * 회원시퀀스
     */
    @Column(name = "mem_seq", nullable = false)
    private Integer memSeq;

    /**
     * 상점시퀀스
     */
    @Column(name = "shop_seq", nullable = false)
    private Integer shopSeq;

}