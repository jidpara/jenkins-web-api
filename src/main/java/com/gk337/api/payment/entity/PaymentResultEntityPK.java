package com.gk337.api.payment.entity;

import com.gk337.common.model.AuditEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 결제결과
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class PaymentResultEntityPK extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EqualsAndHashCode.Include
    private Integer shopSeq;

    @EqualsAndHashCode.Include
    private Integer packageSeq;

    @EqualsAndHashCode.Include
    private String paymentCode;

}