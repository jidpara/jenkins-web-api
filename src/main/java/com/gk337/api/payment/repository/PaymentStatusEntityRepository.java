package com.gk337.api.payment.repository;

import com.gk337.api.payment.entity.PaymentStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaymentStatusEntityRepository extends JpaRepository<PaymentStatusEntity, Integer>, JpaSpecificationExecutor<PaymentStatusEntity> {

    PaymentStatusEntity save(PaymentStatusEntity paymentStatusEntity);

}