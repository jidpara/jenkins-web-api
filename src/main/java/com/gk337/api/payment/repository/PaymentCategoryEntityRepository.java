package com.gk337.api.payment.repository;

import com.gk337.api.payment.entity.PaymentCategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaymentCategoryEntityRepository extends JpaRepository<PaymentCategoryEntity, Integer>, JpaSpecificationExecutor<PaymentCategoryEntity> {

}