package com.gk337.api.payment.repository;

import com.gk337.api.payment.entity.PaymentAreaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaymentAreaEntityRepository extends JpaRepository<PaymentAreaEntity, Integer>, JpaSpecificationExecutor<PaymentAreaEntity> {

}