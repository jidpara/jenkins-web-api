package com.gk337.api.payment.repository;

import com.gk337.api.payment.entity.PackageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PackageEntityRepository extends JpaRepository<PackageEntity, Integer>, JpaSpecificationExecutor<PackageEntity> {

}