package com.gk337.api.payment.repository;

import com.gk337.api.payment.entity.PaymentResultEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaymentResultEntityRepository extends JpaRepository<PaymentResultEntity, String>, JpaSpecificationExecutor<PaymentResultEntity> {

    PaymentResultEntity save(PaymentResultEntity paymentResultEntity);

}