package com.gk337.api.payment.repository;

import com.gk337.api.payment.entity.PaymentHistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaymentHistoryEntityRepository extends JpaRepository<PaymentHistoryEntity, String>, JpaSpecificationExecutor<PaymentHistoryEntity> {

}