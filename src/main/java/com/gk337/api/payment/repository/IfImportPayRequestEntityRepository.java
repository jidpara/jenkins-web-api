package com.gk337.api.payment.repository;

import com.gk337.api.payment.entity.IfImportPayRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IfImportPayRequestEntityRepository extends JpaRepository<IfImportPayRequestEntity, String>, JpaSpecificationExecutor<IfImportPayRequestEntity> {

}