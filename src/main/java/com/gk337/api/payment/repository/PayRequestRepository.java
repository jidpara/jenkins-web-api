package com.gk337.api.payment.repository;

import com.gk337.api.payment.entity.PayRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface PayRequestRepository extends JpaRepository<PayRequest, String>, JpaSpecificationExecutor<PayRequest> {

    Optional<PayRequest> findPayRequestByAppAndMerchantUid(String app, String merchant_uid);


    @Transactional
    @Modifying
    @Query(value=" UPDATE if_import_pay_request "
            + " SET status = :status "
            + " WHERE app = :app and merchant_uid = :merchant_uid ", nativeQuery = true)
    void updatePaymentStatus(
            @Param("app") String app,
            @Param("merchant_uid") String merchant_uid,
            @Param("status") String status) throws Exception;
}
