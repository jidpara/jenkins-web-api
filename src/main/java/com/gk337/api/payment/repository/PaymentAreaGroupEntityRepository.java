package com.gk337.api.payment.repository;

import com.gk337.api.payment.entity.PaymentAreaGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaymentAreaGroupEntityRepository extends JpaRepository<PaymentAreaGroupEntity, Integer>, JpaSpecificationExecutor<PaymentAreaGroupEntity> {

}