package com.gk337.api.payment.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.gk337.api.payment.component.PackageComponent;
import com.gk337.api.payment.entity.*;
import com.gk337.api.payment.repository.PayRequestRepository;
import com.gk337.api.payment.service.PaymentService;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.util.ByteUtils;
import com.gk337.common.util.MaskingUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 결제 관련 RestAPI Controller
 * 프로그램명 : PackageRestController.java
 *
 *  - 패키지 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.03.15
 * @author sejoon
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/payments")
@Api(description="결제관련 API", tags= {"[결제정보] - 결제 API"}, protocols="http", produces="application/json", consumes="application/json")
public class PaymentRestController {

	@Autowired
	private PaymentService paymentService;

	@Autowired
    private PayRequestRepository payRequestRepository;

	@Autowired
	private PackageComponent packageComponent;

    @Value("${pay.import.imp_key}")
    private String IMP_KEY;

    @Value("${pay.import.imp_secret}")
    private String IMP_SECRET;

    @ApiOperation(value = "딜러등록 정보 저장 API",
			notes = "딜러등록 프로세스 중 패키지 정보 저장",
			response= PackageResponse.class)
	@RequestMapping(method = RequestMethod.PUT, path="/package", produces = "application/json")
	public ResponseEntity<PackageResponse> packageInsert(@RequestBody @Valid PackageRequest request) {
		return BaseResponse.ok(packageComponent.processPaymentAllV2(request));
	}


	/**
	 * 최근 결제내역 조회
	 * @param pageNo
	 * @param pageSize
	 * @param memSeq
	 * @param period
	 * @return
	 */
	@ApiOperation(value = "[최근 결제내역 조회] 목록조회 페이징처리 API",
			notes = "[최근 결제내역 조회] 기간에 따라 조회가능 목록조회 페이징처리",
			response= PackageResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<PaymentResult>> paymentResultListPaging(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="회원시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq,
			@ApiParam(value="기간[A:전체 / W:1주일 / 1:1개월 / 3:3개월]", required=true, defaultValue = "A") @RequestParam(value = "period", required = true) String period) {
		//return BaseResponse.ok(requestService.selectReceiveListPaging(pageNo, pageSize, memSeq, reqType, memKind, dealerYn));


//		List<PaymentResult> paymentResultList = new ArrayList<>();
//
//		PaymentResult result1 = new PaymentResult();
//		result1.setAmount(10000000);
//		result1.setPaymentDate(LocalDateTime.of(2020, 6, 27, 11, 15, 10));
//		result1.setPaymentDateStr(LocalDateTime.of(2020, 6, 27, 11, 15, 10).toString());
//		result1.setPaymentMethod("card");
//		result1.setPaymentMethodName("신용카드");
//		result1.setShopSeq(1);
//		result1.setPaymentStatus("paid");
//		result1.setPaymentStatusName("결제완료");
//
//		PaymentResult result2 = new PaymentResult();
//		result2.setAmount(4000000);
//		result2.setPaymentDate(LocalDateTime.of(2020, 5, 27, 11, 15, 10));
//		result2.setPaymentDateStr(LocalDateTime.of(2020, 5, 27, 11, 15, 10).toString());
//		result2.setPaymentMethod("trans");
//		result2.setPaymentMethodName("실시간계좌이체");
//		result2.setShopSeq(1);
//		result2.setPaymentStatus("cancelled");
//		result2.setPaymentStatusName("결제취소");
//
//		PaymentResult result3 = new PaymentResult();
//		result3.setAmount(7000000);
//		result3.setPaymentDate(LocalDateTime.of(2020, 5, 27, 11, 15, 10));
//		result3.setPaymentDateStr(LocalDateTime.of(2020, 5, 27, 11, 15, 10).toString());
//		result3.setPaymentMethod("bank");
//		result3.setPaymentMethodName("무통장입금");
//		result3.setShopSeq(1);
//		result3.setPaymentStatus("ready");
//		result3.setPaymentStatusName("미결제");
//
//		PaymentResult result4 = new PaymentResult();
//		result4.setAmount(10000000);
//		result4.setPaymentDate(LocalDateTime.of(2020, 6, 27, 11, 15, 10));
//		result4.setPaymentDateStr(LocalDateTime.of(2020, 6, 27, 11, 15, 10).toString());
//		result4.setPaymentMethod("card");
//		result4.setPaymentMethodName("신용카드");
//		result4.setShopSeq(1);
//		result4.setPaymentStatus("paid");
//		result4.setPaymentStatusName("결제완료");
//
//		PaymentResult result5 = new PaymentResult();
//		result5.setAmount(4000000);
//		result5.setPaymentDate(LocalDateTime.of(2020, 5, 27, 11, 15, 10));
//		result5.setPaymentDateStr(LocalDateTime.of(2020, 5, 27, 11, 15, 10).toString());
//		result5.setPaymentMethod("trans");
//		result5.setPaymentMethodName("실시간계좌이체");
//		result5.setShopSeq(1);
//		result5.setPaymentStatus("cancelled");
//		result5.setPaymentStatusName("결제취소");
//
//		PaymentResult result6 = new PaymentResult();
//		result6.setAmount(7000000);
//		result6.setPaymentDate(LocalDateTime.of(2020, 5, 27, 11, 15, 10));
//		result6.setPaymentDateStr(LocalDateTime.of(2020, 5, 27, 11, 15, 10).toString());
//		result6.setPaymentMethod("bank");
//		result6.setPaymentMethodName("무통장입금");
//		result6.setShopSeq(1);
//		result6.setPaymentStatus("ready");
//		result6.setPaymentStatusName("미결제");
//
//		PaymentResult result7 = new PaymentResult();
//		result7.setAmount(10000000);
//		result7.setPaymentDate(LocalDateTime.of(2020, 6, 27, 11, 15, 10));
//		result7.setPaymentDateStr(LocalDateTime.of(2020, 6, 27, 11, 15, 10).toString());
//		result7.setPaymentMethod("card");
//		result7.setPaymentMethodName("신용카드");
//		result7.setShopSeq(1);
//		result7.setPaymentStatus("paid");
//		result7.setPaymentStatusName("결제완료");
//
//		PaymentResult result8 = new PaymentResult();
//		result8.setAmount(4000000);
//		result8.setPaymentDate(LocalDateTime.of(2020, 5, 27, 11, 15, 10));
//		result8.setPaymentDateStr(LocalDateTime.of(2020, 5, 27, 11, 15, 10).toString());
//		result8.setPaymentMethod("trans");
//		result8.setPaymentMethodName("실시간계좌이체");
//		result8.setShopSeq(1);
//		result8.setPaymentStatus("cancelled");
//		result8.setPaymentStatusName("결제취소");
//
//		PaymentResult result9 = new PaymentResult();
//		result9.setAmount(7000000);
//		result9.setPaymentDate(LocalDateTime.of(2020, 5, 27, 11, 15, 10));
//		result9.setPaymentDateStr(LocalDateTime.of(2020, 5, 27, 11, 15, 10).toString());
//		result9.setPaymentMethod("bank");
//		result9.setPaymentMethodName("무통장입금");
//		result9.setShopSeq(1);
//		result9.setPaymentStatus("ready");
//		result9.setPaymentStatusName("미결제");
//
//		paymentResultList.add(result1);
//		paymentResultList.add(result2);
//		paymentResultList.add(result3);
//		if("A".equals(period) || "3".equals(period) ) {
//			paymentResultList.add(result4);
//			paymentResultList.add(result5);
//			paymentResultList.add(result6);
//		}
//
//		if("A".equals(period) || "6".equals(period) ) {
//			paymentResultList.add(result7);
//			paymentResultList.add(result8);
//			paymentResultList.add(result9);
//		}
//
//		PageInfo<PaymentResult> pageInfo = new PageInfo<PaymentResult>(paymentResultList);
		return BaseResponse.ok(paymentService.getPaymentHistoryListPaging(pageNo, pageSize, period, memSeq));
	}



    /**
     * 결제 일련 번호 생성 및 결재 요청 정보 저장
     *
     * @param app
     * @param order_name
     * @param amount
     * @param product_id
     * @param user_id
     * @return
     */
    @PostMapping("/generate")
    public Map<String, Object> payGenerate(
            @RequestParam(value = "app", defaultValue = "jgw") String app,
            @RequestParam(value = "order_name", defaultValue = "") String order_name,
            @RequestParam(value = "amount", defaultValue = "") String amount,
            @RequestParam(value = "product_id", defaultValue = "") String product_id,
            @RequestParam(value = "user_id", defaultValue = "") String user_id,
            HttpServletRequest req
    ) {

        // 결과 반환 정보
        Map<String, Object> map = new HashMap<>();

		// 1. merchant_uid 생성
		UUID uuid = UUID.randomUUID();
		String digest = "";
		try {
			MessageDigest salt = MessageDigest.getInstance("SHA-256");
			salt.update(uuid.toString().getBytes(StandardCharsets.UTF_8));
			// TODO : hash값 생성로직 수정 -> 사이즈 45미만으로 생성되도록 변경.  우선 40자리로 임시처리
			digest = ByteUtils.bytesToHex(salt.digest()).substring(0, 40);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		// 2. 요청 정보 저장
		PayRequest payRequest = new PayRequest();
		payRequest.setMerchantUid(digest);
		payRequest.setApp(app);
		payRequest.setOrderName(order_name);
		payRequest.setAmount(amount);
		payRequest.setProductId(product_id);
		payRequest.setUserId(user_id);
		payRequest.setImpUid("");
		payRequest.setStatus("");
		payRequest.setMessage("");
		payRequestRepository.save(payRequest);

		map.put("merchant_uid", digest);

        return map;
    }

    /**
     * 결재 조회 토큰 조회 함수
     * @param imp_key
     * @param imp_secret
     * @return
     */
    private Map<String, Object> payGetToken(String imp_key,
                                            String imp_secret){

        Map<String, Object> map;
        map = new HashMap<>();

        // 1. 엑세스 토큰 발급 받기
        int responseCode = -1;
        String responseMessage = "";
        String responseData = "";
        JsonNode responseBody = null;

        String tokenUrl = "https://api.iamport.kr/users/getToken";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

        MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<String, String>();
        requestMap.add("imp_key", imp_key);
        requestMap.add("imp_secret", imp_secret);

        HttpEntity<MultiValueMap<String, String>> body = new HttpEntity<MultiValueMap<String, String>>(requestMap, headers);

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.postForEntity(new URI(tokenUrl), body, String.class);
            responseCode = response.getStatusCode().value();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (HttpClientErrorException e) {
            responseCode = e.getStatusCode().value();
            if (responseCode == HttpStatus.BAD_REQUEST.value()) {
                responseMessage = "400:: 해당 명령을 실행할 수 없음";
            } else if (responseCode == HttpStatus.UNAUTHORIZED.value()) {
                responseMessage = "401:: 인증에 실패하였습니다. API키와 secret을 확인하세요. imp_key, imp_secret";
            } else if (responseCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                responseMessage = "500:: 서버 에러, 문의 필요";
            }
        }
        if (responseCode == HttpStatus.OK.value()) {
            System.out.println("Request getToken Successful");
            responseMessage = "발급 성공";
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                responseBody = objectMapper.readTree(Objects.requireNonNull(response).getBody());
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonNode resData = responseBody.get("response");
            responseData = resData.toString();
        }
        // 3. 결과 반환 정보
        map.put("status", responseCode);
        map.put("message", responseMessage);
        map.put("data", responseData);

        return map;
    }

    /**
     * 결재 토큰 조회
     * @return
     */
    @PostMapping("/token")
    public Map<String, Object> payToken(HttpServletRequest req) {
        // 1. 결과 반환 정보
        Map<String, Object> map = new HashMap<>();

		Map<String, Object> tokenData = payGetToken(IMP_KEY, IMP_SECRET);
		// 1. 결과 반환 정보
		map.put("status", tokenData.get("status"));
		map.put("message", tokenData.get("message"));
		map.put("data", tokenData.get("data"));

        return map;
    }

    /**
     * 결제 정보 조회
     * @param app
     * @param merchant_uid
     * @return
     */
    @PostMapping("/info")
    public Map<String, Object> payInfo(
            @RequestParam(value = "app", defaultValue = "jgw") String app,
            @RequestParam(value = "merchant_uid", defaultValue = "") String merchant_uid,
            HttpServletRequest req
    ) {
        String responseMessage = "";
        String responseStatus = "";
        String responseData = "";

        // 1. 결과 반환 정보
        Map<String, Object> map = new HashMap<>();

		// 1. 결과 정보 조회
		PayRequest payRequest = new PayRequest();
		payRequest.setMerchantUid(merchant_uid);
		payRequest.setApp(app);
		Optional<PayRequest> payRequestList = payRequestRepository.findPayRequestByAppAndMerchantUid(app, merchant_uid);

		if (payRequestList.isPresent()) {
			PayRequest paymentItem = payRequestList.get();
			ObjectMapper objectMapper = new ObjectMapper();

			try {
				responseData = objectMapper.writeValueAsString(paymentItem);
				responseStatus = "success";
				responseMessage = "성공";
			} catch (JsonProcessingException e) {
				responseStatus = "error";
				responseMessage = "내부 실행 오류";
			}
		}

		// 2. 결과 반환 정보
		map.put("status", responseMessage);
		map.put("message", responseStatus);
		map.put("data", responseData);

        return map;
    }

    /**
     * 결재 완료
     * @param app
     * @param imp_uid
     * @param merchant_uid
     * @return
     */
    @PostMapping("/complete")
    public Map<String, Object> payComplete(
            @RequestParam(value = "app", defaultValue = "jgw") String app,
            @RequestParam(value = "imp_uid", defaultValue = "") String imp_uid,
            @RequestParam(value = "merchant_uid", defaultValue = "") String merchant_uid,
            HttpServletRequest req
    ) {

    	Map<String, Object> map = new HashMap<>();
		// 1. 결제 되어야 하는 금액 조회
		PayRequest payRequest = new PayRequest();
		payRequest.setMerchantUid(merchant_uid);
		payRequest.setApp(app);
		Optional<PayRequest> payRequestList = payRequestRepository.findPayRequestByAppAndMerchantUid(app, merchant_uid);

		// 2. 엑세스 토큰 발급 받기
		int responseCode = -1;
		String responseMessage = "";
		String responseStatus = "";
		JsonNode responseBody = null;

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);
		headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8");

		Map<String, Object> tokenData = payGetToken(IMP_KEY, IMP_SECRET);
		String tokenDataString = tokenData.get("data").toString();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode tokenNode = null;
		try {
			tokenNode = mapper.readTree(tokenDataString);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String resAccessToken = tokenNode.get("access_token").asText();
		String resExpiredAt = tokenNode.get("expired_at").asText();
		responseCode = Integer.parseInt(String.valueOf(tokenData.get("status")));

		MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<String, String>();
		requestMap.add("imp_uid", imp_uid);
		requestMap.add("merchant_uid", merchant_uid);

		HttpEntity<MultiValueMap<String, String>> body = new HttpEntity<MultiValueMap<String, String>>(requestMap, headers);
		if (responseCode == HttpStatus.OK.value()) {

			// 3. imp_uid로 아임포트 서버에서 결제 정보 조회
			String paymentUrl = "https://api.iamport.kr/payments/" + imp_uid + "?_token=" +resAccessToken;

			headers.add("Authorization",  resAccessToken);

			ResponseEntity<String> responseAPI = null;
			try {
				responseAPI = restTemplate.getForEntity(paymentUrl, String.class);
				responseCode = responseAPI.getStatusCode().value();

			} catch (HttpClientErrorException e) {
				responseStatus = "error";
				responseCode = e.getStatusCode().value();
				if (responseCode == HttpStatus.UNAUTHORIZED.value()) {
					responseMessage = "인증 Token이 전달되지 않았거나 유효하지 않음";
				} else if (responseCode == HttpStatus.NOT_FOUND.value()) {
					responseMessage = "유효하지 않은 imp_uid";
				}
			}
			Payment payment = null;
			if (responseCode == HttpStatus.OK.value()) {
				System.out.println("Request payments Successful");

				try {
					ObjectMapper objectMapper = new ObjectMapper();
					responseBody = objectMapper.readTree(Objects.requireNonNull(responseAPI).getBody());
				} catch (IOException e) {
					e.printStackTrace();
				}

				JsonNode resAPIData = Objects.requireNonNull(responseBody).get("response");
				ObjectMapper objMapper = new ObjectMapper();
				try {
					payment = objMapper.readValue(resAPIData.toString(), Payment.class);
				} catch (IOException e) {
					responseStatus = "error";
					responseMessage = "결재 완료 처리 내부 오류";

					// 황세준 임시처리
					// TODO : 프로그램 적으로 이곳에 도달 할 수 없어야 함. 우선 리턴하고 종료함
					map.put("status", responseStatus);
					map.put("message", responseMessage);
					return map;
				}

				if (payRequestList.isPresent() && payment != null) {
					PayRequest paymentItem = payRequestList.get();
					if (paymentItem.getAmount().equals(Objects.requireNonNull(payment).getAmount().toString())) {

						// 4. DB에 결제 정보 저장
						paymentItem.setImpUid(payment.getImp_uid());
						paymentItem.setPayMethod(payment.getPay_method());
						paymentItem.setChannel(payment.getChannel());
						paymentItem.setPgProvider(payment.getPg_provider());
						paymentItem.setStatus(payment.getStatus());
						paymentItem.setPgTid(payment.getPg_tid());
						paymentItem.setPgId(payment.getPg_id());
						paymentItem.setEscrow((byte) (payment.getEscrow() ? 1 : 0));
						paymentItem.setApplyNum(payment.getApply_num());
						paymentItem.setBankCode(payment.getBank_code());
						paymentItem.setBankName(payment.getBank_name());
						paymentItem.setCardCode(payment.getCard_code());
						paymentItem.setCardName(payment.getCard_name());
						paymentItem.setCardNumber(MaskingUtils.maskCardNumber(payment.getCard_number(), "##**********####"));
						paymentItem.setCardQuota(payment.getCard_quota());
						paymentItem.setCardType(payment.getCard_type());
						paymentItem.setVbankCode(payment.getVbank_code());
						paymentItem.setVbankName(payment.getVbank_name());
						paymentItem.setVbankNum(payment.getVbank_num());
						paymentItem.setVbankHolder(payment.getVbank_holder());
						paymentItem.setVbankDate(payment.getVbank_date());
						paymentItem.setVbankIssuedAt(payment.getVbank_issued_at());
						paymentItem.setName(payment.getName());
						paymentItem.setCancelAmount(payment.getCancel_amount());
						paymentItem.setCurrency(payment.getCurrency());
						paymentItem.setBuyerName(payment.getBuyer_name());
						paymentItem.setBuyerEmail(payment.getBuyer_email());
						paymentItem.setBuyerTel(payment.getBuyer_tel());
						paymentItem.setBuyerPostcode(payment.getBuyer_postcode());
						paymentItem.setCustomData(payment.getCustom_data());
						paymentItem.setUserAgent(payment.getUser_agent());
						paymentItem.setStartedAt(payment.getStarted_at());
						paymentItem.setPaidAt(payment.getPaid_at());
						paymentItem.setFailedAt(payment.getFailed_at());
						paymentItem.setCancelledAt(payment.getCancelled_at());
						paymentItem.setFailReason(payment.getFail_reason());
						paymentItem.setReceiptUrl(payment.getReceipt_url());
						String cancelReceiptUrlData = payment.getCancel_receipt_urls().stream()
								.map(String::valueOf)
								.collect(Collectors.joining());
						paymentItem.setCancelReceiptUrls(cancelReceiptUrlData);
						paymentItem.setCashReceiptIssued((byte) (payment.getCash_receipt_issued() ? 1 : 0));
						paymentItem.setCustomerUid(payment.getCustomer_uid());

						try {
							payRequestRepository.save(paymentItem);

							// 가상계좌 발급
							if (payment.getStatus().equals("ready")) {
								responseStatus = "vbankIssued";
								responseMessage = "가상계좌 발급 성공";
								//TODO : DB에 가상계좌 발급 정보 저장
								//TODO : 가상계좌 발급 안내 문자메시지 발송
							}else if (payment.getStatus().equals("paid")) {
								responseStatus = "success";
								responseMessage = "일반 결제 성공";
							}
						} catch (Exception e) {
							responseStatus = "error";
							responseMessage = "결재 상태 코드 업데이트 오류";
						}
					} else {
						responseStatus = "forgery";
						responseMessage = "위조된 결제시도";
					}
				} else if (!payRequestList.isPresent()) {
					responseStatus = "error";
					responseMessage = "DB에 등록되지 않은 요청 정보";
				}
			}
		} else {
			responseStatus = "error";
			responseMessage = "결재 상태 코드 업데이트 오류(내부자료)";
		}

		// 3. 결과 반환 정보
		map.put("status", responseStatus);
		map.put("message", responseMessage);

        return map;
    }

}
