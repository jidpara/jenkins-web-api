package com.gk337.api.payment.controller;

import com.github.pagehelper.PageInfo;
import com.gk337.api.info.entity.FaqResponse;
import com.gk337.api.payment.component.PackageComponent;
import com.gk337.api.payment.entity.PackageRequest;
import com.gk337.api.payment.entity.PackageResponse;
import com.gk337.api.shop.entity.ShopInfoRequest;
import com.gk337.api.shop.entity.ShopInfoResponse;
import com.gk337.api.shop.entity.ShopMainResponse;
import com.gk337.api.shop.entity.ShopMyProductResponse;
import com.gk337.api.shop.service.ShopService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 패키지 관련 RestAPI Controller
 * 프로그램명 : PackageRestController.java
 *
 *  - 패키지 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.03.15
 * @author sejoon
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/payment")
@Api(description="딜러의 패키지 등록 API", tags= {"[딜러패키지등록] - 패키지등록 API"}, protocols="http", produces="application/json", consumes="application/json")
public class PackageRestController {
	
	@Autowired
	private PackageComponent packageComponent;

	@ApiOperation(value = "딜러등록 정보 저장 API",
			notes = "딜러등록 프로세스 중 패키지 정보 저장",
			response= PackageResponse.class)
	@RequestMapping(method = RequestMethod.PUT, path="/package", produces = "application/json")
	public ResponseEntity<PackageResponse> packageInsert(@RequestBody @Valid PackageRequest request) {
		return BaseResponse.ok(packageComponent.processPaymentAll(request));
	}


}
