package com.gk337.api.chat.component;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gk337.api.chat.entity.ChatMessageListResponse;
import com.gk337.api.chat.entity.ChatRequest;
import com.gk337.api.chat.entity.ChatResponse;
import com.gk337.api.chat.service.ChatService;
import com.gk337.common.core.mvc.model.ListVo;

@Component
public class ChatComponent {

	@Autowired
	private ChatService chatService;
	
	/**
	 * 채팅메시지 처음 상대방과 대화시 대화방에 수신인, 발신인 정보를 등록한다.
	 * 등록시 채팅메시지 테이블의 처음 대화 시퀀스를 대화방의 시퀀스로 지정한다.
	 * @param request
	 * @return
	 */
	@Transactional
	public ChatResponse joinChatRoom(ChatRequest request) {
		
		ChatResponse chatResponse = new ChatResponse();
			
		// 신규 채팅방 번호를 생성과 채팅 메시지 등록.
		chatResponse = chatService.saveChat(request);
		int roomSeq = chatResponse.getSeq();
		chatService.updateRoomSeq(roomSeq);

		// 요청서 정보는 비필수로 변경 됨.
		if(request.getReqSeq() != null){
			// 요청건에 대한 응답자의 응답시퀀스 조회
			int resSeq = chatService.selectResponseSeq(request.getReqSeq(), request.getRecvMemSeq());
			request.setResSeq(resSeq);
		}

		// 채팅룸 정보 저장.
		request.setRoomSeq(roomSeq);
		chatService.saveChatRoom(request, request.getSendMemSeq());
		chatService.saveChatRoom(request, request.getRecvMemSeq());
		chatService.updateMessageUnread(roomSeq, request.getRecvMemSeq());
		
		// 시퀀스로 채팅메시지 단건조회.
		chatResponse = chatService.selectChatBySeq(chatResponse.getSeq());
			
		return chatResponse;

	}
	
	/**
	 * 최근 메시지 조회 후 읽음 메시지로 업데이트.
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	public ListVo<ChatMessageListResponse> seletRecentlyMessage(int roomSeq, int memSeq, int messageCnt) {
		
		ListVo<ChatMessageListResponse> listVo = null;
			
		listVo = chatService.selectMessage(roomSeq, memSeq, messageCnt);
		chatService.updateMessageRead(roomSeq, memSeq);
			
		return listVo;
		
	}
	
	/**
	 * 메시지 전송 후 상대 메시지 상태를 읽지 않음 상태로 업데이트.
	 * @param roomSeq
	 * @param request
	 * @return
	 */
	public ChatResponse sendChatMessage(int roomSeq, ChatRequest request) {
		ChatResponse chatResponse = new ChatResponse();

		request.setRoomSeq(roomSeq);
		chatResponse = chatService.saveChat(request);
		
		int recvMemSeq = chatResponse.getRecvMemSeq();
		chatService.updateMessageUnread(roomSeq, recvMemSeq);
		
		int sendMemSeq = chatResponse.getSendMemSeq();
		chatService.updateMessageRead(roomSeq, sendMemSeq);

		// TODO : chat 테이블 반정규화 필요..  recv_mem_name, send_mem_name 때문에 재조회
		chatResponse = chatService.selectChatBySeq(chatResponse.getSeq());
		return chatResponse;
	}
	
}