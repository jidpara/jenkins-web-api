package com.gk337.api.chat.service;

import java.util.HashMap;

import com.gk337.api.chat.entity.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gk337.api.chat.mapper.ChatMapper;
import com.gk337.api.chat.repository.ChatRepository;
import com.gk337.api.chat.repository.ChatRoomRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;

@Service
public class ChatService {
	
	@Autowired
	private ChatRepository chatRepositoy;
	
	@Autowired
	private ChatRoomRepository chatRoomRepositoy;
	
	@Autowired
	private ChatMapper chatMapper;
	
	/**
	 * 방개설 후 메시지 전송 
	 * @param request
	 * @return
	 */
	@Transactional
	public ChatResponse saveChat(ChatRequest request) {
		ChatResponse response = new ChatResponse();
		try {
			ChatEntity entity = new ChatEntity();
			BeanUtils.copyProperties(request, entity);
			entity = chatRepositoy.save(entity);
			BeanUtils.copyProperties(entity, response);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", "저장 또는 수정");
		}
		return response;
	}
	
	/**
	 * 신규 채팅방 번호를 생성함.
	 * 채팅메시지 첫 시퀀스와 동일.
	 * @param seq
	 */
	@Transactional
	public void updateRoomSeq(int seq) {
		try {
			chatRepositoy.updateRoomSeq(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", "저장 또는 수정");
		}
	}

	/**
	 * 채팅방 cstatus update
	 * @param roomSeq
	 * @param memSeq
	 * @param cstatus
	 */
	@Transactional
	public ChatRoomStatusResponse updateRoomStatus(int roomSeq, int memSeq, String cstatus) {

		ChatRoomStatusResponse response = new ChatRoomStatusResponse();
		try {
			chatRoomRepositoy.updateChatRoomCstatus(roomSeq, memSeq, cstatus);
			response.setMemSeq(memSeq);
			response.setCstatus(cstatus);
			response.setRoomSeq(roomSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", "저장 또는 수정");
		}

		return response;
	}
	
	/**
	 * 시퀀스로 채팅메시지 단건조회 
	 * @param seq
	 * @return
	 */
	public ChatResponse selectChatBySeq(int seq) {
		ChatResponse response = new ChatResponse();
		try {
			response = chatMapper.selectChatBySeq(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", "조건조회");
		}
		return response;
	}
	
	/**
	 * 채팅방 정보 저장.
	 * @param request
	 * @param memSeq
	 * @return
	 */
	@Transactional
	public ErrorVo saveChatRoom(ChatRequest request, int memSeq) {
		ErrorVo response = new ErrorVo();
		try {
			ChatRoomEntity entity = new ChatRoomEntity();
			BeanUtils.copyProperties(request, entity);
			entity.setMemSeq(memSeq);
			entity = chatRoomRepositoy.save(entity);
			if(entity.getSeq() != null) 
				return new ErrorVo();
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", "저장 또는 수정");
		}
		return response;
	}
	
	/**
	 * 채팅방 리스트 조회.
	 * @param memSeq
	 * @return
	 */
	public ListVo<ChatRoomListResponse> selectChatRoomList(int memSeq) {
		ListVo<ChatRoomListResponse> listVo = null;
		try {
			listVo = new ListVo<ChatRoomListResponse>(chatMapper.selectChatRoomList(memSeq));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", "채팅방 리스트 ");
		}
		return listVo;
	}
	
	/**
	 * 채팅메시지 목록조회. - 최근목록조회 20건 또는 최근읽지않은 건       
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	public ListVo<ChatMessageListResponse> selectMessage(int roomSeq, int memSeq, int messageCnt) {
		ListVo<ChatMessageListResponse> listVo = null;
		try {
			HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
			hashMap.put("roomSeq", roomSeq);
			hashMap.put("memSeq", memSeq);
			hashMap.put("messageCnt", messageCnt);
			listVo = new ListVo<ChatMessageListResponse>(chatMapper.selectMessage(hashMap));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", "최근목록조회");
		}
		return listVo;
	}
	
	/**
	 * 채팅메시지 목록조회. - 최근목록조회 이후 더보기.     
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	public ListVo<ChatMessageListResponse> selectMessageMore(int chatSeq, int roomSeq, int memSeq) {
		ListVo<ChatMessageListResponse> listVo = null;
		try {
			HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
			hashMap.put("roomSeq", roomSeq);
			hashMap.put("memSeq", memSeq);
			hashMap.put("seq", chatSeq);
			listVo = new ListVo<ChatMessageListResponse>(chatMapper.selectMessageMore(hashMap));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", "목록조회 ");
		}
		return listVo;
	}
	
	/**
	 * 채팅메시지 상태정보 조회.
	 * @param chatSeq
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	public ChatRoomResponse selectMessageStatus(int roomSeq, int memSeq) {
		ChatRoomResponse response = new ChatRoomResponse();
		try {
			HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
			hashMap.put("roomSeq", roomSeq);
			hashMap.put("memSeq", memSeq);
			response = chatMapper.selectMessageStatus(hashMap);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", "상태정보 조회 중 ");
		}
		return response;
	}
	
	/**
	 * 읽지않은 메시지 카운트 증가.
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	@Transactional
	public ErrorVo updateMessageUnread(int roomSeq, int memSeq) {
		try {
			chatRoomRepositoy.updateMessageUnread(roomSeq, memSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", "읽지않은 메시지 카운트 증가");
		}
		return new ErrorVo();
	}
	
	/**
	 * 메시지 읽었을 경우 초기화.
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	@Transactional
	public ErrorVo updateMessageRead(int roomSeq, int memSeq) {
		try {
			chatRoomRepositoy.updateMessageRead(roomSeq, memSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", " 메시지 읽었을 경우 초기화");
		}
		return new ErrorVo();
	}
	
	/**
	 * 요청건에 대한 응답자의 응답시퀀스 조회
	 * @param reqSeq
	 * @param recvMemSeq
	 * @return
	 */
	public Integer selectResponseSeq(int reqSeq, int recvMemSeq) {
		int resSeq = 0;
		try {
			HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
			hashMap.put("reqSeq", reqSeq);
			hashMap.put("recvMemSeq", recvMemSeq);
			if(chatMapper.selectResponseSeq(hashMap) != null) {
				resSeq = chatMapper.selectResponseSeq(hashMap);
			} else {
				reqSeq = 0;
			}
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.CHAT.0001", " 응답시퀀스 조회 중");
		}
		return resSeq;
	}

}

