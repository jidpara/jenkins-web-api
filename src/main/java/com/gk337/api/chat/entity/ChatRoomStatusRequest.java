package com.gk337.api.chat.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "채팅방 상태변경 Request Vo")
public class ChatRoomStatusRequest {
	
	@ApiModelProperty(notes = "회원시퀀스", example="10")
    private Integer memSeq;

	@ApiModelProperty(notes = "상태값 (Y:정상, O:나감)", example="O")
    private String cstatus = "O";

}
