package com.gk337.api.chat.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * chat response
 */

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "채팅메시지 Response Vo")
public class ChatResponse extends BaseVo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

	@JsonIgnore
	@ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus;

	@ApiModelProperty(notes = "채팅방 시퀀스", example="1")
    private Integer roomSeq;

	@ApiModelProperty(notes = "메시지 보낸 회원의 시퀀스", example="2")
    private Integer sendMemSeq;

	@ApiModelProperty(notes = "메시지 받는 회원의 시퀀스", example="1")
    private Integer recvMemSeq;

	@ApiModelProperty(notes = "메시지 보낸 회원의 이름", example="홍길동")
	private String sendMemName;

	@ApiModelProperty(notes = "메시지 받는 회원의 이름", example="이순신")
	private String recvMemName;

	@ApiModelProperty(notes = "채팅메시지", example="채팅메시지입니다.")
    private String chat;

	@ApiModelProperty(notes = "등록일자", example="2020-02-22")
    private LocalDateTime regDate;

}
