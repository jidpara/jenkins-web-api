package com.gk337.api.chat.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "채팅메시지 Request Vo")
public class ChatRequest {
	
	@JsonIgnore
	@ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

	@JsonIgnore
	@ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus = "Y";

	@JsonIgnore
	@ApiModelProperty(notes = "채팅방 시퀀스", example="1")
    private Integer roomSeq = 0;
	
	@ApiModelProperty(notes = "요청서 시퀀스", example="42")
    private Integer reqSeq = 0;

	@ApiModelProperty(notes = "물품 시퀀스", example="42")
    private Integer prodSeq = 0;
	
	@JsonIgnore
	@ApiModelProperty(notes = "답변 시퀀스", example="42")
    private Integer resSeq = 0;

	@ApiModelProperty(notes = "메시지 보낸 회원 시퀀스", example="2")
    private Integer sendMemSeq;

	@ApiModelProperty(notes = "메시지 받는 회원 시퀀스", example="1")
    private Integer recvMemSeq;

	@ApiModelProperty(notes = "채팅 메시지 내용.", example="채팅메시지입니다.")
    private String chat;

}
