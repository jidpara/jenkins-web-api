package com.gk337.api.chat.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * chat response
 */

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "채팅 대화 메시지 리스트 Vo")
public class ChatMessageListResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "대화방시퀀스", example="1")
    private Integer roomSeq;
	
	@ApiModelProperty(notes = "채팅시퀀스", example="1")
    private Integer chatSeq;
	
	@JsonIgnore
	@ApiModelProperty(notes = "상태", example="Y")
    private String cstatus;
	
	@ApiModelProperty(notes = "대화메시지", example="채팅메시지1입니다.")
    private String chat;
	
	@ApiModelProperty(notes = "받는회원시퀀스", example="1")
    private Integer recvMemSeq;
	
	@ApiModelProperty(notes = "보낸회원시퀀스", example="7")
    private Integer sendMemSeq;
	
	@ApiModelProperty(notes = "받는회원이름", example="홍길동")
    private String recvMemName;
	
	@ApiModelProperty(notes = "보낸회원이름", example="솜베게")
    private String sendMemName;
	
	@ApiModelProperty(notes = "메시지노출위치(right/left)", example="right")
    private String msgPos;
	
	@ApiModelProperty(notes = "등록일자", example="2020-02-22")
    private LocalDateTime regdate;

	@ApiModelProperty(notes = "등록일문자열(1일 이내: %h:%i, 1일 이전:%y-%m-%d %p %h:%i)", example="오전 11:05")
	private String regDateStr;

}

