package com.gk337.api.chat.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 채팅로그
 */
@Setter
@Getter
@NoArgsConstructor
@ApiModel(description = "채팅방 Response Vo")
public class ChatRoomResponse extends BaseVo implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ApiModelProperty(notes = "시퀀스", example="2")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제) ", example="Y")
    private String cstatus;

    @ApiModelProperty(notes = "회원의 대화방 시퀀스", example="42")
    private Integer roomSeq;
    
    @ApiModelProperty(notes = "요청서번호", example="12")
    private Integer reqSeq;
    
    @ApiModelProperty(notes = "문의(응답)시퀀스", example="33")
    private Integer resSeq;

    @ApiModelProperty(notes = "회원시퀀스", example="2")
    private Integer memSeq;
    
    @ApiModelProperty(notes = "읽지않은 메시지 수", example="1")
    private Integer unreadCnt;

    @ApiModelProperty(notes = "상대방 방 나감 여부", example="N")
    private String recvOutYn = "N";

}