package com.gk337.api.chat.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * jw_chat_room 테이블 엔티티
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "jw_chat_room")
@ApiModel(description = "jw_chat_room 테이블 엔티티")
public class ChatRoomEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "시퀀스", example="1")
    @Column(insertable = false, name = "seq", nullable = false)
    private Integer seq;

    @NotNull
    @Column(name = "cstatus")
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus = "Y";

    @Column(name = "room_seq", nullable = false)
    @ApiModelProperty(notes = "대화방 시퀀스", example="1")
    private Integer roomSeq;

    @Column(name = "prod_seq", nullable = false)
    @ApiModelProperty(notes = "물품 시퀀스", example="1")
    private Integer prodSeq;

    @Column(name = "req_seq", nullable = false)
    @ApiModelProperty(notes = "요청서 시퀀스", example="1")
    private Integer reqSeq;
    
    @Column(name = "res_seq")
    @ApiModelProperty(notes = "요청서 시퀀스", example="7")
    private Integer resSeq;

    @Column(name = "mem_seq", nullable = false)
    @ApiModelProperty(notes = "참여자 회원시퀀스", example="1")
    private Integer memSeq;
    
    @NotNull
    @Column(name = "unread_cnt")
    @ApiModelProperty(notes = "읽지않은 메시지 수", example="1")
    private Integer unreadCnt = 0;

}