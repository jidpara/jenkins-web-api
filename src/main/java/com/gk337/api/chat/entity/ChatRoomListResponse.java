package com.gk337.api.chat.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * chat response
 */

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "채팅방 리스트 Vo")
public class ChatRoomListResponse extends BaseVo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(notes = "방시퀀스", example="42")
    private Integer roomSeq;

	@ApiModelProperty(notes = "물품시퀀스", example="12")
    private Integer prodSeq;

	@ApiModelProperty(notes = "요청서시퀀스", example="12")
    private Integer reqSeq;
	
	@ApiModelProperty(notes = "문의(응답)시퀀스", example="33")
    private Integer resSeq;
	
	@ApiModelProperty(notes = "읽지않은 메시지 카운트", example="1")
    private Integer unreadCnt;
	
	@JsonIgnore
	@ApiModelProperty(notes = "상태", example="Y")
    private String cstatus;
	
	@ApiModelProperty(notes = "최근 대화자  회원시퀀스", example="1")
    private Integer memSeq;
	
	@ApiModelProperty(notes = "최근 대화자  회원종류", example="L")
    private String memKind;
	
	@ApiModelProperty(notes = "최근 대화자  이메일", example="abcde@gmail.com")
    private String memEmail;
	
	@ApiModelProperty(notes = "최근 대화자  닉네임", example="닉네임")
    private String memNickname;
	
	@ApiModelProperty(notes = "최근 대화자 이름", example="홍길동")
    private String memName;

	@ApiModelProperty(notes = "최근 대화자 통합이름", example="홍길동")
	private String mergeName;
	
	@ApiModelProperty(notes = "최근 대화자  휴대폰번호", example="01012341234")
    private String memHp;
	
	@ApiModelProperty(notes = "최근 대화자  메시지", example="최근메시지입니다.")
    private String chat;
	
	@ApiModelProperty(notes = "프로필이미지", example="https://view01.wemep.co.kr/wmp-product/6/785/163057856/pm_c2ychouklha8.jpg?1559616228.")
    private String profileImage;
	
	@ApiModelProperty(notes = "최근 대화 일자", example="2020-02-22 11:11:11")
    private LocalDateTime regdate;

}
