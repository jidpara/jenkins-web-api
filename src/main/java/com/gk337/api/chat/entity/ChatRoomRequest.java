package com.gk337.api.chat.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "채팅방 Request Vo")
public class ChatRoomRequest {
	
	@JsonIgnore
	@ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

	@JsonIgnore
	@ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus = "Y";
	
	@ApiModelProperty(notes = "문의(응답)시퀀스", example="33")
    private Integer resSeq;

	@ApiModelProperty(notes = "보낸 회원 시퀀스", example="1")
    private Integer sendMemSeq;

	@ApiModelProperty(notes = "받는 회원 시퀀스", example="1")
    private Integer recvMemSeq;
	
	@ApiModelProperty(notes = "읽음 여부 카운트 증가", example="0")
	private Integer unreadCnt = 0;

}
