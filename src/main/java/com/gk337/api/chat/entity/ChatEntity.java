package com.gk337.api.chat.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * jw_chat 테이블 엔티티
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "jw_chat")
@ApiModel(description = "jw_chat 테이블 엔티티")
public class ChatEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

    @NotNull
    @Column(name = "cstatus")
    @ApiModelProperty(notes = " 상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus = "Y";

    @NotNull
    @Column(name = "room_seq")
    @ApiModelProperty(notes = "채팅방시퀀스", example="1")
    private Integer roomSeq = 0;

    @Column(name = "send_mem_seq", nullable = false)
    @ApiModelProperty(notes = "보낸회원 시퀀스", example="1")
    private Integer sendMemSeq;

    @Column(name = "recv_mem_seq", nullable = false)
    @ApiModelProperty(notes = "받는회원 시퀀스", example="1")
    private Integer recvMemSeq;

    @Column(name = "chat", nullable = false)
    @ApiModelProperty(notes = "채팅내용", example="채팅내용")
    private String chat;


}