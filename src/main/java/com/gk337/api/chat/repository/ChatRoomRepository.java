package com.gk337.api.chat.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.chat.entity.ChatRoomEntity;

/**
 * 
 * @author maroojjang
 *
 */
@Repository
public interface ChatRoomRepository extends PagingAndSortingRepository<ChatRoomEntity, Integer>, JpaRepository<ChatRoomEntity, Integer> {
	
	@SuppressWarnings("unchecked")
	ChatRoomEntity save(ChatRoomEntity entity);

	ChatRoomEntity findByRoomSeq(Integer roomSeq);

	@Modifying
	@Query("update ChatRoomEntity q set q.unreadCnt = q.unreadCnt + 1 where q.roomSeq = :roomSeq and q.memSeq = :memSeq")
	void updateMessageUnread(@Param("roomSeq") int roomSeq, @Param("memSeq") int memSeq);
	
	@Modifying
	@Query("update ChatRoomEntity q set q.unreadCnt = 0 where q.roomSeq = :roomSeq and q.memSeq = :memSeq")
	void updateMessageRead(@Param("roomSeq") int roomSeq, @Param("memSeq") int memSeq);

	@Modifying
	@Query("update ChatRoomEntity q set q.cstatus = :cstatus where q.roomSeq = :roomSeq and q.memSeq = :memSeq")
	void updateChatRoomCstatus(@Param("roomSeq") int roomSeq, @Param("memSeq") int memSeq, @Param("cstatus") String cstatus);
	
}