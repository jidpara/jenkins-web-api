package com.gk337.api.chat.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.chat.entity.ChatEntity;

/**
 * 
 * @author maroojjang
 *
 */
@Repository
public interface ChatRepository extends PagingAndSortingRepository<ChatEntity, Integer>, JpaRepository<ChatEntity, Integer> {
	
	@SuppressWarnings("unchecked")
	ChatEntity save(ChatEntity entity);
	
	@Modifying
	@Query("update ChatEntity q set q.roomSeq = :seq where q.seq = :seq")
	void updateRoomSeq(@Param("seq") int seq);
	
}