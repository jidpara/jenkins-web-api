package com.gk337.api.chat.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.chat.entity.ChatMessageListResponse;
import com.gk337.api.chat.entity.ChatResponse;
import com.gk337.api.chat.entity.ChatRoomListResponse;
import com.gk337.api.chat.entity.ChatRoomResponse;
/***
 * 
 * @author maroojjang
 *
 */
@Mapper
public interface ChatMapper {
	
	/**
	 * 채팅메시지 시퀀스 단건조회.
	 * @return
	 */
	public ChatResponse selectChatBySeq(int seq);

	/**
	 * 채팅방 리스트조회.
	 * @param memSeq
	 * @return
	 */
	public List<ChatRoomListResponse> selectChatRoomList(int memSeq);
	
	/**
	 * 최근메시지 조회.
	 * @param hashMap
	 * @return
	 */
	public List<ChatMessageListResponse> selectMessage(HashMap<String, Integer> hashMap);
	
	/**
	 * 최근메시지 이후 더보기.
	 * @param hashMap
	 * @return
	 */
	public List<ChatMessageListResponse> selectMessageMore(HashMap<String, Integer> hashMap);
	
	/**
	 * 채팅룸에 참여한 참여자의 메시지 읽음여부 조회.
	 * @param map
	 * @return
	 */
	public ChatRoomResponse selectMessageStatus(HashMap<String, Integer> map);
	
	/**
	 * 요청건에 대한 응답자의 응답시퀀스 조회.
	 * @param map
	 * @return
	 */
	public Integer selectResponseSeq(HashMap<String, Integer> map);
	
}
