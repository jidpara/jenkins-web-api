package com.gk337.api.chat.controller;

import javax.validation.Valid;

import com.gk337.api.base.entity.FmcMessageRequest;
import com.gk337.api.base.service.NotificationService;
import com.gk337.api.chat.entity.*;
import com.gk337.common.exception.JGWCommunicationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.chat.component.ChatComponent;
import com.gk337.api.chat.service.ChatService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * 
 *
 * @author 
 * @since  
 * @see
 *  <pre>
 *      수정이력
 *          xxxxx
 * </pre>
 */
@CrossOrigin("*")
@Slf4j
@RestController
@RequestMapping("/api/chat")
@Api(tags= {"[메인] - 채팅 API"}, protocols="http", produces="application/json", consumes="application/json")
public class ChatRestController {

	@Autowired
	private ChatService chatService;  
	
	@Autowired
	private ChatComponent chatComponent;

	@Autowired
	NotificationService notificationService;

	/**
	 * 
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "채팅방 개설 후 채팅 메시지 전송 API", notes = "채팅하기 클릭 후 처음 채팅시 호출 API")
	@RequestMapping(method = RequestMethod.POST, path="/", produces = "application/json")
	public ResponseEntity<ChatResponse> joinChatRoom(@RequestBody @Valid ChatRequest request) throws JGWCommunicationException {

		ChatResponse response = chatComponent.joinChatRoom(request);
		// TODO : Async 처리, 다국어 처리, Bulk 처리?
		// Push 메세지 처리 - 요청서에 해당되는 딜러의 모든 기기에 전송
		FmcMessageRequest fmcMessageRequest = new FmcMessageRequest();
		fmcMessageRequest.setName("중고왕의 알림 메세지");
		fmcMessageRequest.setNotificationTitle(String.format("[%s님의 메세지]", response.getSendMemName()));
		fmcMessageRequest.setNotificationBody(String.format(response.getChat()));
		/* DEEP LINK DATA 설정 유의사항
		   메세지 Param의 sendMemSeq 는 메세지를 수신하여 채팅방에 진입하는 사용자의 memSeq 임. recvMemSeq는 이 메세지를 보낸 사용자의 memSeq임
		   즉 request 의 sendMemSeq를 recvMemSeq로 설정,  request 의 recvMemSeq를 sendMemSeq로 설정 해야 함.
		 */
		fmcMessageRequest.setAppData("{\"url\":\"chat_item\", \"param\":{ \"sendMemSeq\": "+ request.getRecvMemSeq() +", \"recvMemSeq\": "+ request.getSendMemSeq() + ", \"roomSeq\": "+ response.getRoomSeq() +", \"reqSeq\": "+ request.getReqSeq() +", \"resSeq\": "+ request.getResSeq() +"}}");

		try {
			CompletableFuture<String> pushNotification = notificationService.sendSync(response.getRecvMemSeq(), fmcMessageRequest);
			CompletableFuture.allOf(pushNotification).join();

			try {
				String firebaseResponse = pushNotification.get();
				System.out.println("firebaseResponse : " + firebaseResponse.toString());
			} catch (InterruptedException e) {
				log.debug("got interrupted!");
				e.printStackTrace();
				throw new JGWCommunicationException("네트워크 오류");
			} catch (ExecutionException e) {
				log.debug("execution error!");
				e.printStackTrace();
				throw new JGWCommunicationException(" 오류");
			}
		} catch (UnsupportedEncodingException | JGWCommunicationException e) {
			log.debug("encoding error!");
			e.printStackTrace();
			throw new JGWCommunicationException("인코딩 오류");
		}
		return BaseResponse.ok(response);
	}
	
	/**
	 * 
	 * @param memSeq
	 * @return
	 */
	@ApiOperation(value = "채팅방 리스트 조회 API", notes = "채팅방 리스트 조회 API")
	@RequestMapping(method = RequestMethod.GET, path="/rooms/{memSeq}", produces = "application/json")
	public ResponseEntity<ChatRoomListResponse> chatRoomList(
			@ApiParam(value="로그인 회원 멤버시퀀스", required=true, defaultValue = "1") @PathVariable(value ="memSeq", required = true) int memSeq) {
		return BaseResponse.ok(chatService.selectChatRoomList(memSeq));
	}
	
	/**
	 * 해당 채팅방 집입시 최근 메시지 조회 API [20건조회]
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	@ApiOperation(value = "채팅방 최근 메시지 리스트 조회 API", notes = "해당 채팅방 집입시 최근 메시지 조회 API [20건조회]")
	@RequestMapping(method = RequestMethod.GET, path="/messages", produces = "application/json")
	public ResponseEntity<ChatMessageListResponse> seletRecentlyMessage(
			@ApiParam(value="채팅방 시퀀스", required=true, defaultValue = "1") @RequestParam(value ="roomSeq", required = true) int roomSeq,
			@ApiParam(value="멤버시퀀스", required=true, defaultValue = "1") @RequestParam(value ="memSeq", required = true) int memSeq) {
		return BaseResponse.ok(chatComponent.seletRecentlyMessage(roomSeq, memSeq, 20));
	}
	
	/**
	 * 해당 채팅방에서 최근메시지 이전 메시지 더보기 조회시 호출 API
	 * @param chatSeq
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	@ApiOperation(value = "채팅방 최근 메시지 더보기 조회 API", notes = "해당 채팅방에서 최근메시지 이전 메시지 더보기 조회시 호출 API")
	@RequestMapping(method = RequestMethod.GET, path="/messages/{chatSeq}", produces = "application/json")
	public ResponseEntity<ChatMessageListResponse> selectMessageMore(
			@ApiParam(value="이전 리스트의 처음 채팅시퀀스", required=true, defaultValue = "1") @PathVariable(value = "chatSeq", required = true) int chatSeq,
			@ApiParam(value="채팅방 시퀀스", required=true, defaultValue = "1") @RequestParam(value = "roomSeq", required = true) int roomSeq,
			@ApiParam(value="멤버시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq) {
		return BaseResponse.ok(chatService.selectMessageMore(chatSeq, roomSeq, memSeq));
	}
	
	/**
	 * 해당 채팅방에서 상태조회 후 읽지않은 메시지가 있을 경우 조회 카운트 건으로 리스트 조회
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	@ApiOperation(value = "채팅방 상태조회 후 읽지않은 메시지 리스트 조회 API", notes = "해당 채팅방에서 상태조회 후 읽지않은 메시지가 있을 경우 조회 카운트 건으로 리스트 조회")
	
	@RequestMapping(method = RequestMethod.GET, path="/messages/new", produces = "application/json")
	public ResponseEntity<ChatMessageListResponse> selectNewlyMessage(
			@ApiParam(value="채팅방 시퀀스", required=true, defaultValue = "1") @RequestParam(value ="roomSeq", required = true) int roomSeq,
			@ApiParam(value="멤버시퀀스", required=true, defaultValue = "1") @RequestParam(value ="memSeq", required = true) int memSeq,
			@ApiParam(value="읽지 않은 메시지 카운트 ", required=true, defaultValue = "3") @RequestParam(value ="messageCnt", required = true) int messageCnt) {
		return BaseResponse.ok(chatComponent.seletRecentlyMessage(roomSeq, memSeq, messageCnt));
	}
	
	/**
	 * 해당 채팅방에서 메시지 입력 후 호출 API
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "채팅 메시지 전송 API", notes = "해당 채팅방에서 메시지 입력 후 호출 API")
	@RequestMapping(method = RequestMethod.POST, path="/message/{roomSeq}", produces = "application/json")
	public ResponseEntity<ChatResponse> sendChatMessage(
			@ApiParam(value="채팅방 시퀀스", required=true, defaultValue = "1") @PathVariable(value ="roomSeq", required = true) int roomSeq,
			@RequestBody @Valid ChatRequest request) throws JGWCommunicationException {

		ChatResponse response = chatComponent.sendChatMessage(roomSeq, request);
		// TODO : Async 처리, 다국어 처리, Bulk 처리?
		// Push 메세지 처리 - 요청서에 해당되는 딜러의 모든 기기에 전송
//		FmcMessageRequest fmcMessageRequest = new FmcMessageRequest();
//		fmcMessageRequest.setName("중고왕의 알림 메세지");
//		fmcMessageRequest.setNotificationTitle(String.format("[%s님의 메세지]", response.getSendMemName()));
//		fmcMessageRequest.setNotificationBody(String.format(response.getChat()));
//
//		try {
//			CompletableFuture<String> pushNotification = notificationService.send(response.getRecvMemSeq(), fmcMessageRequest);
//			CompletableFuture.allOf(pushNotification).join();
//
//			try {
//				String firebaseResponse = pushNotification.get();
//				System.out.println("firebaseResponse : " + firebaseResponse.toString());
//			} catch (InterruptedException e) {
//				log.debug("got interrupted!");
//				e.printStackTrace();
//				throw new JGWCommunicationException("네트워크 오류");
//			} catch (ExecutionException e) {
//				log.debug("execution error!");
//				e.printStackTrace();
//				throw new JGWCommunicationException(" 오류");
//			}
//		} catch (UnsupportedEncodingException | JGWCommunicationException e) {
//			log.debug("encoding error!");
//			e.printStackTrace();
//			throw new JGWCommunicationException("인코딩 오류");
//		}
		return BaseResponse.ok(response );
	}
	
	/**
	 * 해당 채팅방에서 메시지 상태조회 API
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	@ApiOperation(value = "채팅방 상태조회 API", notes = "해당 채팅방에서 메시지 상태조회 API")
	@RequestMapping(method = RequestMethod.GET, path="/messages/status", produces = "application/json")
	public ResponseEntity<ChatRoomResponse> selectMessageStatus(
			@ApiParam(value="채팅방 시퀀스", required=true, defaultValue = "1") @RequestParam(value = "roomSeq", required = true) int roomSeq,
			@ApiParam(value="멤버시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq) {
		return BaseResponse.ok(chatService.selectMessageStatus(roomSeq, memSeq));
	}
	
	/**
	 * 
	 * @param roomSeq
	 * @param memSeq
	 * @return
	 */
	/*
	@ApiOperation(value = "채팅메시지 읽지않음 처리 API - 화면에서 사용안함.", notes = "채팅메시지 읽지않음 처리 API")
	@RequestMapping(method = RequestMethod.PUT, path="/messages/unread", produces = "application/json")
	public ResponseEntity<ErrorVo> updateMessageUnread(
			@ApiParam(value="채팅방 시퀀스", required=true, defaultValue = "1") @RequestParam(value = "roomSeq", required = true) int roomSeq,
			@ApiParam(value="멤버시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq) {
		return BaseResponse.ok(chatService.updateMessageUnread(roomSeq, memSeq));
	}
	*/
	
	/**
	 * 
	 * @param roomSeq
	 * @param request
	 * @return
	 */
	/*
	@ApiOperation(value = "채팅메시지 읽음처리 처리 API - 화면에서 사용안함.", notes = "채팅메시지 읽음처리 처리 API")
	@RequestMapping(method = RequestMethod.PUT, path="/messages/read", produces = "application/json")
	public ResponseEntity<ErrorVo> updateMessageRead(
			@ApiParam(value="채팅방 시퀀스", required=true, defaultValue = "1") @RequestParam(value = "roomSeq", required = true) int roomSeq,
			@ApiParam(value="멤버시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq) {
		return BaseResponse.ok(chatService.updateMessageRead(roomSeq, memSeq));
	}
	*/
	
	@ApiOperation(value = "채팅방 상태 업데이트(나가기, 채팅하기 등) API", notes = "채팅방 업데이트(나가기, 나가기 채팅하기 등) API ")
	@RequestMapping(method = RequestMethod.PUT, path="/chatRoom/{roomSeq}", produces = "application/json")
	public ResponseEntity<ChatRoomStatusResponse> chatRoom (
			@ApiParam(value="채팅방 시퀀스", required=true, defaultValue = "1") @PathVariable(value ="roomSeq", required = true) int roomSeq,
			@RequestBody ChatRoomStatusRequest request) {

		return ResponseEntity.ok(chatService.updateRoomStatus(roomSeq, request.getMemSeq(), request.getCstatus()));
	}


	
}
