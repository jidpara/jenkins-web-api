package com.gk337.api.code.service;

import java.util.List;
import java.util.stream.Collectors;

import com.gk337.api.code.repository.CodeInfoRepository;
import com.gk337.api.code.model.CodeInfoEntity;
import com.gk337.api.code.model.CodeInfoGrpEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.util.DateUtil;

import lombok.extern.slf4j.Slf4j;
/**
 * 스마트플래너 공통코드관련 Biz 로직을 구현
 *
 * @author P117132
 * @since  2019.09.26
 * @see
 * 	<pre>
 * 		수정이력
 * 			2019.09.26 P117132 최초작성
 * </pre>
 */
@Slf4j
@Service
public class CodeInfoService {

	@Autowired
	private CodeInfoRepository codeInfoRepository;

	private final String delYn = "N";
	private final String sysdate = DateUtil.getShortDateString();

	/**
	 * 그룹아이디로 스마트플래너 공통코드 조회
	 * @param
	 * 		String smplnrStrdGrpId : 스마트플래너기준 그룹ID
	 * @return
	 * 		ListVo<CodeInfoGrpEntity> : 스마트플래너기준정보 리스트
	 */
	@Transactional
	public ListVo<CodeInfoGrpEntity> findBysmplnrStrdGrpId(String smplnrStrdGrpId) {
		log.debug("CodeInfoService findBysmplnrStrdGrpId() =============== " );
		ListVo<CodeInfoGrpEntity> listVo = null;
		try {

			List<CodeInfoEntity> list = codeInfoRepository.findBySmplnrStrdGrpId(smplnrStrdGrpId).stream()
					.flatMap(data -> data.getCodeInfo().stream())
					.filter(data2 -> data2.getDelYn().equals(delYn) && this.checkDate(sysdate, data2.getEffStaDt(), data2.getEffEndDt()))
					.collect(Collectors.toList());

			List<CodeInfoGrpEntity> grpList = codeInfoRepository.findBySmplnrStrdGrpId(smplnrStrdGrpId);
			grpList.get(0).setCodeInfo(list);
			listVo = new ListVo<CodeInfoGrpEntity>(grpList);
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("INFO.COUNSEL.MESSAGE.0001");
		}
		return listVo;
	}

	/**
	 * 유효기간 체크
	 * @param
	 * 		String sysdate : 현재날짜
	 * 		String staDate : 유효시작일자
	 * 		String EndDate : 유효종료일자
	 * @return
	 * 		boolean : true/false
	 */
	public boolean checkDate(String sysdate, String staDate, String EndDate) {
		boolean flag = false;
		if(Integer.parseInt(staDate) <= Integer.parseInt(sysdate) && Integer.parseInt(EndDate) >= Integer.parseInt(sysdate))
			return flag = true;
		return flag;
	}

}
