package com.gk337.api.code.model;

import java.io.Serializable;

import com.gk337.common.core.domain.PKEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class CodeInfoPK implements Serializable, PKEntity {

	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	private String smplnrStrdGrpId;                   

	@EqualsAndHashCode.Include
	private String smplnrStrdId;					  

}
