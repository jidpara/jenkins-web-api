package com.gk337.api.code.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gk337.common.core.domain.BaseEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("deprecation")
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(CodeInfoPK.class)
@Table(name="ZSMP_SMPLNR_STRD_INFO") /* 스마트플래너기준정보 */
@ApiModel(description = "스마트플래너 공통코드 엔티티")
@JsonNaming(PropertyNamingStrategy.LowerCaseWithUnderscoresStrategy.class)
public class CodeInfoEntity extends BaseEntity {

	@Id @ApiModelProperty(notes = "스마트플래너기준그룹ID")
	private String smplnrStrdGrpId;
	@Id @ApiModelProperty(notes = "스마트플래너기준ID")
	private String smplnrStrdId;
	@ApiModelProperty(notes = "스마트플래너기준값")
	private String smplnrStrdVal;
	@ApiModelProperty(notes = "스마트플래너기준설명")
	private String smplnrStrdDesc;
	@ApiModelProperty(notes = "정렬순번")
	private String sortSeq;
	@ApiModelProperty(notes = "삭제여부")
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String delYn;
	@ApiModelProperty(notes = "유효시작일자")
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String effStaDt;
	@ApiModelProperty(notes = "유효종료일자")
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String effEndDt;
	@ApiModelProperty(notes = "확장01값")
	private String expnd_01_Val;
	@ApiModelProperty(notes = "확장02값")
	private String expnd_02_Val;
	@ApiModelProperty(notes = "확장03값")
	private String expnd_03_Val;

}
