package com.gk337.api.code.model;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gk337.common.core.domain.AggregateRoot;
import com.gk337.common.core.domain.AggregateRootEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SuppressWarnings("deprecation")
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name="ZSMP_SMPLNR_STRD_INFO_GRP") /* 스마트플래너기준정보 */
@ApiModel(description = "스마트플래너 공통 그룹코드")
@JsonNaming(PropertyNamingStrategy.LowerCaseWithUnderscoresStrategy.class)
public class CodeInfoGrpEntity extends AggregateRootEntity implements AggregateRoot {

	@Id @ApiModelProperty(notes = "스마트플래너기준그룹ID")
	private String smplnrStrdGrpId;
	@ApiModelProperty(notes = "기준그룹명")
	private String strdGrpNm;
	@ApiModelProperty(notes = "기준그룹설명")
	private String strdGrpDesc;

	@OrderBy("sortSeq ASC")
	@JoinColumn(name = "smplnrStrdGrpId", insertable = false, updatable = false)
	@NotFound(action = NotFoundAction.IGNORE)
	@OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Collection<CodeInfoEntity> codeInfo;   /* 스마트플래너기준정보 */
}
