package com.gk337.api.code.repository;

import java.util.List;

import com.gk337.api.code.model.CodeInfoGrpEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * 스마트플래너 공통코드관련 @Repository
 *
 * @author P117132
 * @since  2019.09.26
 * @see
 * 	<pre>
 * 		수정이력
 * 			2019.09.26 P117132 최초작성
 * </pre>
 */
@Repository
public interface CodeInfoRepository extends PagingAndSortingRepository<CodeInfoGrpEntity, String>, JpaRepository<CodeInfoGrpEntity, String> {

	List<CodeInfoGrpEntity> findBySmplnrStrdGrpId(String smplnrStrdGrpId);

}
