package com.gk337.api.code.controller;

import java.util.List;

import com.gk337.api.code.service.CodeInfoService;
import com.gk337.api.code.model.CodeInfoGrpEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 *
 * @author 
 * @since  
 * @see
 *  <pre>
 *      수정이력
 *          xxxxx
 * </pre>
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/code-info")
@Api(tags= {"[샘플] 공통코드 API"}, protocols="http", produces="application/json", consumes="application/json")
public class CodeInfoRestController {
//
//	@Autowired
//	private CodeInfoService codeInfoService;
//	
//	@ApiOperation(value = "[1-API-001] : 공통코드조회", notes="공통코드를 조회한다.", position=1)
//    @RequestMapping(method = RequestMethod.GET, path="/{smplnrStrdGrpId}", produces = "application/json")
//	public ResponseEntity<List<CodeInfoGrpEntity>> findBysmplnrStrdGrpIdAndDelYnAndEffDt(
//			@ApiParam(value="스마트플래너기준 그룹ID", required=true, defaultValue = "BNPROD_CL_CD") @PathVariable(value = "smplnrStrdGrpId", required = true) String smplnrStrdGrpId) {
//		return BaseResponse.ok( codeInfoService.findBysmplnrStrdGrpId(smplnrStrdGrpId) );
//	}
}
