package com.gk337.api.verify.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.verify.entity.UserMatchingResponse;

@Mapper
public interface VerifyMapper {
	
	public UserMatchingResponse selectAreaAndCategory(int reqSeq);
	
	public List<UserMatchingResponse> selectVerifyUser(HashMap map);

	public List<UserMatchingResponse> selectVerifyUserForGmidas(HashMap map);

	public List<UserMatchingResponse> selectMatchingUser(HashMap map);
		
}
