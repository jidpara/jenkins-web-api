package com.gk337.api.verify.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.verify.entity.UserMatchingResponse;
import com.gk337.api.verify.service.VerifyService;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/verify")
@Api(tags= {"[검증] - 확인용 API"}, protocols="http", produces="application/json", consumes="application/json")
public class VerifyRestController {
	
	@Autowired
	private VerifyService verifyService;
	
	@ApiOperation(value = "해당 요청서를 받는 유저 정보조회 API", notes = "해당 요청서를 받는 유저 정보조회 API", response = UserMatchingResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/request/{reqSeq}", produces = "application/json")
	public ResponseEntity<UserMatchingResponse> selectRequestInfo(
			@ApiParam(value="요청서시퀀스", required=true, defaultValue = "191") @PathVariable(value = "reqSeq", required = true) int reqSeq,
			@ApiParam(value="요청서타입", required=true, defaultValue = "S") @PathVariable(value = "reqSeq", required = false) String reqType) {
		return BaseResponse.ok(verifyService.selectVerifyUser(reqSeq, reqType));
	}
	
	@ApiOperation(value = "지역정보와 카테고리에 매칭되는 딜러정보 조회 API", notes = "지역정보와 카테고리에 매칭되는 딜러정보 조회 API", response = UserMatchingResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/matching", produces = "application/json")
	public ResponseEntity<UserMatchingResponse> selectRequestInfo(
			@ApiParam(value="지역코드(2Depth)", required=true, defaultValue = "1009") @RequestParam(value = "areaSeq", required = true) int areaSeq,
			@ApiParam(value="카테고리코드", required=true, defaultValue = "1") @RequestParam(value = "cateSeq", required = true) int cateSeq) {
		return BaseResponse.ok(verifyService.selectMatchingUser(areaSeq, cateSeq));
	}
}