package com.gk337.api.verify.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "요청서 받는 유저 정보")
public class UserMatchingResponse {
	
	@JsonIgnore
	@ApiModelProperty(notes = "지역 시퀀스", example = "11")
    private Integer areaSeq;
    
	@JsonIgnore
    @ApiModelProperty(notes = "카테고리 시퀀스", example = "26")
    private Integer cateSeq;
	
    @ApiModelProperty(notes = "회원 시퀀스", example = "11")
    private Integer memSeq;
    
    @ApiModelProperty(notes = "상점 시퀀스", example = "26")
    private Integer shopSeq;

    @ApiModelProperty(notes = "상점명", example = "알파문구.")
    private String shopName;

    @ApiModelProperty(notes = "회원명", example = "홍길동")
    private String memName;
    
    @ApiModelProperty(notes = "유저아이디", example = "userid")
    private String userId;
    
    @ApiModelProperty(notes = "닉네임", example = "홍길동")
    private String memNickname;
    
    @ApiModelProperty(notes = "이메일", example = "abc@abc.com")
    private String memEmail;
    
    @ApiModelProperty(notes = "샵전화번호", example = "01012112121")
    private String shopPhone;

    @JsonIgnore
    @ApiModelProperty(notes = "딜러사용자의 디바이스토큰")
    private String deviceToken;
    
}
