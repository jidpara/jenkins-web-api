package com.gk337.api.verify.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk337.api.verify.entity.UserMatchingResponse;
import com.gk337.api.verify.mapper.VerifyMapper;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;

@Service
public class VerifyService {
	
	@Autowired
	private VerifyMapper verifyMapper;

	public List<UserMatchingResponse> selectVerifyUserList(int reqSeq, String reqType) {
		List<UserMatchingResponse> list = null;
		try {

			UserMatchingResponse response = verifyMapper.selectAreaAndCategory(reqSeq);

			if(response != null){
				HashMap<String, Object> hashMap = new HashMap<String, Object>();
				hashMap.put("areaSeq", response.getAreaSeq());
				hashMap.put("cateSeq", response.getCateSeq());
				hashMap.put("reqType", reqType);
				list = verifyMapper.selectVerifyUser(hashMap);
			}

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "목록조회");
		}
		return list;
	}

	public List<UserMatchingResponse> selectVerifyUserListForGmidas(Integer reqSeq, String reqType) {
		List<UserMatchingResponse> list = null;
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("reqSeq", reqSeq);
			hashMap.put("reqType", reqType);
			list = verifyMapper.selectVerifyUserForGmidas(hashMap);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "목록조회");
		}
		return list;
	}

	public ListVo<UserMatchingResponse> selectVerifyUser(int reqSeq, String reqType) {
		ListVo<UserMatchingResponse> listVo = new ListVo<UserMatchingResponse>(new ArrayList<UserMatchingResponse>());;
		try {
			
			UserMatchingResponse response = verifyMapper.selectAreaAndCategory(reqSeq);

			if(response != null){
				HashMap<String, Object> hashMap = new HashMap<String, Object>();
				hashMap.put("areaSeq", response.getAreaSeq());
				hashMap.put("cateSeq", response.getCateSeq());
				hashMap.put("reqType", reqType);
				listVo = new ListVo<UserMatchingResponse>(verifyMapper.selectVerifyUser(hashMap));
			}
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "목록조회");
		}
		return listVo;
	}
	
	public ListVo<UserMatchingResponse> selectMatchingUser(int areaSeq, int cateSeq) {
		ListVo<UserMatchingResponse> listVo = null;
		try {
			
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("areaSeq", areaSeq);
			hashMap.put("cateSeq", cateSeq);
			listVo = new ListVo<UserMatchingResponse>(verifyMapper.selectMatchingUser(hashMap));
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "목록조회");
		}
		return listVo;
	}
	
}


