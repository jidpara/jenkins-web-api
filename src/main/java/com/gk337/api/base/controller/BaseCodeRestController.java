package com.gk337.api.base.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.base.entity.BaseCodeEntity;
import com.gk337.api.base.service.BaseCodeService;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 공통 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 공통 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.02.03
 * @author shinhong
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/base")
@Api(tags= {"[공통정보] - 공통코드 API"}, protocols="http", produces="application/json", consumes="application/json")
public class BaseCodeRestController {

	@Autowired
	private BaseCodeService baseCodeService;
	
	/**
	 * 그룹코드로 공통코드조회.
	 * @param groupCode
	 * @return
	 */
	@ApiOperation(value = "그룹코드[단일] 공통코드 조회 API", notes = "공통코드 그룹코드로 조회")
	@RequestMapping(method = RequestMethod.GET, path="/code/{groupCode}", produces = "application/json")
	public ResponseEntity<List<BaseCodeEntity>> findByGroupCode(
			@ApiParam(value="그룹코드", required=true, defaultValue = "FAQ_CATE") @PathVariable(value = "groupCode", required = true) String groupCode) {
		return BaseResponse.ok(baseCodeService.selecCodeByGroupCode(groupCode));
	}
	
	/**
	 * 그룹코드로 공통코드조회.
	 * @param groupCode
	 * @return
	 */
//	@ApiOperation(value = "그룹코드[필터] 공통코드 조회", notes = "공통코드 그룹코드로 조회")
//	@RequestMapping(method = RequestMethod.POST, path="/code", produces = "application/json")
//	public ResponseEntity<List<BaseCodeResponse>> findAll() {
//		return BaseResponse.ok(baseService.findByGroupCode());
//	}

	
}
