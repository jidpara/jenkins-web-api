package com.gk337.api.base.controller;

import com.gk337.api.base.entity.FmcMessageRequest;
import com.gk337.api.base.entity.MsgSendRequest;
import com.gk337.api.base.service.AndroidPushNotificationsService;
import com.gk337.api.base.service.AndroidPushPeriodicNotifications;
import com.gk337.api.base.service.NotificationService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

//test

@Slf4j
@Api(value = "Push Notification 관련 API")
@CrossOrigin("*")
@RequestMapping("/api/base/notification")
@RestController("NotificationController")
public class NotificationController {

    @Autowired
    AndroidPushNotificationsService androidPushNotificationsService;

    @Autowired
    NotificationService notificationService;

    @GetMapping(value = "/send")
    public @ResponseBody
    ResponseEntity<String> send() throws JSONException, InterruptedException {
        String notifications = AndroidPushPeriodicNotifications.PeriodicNotificationJson();

        HttpEntity<String> request = new HttpEntity<>(notifications);

        CompletableFuture<String> pushNotification = androidPushNotificationsService.send(request);
        CompletableFuture.allOf(pushNotification).join();

        try{
            String firebaseResponse = pushNotification.get();
            return new ResponseEntity<>(firebaseResponse, HttpStatus.OK);
        }
        catch (InterruptedException e){
            log.debug("got interrupted!");
            throw new InterruptedException();
        }
        catch (ExecutionException e){
            log.debug("execution error!");
        }

        return new ResponseEntity<>("Push Notification ERROR!", HttpStatus.BAD_REQUEST);
    }


    @PostMapping("/{memSeq}")
    @ApiOperation(value = "Notification 메세지 발송")
    public ResponseEntity<String> sendNotification(
            @PathVariable("memSeq") Integer memSeq,
            @RequestBody @Valid FmcMessageRequest request) throws JSONException, InterruptedException, UnsupportedEncodingException {

        CompletableFuture<String> pushNotification = notificationService.sendSync(memSeq, request);
        CompletableFuture.allOf(pushNotification).join();

        try{
            String firebaseResponse = pushNotification.get();
            return BaseResponse.ok(firebaseResponse);
        }
        catch (InterruptedException e){
            log.debug("got interrupted!");
            throw new InterruptedException();
        }
        catch (ExecutionException e){
            log.debug("execution error!");
        }

        ErrorVo error = new ErrorVo();
        error.setMessage("Push Notification ERROR");
        error.setCode("00000");
        return BaseResponse.error(error, HttpStatus.BAD_REQUEST);

    }

    @PostMapping("/sendBulk")
    @ApiOperation(value = "Notification 대량 메세지 발송")
    public ResponseEntity<String> sendBulkNotification(
            @RequestBody @Valid MsgSendRequest request) throws JSONException, InterruptedException, UnsupportedEncodingException {


        List<Integer> memSeqList = request.getMemSeqList();
        String title = request.getTitle();
        String content = request.getContent();

        CompletableFuture<String> pushNotification = notificationService.adminSendBulk(memSeqList,title,content);
        CompletableFuture.allOf(pushNotification).join();

        try{
            String firebaseResponse = pushNotification.get();
            return BaseResponse.ok(firebaseResponse);
        }
        catch (InterruptedException e){
            log.debug("got interrupted!");
            throw new InterruptedException();
        }
        catch (ExecutionException e){
            log.debug("execution error!");
        }

        ErrorVo error = new ErrorVo();
        error.setMessage("Push Notification ERROR");
        error.setCode("00000");
        return BaseResponse.error(error, HttpStatus.BAD_REQUEST);

    }
}