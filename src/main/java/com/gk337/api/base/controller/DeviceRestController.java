package com.gk337.api.base.controller;

import com.gk337.api.base.entity.DeviceInfoRequest;
import com.gk337.api.base.entity.DeviceInfoResponse;
import com.gk337.api.base.entity.DeviceSettingRequest;
import com.gk337.api.base.entity.DeviceSettingResponse;
import com.gk337.api.base.service.DeviceInfoService;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.exception.JGWServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <PRE>
 * 시스템명    : 중고왕 서비스
 * 업무명       : 공통모듈
 * 프로그램명 : DeviceController.java
 *
 *  - 공통 디바이스정보 RestController 클래스 -
 *
 * </PRE>
 *
 * @since 2020. 02.16
 * @author sejoon
 * @version 1.0
 * @see RestController
 *
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */
@Slf4j
@Api(tags= {"[공통정보] - 디바이스 정보 API"}, protocols="http", produces="application/json")
@CrossOrigin("*")
@RequestMapping("/api/base")
@RestController
public class DeviceRestController {

    @Autowired
    private DeviceInfoService deviceInfoService;


    @ApiOperation(value = "디바이스정보 등록 API", notes = "사용자의 기기정보를 기록한다. ")
    @PutMapping("/device")
    public ResponseEntity<DeviceInfoResponse> device(@RequestBody @Valid DeviceInfoRequest request) throws JGWServiceException {
        return BaseResponse.ok(deviceInfoService.updateDeviceInfo(request));
    }

    @ApiOperation(value = "디바이스 설정정보 등록 API", notes = "사용자의 기기 설정정보를 기록한다. ")
    @PutMapping("/device/setting")
    public ResponseEntity<DeviceSettingResponse> deviceSetting(@RequestBody @Valid DeviceSettingRequest request) throws JGWServiceException {
        return BaseResponse.ok(deviceInfoService.updateDeviceSetting(request));
    }

}
