package com.gk337.api.base.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.base.entity.BaseCategoryRequest;
import com.gk337.api.base.entity.BaseCategoryResponse;
import com.gk337.api.base.entity.BaseCategorySearchResponse;
import com.gk337.api.base.service.BaseCategoryServce;
import com.gk337.api.info.entity.FaqResponse;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 공통 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 공통 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.02.03
 * @author shinhong
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/base")
@Api(tags= {"[공통정보] - 카테고리 API"}, protocols="http", produces="application/json", consumes="application/json")
public class BaseCategoryRestController {

	@Autowired
	private BaseCategoryServce baseCategoryServce;

	@ApiOperation(value = "카테고리 등록 API", notes = "카테고리를 등록한다.", response = BaseCategoryResponse.class)
	@RequestMapping(method = RequestMethod.POST, path="/category", produces = "application/json")
	public ResponseEntity<BaseCategoryResponse> saveCategory(@RequestBody @Valid BaseCategoryRequest request) {
		return BaseResponse.ok(baseCategoryServce.saveCategory(request));
	}
	
	@ApiOperation(value = "카테고리 수정 API", notes = "카테고리를 수정한다.", response = BaseCategoryResponse.class)
	@RequestMapping(method = RequestMethod.PUT, path="/category/{seq}", produces = "application/json")
	public ResponseEntity<BaseCategoryResponse> updateCategory(@RequestBody @Valid BaseCategoryRequest request) {
		return BaseResponse.ok(baseCategoryServce.saveCategory(request));
	}
	
	@ApiOperation(value = "카테고리 삭제 API", notes = "카테고리 삭제한다.", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.DELETE, path="/category/{seq}", produces = "application/json")
	public ResponseEntity<ErrorVo> deleteCategory(
			@ApiParam(value="시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq) {
		return BaseResponse.ok(baseCategoryServce.deleteBySeq(seq));
	}
	
	@ApiOperation(value = "카테고리 그룹시퀀스 목록조회 API", notes = "카테고리 레벨 목록조회.", response = BaseCategoryResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/category/{parentSeq}", produces = "application/json")
	public ResponseEntity<List<BaseCategoryResponse>> selectCategoryByParentSeq(
			@ApiParam(value="부모시퀀스", required=false, defaultValue = "1") @PathVariable(value = "parentSeq", required = false) int parentSeq) {
		return BaseResponse.ok(baseCategoryServce.selectCategoryByParentSeq(parentSeq));
	}
	
	@ApiOperation(value = "카테고리 키워드 목록조회 API", notes = "카테고리 키워드 목록조회", response = BaseCategorySearchResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/category", produces = "application/json")
	public ResponseEntity<List<BaseCategorySearchResponse>> selectCategoryByKeyword(
			@ApiParam(value="검색어", required=true, defaultValue = "기계") @RequestParam(value = "parentSeq", required = true) String keyword) {
		return BaseResponse.ok(baseCategoryServce.selectCategoryByKeyword(keyword));
	}

	@ApiOperation(value = "카테고리명 전체 조회 API", notes = "카테고리명 전체조회", response = BaseCategorySearchResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/categoryFullName", produces = "application/json")
	public ResponseEntity<List<BaseCategorySearchResponse>> selectFullCategory(
		@ApiParam(value="cate_seq_1dp", required=true, defaultValue = "1") @RequestParam(value = "cate_seq_1dp", required = true) int cate_seq_1dp,
		@ApiParam(value="cate_seq_2dp", required=true, defaultValue = "10004") @RequestParam(value = "cate_seq_2dp", required = true) int cate_seq_2dp,
		@ApiParam(value="cate_seq_3dp", required=true, defaultValue = "10004004") @RequestParam(value = "cate_seq_3dp", required = true) int cate_seq_3dp) {
		return BaseResponse.ok(baseCategoryServce.selectFullCategory(cate_seq_1dp, cate_seq_2dp, cate_seq_3dp));
	}
	
}
