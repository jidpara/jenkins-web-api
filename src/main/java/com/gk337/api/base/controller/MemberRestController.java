package com.gk337.api.base.controller;

import com.gk337.api.base.entity.*;
import com.gk337.api.base.service.DeviceInfoService;
import com.gk337.api.base.service.MemberService;
import com.gk337.api.shop.entity.BizNumCheckResponse;
import com.gk337.api.shop.entity.ShopInfoRequest;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.exception.JGWServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <PRE>
 * 시스템명    : 중고왕 서비스
 * 업무명       : 공통모듈
 * 프로그램명 : MemberController.java
 *
 *  - 공통 사용자정보 RestController 클래스 -
 *
 * </PRE>
 *
 * @since 2020. 02.16
 * @author sejoon
 * @version 1.0
 * @see RestController
 *
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */
@Slf4j
@Api(tags= {"[공통정보] - 사용자 정보 API"}, protocols="http", produces="application/json")
@CrossOrigin("*")
@RequestMapping("/api/base")
@RestController
public class MemberRestController {

    @Autowired
    private MemberService memberService;


    @ApiOperation(value = "사용자 등록 API", notes = "사용자 정보를 기록한다. ")
    @PutMapping("/member")
    public ResponseEntity<MemberResponse> member(@RequestBody @Valid MemberRequest request) throws JGWServiceException {
        return BaseResponse.ok(memberService.updateMember(request));
    }

    @ApiOperation(value = "중복아이디 체크 API", notes = "중복아이디 체크")
    @GetMapping("/member/idCheck")
    public String id_check(String userId) {
        String str = memberService.idCheck(userId);
        return str;
    }

    @ApiOperation(value = "중고전문가 등록 API", notes = "중고전문가 정보를 기록한다. ")
    @PutMapping("/member/dealer")
    public ResponseEntity<MemberResponse> memberDealer(@RequestBody @Valid MemberRequestShopInfo request) throws JGWServiceException {
        return BaseResponse.ok(memberService.insertDealer(request));
    }

}
