package com.gk337.api.base.controller;

import com.gk337.api.base.entity.*;
import com.gk337.api.base.service.ConnectService;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.exception.JGWServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <PRE>
 * 시스템명    : 중고왕 서비스
 * 업무명       : 공통모듈
 * 프로그램명 : BasePolicyController.java
 *
 *  - 공통 정책정보 RestController 클래스 -
 *
 * </PRE>
 *
 * @since 2020. 07.26
 * @author sejoon
 * @version 1.0
 * @see RestController
 *
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */
@Slf4j
@Api(tags= {"[001. 공통정보] - 정책정보 API"}, protocols="http", produces="application/json")
@CrossOrigin("*")
@RequestMapping("/api/base")
@RestController
public class BasePolicyController {

    @Autowired
    private ConnectService connectService;


    @ApiOperation(value = "그룹코드[단일] 공통코드 조회 API", notes = "공통코드 그룹코드로 조회")
	@RequestMapping(method = RequestMethod.GET, path="/policy/{typeCode}", produces = "application/json")
	public ResponseEntity<BasePolicyResponse> findByGroupCode(
			@ApiParam(value="그룹코드", required=true, defaultValue = "UPDATE") @PathVariable(value = "typeCode", required = true) String typeCode) {
        BasePolicyResponse response = new BasePolicyResponse();
        BaseUpdatePolicy updatePolicy = new BaseUpdatePolicy();
        response.setBaseUpdatePolicy(updatePolicy);
		return BaseResponse.ok(response);
	}

}
