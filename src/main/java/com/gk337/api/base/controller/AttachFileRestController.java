package com.gk337.api.base.controller;

import com.gk337.api.base.entity.BaseUploadS3Response;
import com.gk337.api.base.service.AttachFileService;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.exception.JGWServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <PRE>
 * 시스템명    : 중고왕 서비스
 * 업무명       : 공통모듈
 * 프로그램명 : FileController.java
 *
 *  - 공통 파일업로드 RestController 클래스 -
 *
 * </PRE>
 *
 * @since 2020. 02.16
 * @author sejoon
 * @version 1.0
 * @see RestController
 *
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */
@Slf4j
@Api(tags= {"[공통정보] - 첨부파일 API"}, protocols="http", produces="application/json")
@CrossOrigin("*")
@RequestMapping("/api/base")
@RestController
public class AttachFileRestController {

    @Autowired
    private AttachFileService attachFileService;

     
    @ApiOperation(value = "첨부파일[단일] 등록 API", notes = "AWS S3에 저장 후 결과값을 jw_attach_file 에 기록")
    @PostMapping("/uploadS3/{subPath}")
    public ResponseEntity<BaseUploadS3Response> uploadS3(
            @ApiParam(value="버킷하위경로", required=false, defaultValue = "product") @PathVariable("subPath") String subPath,
            @ApiParam(value="파일 formData", required=true) @RequestParam("file") MultipartFile file) throws JGWServiceException {
        return BaseResponse.ok(attachFileService.uploadFile(file, subPath));
    }

    @ApiOperation(value = "첨부파일[복수] 등록 API", notes = "AWS S3에 저장 후 결과값을 jw_attach_file 에 기록")
    @PostMapping("/uploadS3Multi/{subPath}")
    public ResponseEntity<List<BaseUploadS3Response>> uploadS3Multi(
            @ApiParam(value="버킷하위경로", required=false, defaultValue = "product") @PathVariable("subPath") String subPath,
            @ApiParam(value="파일 formData", required=true) @RequestParam("files") MultipartFile[] files) throws JGWServiceException {
        return BaseResponse.ok(Arrays.asList(files)
                .stream()
                .map(file -> attachFileService.uploadFile(file, subPath))
                .collect(Collectors.toList())
        );
    }
//
//    @GetMapping("/downloadFile/{fileName:.+}")
//    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
//        // Load file as Resource
//        Resource resource = fileStorageService.loadFileAsResource(fileName);
//
//        // Try to determine file's content type
//        String contentType = null;
//        try {
//            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
//        } catch (IOException ex) {
//            logger.info("Could not determine file type.");
//        }
//
//        // Fallback to the default content type if type could not be determined
//        if(contentType == null) {
//            contentType = "application/octet-stream";
//        }
//
//        return ResponseEntity.ok()
//                .contentType(MediaType.parseMediaType(contentType))
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
//                .body(resource);
//    }

}
