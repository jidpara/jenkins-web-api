package com.gk337.api.base.controller;

import com.gk337.api.base.entity.ConnectRequest;
import com.gk337.api.base.entity.ConnectResponse;
import com.gk337.api.base.entity.DeviceInfoRequest;
import com.gk337.api.base.entity.DeviceInfoResponse;
import com.gk337.api.base.service.ConnectService;
import com.gk337.api.base.service.DeviceInfoService;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.exception.JGWServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <PRE>
 * 시스템명    : 중고왕 서비스
 * 업무명       : 공통모듈
 * 프로그램명 : BaseController.java
 *
 *  - 공통 기본정보 RestController 클래스 -
 *
 * </PRE>
 *
 * @since 2020. 03.08
 * @author sejoon
 * @version 1.0
 * @see RestController
 *
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */
@Slf4j
@Api(tags= {"[001. 공통정보] - 공통정보 API"}, protocols="http", produces="application/json")
@CrossOrigin("*")
@RequestMapping("/api/base")
@RestController
public class BaseRestController {

    @Autowired
    private ConnectService connectService;


    @ApiOperation(value = "BOX337 -> 중고왕 연결 API", notes = "중고왕 진입 시 필수 호출. ")
    @PostMapping("/connect")
    public ResponseEntity<ConnectResponse> connect(@RequestBody @Valid ConnectRequest request) throws JGWServiceException {
        return BaseResponse.ok(connectService.connect(request));
    }

}
