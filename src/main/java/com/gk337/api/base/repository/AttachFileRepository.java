package com.gk337.api.base.repository;

import com.gk337.api.base.entity.BaseUploadS3Entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AttachFileRepository extends JpaRepository<BaseUploadS3Entity, Integer>, JpaSpecificationExecutor<BaseUploadS3Entity> {

    BaseUploadS3Entity save(BaseUploadS3Entity entity);
}