package com.gk337.api.base.repository;

import com.gk337.api.base.entity.DeviceSettingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DeviceSettingRepository extends JpaRepository<DeviceSettingEntity, String>, JpaSpecificationExecutor<DeviceSettingEntity> {
    DeviceSettingEntity save(DeviceSettingEntity entity);

    DeviceSettingEntity findByMemSeqAndUuid(Integer memSeq, String uuid);
}