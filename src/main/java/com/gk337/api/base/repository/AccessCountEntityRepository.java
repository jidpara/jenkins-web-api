package com.gk337.api.base.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.gk337.api.base.entity.AccessCountEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface AccessCountEntityRepository extends JpaRepository<AccessCountEntity, String>, JpaSpecificationExecutor<AccessCountEntity> {

    AccessCountEntity findByMethodAndUri(String method, String uri);

    @Transactional
    @Modifying
    @Query("update AccessCountEntity e set e.count = e.count + 1  where e.method = :method and e.uri = :uri")
    void updateCount(@Param("method") String method, @Param("uri") String uri);
}