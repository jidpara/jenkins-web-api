package com.gk337.api.base.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.base.entity.BaseCategoryEntity;

@Repository
public interface BaseCategoryRepository extends PagingAndSortingRepository<BaseCategoryEntity, String>, JpaRepository<BaseCategoryEntity, String> {
	
	@SuppressWarnings("unchecked")
	BaseCategoryEntity save(BaseCategoryEntity entity);
	
	void deleteBySeq(int seq);
	
	List<BaseCategoryEntity> findByParentSeq(Integer parentSeq);

}