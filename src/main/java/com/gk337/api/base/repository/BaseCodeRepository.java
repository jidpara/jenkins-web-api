package com.gk337.api.base.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.base.entity.BaseCodeEntity;

@Repository
public interface BaseCodeRepository extends PagingAndSortingRepository<BaseCodeEntity, String>, JpaRepository<BaseCodeEntity, String> {

//	@Query("select BaseCodeResponse where q.groupCode = :groupCode")
//	List<BaseCodeEntity> findByGroupCode(String groupCode);
	
}