package com.gk337.api.base.repository;

import com.gk337.api.base.entity.DeviceInfoEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface DeviceInfoRepository extends JpaRepository<DeviceInfoEntity, String>, JpaSpecificationExecutor<DeviceInfoEntity> {

    DeviceInfoEntity save(DeviceInfoEntity entity);

    DeviceInfoEntity findByMemSeqAndUuid(Integer memSeq, String uuid);

    DeviceInfoEntity findTopByMemSeqAndLastLoginYn(Integer memSeq, String lastLoginYn);

    @Transactional
    @Modifying
    @Query("update DeviceInfoEntity d set d.lastLoginYn = :lastLoginYn where d.uuid = :uuid")
    void updateLastLoginYn(@Param("uuid") String uuid, @Param("lastLoginYn") String lastLoginYn);

    @Transactional
    @Modifying
    @Query("update DeviceInfoEntity d set d.lastLoginYn = :lastLoginYn where d.memSeq = :memSeq")
    void updateLastLoginYn(@Param("memSeq") Integer memSeq, @Param("lastLoginYn") String lastLoginYn);
}