package com.gk337.api.base.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.base.entity.BaseCategoryResponse;
import com.gk337.api.base.entity.BaseCategorySearchResponse;
import com.gk337.api.base.entity.BaseCodeResponse;

@Mapper
public interface BaseMapper {

	/**
	 * 카테고리 목록조회.
	 * @param parentSeq
	 * @return
	 */
	public List<BaseCategoryResponse> selectCategoryByParentSeq(HashMap<String, Integer> map);
	
	/**
	 * 키워드로 전체 Depth에서 조회.
	 * @param keyword
	 * @return
	 */
	public List<BaseCategorySearchResponse> selectCategoryByKeyword(HashMap<String, String> hashMap);
	
	/**
	 * 공통코드 목록조회.
	 * @param hashMap
	 * @return
	 */
	public List<BaseCodeResponse> selecCodeByGroupCode(HashMap<String, String> hashMap);


	/**
	 * 카테고리 전체이름
	 * @param 
	 * @return
	 */
	public List<BaseCategorySearchResponse> selectFullCategory(HashMap<String, Integer> hashMap);
}
