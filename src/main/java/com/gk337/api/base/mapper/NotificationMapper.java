package com.gk337.api.base.mapper;

import com.gk337.api.base.entity.DeviceInfoSetting;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Mapper
public interface NotificationMapper {

	/**
	 * 디바이스 및 설정정보 목록조회.
	 * @param Integer memSeq
	 * @return
	 */
	public List<DeviceInfoSetting> selectDeviceInfo(Integer memSeq);

	public List<DeviceInfoSetting> selectDeviceInfoList(Map<String,Object> param);
}
