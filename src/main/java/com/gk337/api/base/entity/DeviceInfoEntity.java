package com.gk337.api.base.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 기기정보
 */
@Data
@Table(name = "jw_device_info")
@Entity
@IdClass(DeviceInfoPK.class)
public class DeviceInfoEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 회원시퀀스
     */
    @Id
    @Column(insertable = false, name = "mem_seq", nullable = false)
    private Integer memSeq;

    /**
     * 기기고유ID
     */
    @Id
    @Column(name = "uuid", insertable = false, nullable = false)
    private String uuid;

    /**
     * 상태값 (Y:정상, N:비노출, D:삭제)
     */
    @Column(name = "cstatus", nullable = false)
    private String cstatus;

    /**
     * 플랫폼구분 (Android:안드로이드, IOS:아이폰)
     */
    @Column(name = "platform", nullable = false)
    private String platform;

    /**
     * FMC토큰
     */
    @Column(name = "device_token")
    private String deviceToken = "";

    /**
     * 모델명
     */
    @Column(name = "model")
    private String model;

    /**
     * 가상기기 여부 (Y:가상기기, N:실제기기)
     */
    @Column(name = "virtual_yn")
    private String virtualYn;

    /**
     * 최종로그인 기기여부 (Y값만 푸시 수신)
     */
    @Column(name = "last_login_yn")
    private String lastLoginYn;
}