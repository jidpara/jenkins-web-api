package com.gk337.api.base.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 공통코드
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "공통코드")
public class BaseCodeRequest implements Serializable {
    
	private static final long serialVersionUID = 1L;
    
    @ApiModelProperty(notes = "그룹코드")
    private String groupCode;

    @ApiModelProperty(notes = "코드")
    private String code;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "값")
    private String value;

}

