package com.gk337.api.base.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * base
 */

@Data
@ApiModel(description = "정책 정보 수신")
public class BasePolicyResponse implements Serializable {

    BaseUpdatePolicy baseUpdatePolicy;
}
