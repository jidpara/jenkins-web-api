package com.gk337.api.base.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "카테고리 Request Vo")
public class BaseCategoryRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "카테고리명", example="카테고리명")
    private String cateName;

    @ApiModelProperty(notes = "그룹시퀀스", example="0")
    private Integer groupSeq;

    @ApiModelProperty(notes = "부모시퀀스", example="0")
    private Integer parentSeq;

    @ApiModelProperty(notes = "레벨", example="0")
    private Integer cateLevel;

    @ApiModelProperty(notes = "순서", example="1")
    private Integer orderBy;

    @ApiModelProperty(notes = "키워드", example="카테고리 키워드")
    private String keyWords;

    
}