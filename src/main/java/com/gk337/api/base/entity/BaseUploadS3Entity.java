package com.gk337.api.base.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 공통 첨부파일 테이블
 */
@Data
@Table(name = "jw_attach_file")
@Entity
public class BaseUploadS3Entity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "seq", nullable = false)
    private Integer seq;

    /**
     * 파일 물리경로
     */
    @Column(name = "file_path", nullable = false)
    private String filePath;

    /**
     * 원본 파일명
     */
    @Column(name = "file_name", nullable = false)
    private String fileName;

    /**
     * 변경 파일명
     */
    @Column(name = "file_chg_name")
    private String fileChgName;

    /**
     * 확장자
     */
    @Column(name = "file_ext")
    private String fileExt;

    /**
     * 파일사이즈
     */
    @Column(name = "file_size")
    private Integer fileSize = 0;

    /**
     * 파일URL
     */
    @Column(name = "file_url")
    private String fileUrl;

    /**
     * 썸네일 URL (이미지일 경우)
     */
    @Column(name = "file_thumb_url")
    private String fileThumbUrl;

}