package com.gk337.api.base.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 기기설정 정보
 */
@Entity
@Table(name = "jw_device_setting")
@Data
@IdClass(DeviceInfoPK.class)
public class DeviceSettingEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 회원시퀀스
     */
    @Id
    @Column(insertable = false, name = "mem_seq", nullable = false)
    private Integer memSeq;

    /**
     * 기기고유ID
     */
    @Id
    @Column(name = "uuid", insertable = false, nullable = false)
    private String uuid;

    /**
     * 상태값 (Y:정상, N:비노출, D:삭제)
     */
    @Column(name = "cstatus", nullable = false)
    private String cstatus;

    /**
     * 구매요청 푸시 수신여부 (Y:수신, N:미수신)
     */
    @Column(name = "noti_buy_yn", nullable = false)
    private String notiBuyYn;

    /**
     * 판매요청 푸시 수신여부 (Y:수신, N:미수신)
     */
    @Column(name = "noti_sell_yn", nullable = false)
    private String notiSellYn;

    /**
     * 공지사항 푸시 수신여부 (Y:수신, N:미수신)
     */
    @Column(name = "noti_notice_yn", nullable = false)
    private String notiNoticeYn;

    /**
     * 채팅대화 푸시 수신여부 (Y:수신, N:미수신)
     */
    @Column(name = "noti_chat_yn", nullable = false)
    private String notiChatYn;
}