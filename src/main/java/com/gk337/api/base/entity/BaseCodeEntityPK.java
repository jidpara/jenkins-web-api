package com.gk337.api.base.entity;

import java.io.Serializable;

import com.gk337.common.core.domain.PKEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class BaseCodeEntityPK implements Serializable, PKEntity {

	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	private String groupCode;                   

	@EqualsAndHashCode.Include
	private String code;					  

}
