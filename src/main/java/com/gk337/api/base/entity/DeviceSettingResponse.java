package com.gk337.api.base.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * base
 */

@Data
@ApiModel(description = "디바이스 정보")
public class DeviceSettingResponse {

    @ApiModelProperty(notes = "회원시퀀스", example = "1")
    private Integer memSeq;

    @ApiModelProperty(notes = "기기고유아이디", example = "7ec0fea929d728cf")
    private String uuid;

    @JsonIgnore
    private String cstatus;

    @ApiModelProperty(notes = "구매요청 푸시 수신여부 (Y:수신, N:미수신)", example = "Y")
    private String notiBuyYn;

    @ApiModelProperty(notes = "판매요청 푸시 수신여부 (Y:수신, N:미수신)", example = "Y")
    private String notiSellYn;

    @ApiModelProperty(notes = "공지사항 푸시 수신여부 (Y:수신, N:미수신)", example = "Y")
    private String notiNoticeYn;

    @ApiModelProperty(notes = "채팅대화 푸시 수신여부 (Y:수신, N:미수신)", example = "Y")
    private String notiChatYn;

    @ApiModelProperty(notes = "내상점 공개 여부 (Y:공개, N:비공개)", example = "Y")
    private String openMyShopYn;

    @ApiModelProperty(notes = "내대화 공개 여부 (Y:공개, N:비공개)", example = "Y")
    private String openMyChatYn;
}
