package com.gk337.api.base.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * base
 */

@Data
@ApiModel(description = "오류정보 전송")
public class BaseErrorResponse implements Serializable{

}
