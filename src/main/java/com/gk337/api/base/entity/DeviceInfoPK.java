package com.gk337.api.base.entity;

import com.gk337.common.core.domain.PKEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class DeviceInfoPK implements Serializable, PKEntity {

	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	private Integer memSeq;

	@EqualsAndHashCode.Include
	private String uuid;
}
