package com.gk337.api.base.entity;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * base
 */

@Data
@ApiModel(description = "푸시 메세지 전송")
public class BaseNotificationChannelsResponse implements Serializable{

}
