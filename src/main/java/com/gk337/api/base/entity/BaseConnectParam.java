package com.gk337.api.base.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * base
 */

@Data
@ApiModel(description = "연결")
public class BaseConnectParam {

}
