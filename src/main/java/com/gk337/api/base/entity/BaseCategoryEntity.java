package com.gk337.api.base.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 카테고리
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_category")
@ApiModel(description = "카테고리 Entity")
public class BaseCategoryEntity extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 시퀀스
     */
    @Id
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

    /**
     * 상태값 (Y:정상, N:비노출, D:삭제)
     */
    @Column(name = "cstatus", nullable = false)
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus;

    /**
     * 카테고리명
     */
    @Column(name = "cate_name", nullable = false)
    @ApiModelProperty(notes = "카테고리명", example="카테고리")
    private String cateName;

    /**
     * 카테고리명
     */
    @Column(name = "icon_file", nullable = false)
    @ApiModelProperty(notes = "아이콘파일명", example="1-blue.png")
    private String iconFile;
    /**
     * 그룹시퀀스
     */
    @Column(name = "group_seq", nullable = false)
    @ApiModelProperty(notes = "그룹시퀀스", example="1")
    private Integer groupSeq;

    /**
     * 부모시퀀스
     */
    @Column(name = "parent_seq")
    @ApiModelProperty(notes = "부모시퀀스", example="1")
    private Integer parentSeq;

    /**
     * 레벨
     */
    @Column(name = "cate_level", nullable = false)
    @ApiModelProperty(notes = "레벨", example="1")
    private Integer cateLevel;

    /**
     * 순서
     */
    @Column(name = "order_by", nullable = false)
    @ApiModelProperty(notes = "순서", example="1")
    private Integer orderBy;

    /**
     * 키워드 
     */
    @Column(name = "key_words")
    @ApiModelProperty(notes = "키워드", example="키워드")
    private String keyWords;

    
}