package com.gk337.api.base.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "정책정보 Request Vo")
public class PolicyRequest {

    @ApiModelProperty(notes = "정책코드 (UPDATE:업데이트정책, PWD:비밀번호, ETC:기타)", example="UPDATE")
    private String typeCode;

    @ApiModelProperty(notes = "APP코드 (JGW001:중고왕안드로이드, JGW002:중고왕IOS)", example="JGW001")
    private String appCode;

}