package com.gk337.api.base.entity;

import java.io.Serializable;
import javax.persistence.*;

import com.gk337.common.model.BaseEntity;
import lombok.Data;

/**
 * access log based on request uri for counting
 */
@Entity
@Table(name = "jw_access_count")
@Data
@IdClass(AccessCountPK.class)
public class AccessCountEntity extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 호출URI
     */
    @Id
    @Column(insertable = true, name = "uri", nullable = false)
    private String uri;

    /**
     * 호출METHOD
     */
    @Id
    @Column(insertable = true, name = "method", nullable = false)
    private String method;

    /**
     * 호출횟수
     */
    @Column(name = "count", nullable = false)
    private Integer count = 0;

}