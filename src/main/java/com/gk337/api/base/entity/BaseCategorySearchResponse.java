package com.gk337.api.base.entity;

import java.io.Serializable;

import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "카테고리 검색 Response Vo")
public class BaseCategorySearchResponse extends BaseVo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @ApiModelProperty(value = "검색된 카테고리 풀네임", example="1")
    private String cateFullName;

    @ApiModelProperty(value = "1단계 카테고리 코드 ", example="1")
    private Integer cateSeq1dp;

    @ApiModelProperty(value = "1단계 카테고리 아이콘 파일", example="1-blue.png")
    private String cateSeq1dpIconFile;
    
    @ApiModelProperty(value = "1단계 카테고리명 ", example="기계류")
    private String cateSeq1dpName;
    
    @ApiModelProperty(value = "2단계 카테고리 코드 ", example="11")
    private Integer cateSeq2dp;
    
    @ApiModelProperty(value = "2단계 카테고리명 ", example="공작기계")
    private String cateSeq2dpName;
    
    @ApiModelProperty(value = "3단계 카테고리코드 ", example="1001015")
    private Integer cateSeq3dp;
    
    @ApiModelProperty(value = "3단계 카테고리명 ", example="관련부품")
    private String cateSeq3dpName;
    
    @ApiModelProperty(value = "키워드", example="기타 농기계 입니다.")
    private String keyWords;
    
}