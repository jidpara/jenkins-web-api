package com.gk337.api.base.entity;

import com.gk337.common.core.mvc.model.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "파일업로드(S3) Response Vo")
public class BaseUploadS3Response extends BaseVo {

    @ApiModelProperty(notes = "SEQ", example = "1")
    public Integer seq;

    @ApiModelProperty(notes = "파일명", example = "jgw.jpg")
    public String fileName;

    @ApiModelProperty(notes = "변경된파일명", example = "PRODUCT_PIC11534754984573.JPG")
    public String fileChgName;

    @ApiModelProperty(notes = "물리적 파일경로", example = "/product/image/20190109/")
    public String filePath;

    @ApiModelProperty(notes = "파일확장자", example = "jpg")
    public String ext;

    @ApiModelProperty(notes = "파일URL", example = "https://kin-phinf.pstatic.net/20170807_2/1502074294776YJVD1_JPEG/2016-08-29_17-31-33.jpg?type=w750")
    public String fileUrl;

}
