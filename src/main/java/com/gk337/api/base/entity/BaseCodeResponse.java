package com.gk337.api.base.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.gk337.common.core.mvc.model.BaseVo;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "공통코드")
public class BaseCodeResponse extends BaseVo implements Serializable {
    
	private static final long serialVersionUID = 1L;
    
    @ApiModelProperty(notes = "그룹코드", example="G_CD")
    private String groupCode;

    @ApiModelProperty(notes = "코드", example="C_CD")
    private String code;

    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus;

    @ApiModelProperty(notes = "값", example="코드값")
    private String value;

}

