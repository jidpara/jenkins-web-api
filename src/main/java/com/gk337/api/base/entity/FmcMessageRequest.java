package com.gk337.api.base.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.json.JSONObject;

/**
 * base
 */

@Data
@ApiModel(description = "firebase message request VO")
public class FmcMessageRequest {

    @ApiModelProperty(notes = "확인필요", example = "중고왕의 메세지")
    private String name = "중고왕의 메세지";

//    @ApiModelProperty(notes = "앱 열려있을 때에도 알림 표시여부", example = "true")
//    private String notificationForeground = "true";

    @ApiModelProperty(notes = "알림 제목", example = "판매 요청서 알림")
    private String notificationTitle;

    @ApiModelProperty(notes = "알림 내용", example = "아무개의 판매 요청서가 도착하였습니다. \n삼성냉장고 : 150,000원")
    private String notificationBody;

    @ApiModelProperty(notes = "(1:락스크린에도 보임, 0:민감한 정보 가리고 보임, -1: 락스크린에서는 안보임)", example = "1")
    private String notificationAndroidVisibility = "1";

    @ApiModelProperty(notes = "알림 색상", example = "#ff0000")
    private String notificationAndroidColor = "#ff0000";

    @ApiModelProperty(notes = "안드로이드 알림 아이콘", example = "")
    private String notificationAndroidIcon = "iconFileName";

    @ApiModelProperty(notes = "알림 사운드", example = "default")
    private String notificationAndroidSound = "default";

    @ApiModelProperty(notes = "진동 패턴", example = "500, 400, 500")
    private String notificationAndroidVibrate = "1500, 700, 500";

    @ApiModelProperty(notes = "알림 led", example = "ARGB")
    private String notificationAndroidLights = "ARGB";

    @ApiModelProperty(notes = "채널 ID", example = "box337")
    private String notificationAndroidChannelId = "box337";

    @ApiModelProperty(notes = "CLICK_ACTION", example = "FIREBASE_ACTION")
    private String clickAction = "FIREBASE_ACTION";

    @ApiModelProperty(notes = "앱 구분(jgw:중고왕, box337:Box337)", example = "jgw")
    private String app = "jgw";

    @ApiModelProperty(notes = "앱 데이터", example = "\"{\"linkUrl\":\"index.html?a=1\"}\"")
    private String appData = "{\"linkUrl\":\"index.html?a=1\"}";

    public JSONObject toJSONObject() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", this.name);
        //jsonObject.put("notification_foreground", this.notificationForeground);
        jsonObject.put("notification_title", this.notificationTitle);
        jsonObject.put("notification_body", this.notificationBody);
        jsonObject.put("notification_android_visibility", this.notificationAndroidVisibility);
        jsonObject.put("notification_android_color", this.notificationAndroidColor);
        jsonObject.put("notification_android_icon", this.notificationAndroidIcon);
        jsonObject.put("notification_android_sound", this.notificationAndroidSound);
        jsonObject.put("notification_android_vibrate", this.notificationAndroidVibrate);
        jsonObject.put("notification_android_lights", this.notificationAndroidLights);
        jsonObject.put("notification_android_channel_id", this.notificationAndroidChannelId);
        jsonObject.put("click_action", this.clickAction);
        jsonObject.put("app", this.app);
        jsonObject.put("app_data", this.appData);
        return jsonObject;
    }
}
