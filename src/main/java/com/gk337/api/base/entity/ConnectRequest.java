package com.gk337.api.base.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "BOX337 to 중고왕 Connect Request Vo")
public class ConnectRequest {
	
    @ApiModelProperty(notes = "BOX337 회원아이디 (가입 구분별로 상이함)", example="sejoon")
    private String userId;

    @ApiModelProperty(notes = "BOX337 회원 이름", example="중고왕유저1")
    private String memName;

    @ApiModelProperty(notes = "프로필 이미지URL", example="https://item.kakaocdn.net/do/aaacfe7e3c6d0564fded2313c5397779f43ad912ad8dd55b04db6a64cddaf76d")
    private String profileImage;

    @ApiModelProperty(notes = "우편번호", example="123-123")
    private String memZip;

    @ApiModelProperty(notes = "주소", example="경기도 이천시 부발읍 아미리")
    private String memAddr;

    @ApiModelProperty(notes = "주소상세", example="728-1 양연화로 1층")
    private String memAddrDetail;

    @ApiModelProperty(notes = "디바이스 정보", example="")
    private DeviceInfoRequest deviceInfo;
}