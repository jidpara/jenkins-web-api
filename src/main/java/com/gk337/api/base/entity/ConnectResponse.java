package com.gk337.api.base.entity;

import com.gk337.common.core.mvc.model.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "BOX337 to 중고왕 Connect Request Vo")
public class ConnectResponse extends BaseVo {
	
    @ApiModelProperty(notes = "BOX337 회원아이디 (가입 구분별로 상이함)", example="sejoon")
    private String userId;

    @ApiModelProperty(notes = "디바이스 정보", example="")
    private DeviceInfoRequest deviceInfo;

    @ApiModelProperty(notes = "중고왕 사용자 정보", example="")
    private MemberResponse memberInfo;
}