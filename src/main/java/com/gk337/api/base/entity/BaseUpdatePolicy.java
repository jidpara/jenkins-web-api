package com.gk337.api.base.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * base
 */

@Data
@ApiModel(description = "업데이트 정책 정보")
public class BaseUpdatePolicy implements Serializable {

    @ApiModelProperty(notes = "앱코드", example="JGW001")
    private String appCode = "JGW001";

    @ApiModelProperty(notes = "앱코드", example="중고왕")
    private String appName = "중고왕";

    @ApiModelProperty(notes = "최소버전", example="2.13.0")
    private String minVersion = "2.11.0";

    @ApiModelProperty(notes = "최신버전", example="2.13.0")
    private String currVersion = "2.15.0";

    @ApiModelProperty(notes = "메세지", example="업데이트가 필요합니다. \n새로운 버전을 다운로드 받아주세요.")
    private String message = "업데이트가 필요합니다. \n새로운 버전을 다운로드 받아주세요.";

    @ApiModelProperty(notes = "업데이트타입 (normal: 기본, mandatory:필수)", example="mandatory")
    private String updateType = "mandatory";

    @ApiModelProperty(notes = "다운로드URL", example="https://jungoking-bucket-assets.s3.ap-northeast-2.amazonaws.com/upload/APK/knox/app-release-knox-PRD.apk")
    private String downloadURL = "https://jungoking-bucket-assets.s3.ap-northeast-2.amazonaws.com/upload/APK/knox/app-release-knox-PRD.apk";


}
