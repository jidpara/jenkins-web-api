package com.gk337.api.base.entity;

import com.gk337.common.model.AuditEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 회원정보
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "jw_member")
@ApiModel(description = "회원관리 엔티티 ")
public class MemberEntity extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 시퀀스
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "seq", nullable = false)
    private Integer seq;

    /**
     * user id (box337 user id)
     */
    @Column(name = "user_id")
    private String userId = "";

    /**
     * 상태값 (Y:정상, N:비노출, D:삭제, L:탈퇴)
     */
    @Column(name = "cstatus")
    private String cstatus = "Y";

    /**
     * 가입구분 (B:BOX337, E:이메일, N:네이버, G:구글, F:페이스북, K:카카오)
     */
    @Column(name = "join_kind")
    private String joinKind = "B";

    /**
     * 회원구분 (G:일반회원, P:중고전문가)
     */
    @Column(name = "mem_kind")
    private String memKind = "G";

    /**
     * 중고전문가신청여부
     */
    @Column(name = "pro_request_yn")
    private String proRequestYn = "N";

    /**
     * 중고전문가신청일자
     */
    @Column(name = "pro_request_date")
    private LocalDateTime proRequestDate;

    /**
     * 중고전문가승인일자
     */
    @Column(name = "pro_ok_date")
    private LocalDateTime proOkDate;

    /**
     * 회원코드
     */
    @Column(name = "mem_code")
    private String memCode = "NULL";

    /**
     * 회원이메일
     */
    @Column(name = "mem_email")
    private String memEmail = "NULL";

    /**
     * 비밀번호 (6~16자리, 영문,숫자혼합)
     */
    @Column(name = "mem_pwd")
    private String memPwd = "NULL";

    @Column(name = "file_seq")
    private Integer fileSeq = 0;

    /**
     * 이미지 파일명
     */
    @Column(name = "filename1")
    private String filename1 = "NULL";

    /**
     * 이미지 배경색
     */
    @Column(name = "filename1_hex")
    private String filename1Hex = "NULL";

    /**
     * 닉네임 (2~10자리)
     */
    @Column(name = "mem_nickname")
    private String memNickname = "NULL";

    /**
     * 회원이름
     */
    @Column(name = "mem_name")
    private String memName = "NULL";

    /**
     * 휴대전화
     */
    @Column(name = "mem_hp")
    private String memHp = "NULL";

    /**
     * 통신사 (SKT, KT, LG)
     */
    @Column(name = "mem_hp_corp")
    private String memHpCorp = "NULL";

    /**
     * 휴대폰 본인인증 여부
     */
    @Column(name = "mem_auth_yn")
    private String memAuthYn = "NULL";

    /**
     * 휴대폰 본인인증 일자
     */
    @Column(name = "mem_auth_date")
    private LocalDateTime memAuthDate;

    /**
     * 생년월일
     */
    @Column(name = "mem_birth")
    private String memBirth = "NULL";

    /**
     * 성별 (F:여성, M:남성)
     */
    @Column(name = "mem_gender")
    private String memGender = "NULL";

    /**
     * 우편번호
     */
    @Column(name = "mem_zip")
    private String memZip = "NULL";

    /**
     * 주소
     */
    @Column(name = "mem_addr")
    private String memAddr = "NULL";

    /**
     * 상세주소
     */
    @Column(name = "mem_addr_detail")
    private String memAddrDetail = "NULL";

    /**
     * 소개글
     */
    @Column(name = "mem_desc")
    private String memDesc = "NULL";

    /**
     * 은행시퀀스
     */
    @Column(name = "bank_seq")
    private Integer bankSeq = 0;

    /**
     * 은행명
     */
    @Column(name = "bank_name")
    private String bankName = "NULL";

    /**
     * 계좌번호
     */
    @Column(name = "account_num")
    private String accountNum = "NULL";

    /**
     * SMS수신여부 (Y:수신, N:수신안함)
     */
    @Column(name = "sms_yn")
    private String smsYn = "NULL";

    /**
     * 이메일수신여부 (Y:수신, N:수신안함)
     */
    @Column(name = "email_yn")
    private String emailYn = "NULL";

    /**
     * 메모
     */
    @Column(name = "memo")
    private String memo = "NULL";

    /**
     * 내방공개여부
     */
    @Column(name = "room_open_yn")
    private String roomOpenYn = "NULL";

    /**
     * 채팅공여여부
     */
    @Column(name = "chat_open_yn")
    private String chatOpenYn = "NULL";

    /**
     * 안심번호공개여부
     */
    @Column(name = "safe_num_open_yn")
    private String safeNumOpenYn = "NULL";

    /**
     * 안심번호통화가능시간fr (미인증회원은 비노출)
     */
    @Column(name = "safe_time_fr")
    private String safeTimeFr = "NULL";

    /**
     * 안심번호통화가능시간to (미인증회원은 비노출)
     */
    @Column(name = "safe_time_to")
    private String safeTimeTo = "NULL";

    /**
     * 알림설정_구매요청
     */
    @Column(name = "noti_buy_yn")
    private String notiBuyYn = "NULL";

    /**
     * 알림설정_판매요청
     */
    @Column(name = "noti_sell_yn")
    private String notiSellYn = "NULL";

    /**
     * 알림설정_구매신청
     */
    @Column(name = "noti_buy_req_yn")
    private String notiBuyReqYn = "NULL";

    /**
     * 알림설정_공지사항
     */
    @Column(name = "noti_notice_yn")
    private String notiNoticeYn = "NULL";

    /**
     * 알림설정_1:1문의
     */
    @Column(name = "noti_qna_yn")
    private String notiQnaYn = "NULL";

    /**
     * 알림설정_채팅대화
     */
    @Column(name = "noti_chat_yn")
    private String notiChatYn = "NULL";

    /**
     * 탈퇴IP
     */
    @Column(name = "out_ip")
    private String outIp = "NULL";

    /**
     * 탈퇴일자
     */
    @Column(name = "out_date")
    private LocalDateTime outDate;

    /**
     * 탈퇴사유
     */
    @Column(name = "out_memo")
    private String outMemo = "NULL";

    /**
     * 삭제예정일자
     */
    @Column(name = "deletion_scheduled_date")
    private String deletionScheduledDate = "NULL";

    /**
     * 로그인IP
     */
    @Column(name = "login_ip")
    private String loginIp = "NULL";

    /**
     * 로그인일자
     */
    @Column(name = "login_date")
    private LocalDateTime loginDate;

    /**
     * 등록ID
     */
    @Column(name = "reg_id")
    private String regId = "NULL";

    /**
     * 등록IP
     */
    @Column(name = "reg_ip")
    private String regIp = "NULL";

    /**
     * 등록일자
     */
    @Column(name = "reg_date")
    private LocalDateTime regDate;

    /**
     * 최종수정ID
     */
    @Column(name = "chg_id")
    private String chgId = "NULL";

    /**
     * 최종수정IP
     */
    @Column(name = "chg_ip")
    private String chgIp = "NULL";

    /**
     * 최종수정일자
     */
    @Column(name = "chg_date")
    private LocalDateTime chgDate;

    @Column(name = "mem_phone_safety")
    private String memPhoneSafety = "NULL";

    @Column(name = "noti_rent_yn")
    private String notiRentYn = "NULL";

    @Column(name = "check_n")
    private String checkN = "NULL";

    @Column(name = "check_g")
    private String checkG = "NULL";

    @Column(name = "check_f")
    private String checkF = "NULL";

    @Column(name = "check_k")
    private String checkK = "NULL";

    @Column(name = "check_e")
    private String checkE = "NULL";


}