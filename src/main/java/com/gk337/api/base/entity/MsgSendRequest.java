package com.gk337.api.base.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "다량 알림 전송 Request Vo")
public class MsgSendRequest {

    @ApiModelProperty(notes = "전송할 회원 시퀀스 리스트", example="[1,2,3]")
    List<Integer> memSeqList;

    @ApiModelProperty(notes = "알림 제목", example="제목")
    String title;
    @ApiModelProperty(notes = "알림 내용", example="내용 입력")
    String content;
}
