package com.gk337.api.base.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 공통코드
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(BaseCodeEntityPK.class)
@Table(name = "jw_common_code")
@ApiModel(description = "공통코드")
public class BaseCodeEntity extends AuditEntity implements Serializable {
    
	private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "group_code", nullable = false)
    @ApiModelProperty(notes = "그룹코드")
    private String groupCode;

    @Id
    @Column(name = "code", nullable = false)
    @ApiModelProperty(notes = "코드")
    private String code;

    @Column(name = "cstatus", nullable = false)
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus;

    @Column(name = "value", nullable = false)
    @ApiModelProperty(notes = " 값")
    private String value;
    
}

