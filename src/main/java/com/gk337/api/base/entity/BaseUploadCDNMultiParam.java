package com.gk337.api.base.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * base
 */

@Data
@ApiModel(description = "복수 파일업로드")
public class BaseUploadCDNMultiParam {

}
