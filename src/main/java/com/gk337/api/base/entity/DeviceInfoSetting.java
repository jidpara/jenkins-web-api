package com.gk337.api.base.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * base
 */

@Data
@ApiModel(description = "디바이스 + 설정 정보")
public class DeviceInfoSetting {

    @ApiModelProperty(notes = "회원시퀀스", example = "1")
    private Integer memSeq;

    @ApiModelProperty(notes = "기기고유아이디", example = "7ec0fea929d728cf")
    private String uuid;

    @JsonIgnore
    private String cstatus;

    @ApiModelProperty(notes = "플랫폼구분", example = "Android")
    private String platform;

    @ApiModelProperty(notes = "디바이스토큰", example = "cUPgNNJTvNc:APA91bFY4URWjb593tQZqBpOGp2jSpUf68V6Jx_s79zDQVnmCoKHefhtwnZZfK2GcpEC9qwJy0ieToZ1vtbSlGS9NQHMjk3sC0dueixH1QQ1Wf9NMv_hZz7Klix3_cOnUNZNk4j2LydF")
    private String deviceToken;

    @ApiModelProperty(notes = "모델명", example = "SM-N920S")
    private String model;

    @ApiModelProperty(notes = "가상기기 여부 (Y:가상기기, N:실제기기)", example = "Y")
    private String virtualYn;

    @ApiModelProperty(notes = "구매요청 푸시 수신여부 (Y:수신, N:미수신)", example = "Y")
    private String notiBuyYn;

    @ApiModelProperty(notes = "판매요청 푸시 수신여부 (Y:수신, N:미수신)", example = "Y")
    private String notiSellYn;

    @ApiModelProperty(notes = "공지사항 푸시 수신여부 (Y:수신, N:미수신)", example = "Y")
    private String notiNoticeYn;

    @ApiModelProperty(notes = "채팅대화 푸시 수신여부 (Y:수신, N:미수신)", example = "Y")
    private String notiChatYn;

    @ApiModelProperty(notes = "내상점 공개 여부 (Y:공개, N:비공개)", example = "Y")
    private String openMyShopYn;

    @ApiModelProperty(notes = "내대화 공개 여부 (Y:공개, N:비공개)", example = "Y")
    private String openMyChatYn;

}
