package com.gk337.api.base.entity;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * base
 */

@Data
@ApiModel(description = "단일 파일업로드")
public class BaseUploadCDNParam {

}
