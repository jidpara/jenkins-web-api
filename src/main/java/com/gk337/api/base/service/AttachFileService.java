package com.gk337.api.base.service;

import com.gk337.api.base.entity.BaseUploadS3Entity;
import com.gk337.api.base.entity.BaseUploadS3Response;
import com.gk337.api.base.repository.AttachFileRepository;
import com.gk337.common.components.AwsS3Service;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.ImageUtils;
import com.google.common.io.Files;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Service
public class AttachFileService {

	@Autowired
	AwsS3Service awsS3Service;

	@Autowired
	AttachFileRepository attachFileRepository;

	@Value("${aws.assets-path}")
	private String uploadAssetPath;

	// TODO : 업로드 image 분리 및 thumbnail 생성 optional 추가. 현재 Lambda 적용으로 인하여 파일업로드는 이미지용으로 전락함
	public BaseUploadS3Response uploadFile(MultipartFile multipartFile, String subPath) {

		Boolean imageRotated = true;
		UUID uuid = UUID.randomUUID();
		SimpleDateFormat date = new SimpleDateFormat("yyyyMMdd");

		String fileChgName = uuid.toString() + "." + Files.getFileExtension(multipartFile.getOriginalFilename());
		String assetPath = uploadAssetPath + "/" + subPath + "/" + date.format(new Date());
		String fileUrl = "";

		// 이미지 EXIF 따른 rotation 수행
		// TODO : AWS Lambda 수행으로 전환. IOS에서 동작 확인
		File file = ImageUtils.convertFile(multipartFile);
		BufferedImage image = null;
		try {
			image = ImageUtils.checkImage(file, ImageUtils.getOrientation(file));
			ImageIO.write(image, Files.getFileExtension(multipartFile.getOriginalFilename()), file);
		}
		catch (Exception e) {
			// 이미지 파일 아닌 경우
			log.error("이미지 Rotation 변환 실패 -> 원본 이미지 업로드");
			imageRotated = false;
		}

		try {
			// 이미지 변환 실패 시
			if(imageRotated) {
				fileUrl = awsS3Service.uploadObject(file, assetPath, fileChgName);
			}else{
				fileUrl = awsS3Service.uploadObject(multipartFile, assetPath, fileChgName);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.UPLOAD_S3.0001", "등록");
		} finally {
			if(!file.delete()) { file.deleteOnExit(); }
        }

		BaseUploadS3Response response = new BaseUploadS3Response();

		BaseUploadS3Entity entity = new BaseUploadS3Entity();
		entity.setFilePath(assetPath);
		entity.setFileName(multipartFile.getOriginalFilename());
		entity.setFileChgName(fileChgName);
		entity.setFileExt(Files.getFileExtension(multipartFile.getOriginalFilename()));
		entity.setFileSize(new Long(file.length()).intValue());
		entity.setFileUrl(fileUrl);
		entity.setFileThumbUrl(fileUrl.replace("origin", "resize"));

		entity = attachFileRepository.save(entity);
		BeanUtils.copyProperties(entity, response);
		response.setExt(entity.getFileExt());

		return response;
	}
}
