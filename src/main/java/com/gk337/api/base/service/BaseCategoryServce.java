package com.gk337.api.base.service;

import java.util.HashMap;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gk337.api.base.entity.BaseCategoryEntity;
import com.gk337.api.base.entity.BaseCategoryRequest;
import com.gk337.api.base.entity.BaseCategoryResponse;
import com.gk337.api.base.entity.BaseCategorySearchResponse;
import com.gk337.api.base.mapper.BaseMapper;
import com.gk337.api.base.repository.BaseCategoryRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BaseCategoryServce {

	@Autowired
	private BaseCategoryRepository baseCategoryRepository;
	
	@Autowired
	@SuppressWarnings("unused")
	@Qualifier(value="baseMapper")
	private BaseMapper baseMapper;
	
	/**
	 * 카테고리 저장 또는 수정.
	 * @param request
	 * @return
	 */
	@Transactional
	public BaseCategoryResponse saveCategory(BaseCategoryRequest request) {
		log.debug("========BaseCategoryServce.saveCategory========");
		BaseCategoryResponse response = new BaseCategoryResponse();
		try {
			BaseCategoryEntity entity = new BaseCategoryEntity();
			BeanUtils.copyProperties(request, entity);
			entity = baseCategoryRepository.save(entity);
			BeanUtils.copyProperties(entity, response);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.BASE.0002", "저장 또는 수정");
		}
		return response;
	}
	
	/**
	 * 카테고리 삭제.
	 * @param seq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteBySeq(int seq) {
		log.debug("========BaseCategoryServce.deleteBySeq========");
		ErrorVo result = new ErrorVo();
		try {
			baseCategoryRepository.deleteBySeq(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.BASE.0002", "삭제");
		}
		return result;
	}
	
	/**
	 * 카테고리 그룹목록 조회.
	 * @param faqKind
	 * @param title
	 * @return
	 */
	public ListVo<BaseCategoryResponse> selectCategoryByParentSeq(int parentSeq) {
		log.debug("========BaseCategoryServce.selectCategoryByParentSeq========");
		log.debug("parentSeq:::::: "+parentSeq);
		ListVo<BaseCategoryResponse> listVo = null;
		try {
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			map.put("parentSeq", parentSeq);
			listVo = new ListVo<BaseCategoryResponse>(baseMapper.selectCategoryByParentSeq(map));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.BASE.0002", "조건조회");
		}
		return listVo;
	}
	
	/**
	 * 카테고리 키워드 검색목록 조회.
	 * @param parentSeq
	 * @param cateLevel
	 * @return
	 */
	public ListVo<BaseCategorySearchResponse> selectCategoryByKeyword(String keyword) {
		log.debug("========BaseCategoryServce.selectCategoryByKeyword========");
		ListVo<BaseCategorySearchResponse> listVo = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("keyword", keyword);
			listVo = new ListVo<BaseCategorySearchResponse>(baseMapper.selectCategoryByKeyword(map));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.BASE.0002", "조건조회");
		}
		return listVo;
	}


	/**
	 * 카테고리 이름.
	 * @param cate_seq_1dp, cate_seq_2dp, cate_seq_3dp
	 * @param cateLevel
	 * @return
	 */
	public ListVo<BaseCategorySearchResponse> selectFullCategory(int cate_seq_1dp, int cate_seq_2dp, int cate_seq_3dp) {
		log.debug("========BaseCategoryServce.selectFullCategory========");
		ListVo<BaseCategorySearchResponse> listVo = null;
		try {
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			map.put("cate_seq_1dp", cate_seq_1dp);
			map.put("cate_seq_2dp", cate_seq_2dp);
			map.put("cate_seq_3dp", cate_seq_3dp);
			listVo = new ListVo<BaseCategorySearchResponse>(baseMapper.selectFullCategory(map));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.BASE.0003", "전체 카테고리");
		}
		return listVo;
	}
	
	
}
