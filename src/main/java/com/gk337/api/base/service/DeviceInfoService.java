package com.gk337.api.base.service;

import com.gk337.api.base.entity.*;
import com.gk337.api.base.repository.DeviceInfoRepository;
import com.gk337.api.base.repository.DeviceSettingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class DeviceInfoService {

	@Autowired
	DeviceInfoRepository deviceInfoRepository;

	@Autowired
	DeviceSettingRepository deviceSettingRepository;

	public DeviceInfoResponse updateDeviceInfo(DeviceInfoRequest request) {

		UUID uuid = UUID.randomUUID();

		DeviceInfoEntity entity = new DeviceInfoEntity();
		DeviceInfoResponse response = new DeviceInfoResponse();

		// 해당 uuid의 최종로그인여부 전체 N 으로 설정
		deviceInfoRepository.updateLastLoginYn(request.getUuid(), "N");
		// 해당 user 의 최종로그인여부 N으로 설정
		deviceInfoRepository.updateLastLoginYn(request.getMemSeq(), "N");

		DeviceInfoEntity exists = deviceInfoRepository.findByMemSeqAndUuid(request.getMemSeq(), request.getUuid());
		if(exists != null) {
			// 기 등록 디바이스토큰이 있는 경우에 새로운 데이터의 디바이스 토큰 null 은 업데이트 하지 않음
			if(exists.getDeviceToken() != null && !"".equals(exists.getDeviceToken())
					&& ( request.getDeviceToken() == null || "".equals(request.getDeviceToken())) ) {
				exists.setLastLoginYn("Y");
				BeanUtils.copyProperties(exists, response);
				entity = deviceInfoRepository.save(exists);
				return response;
			}
		}
		BeanUtils.copyProperties(request, entity);
		entity.setCstatus("Y");
		entity.setLastLoginYn("Y");
		entity = deviceInfoRepository.save(entity);
		BeanUtils.copyProperties(entity, response);

		return response;
	}

	public DeviceSettingResponse updateDeviceSetting(DeviceSettingRequest request) {

		DeviceSettingEntity entity = new DeviceSettingEntity();
		DeviceSettingResponse response = new DeviceSettingResponse();

		DeviceSettingEntity exists = deviceSettingRepository.findByMemSeqAndUuid(request.getMemSeq(), request.getUuid());

		BeanUtils.copyProperties(request, entity);
		entity.setCstatus("Y");

		if(exists == null) {
			entity = deviceSettingRepository.save(entity);
		}

		BeanUtils.copyProperties(entity, response);
		return response;
	}
}
