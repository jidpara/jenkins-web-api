package com.gk337.api.base.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

// TODO : 삭제
@ApiIgnore
@Service
public class AndroidPushNotificationsService {
    private static final String FIREBASE_SERVER_KEY="AAAA1neNhhU:APA91bEJaFHQ3eQN0uEUce-lur3eNur17LKICF3oxkpCrRQF6XgTpb-booFjqBUSirZMhLUz9KhYcU3Z_asZvZchBcsIZ5zLCZKhmX8Aw36TI9N1KqSWomWI92IgRumR5gGwbFOjQm0M";
    private static final String FIREBASE_API_URL="https://fcm.googleapis.com/fcm/send";

    @Async
    public CompletableFuture<String> send(HttpEntity<String> entity) {
 //wqerewrwer
        RestTemplate restTemplate = new RestTemplate();

        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();

        interceptors.add(new HeaderRequestInterceptor("Authorization",  "key=" + FIREBASE_SERVER_KEY));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json; UTF-8 "));
        restTemplate.setInterceptors(interceptors);

        String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);

        return CompletableFuture.completedFuture(firebaseResponse);
    }
}