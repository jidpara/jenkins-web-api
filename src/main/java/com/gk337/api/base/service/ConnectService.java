package com.gk337.api.base.service;

import com.gk337.api.base.entity.*;
import com.gk337.api.base.repository.MemberRepository;
import com.gk337.common.exception.JGWServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ConnectService {

	@Autowired
	MemberService memberService;

	@Autowired
	DeviceInfoService deviceInfoService;

	@Autowired
	MemberRepository memberRepository;

	/**
	 *
	 * @param request
	 * @return
	 */
	public ConnectResponse connect(ConnectRequest request) {

		if(request == null){
			throw new JGWServiceException("ERROR.CONNECT.0001", "BODY");
		}

		if(request.getUserId() == null || "".equals(request.getUserId())){
			throw new JGWServiceException("ERROR.CONNECT.0001", "loginid");
		}

		// 초기 회원 생성
		MemberResponse member = memberService.createMemberDefault(request);

		DeviceInfoRequest deviceInfoRequest = request.getDeviceInfo();
		deviceInfoRequest.setMemSeq(member.getSeq());

		// 디바이스 정보 업데이트
		deviceInfoService.updateDeviceInfo(request.getDeviceInfo());

		// 디바이스 설정 정보 업데이트(초기값)
		DeviceSettingRequest deviceSettingRequest = new DeviceSettingRequest();
		deviceSettingRequest.setMemSeq(member.getSeq());
		deviceSettingRequest.setUuid(deviceInfoRequest.getUuid());
		deviceInfoService.updateDeviceSetting(deviceSettingRequest);

		ConnectResponse response = new ConnectResponse();

		BeanUtils.copyProperties(request, response);

		response.setMemberInfo(member);
		return response;
	}
}
