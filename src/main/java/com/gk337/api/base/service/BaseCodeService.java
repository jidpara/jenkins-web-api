package com.gk337.api.base.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gk337.api.base.entity.BaseCategoryEntity;
import com.gk337.api.base.entity.BaseCodeEntity;
import com.gk337.api.base.entity.BaseCodeResponse;
import com.gk337.api.base.mapper.BaseMapper;
import com.gk337.api.base.repository.BaseCategoryRepository;
import com.gk337.api.base.repository.BaseCodeRepository;
import com.gk337.api.info.entity.FaqEntity;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BaseCodeService {

	@Autowired
	@SuppressWarnings("unused")
	private BaseCodeRepository baseCodeRepository;
	
	@Autowired
	private BaseMapper baseMapper;
	
	/**
	 * 공통코드 그룹코드 목록조회.
	 * @param groupCode
	 * @return
	 */
	public ListVo<BaseCodeResponse> selecCodeByGroupCode(String groupCode) {
		log.debug("========BaseCodeService.selecCodeByGroupCode========");
		ListVo<BaseCodeResponse> listVo = null;
		try {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("groupCode", groupCode);
			listVo = new ListVo<BaseCodeResponse>(baseMapper.selecCodeByGroupCode(hashMap));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.BASE.0001", "그룹코드 조회");
		}
		return listVo;
	}
	
	/**
	 * 공통코드 그룹필터 목록조회.
	 * @param groupCode
	 * @return
	 */
	
	
	
//	public ListVo<BaseCodeResponse> findByGroupCode(String groupCode) {
//		ListVo<BaseCodeResponse> listVo = null;
//		try {
//			listVo = new ListVo<BaseCodeResponse>(baseCodeRepository.findByGroupCode(groupCode));
//		} catch (BizException be) {
//			throw new JGWServiceException(be.getMessage());
//		} catch(Exception e) {
//			e.printStackTrace();
//			throw new JGWServiceException("ERROR.BASE.0001", "그룹코드 조회");
//		}
//		return listVo;
//	}
	
}
