package com.gk337.api.base.service;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

// TODO : 삭제
@ApiIgnore
public class AndroidPushPeriodicNotifications {

    public static String PeriodicNotificationJson() throws JSONException {
        LocalDate localDate = LocalDate.now();

        String sampleData[] = {"cHOF0H030kA:APA91bHsiUiI2taXf4O4ZQLTyy7F12jFrUre0no_a2TGAkjloSncDRVlfZle3MvNwRPiBRmXfZHG_dJCJUa02WgnmRj8jDUpQDNDAndKJhw4kZ9hiFSyzolR8LvSPkgl0GayL9Jur53_"};

        JSONObject body = new JSONObject();

        List<String> tokenlist = new ArrayList<String>();

        for(int i=0; i<sampleData.length; i++){
            tokenlist.add(sampleData[i]);
        }

        JSONArray array = new JSONArray();

        for(int i=0; i<tokenlist.size(); i++) {
            array.put(tokenlist.get(i));
        }

        body.put("registration_ids", array);

        JSONObject notification = new JSONObject();
        notification.put("title","hello!");
        notification.put("body","Today is "+localDate.getDayOfWeek().name()+"!");

        body.put("notification", notification);

        System.out.println(body.toString());

        return body.toString();
    }
}