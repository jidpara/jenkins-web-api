package com.gk337.api.base.service;

import com.gk337.api.base.entity.DeviceInfoSetting;
import com.gk337.api.base.entity.FmcMessageRequest;
import com.gk337.api.base.mapper.NotificationMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;


@Slf4j
@Service
public class NotificationService {

	private static final String FIREBASE_SERVER_KEY="AAAA1neNhhU:APA91bEJaFHQ3eQN0uEUce-lur3eNur17LKICF3oxkpCrRQF6XgTpb-booFjqBUSirZMhLUz9KhYcU3Z_asZvZchBcsIZ5zLCZKhmX8Aw36TI9N1KqSWomWI92IgRumR5gGwbFOjQm0M";
	private static final String FIREBASE_API_URL="https://fcm.googleapis.com/fcm/send";

	@Autowired
	private NotificationMapper notificationMapper;

	/**
	 * 대상 회원의 기기정보 및 설정정보를 조회한다.
	 * @param Integer memSeq - 대상 회원 시퀀스
	 * @return
	 */
	public List<DeviceInfoSetting> selectDeviceInfo(Integer memSeq) {
		//List<DeviceInfoSetting> list = notificationMapper.selectDeviceInfo(memSeq);
		return notificationMapper.selectDeviceInfo(memSeq);
	}

	@Async
	public CompletableFuture<String> send(Integer memSeq, FmcMessageRequest fmcRequest ) throws UnsupportedEncodingException {

		List<DeviceInfoSetting> notificationList = selectDeviceInfo(memSeq);
		System.out.println(notificationList.toString());

		// TODO : 대상없음 Exception CompletableFuture 추가.  DB기록 추가
		if(notificationList == null || notificationList.size() < 1) {
			return CompletableFuture.completedFuture("푸시 메세지 수신 대상 없음");
		}
		List<String> tokenList = new ArrayList<>();
		for(DeviceInfoSetting vo : notificationList) {
			tokenList.add(vo.getDeviceToken());
		}

		LocalDate localDate = LocalDate.now();

		JSONObject body = new JSONObject();

		JSONArray tokenJsonArray = new JSONArray();
		for(String token : tokenList) {
			tokenJsonArray.put(token);
		}

		JSONObject android = new JSONObject();
		JSONObject notification = new JSONObject();
		notification.put("channel_id", "box337");
		android.put("notification", notification);
		body.put("registration_ids", tokenJsonArray);
		body.put("android", android);

//		JSONObject notification = new JSONObject();
//		notification.put("name", "sample");
//		notification.put("notification_foreground", "true");
//		notification.put("notification_title", fmcRequest.getTitle());
//		notification.put("notification_body", fmcRequest.getBody());
//		notification.put("notification_android_visibility", "1");
//		notification.put("notification_android_color", "#ff0000");
//		notification.put("notification_android_icon", "iconFileName");
//		notification.put("notification_android_sound", "default");
//		notification.put("notification_android_vibrate", "500, 200. 500");
//		notification.put("notification_android_lights", "500, 200. 500");
//		notification.put("app", "jgw");
//		notification.put("app_data", "test haha");

		body.put("data", fmcRequest.toJSONObject());

		System.out.println(body.toString());

		//return body.toString();
		String bodyString = body.toString();
		HttpEntity<String> entity = new HttpEntity<>(new String(bodyString.getBytes(StandardCharsets.UTF_8)));

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();

		interceptors.add(new HeaderRequestInterceptor("Authorization",  "key=" + FIREBASE_SERVER_KEY));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json; UTF-8 "));
		restTemplate.setInterceptors(interceptors);

		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);

		return CompletableFuture.completedFuture(firebaseResponse);
	}

	public CompletableFuture<String> sendSync(Integer memSeq, FmcMessageRequest fmcRequest ) throws UnsupportedEncodingException {

		List<DeviceInfoSetting> notificationList = selectDeviceInfo(memSeq);
		System.out.println(notificationList.toString());

		// TODO : 대상없음 Exception CompletableFuture 추가.  DB기록 추가
		if(notificationList == null || notificationList.size() < 1) {
			return CompletableFuture.completedFuture("푸시 메세지 수신 대상 없음");
		}
		List<String> tokenList = new ArrayList<>();
		for(DeviceInfoSetting vo : notificationList) {
			tokenList.add(vo.getDeviceToken());
		}

		LocalDate localDate = LocalDate.now();

		JSONObject body = new JSONObject();

		JSONArray tokenJsonArray = new JSONArray();
		for(String token : tokenList) {
			tokenJsonArray.put(token);
		}

		JSONObject android = new JSONObject();
		JSONObject notification = new JSONObject();
		notification.put("channel_id", "box337");
		android.put("notification", notification);
		body.put("registration_ids", tokenJsonArray);
		body.put("android", android);

//		JSONObject notification = new JSONObject();
//		notification.put("name", "sample");
//		notification.put("notification_foreground", "true");
//		notification.put("notification_title", fmcRequest.getTitle());
//		notification.put("notification_body", fmcRequest.getBody());
//		notification.put("notification_android_visibility", "1");
//		notification.put("notification_android_color", "#ff0000");
//		notification.put("notification_android_icon", "iconFileName");
//		notification.put("notification_android_sound", "default");
//		notification.put("notification_android_vibrate", "500, 200. 500");
//		notification.put("notification_android_lights", "500, 200. 500");
//		notification.put("app", "jgw");
//		notification.put("app_data", "test haha");

		body.put("data", fmcRequest.toJSONObject());

		System.out.println(body.toString());

		//return body.toString();
		String bodyString = body.toString();
		HttpEntity<String> entity = new HttpEntity<>(new String(bodyString.getBytes(StandardCharsets.UTF_8)));

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();

		interceptors.add(new HeaderRequestInterceptor("Authorization",  "key=" + FIREBASE_SERVER_KEY));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json; UTF-8 "));
		restTemplate.setInterceptors(interceptors);

		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);

		return CompletableFuture.completedFuture(firebaseResponse);
	}

	/**
	 * 다중 사용자에게 푸시메세지 일괄전송
	 * @param memSeq
	 * @param fmcRequest
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public CompletableFuture<String> sendBulk(List<DeviceInfoSetting> notificationList, FmcMessageRequest fmcRequest ) throws UnsupportedEncodingException {

		System.out.println(notificationList.toString());

		// TODO : 대상없음 Exception CompletableFuture 추가.  DB기록 추가
		if(notificationList == null || notificationList.size() < 1) {
			return CompletableFuture.completedFuture("푸시 메세지 수신 대상 없음");
		}
		List<String> tokenList = new ArrayList<>();
		for(DeviceInfoSetting vo : notificationList) {
			tokenList.add(vo.getDeviceToken());
		}

		LocalDate localDate = LocalDate.now();

		JSONObject body = new JSONObject();

		JSONArray tokenJsonArray = new JSONArray();
		for(String token : tokenList) {
			tokenJsonArray.put(token);
		}

		JSONObject android = new JSONObject();
		JSONObject notification = new JSONObject();
		notification.put("channel_id", "box337");
		android.put("notification", notification);
		body.put("registration_ids", tokenJsonArray);
		body.put("android", android);

//		JSONObject notification = new JSONObject();
//		notification.put("name", "sample");
//		notification.put("notification_foreground", "true");
//		notification.put("notification_title", fmcRequest.getTitle());
//		notification.put("notification_body", fmcRequest.getBody());
//		notification.put("notification_android_visibility", "1");
//		notification.put("notification_android_color", "#ff0000");
//		notification.put("notification_android_icon", "iconFileName");
//		notification.put("notification_android_sound", "default");
//		notification.put("notification_android_vibrate", "500, 200. 500");
//		notification.put("notification_android_lights", "500, 200. 500");
//		notification.put("app", "jgw");
//		notification.put("app_data", "test haha");

		body.put("data", fmcRequest.toJSONObject());

		System.out.println(body.toString());

		//return body.toString();
		String bodyString = body.toString();
		HttpEntity<String> entity = new HttpEntity<>(new String(bodyString.getBytes(StandardCharsets.UTF_8)));

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();

		interceptors.add(new HeaderRequestInterceptor("Authorization",  "key=" + FIREBASE_SERVER_KEY));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json; UTF-8 "));
		restTemplate.setInterceptors(interceptors);

		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);

		return CompletableFuture.completedFuture(firebaseResponse);
	}

	public CompletableFuture<String> adminSendBulk(List<Integer>memSeqList, String title, String content ) throws UnsupportedEncodingException {

		FmcMessageRequest fmcRequest = new FmcMessageRequest();
		fmcRequest.setNotificationTitle(title);
		fmcRequest.setNotificationBody(content);


		// TODO : 대상없음 Exception CompletableFuture 추가.  DB기록 추가
		if(memSeqList == null || memSeqList.size() < 1) {
			return CompletableFuture.completedFuture("푸시 메세지 수신 대상 없음");
		}
		List<String> tokenList = new ArrayList<>();

		System.out.println("전달 받은 seq");
		System.out.println(memSeqList.toString());
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("memSeqList",memSeqList);
		List<DeviceInfoSetting> notificationList = notificationMapper.selectDeviceInfoList(param);
		for(DeviceInfoSetting vo : notificationList) {
			tokenList.add(vo.getDeviceToken());
		}

		System.out.println(tokenList.toString());

		JSONObject body = new JSONObject();

		JSONArray tokenJsonArray = new JSONArray();
		for(String token : tokenList) {
			tokenJsonArray.put(token);
		}

		JSONObject android = new JSONObject();
		JSONObject notification = new JSONObject();
		notification.put("channel_id", "box337");
		android.put("notification", notification);
		body.put("registration_ids", tokenJsonArray);
		body.put("android", android);

//		JSONObject notification = new JSONObject();
//		notification.put("name", "sample");
//		notification.put("notification_foreground", "true");
//		notification.put("notification_title", fmcRequest.getTitle());
//		notification.put("notification_body", fmcRequest.getBody());
//		notification.put("notification_android_visibility", "1");
//		notification.put("notification_android_color", "#ff0000");
//		notification.put("notification_android_icon", "iconFileName");
//		notification.put("notification_android_sound", "default");
//		notification.put("notification_android_vibrate", "500, 200. 500");
//		notification.put("notification_android_lights", "500, 200. 500");
//		notification.put("app", "jgw");
//		notification.put("app_data", "test haha");

		body.put("data", fmcRequest.toJSONObject());

		System.out.println(body.toString());

		//return body.toString();
		String bodyString = body.toString();
		HttpEntity<String> entity = new HttpEntity<>(new String(bodyString.getBytes(StandardCharsets.UTF_8)));

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
		ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();

		interceptors.add(new HeaderRequestInterceptor("Authorization",  "key=" + FIREBASE_SERVER_KEY));
		interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json; UTF-8 "));
		restTemplate.setInterceptors(interceptors);

		String firebaseResponse = restTemplate.postForObject(FIREBASE_API_URL, entity, String.class);
//		String firebaseResponse = restTemplate.postForObject("sdfsdfsdf", entity, String.class);

		return CompletableFuture.completedFuture(firebaseResponse);
	}


}


