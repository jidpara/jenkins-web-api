package com.gk337.api.base.service;

import com.gk337.api.base.entity.*;
import com.gk337.api.base.repository.DeviceInfoRepository;
import com.gk337.api.base.repository.DeviceSettingRepository;
import com.gk337.api.base.repository.MemberRepository;
import com.gk337.api.shop.entity.BizNumCheckResponse;
import com.gk337.api.shop.entity.ShopInfoEntity;
import com.gk337.api.shop.entity.ShopInfoRequest;
import com.gk337.api.shop.entity.ShopInfoResponse;
import com.gk337.api.shop.repository.ShopInfoRepository;
import com.gk337.common.util.BizUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Slf4j
@Service
public class MemberService {

	@Autowired
	MemberRepository memberRepository;

	@Autowired
	ShopInfoRepository shopInfoRepository;

	public MemberResponse updateMember(MemberRequest request) {

		MemberEntity entity = memberRepository.findByUserId(request.getUserId());

		System.out.println("entity : " + entity.toString());


		MemberResponse response = new MemberResponse();

		BeanUtils.copyProperties(request, entity);
		entity.setCstatus("Y");
		entity = memberRepository.save(entity);
		BeanUtils.copyProperties(entity, response);

		return response;
	}

	// user_id 를 기준으로 초기 회원을 생성
	public MemberResponse createMemberDefault(ConnectRequest request) {

		MemberEntity entity = memberRepository.findByUserId(request.getUserId());

		MemberResponse response = new MemberResponse();
		MemberRequest memberRequest = new MemberRequest();
		memberRequest.setUserId(request.getUserId());
		memberRequest.setMemKind("T"); // 초기 상태
		memberRequest.setMemAuthYn("Y"); // TODO : 삭제 2.1.0 release 이후 불필요
		memberRequest.setProRequestDate(LocalDateTime.now()); // TODO : 삭제 2.0.13 release 이후 불필요
		memberRequest.setProOkDate(LocalDateTime.now()); // TODO : 삭제 2.0.13 release 이후 불필요
		memberRequest.setMemName(request.getMemName());
		memberRequest.setMemNickname(request.getMemName());

		if(entity == null) {
			entity = new MemberEntity();
			BeanUtils.copyProperties(memberRequest, entity);
			entity = memberRepository.save(entity);
			response.setIsCreated(true);
		}

		BeanUtils.copyProperties(entity, response);

		return response;
	}

	/**
	 * 아이디 중복체크
	 * @param userId
	 * @return
	 */

	public String idCheck(String userId) {
		System.out.println(memberRepository.findByUserId(userId));

		if (memberRepository.findByUserId(userId) == null) {
			return "YES";
		} else {
			return "NO";
		}
	}

	public MemberResponse insertDealer(MemberRequestShopInfo request) {

		MemberEntity entity = new MemberEntity();
		MemberResponse response = new MemberResponse();
		ShopInfoEntity shopEntity = new ShopInfoEntity();

		BeanUtils.copyProperties(request, entity);

		entity.setCstatus("Y");
		entity.setMemKind("P");

		entity = memberRepository.save(entity);
		BeanUtils.copyProperties(entity, response);

		shopEntity.setMemSeq(response.getSeq());
		shopEntity.setCstatus("Y");
		shopEntity.setCompRegNum(request.getCompRegNum());
		shopEntity.setOwnerName(request.getOwnerName());
		shopEntity.setShopName(request.getShopName());
		shopEntity.setFileSeq(request.getFileSeq());
		shopEntity.setFilename1(request.getFilename1());
		shopEntity.setZipCode(request.getZipCode());
		shopEntity.setShopAddr1(request.getShopAddr1());
		shopEntity.setShopAddr2(request.getShopAddr2());
		shopInfoRepository.save(shopEntity);

		return response;
	}




}
