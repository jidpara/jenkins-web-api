package com.gk337.api.shop.controller;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.gk337.api.info.entity.FaqResponse;
import com.gk337.api.shop.component.ShopComponent;
import com.gk337.api.shop.entity.BizNumCheckResponse;
import com.gk337.api.shop.entity.ShopInfoRequest;
import com.gk337.api.shop.entity.ShopInfoResponse;
import com.gk337.api.shop.entity.ShopMainResponse;
import com.gk337.api.shop.entity.ShopMyProductResponse;
import com.gk337.api.shop.service.ShopService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 상점 RestAPI Controller
 * 프로그램명 : ShopController.java
 *
 *  - 상점관련 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.02.04
 * @author maroo
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 * 확인사항 : 
 *  - jw_shop_category테이블 변경일,아이피,변경자 필드 추가 및 seq 키 추가.
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/mystore")
@Api(description="딜러가 등록한 상점관리", tags= {"[메인] - 내 상점 API"}, protocols="http", produces="application/json", consumes="application/json")
public class ShopRestController {
	
	@Autowired
	private ShopService shopService;
	
	@Autowired
	private ShopComponent shopComponent;
	
	@ApiOperation(value = "내 상점 정보 저장 API", 
			notes = "딜러등록 프로세스 중 상점정보 저장", 
			response=ShopInfoResponse.class)
	@RequestMapping(method = RequestMethod.POST, path="/", produces = "application/json")
	public ResponseEntity<ShopInfoResponse> insert(@RequestBody @Valid ShopInfoRequest request) {
		return BaseResponse.ok(shopComponent.insertShopInfo(request));
	}
	
	@ApiOperation(value = "내 상점 정보 수정 API", 
			notes = "딜러등록 프로세스 중 상점정보 수정", 
			response=ShopInfoResponse.class)
	@RequestMapping(method = RequestMethod.PUT, path="/{shopSeq}", produces = "application/json")
	public ResponseEntity<ShopInfoResponse> update(@RequestBody @Valid ShopInfoRequest request,
			@ApiParam(value="상점정보 시퀀스", required=true, defaultValue = "18") @PathVariable(value = "shopSeq", required = true) int shopSeq) {
		request.setSeq(shopSeq);
		return BaseResponse.ok(shopService.updateShopInfo(request));
	}
	
//	@ApiImplicitParams({
//        @ApiImplicitParam(name = "Authorization", value = "로그인 성공 후 access_token", required = true, dataType = "String", paramType = "header")
//	})
	@ApiOperation(value = "내 상점 정보 상세조회 API", 
			notes = "회원시퀀스로 등록된 상점정보를 조회한다.\n"
					+ "카테고리 리스트 포함.", 
			response=ShopInfoResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/{memSeq}", produces = "application/json")
	public ResponseEntity<List<ShopInfoResponse>> findBySeq(
			@ApiParam(value="회원정보 시퀀스", required=true, defaultValue = "1") @PathVariable(value = "memSeq", required = true) int memSeq) {
		return BaseResponse.ok(shopService.selectShopInfo(memSeq));
	}
	
	@ApiOperation(value = "내 상점 메인 및 GNB에 노출할 요청서 건수 조회 API", notes = "내 상점 메인 및 GNB에 노출할 요청서 건수 조회 API", response=ShopMainResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/info/{memSeq}", produces = "application/json")
	public ResponseEntity<List<ShopMainResponse>> selectShopMainInfo(
			@ApiParam(value="회원정보 시퀀스", required=true, defaultValue = "1") @PathVariable(value = "memSeq", required = true) int memSeq,
			@ApiParam(value="회원구분[딜러: P/ 유저: G]", required=true, defaultValue = "P") @RequestParam(value = "memKind", required = true) String memKind) {
		return BaseResponse.ok(shopService.selectShopMainInfo(memSeq, memKind));
	}
	
	@ApiOperation(value = "내 상점 내 물품 목록 조회 API", notes = "내 상점 내 물품 목록 조회 API", response=ShopMyProductResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/product/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<ShopMyProductResponse>> selectMyProductList(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="회원정보 시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq) {
		return BaseResponse.ok(shopService.selectMyProductList(pageNo, pageSize, memSeq));
	}
	
	@ApiOperation(value = "상점정보 조회시 read_cnt 업데이트 API", 
			notes = "상점정보 조회 시 조회수 업데이트.\n"
					+ "다른 유저/딜러가 조회했을 경우만 업데이트.", 
			response = FaqResponse.class)
	@RequestMapping(method = RequestMethod.PUT, path="/count/{shopSeq}", produces = "application/json")
	public ResponseEntity<ErrorVo> updateReadCnt(
			@ApiParam(value="상점시퀀스", required=true, defaultValue = "18") @PathVariable(value = "shopSeq", required = true) int shopSeq,
			@ApiParam(value="회원시퀀스", required=true, defaultValue = "12") @RequestParam(value = "memSeq", required = true) int memSeq) {
		return BaseResponse.ok(shopService.updateReadCnt(shopSeq, memSeq));
	}
	
	@ApiOperation(value = "메인페이지 회원수,딜러수,물품수 조회 API", notes = "메인페이지 요청건수 조회 API", response=ShopMainResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/main", produces = "application/json")
	public ResponseEntity<List<ShopMainResponse>> selectMainInfoCountList() {
		return BaseResponse.ok(shopService.selectMainInfoCountList());
	}
	
	@ApiOperation(value = "사업자등록번호 중복체크 및 체크 API", notes = "사업자등록번호 중복체크 및 체크 API", response=BizNumCheckResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/bizNum/check", produces = "application/json")
	public ResponseEntity<BizNumCheckResponse> bizNumDupCheck(
			@ApiParam(value="사업자등록번호", required=true, defaultValue = "123-12-12345") @RequestParam(value = "compRegNum", required = true) String compRegNum) {
		return BaseResponse.ok(shopService.bizNumDupCheck(compRegNum));
	}
	
	@ApiOperation(value = "상점정보 삭제 API", notes = "시퀀스로 상점정보 삭제 API")
	@RequestMapping(method = RequestMethod.DELETE, path="/{shopSeq}", produces = "application/json")
	public ResponseEntity<ErrorVo> deleteShopInfo(
			@ApiParam(value="상점정보 시퀀스", required=true, defaultValue = "1") @PathVariable(value ="shopSeq", required = true) int shopSeq) {
		return BaseResponse.ok(shopService.deleteShopInfo(shopSeq));
	}	

	
	//	@ApiOperation(value = "상점정보 저장", notes = "가계정보 저장", response=ShopInfoRequest.class)
	//	@RequestMapping(method = RequestMethod.POST, path="/", produces = "application/json")
	//	public ResponseEntity<ShopInfoRequest> save(@RequestBody @Valid ShopInfoRequest request) {
	//		return BaseResponse.ok(shopService.saveOrUpdateShop(request));
	//	}
		
	//	@ApiOperation(value = "상점정보 저장", notes = "가계정보 저장", response=ShopCategoryRequest.class)
	//	@RequestMapping(method = RequestMethod.POST, path="/category", produces = "application/json")
	//	public ResponseEntity<ShopInfoRequest> saveCategory(@RequestBody @Valid ShopCategoryRequest request) {
	//		return BaseResponse.ok(shopService.saveOrUpdateCategory(request));
	//	}
		
		
	//	@ApiOperation(value = "가계정보 저장.", notes = "가계정보 저장.")
	//	@RequestMapping(method = RequestMethod.POST, path="/", produces = "application/json")
	//	public ResponseEntity<ShopMastResponse> save(@RequestBody @Valid ShopMastResponse entity) {
	//		return BaseResponse.ok(shopService.save(entity));
	//	}
		
	//	@ApiOperation(value = "가계정보 수정.", notes = "가계정보 수정.")
	//	@RequestMapping(method = RequestMethod.PUT, path="/", produces = "application/json")
	//	public ResponseEntity<ShopMastResponse> update(@RequestBody @Valid ShopMastResponse entity) {
	//		return BaseResponse.ok(shopService.save(entity));
	//	}
	//	
	//	@ApiOperation(value = "가계정보 목록조회.", notes = "가계정보 목록조회.")
	//	@RequestMapping(method = RequestMethod.GET, path="/", produces = "application/json")
	//	public ResponseEntity<List<ShopMastResponse>> findAll() {
	//		return BaseResponse.ok(shopService.findAll());
	//	}
	//	
	//	@ApiOperation(value = "SEQ로 가계정보 목록조회", notes = "SEQ로 가계정보 목록조회.")
	//	@RequestMapping(method = RequestMethod.GET, path="/{seq}", produces = "application/json")
	//	public ResponseEntity<List<ShopMastResponse>> findBySeq(
	//			@ApiParam(value="가계정보 시퀀스", required=true, defaultValue = "1") @PathVariable(value = "seq", required = true) int seq) {
	//		return BaseResponse.ok(shopService.findBySeq(seq));
	//	}
	//	
	//	@SuppressWarnings("deprecation")
	//	@ApiOperation(value = "가계정보 목록조회 페이징.", notes = "가계정보 페이징으로 상품목록조회.")
	//	@RequestMapping(method = RequestMethod.GET, path="/{page_no}/{page_size}", produces = "application/json")
	//	public Page<ShopMastResponse> findAllPaging(
	//			@ApiParam(value="페이지번호", required=true) @PathVariable(value = "page_no", required = true) int pNo,
	//			@ApiParam(value="사이즈", required=true) @PathVariable(value = "page_size", required = true) int size
	//			) {
	//		Direction dir = Sort.Direction.DESC;
	//		PageRequest pageable = new PageRequest(pNo-1, size, dir, "shopName");
	//		return shopService.findAllPaging(pageable);
	//	}
		
	//	@ApiOperation(value = "검색조건[필터]으로 가계정보 목록조회", notes = "검색조건으로 가계정보 목록조회.")
	//	@RequestMapping(method = RequestMethod.POST, path="/search", produces = "application/json")
	//	public ResponseEntity<List<ShopMastResponse>> findBySearchKey(@RequestBody @Valid ProductSearchParam requestDto) {
	//		return BaseResponse.ok(shopService.findBySearchKey(requestDto));
	//	}
	
}
