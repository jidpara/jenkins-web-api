package com.gk337.api.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.gk337.api.shop.component.ShopComponent;
import com.gk337.api.shop.entity.ShopInfoResponse;
import com.gk337.api.shop.entity.ShopMyProductResponse;
import com.gk337.api.shop.service.ShopService;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/dealerstore")
@Api(description="타인 상점", tags= {"[메인] - 타인 상점 API"}, protocols="http", produces="application/json", consumes="application/json")
public class DealerShopRestController {
	
	@Autowired
	private ShopService shopService;
	
	@Autowired
	private ShopComponent shopComponent;
	
	@ApiOperation(value = "내 상점 정보 상세조회 API", 
			notes = "회원시퀀스로 등록된 상점정보를 조회한다.\n"
					+ "카테고리 리스트 포함.", 
			response=ShopInfoResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/{memSeq}", produces = "application/json")
	public ResponseEntity<List<ShopInfoResponse>> findBySeq(
			@ApiParam(value="회원정보 시퀀스", required=true, defaultValue = "1") @PathVariable(value = "memSeq", required = true) int memSeq) {
		return BaseResponse.ok(shopService.selectShopInfo(memSeq));
	}
	
	@ApiOperation(value = "타인 상점 목록 조회 API", notes = "내 상점 내 물품 목록 조회 API", response=ShopMyProductResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/products/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<ShopMyProductResponse>> selectDealerProductList(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="회원정보 시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq) {
		return BaseResponse.ok(shopService.selectDealerProductList(pageNo, pageSize, memSeq));
	}

}
