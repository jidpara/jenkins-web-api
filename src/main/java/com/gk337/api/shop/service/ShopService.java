package com.gk337.api.shop.service;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gk337.api.area.repository.ShopAreaRepository;
import com.gk337.api.base.repository.BaseCategoryRepository;
import com.gk337.api.mypage.repository.ProfileRepository;
import com.gk337.api.payment.entity.PackpageShopArea;
import com.gk337.api.shop.entity.BizNumCheckResponse;
import com.gk337.api.shop.entity.ShopInfoEntity;
import com.gk337.api.shop.entity.ShopInfoRequest;
import com.gk337.api.shop.entity.ShopInfoResponse;
import com.gk337.api.shop.entity.ShopMainResponse;
import com.gk337.api.shop.entity.ShopMyProductResponse;
import com.gk337.api.shop.mapper.ShopMapper;
import com.gk337.api.shop.repository.ShopCategoryRepository;
import com.gk337.api.shop.repository.ShopInfoRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.BizUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ShopService {

	@Value("${test.enable}")
	private boolean testEnabled;

	@Value("${test.biz-num}")
	private String testBizNum;
	
	@Autowired
	private ShopInfoRepository shopInfoRepository;
	
	@Autowired
	private ShopCategoryRepository shopCategoryRepository;
	
	@Autowired
	private ShopAreaRepository shopAreaRepository;
	
	@Autowired
	private ShopMapper shopMapper;
	
	@Autowired
	private BaseCategoryRepository baseCategoryRepository;
	
	/**
	 * 딜러 상점 정보 저장.
	 * @param request
	 * @return
	 */
	@Transactional
	public ShopInfoResponse insertShopInfo(ShopInfoRequest request) {
		log.debug("========ShopService.insertShopInfo========");
		ShopInfoResponse response = new ShopInfoResponse();
		try {
			
			// jw_shop_info 저장.
			ShopInfoEntity infoEntity = new ShopInfoEntity();
			BeanUtils.copyProperties(request, infoEntity);
			// TODO : front 변경분 같이 적용 필요하여 임시처리
			// TODO : 증명자료 개선 -> 프로필 이미지로 올라가지 않도록 임시처리 한 내용 개선
			infoEntity.setCertFileSeq(infoEntity.getFileSeq());
			infoEntity.setFileSeq(null);

			infoEntity = shopInfoRepository.saveAndFlush(infoEntity);
			// 조회 리턴.
			response = shopMapper.selectShop(infoEntity.getSeq());
			
			// jw_shop_category 저장.
//			List<ShopCategoryEntity> categoryList = new ArrayList<ShopCategoryEntity>();
//			for(ShopCategoryRequest category : request.getShopCategoryRequest()) {
//				ShopCategoryEntity categoryEntity = new ShopCategoryEntity();
//				category.setShopSeq(infoEntity.getSeq());
//				BeanUtils.copyProperties(category, categoryEntity);
//				categoryList.add(categoryEntity);
//			}
//			shopCategoryRepository.saveAll(categoryList);
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "저장 또는 수정");
		}
		return response;
	}
	
	/**
	 * 상점정보 수정.
	 * @param request
	 * @return
	 */
	@Transactional
	public ShopInfoResponse updateShopInfo(ShopInfoRequest request) {
		log.debug("========ShopService.updateShopInfo========");
		ShopInfoResponse response = new ShopInfoResponse();
		try {
			
			// jw_shop_info 저장.
//			ShopInfoEntity infoEntity = new ShopInfoEntity();
//			BeanUtils.copyProperties(request, infoEntity);
//			infoEntity = shopInfoRepository.saveAndFlush(infoEntity);

			shopMapper.updateShopInfo(request);

			if(request.getFileSeq() != null && request.getFileSeq() != 0){
				String profileImage = selectProfileImage(request.getFileSeq());
				updateProfileImage(profileImage, request.getMemSeq());
			}

			// 조회 후 리턴.
			response = shopMapper.selectShop(request.getSeq());
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "저장 또는 수정");
		}
		return response;
	}
	
	@Transactional
//	public ShopInfoResponse updateShopInfo(ShopInfoRequest request) {
//		ShopInfoResponse response = new ShopInfoResponse();
//		shopMapper.updateShopInfo(request);
//		response = shopMapper.selectShop(request.getSeq());
//	}
//	
	/**
	 * 딜러 상점정보 상세조회.
	 * @param seq
	 * @return
	 */
	public ShopInfoResponse selectShopInfo(int seq) {
		log.debug("========ShopService.selectShopInfo========");
		ShopInfoResponse response = new ShopInfoResponse();
		try {
			response = shopMapper.selectShopInfo(seq);
			if(response == null) {
				throw new JGWServiceException("MESSAGE.COMMON.0001");
			}
			List<PackpageShopArea> areaGroupList = shopMapper.selectPackageShopArea(seq);
			response.setShopAreaResponse(areaGroupList);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "상세조회");
		}
		return response;
	}
	
	/**
	 * 타인 상점정보 상세조회.
	 * @param seq
	 * @return
	 */
	public ShopInfoResponse selectDealerShopInfo(int seq) {
		log.debug("========ShopService.selectShopInfo========");
		ShopInfoResponse response = new ShopInfoResponse();
		try {
			response = shopMapper.selectShopInfo(seq);
			if(response == null) {
				throw new JGWServiceException("MESSAGE.COMMON.0001");
			}
			List<PackpageShopArea> areaGroupList = shopMapper.selectPackageShopArea(seq);
			response.setShopAreaResponse(areaGroupList);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "상세조회");
		}
		return response;
	}
	
	/**
	 * 메인정보조회.
	 * @param memSeq
	 * @return
	 */
	public ListVo<ShopMainResponse> selectShopMainInfo(int memSeq, String memKind) {
		log.debug("========ShopService.selectShopMainInfo========");
		ListVo<ShopMainResponse> listVo = null;
		try {
			if(memKind.equals("P")) { //딜러.
				listVo = new ListVo<ShopMainResponse>(shopMapper.selectShopMainInfoDealerNew(memSeq));
			} else { // 유저.
				listVo = new ListVo<ShopMainResponse>(shopMapper.selectShopMainInfoUserNew(memSeq));
			}
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "메인정보 조회");
		}
		return listVo;
	}
	
	/**
	 * 내 물품 리스트 조회 [페이징]
	 * @param pageNo
	 * @param pageSize
	 * @param memSeq
	 * @return
	 */
	public PageInfo<ShopMyProductResponse> selectMyProductList(int pageNo, int pageSize, int memSeq) {
		PageInfo<ShopMyProductResponse> pageInfo = new PageInfo<ShopMyProductResponse>();
		try {
			int totalSize = shopMapper.selectMyProductListNew(memSeq).size();
			PageHelper.startPage(pageNo, pageSize); 
			List<ShopMyProductResponse> list = shopMapper.selectMyProductListNew(memSeq);
			pageInfo = new PageInfo<ShopMyProductResponse>(list);
			pageInfo.setTotal(totalSize);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "내 물품 조회");
		}
		return pageInfo;
	}
	
	public PageInfo<ShopMyProductResponse> selectDealerProductList(int pageNo, int pageSize, int memSeq) {
		PageInfo<ShopMyProductResponse> pageInfo = new PageInfo<ShopMyProductResponse>();
		try {
			int totalSize = shopMapper.selectDealerListNew(memSeq).size();
			PageHelper.startPage(pageNo, pageSize); 
			List<ShopMyProductResponse> list = shopMapper.selectDealerListNew(memSeq);
			pageInfo = new PageInfo<ShopMyProductResponse>(list);
			pageInfo.setTotal(totalSize);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "내 물품 조회");
		}
		return pageInfo;
	}
	
	/**
	 * 조회수 업데이트.
	 * @param seq
	 * @return
	 */
	@Transactional
	public ErrorVo updateReadCnt(int shopSeq, int memSeq) {
		log.debug("========ShopService.updateReadCnt========");
		try {
			shopInfoRepository.updateReadCnt(shopSeq, memSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "조회수 업데이트");
		}
		return new ErrorVo();
	}
	
	/**
	 * 메인정보 카운트 조회.
	 * @return
	 */
	public ListVo<ShopMainResponse> selectMainInfoCountList() {
		ListVo<ShopMainResponse> listVo = null;
		try {
			listVo = new ListVo<ShopMainResponse>(shopMapper.selectMainInfoCountList());
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "메인정보 조회");
		}
		return listVo;
	}
	
	/**
	 * 사업자등록번호 중복체크
	 * @param compRegNum
	 * @return
	 */
	public BizNumCheckResponse bizNumDupCheck(String compRegNum) {
		BizNumCheckResponse response = new BizNumCheckResponse();
		
		String bizNum = StringUtils.replace(compRegNum, "-", "");
		log.debug("bizNum ::: {} ", bizNum);
		
		boolean flag = BizUtils.checkBizNum(bizNum);
		String result = shopMapper.bizNumDupCheck(compRegNum);

		// test or 예외처리
		if(testEnabled || testBizNum.equals(compRegNum)) {
			response.setResultCode("Y");
			response.setResultMessage("테스트 사업자등록번호입니다.");
		} else {
			if(flag && result.equals("N")) {
				response.setResultCode("Y");
				response.setResultMessage("사용 가능한 사업자등록번호입니다.");
			} else {
				if(result.equals("Y")) {
					response.setResultCode("N");
					response.setResultMessage("이미 등록된 사업자등록번호가 있습니다.");
				}
				if(!flag) {
					response.setResultCode("N");
					response.setResultMessage("올바른 사업자등록번호가 아닙니다.");
				}
			}
		}
		return response;
	}
	
	/**
	 * 상점정보 삭제.
	 * @param shopSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteShopInfo(int shopSeq) {
		ErrorVo result = new ErrorVo();
		try {
			
			shopCategoryRepository.deleteByShopSeq(shopSeq);
			shopAreaRepository.deleteByShopSeq(shopSeq);
			shopInfoRepository.deleteBySeq(shopSeq);
			
			result.setMessage("상점정보가 삭제되었습니다.");
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "상점정보 삭제시");
		}
		return result;
	}
	
	/**
	 * 
	 * @param memSeq
	 * @param compRegNum
	 * @return
	 */
	public Integer bizExistCheck(int memSeq) {
		return shopMapper.bizExistCheck(memSeq);
	}
	
	@Autowired
	private ProfileRepository profileRepository;
	public void updateProfileImage(String profileImage, int memSeq) {
		profileRepository.updateProfilImage(profileImage, memSeq);
	}
	
	public String selectProfileImage(int fileSeq) {
		return shopMapper.selectProfilImage(fileSeq);
	}
	
}

