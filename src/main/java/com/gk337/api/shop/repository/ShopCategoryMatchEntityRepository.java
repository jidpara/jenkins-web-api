package com.gk337.api.shop.repository;

import com.gk337.api.shop.entity.ShopCategoryMatchEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ShopCategoryMatchEntityRepository extends JpaRepository<ShopCategoryMatchEntity, Integer>, JpaSpecificationExecutor<ShopCategoryMatchEntity> {

}