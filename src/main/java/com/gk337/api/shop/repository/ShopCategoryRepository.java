package com.gk337.api.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.shop.entity.ShopCategoryEntity;

@Repository
@SuppressWarnings("unchecked")
public interface ShopCategoryRepository extends PagingAndSortingRepository<ShopCategoryEntity, Integer>, JpaRepository<ShopCategoryEntity, Integer> {
	
	void deleteByShopSeq(int shopSeq);
}