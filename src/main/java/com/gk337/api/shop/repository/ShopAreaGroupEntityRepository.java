package com.gk337.api.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.gk337.api.shop.entity.ShopAreaGroupEntity;

public interface ShopAreaGroupEntityRepository extends JpaRepository<ShopAreaGroupEntity, Integer>, JpaSpecificationExecutor<ShopAreaGroupEntity> {

}