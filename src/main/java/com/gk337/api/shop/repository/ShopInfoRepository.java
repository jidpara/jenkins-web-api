package com.gk337.api.shop.repository;

import java.util.Optional;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.shop.entity.ShopInfoEntity;

@Repository
@SuppressWarnings("unchecked")
public interface ShopInfoRepository extends PagingAndSortingRepository<ShopInfoEntity, Integer>, JpaRepository<ShopInfoEntity, Integer> {

	@Modifying
	@Query("update ShopInfoEntity q set q.readCnt = q.readCnt + 1 where q.seq = :shopSeq and q.memSeq <> :memSeq")
	void updateReadCnt(@Param("shopSeq") int shopSeq, @Param("memSeq") int memSeq);
	
	void deleteBySeq(int seq);
	
	Optional<ShopInfoEntity> findByMemSeq(Integer memSeq);
}