package com.gk337.api.shop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "유저 상점 지역 REQUEST VO")
public class ShopAreaResponse {

    @ApiModelProperty(notes = "지역코드 ", example = "1")
    private Integer areaSeq;

    @ApiModelProperty(notes = "지역명", example = "1")
    private String areaName;

}
