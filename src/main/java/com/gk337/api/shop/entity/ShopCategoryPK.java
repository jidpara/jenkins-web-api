package com.gk337.api.shop.entity;

import java.io.Serializable;

import com.gk337.common.core.domain.PKEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ShopCategoryPK implements Serializable, PKEntity {

	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	private Integer shopSeq;                   

	@EqualsAndHashCode.Include
	private Integer cateSeq;					  

}
