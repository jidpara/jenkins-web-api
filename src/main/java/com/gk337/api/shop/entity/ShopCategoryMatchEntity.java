package com.gk337.api.shop.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 상점카테고리매칭
 */
@Table(name = "jw_shop_category_match")
@Data
@Entity
@IdClass(ShopCategoryPK.class)
public class ShopCategoryMatchEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 상점시퀀스
     */
    @Id
    @Column(name = "shop_seq", insertable = false, nullable = false)
    private Integer shopSeq;

    /**
     * 카테고리시퀀스
     */
    @Id
    @Column(name = "cate_seq", insertable = false, nullable = false)
    private Integer cateSeq;

    /**
     * 등록ID
     */
    @Column(name = "reg_id")
    private String regId = "NULL";

    /**
     * 등록IP
     */
    @Column(name = "reg_ip")
    private String regIp = "NULL";

    /**
     * 등록일자
     */
    @Column(name = "reg_date")
    private LocalDateTime regDate;

    @Column(name = "chg_id")
    private String chgId = "NULL";

    @Column(name = "chg_ip")
    private String chgIp = "NULL";

    @Column(name = "chg_date")
    private LocalDateTime chgDate;

    
}