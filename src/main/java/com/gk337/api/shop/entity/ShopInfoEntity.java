package com.gk337.api.shop.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.gk337.api.base.entity.BaseUploadS3Entity;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상점 jw_shop_info  jw_shop_mast로 변경예정.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_shop_info")
@ApiModel(description = "상점정보 마스터 Entity")
public class ShopInfoEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @Column(name = "cstatus", nullable = false)
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, A:대기, D:삭제)")
    private String cstatus;

    @Column(name = "mem_seq")
    @ApiModelProperty(notes = "회원시퀀스")
    private Integer memSeq;

    @Column(name = "shop_name")
    @ApiModelProperty(notes = "상호명")
    private String shopName;
    
    @Column(name = "shop_phone")
    @ApiModelProperty(notes = "연락처")
    private String shopPhone;
    
    @Column(name = "file_seq")
    @ApiModelProperty(notes = "파일시퀀스")
    private Integer fileSeq;

    @Column(name = "filename1")
    @ApiModelProperty(notes = "증명자료(미사용)")
    private String filename1;

    @Column(name = "cert_file_seq")
    @ApiModelProperty(notes = "증명파일시퀀스")
    private Integer certFileSeq;

    @Column(name = "read_cnt")
    @ApiModelProperty(notes = "조회수")
    private Integer readCnt = 0;
    
    @Column(name = "shop_addr1")
    @ApiModelProperty(notes = "주소1")
    private String shopAddr1;

    @Column(name = "shop_addr2")
    @ApiModelProperty(notes = "주소2")
    private String shopAddr2;
    
    @Column(name = "zip_code")
    @ApiModelProperty(notes = "우편번호.")
    private String zipCode;
    
    @Column(name = "comp_reg_num")
    @ApiModelProperty(notes = "사업자등록번호")
    private String compRegNum;
    
    @Column(name = "owner_name")
    @ApiModelProperty(notes = "대표자명")
    private String ownerName;
    
    @OneToOne(fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name="file_seq" , referencedColumnName = "seq" ,insertable = false, updatable = false)
    private BaseUploadS3Entity file;
    
    

}