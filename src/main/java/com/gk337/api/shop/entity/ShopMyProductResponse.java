package com.gk337.api.shop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.api.request.entity.RequestProductAtfilResponse;
import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상점
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "내 상점 내의 내물품 Entity")
public class ShopMyProductResponse implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "요청서시퀀스", example = "12")
    private Integer reqSeq;
	
	@ApiModelProperty(notes = "상품시퀀스", example = "12")
    private Integer prodSeq;
	
	@ApiModelProperty(notes = "요청서구분", example = "S")
    private String reqType;
	
	@ApiModelProperty(notes = "요청서구분명", example = "판매")
    private String reqTypeName;
	
	@ApiModelProperty(notes = "요청서상태코드 (I:거래중, Y:거래완료)", example = "Y")
    private String reqStatus;
	
	@ApiModelProperty(notes = "요청서상태명", example = "거래완료")
    private String reqStatusName;
	
	@ApiModelProperty(notes = "회원시퀀스", example = "1")
	private Integer memSeq;
	
	@ApiModelProperty(notes = "회원구분", example = "P")
	private String memKind;
	
	@ApiModelProperty(notes = "회원구분명", example = "딜러")
	private String memKindName;
	
	@ApiModelProperty(notes = "이메일", example = "abc@abc.com")
	private String memEmail;
	
	@ApiModelProperty(notes = "회원닉네임", example = "닉네임.")
	private String memNickname;
	
	@ApiModelProperty(notes = "회원이름", example = "이름명")
	private String memName;
	
	@ApiModelProperty(notes = "회원휴대폰번호", example = "01012349876")
    private String memHp;
	
	@ApiModelProperty(notes = "상품명", example = "상품명.")
	private String prodName;
	
	@ApiModelProperty(notes = "상품상세", example = "상품상세...")
	private String prodDesc;
	
	@ApiModelProperty(notes = "가격", example = "12000")
	private String price;
	
	@ApiModelProperty(notes = "키워드", example = "키워드")
	private String keyWords;
	
	@ApiModelProperty(notes = "답변건수", example = "11")
	private String resCnt;
	
	@ApiModelProperty(notes = "새로운 문의내용 체크", example = "Y")
	private String isNew;
	
	@ApiModelProperty(notes = "등록일자", example = "2020-02-22")
	private String regDate;
	
	@ApiModelProperty(notes = "AS여부", example = "Y")
	private String asYn;

	@ApiModelProperty(notes = "거래 완료 여부(Y:완료 N:미완료)", example = "N")
	private String completeYn;

	@ApiModelProperty(notes = "상품보이기(Y:보이기, N:숨기기)", example = "Y")
	private String showYn;

	@ApiModelProperty(notes = "보관여부")
	private String tempYn;
	
	private List<RequestProductAtfilResponse> requestProductAtfilResponse; 

}