package com.gk337.api.shop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;

import com.gk337.common.model.AuditEntity;
import lombok.Data;

/**
 * 상점지역그룹
 */
@Table(name = "jw_shop_area_group")
@Data
@Entity
@IdClass(ShopAreaGroupPK.class)
public class ShopAreaGroupEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 상점시퀀스
     */
    @Id
    @Column(name = "shop_seq", insertable = false, nullable = false)
    private Integer shopSeq;

    /**
     * 지역그룹시퀀스
     */
    @Id
    @Column(insertable = false, name = "area_group_seq", nullable = false)
    private Integer areaGroupSeq;

    /**
     * 상태값 (Y:정상, N:비노출, D:삭제)
     */
    @Column(name = "cstatus", nullable = false)
    private String cstatus = "Y";

    
}