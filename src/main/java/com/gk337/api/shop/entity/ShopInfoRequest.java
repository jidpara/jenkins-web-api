package com.gk337.api.shop.entity;

import java.util.List;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "내 상점 정보 REQUEST VO")
public class ShopInfoRequest {

	@JsonIgnore
    @ApiModelProperty(notes = "SEQ", example = "1")
    private Integer seq;

	@JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:판매중, H:대기, E:마감, N:차단, D:삭제)", example = "Y")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "회원시퀀스", example = "1")
    private Integer memSeq;
    
    @ApiModelProperty(notes = "상호명", example = "상호명")
    private String shopName;
    
    @ApiModelProperty(notes = "연락처", example = "01012341234")
    private String shopPhone;
    
    @JsonIgnore
    @ApiModelProperty(notes = "증명자료", example = "test.doc")
    private String filename1;

    @JsonIgnore
    @ApiModelProperty(notes = "조회수", example = "0")
    private Integer readCnt = 0;
    
    @ApiModelProperty(notes = "주소1", example = "경기도 성남시 수정구 시흥동 293 ")
    private String shopAddr1;

    @ApiModelProperty(notes = "주소2", example = "판교 제2테크노밸리 경기 기업성장센터")
    private String shopAddr2;
    
    @ApiModelProperty(notes = "우편번호.", example = "13449")
    private String zipCode;
    
    @ApiModelProperty(notes = "사업자등록번호", example = "111-22-33333")
    private String compRegNum;
    
    @ApiModelProperty(notes = "대표자명", example = "대표자")
    private String ownerName;

    @ApiModelProperty(notes = "내상점소개", example = "중고나라1호점입니다.")
    private String shopDesc;
    
    @ApiModelProperty(notes = "이미지파일시퀀스", example = "38")
    private Integer fileSeq = 0;

    @JsonIgnore
    @ApiModelProperty(notes = "카테고리 리스트")
    private List<ShopCategoryRequest> shopCategoryRequest;

}
