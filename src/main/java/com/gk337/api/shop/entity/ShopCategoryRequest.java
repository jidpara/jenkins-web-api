package com.gk337.api.shop.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "상점정보 카테고리 REQUEST VO")
public class ShopCategoryRequest {

	@JsonIgnore
    @ApiModelProperty(notes = "상점 시퀀스", example = "1")
    private Integer shopSeq;

    @ApiModelProperty(notes = "카테고리 시퀀스", example = "1")
    private Integer cateSeq;

}
