package com.gk337.api.shop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.api.payment.entity.PackpageShopArea;
import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상점
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "내 상점 정보 Entity")
public class ShopInfoResponse extends BaseVo implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "시퀀스", example="1:상점시퀀스")
    private Integer seq;

    @ApiModelProperty(notes = "회원시퀀스", example="1:회원시퀀스")
    private Integer memSeq;

    @ApiModelProperty(notes = "상호명", example="중고왕회사")
    private String shopName;
    
    @ApiModelProperty(notes = "연락처", example="01012341234")
    private String shopPhone;

    @JsonIgnore
    @ApiModelProperty(notes = "증명자료")
    private String filename1;

    @ApiModelProperty(notes = "조회수", example="1")
    private Integer readCnt;
    
    @ApiModelProperty(notes = "주소1", example = "경기도 성남시 수정구 시흥동 293 ")
    private String shopAddr1;

    @ApiModelProperty(notes = "주소2", example = "판교 제2테크노밸리 경기 기업성장센터")
    private String shopAddr2;
    
    @ApiModelProperty(notes = "우편번호.", example = "13449")
    private String zipCode;
    
    @ApiModelProperty(notes = "사업자등록번호", example="123-45-67890")
    private String compRegNum;
    
    @ApiModelProperty(notes = "대표자명", example="대표자명")
    private String ownerName;

    @ApiModelProperty(notes = "내상점소개", example = "중고나라1호점입니다.")
    private String shopDesc;

    @ApiModelProperty(notes = "등록일자", example="2020-02-22")
    private String regDate;
    
    @ApiModelProperty(notes = "이미지파일 URL", example="http://d1n8fbd6e2ut61.cloudfront.net/asset/upload/product/20200220/2e91cd94-1cc3-48bc-84d7-f8171992276b")
    private String fileUrl;
    
    @ApiModelProperty(notes = "실제파일명", example="3333333.png")
    private String fileName;
    
    @ApiModelProperty(notes = "카테고리 리스트")
    private List<ShopCategoryResponse> shopCategoryResponse;
    
    @ApiModelProperty(notes = "상점지역 리스트")
    private List<PackpageShopArea> shopAreaResponse;
    
    @ApiModelProperty(notes = "회원 구분")
    private String memKind;

}