package com.gk337.api.shop.entity;

import com.gk337.common.core.domain.PKEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ShopAreaGroupPK implements Serializable, PKEntity {

	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	private Integer shopSeq;                   

	@EqualsAndHashCode.Include
	private Integer areaGroupSeq;

}
