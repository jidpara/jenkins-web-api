package com.gk337.api.shop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상점
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "사업자등록번호 체크 결과 Entity")
public class BizNumCheckResponse implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "결과코드", example="Y")
    private String resultCode;

    @ApiModelProperty(notes = "결과메시지", example="사용가능한 사업자등록번호입니다.")
    private String resultMessage;

}