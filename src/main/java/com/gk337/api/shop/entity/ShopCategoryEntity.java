package com.gk337.api.shop.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상점카테고리
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@IdClass(ShopCategoryPK.class)
@Table(name = "jw_shop_category")
@ApiModel(description = "상점정보 카테고리 Entity")
public class ShopCategoryEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "shop_seq", nullable = false)
    @ApiModelProperty(notes = "상점시퀀스")
    private Integer shopSeq;

    @Id
    @Column(name = "cate_seq", nullable = false)
    @ApiModelProperty(notes = "카테고리시퀀스")
    private Integer cateSeq;
    
    
}