package com.gk337.api.shop.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상점
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "내 상점 정보 메인 Entity")
public class ShopMainResponse implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "구분", example="받은요청서카운트")
    private String type;

    @ApiModelProperty(notes = "건수", example="20")
    private String cnt;

}