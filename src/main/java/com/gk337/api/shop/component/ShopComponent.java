package com.gk337.api.shop.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gk337.api.shop.entity.ShopInfoRequest;
import com.gk337.api.shop.entity.ShopInfoResponse;
import com.gk337.api.shop.service.ShopService;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.exception.JGWServiceException;

@Component
public class ShopComponent {

	@Autowired
	private ShopService shopService;

	@Value("${test.enable}")
	private boolean testEnabled;

	@Value("${test.biz-num}")
	private String testBizNum;
	
	@Transactional
	public ShopInfoResponse insertShopInfo(ShopInfoRequest request) {
		ShopInfoResponse requestResponse = new ShopInfoResponse();
		try {

			// 이미 등록되어 있는지 memSeq 로 중복여부 체크.
			Integer shopSeq = shopService.bizExistCheck(request.getMemSeq());
			if(shopSeq > 0) {
				request.setSeq(shopSeq);
				// TODO : 딜러(상점) 최초 등록 시에 fileSeq 는 사업자 등록증 첨부파일임.  프로필 이미지로 업데이트 되서는 안됨. 아래 코드 주석 처리 - 황세준
//				String profileImage = shopService.selectProfileImage(request.getFileSeq());
//				shopService.updateProfileImage(profileImage, request.getMemSeq());
			}

			requestResponse = shopService.insertShopInfo(request);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "저장 또는 수정");
		}
		return requestResponse;
		
	}
	
}
