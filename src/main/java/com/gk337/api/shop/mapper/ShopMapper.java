package com.gk337.api.shop.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.payment.entity.PackpageShopArea;
import com.gk337.api.request.entity.RequestResponse;
import com.gk337.api.shop.entity.ShopInfoRequest;
import com.gk337.api.shop.entity.ShopInfoResponse;
import com.gk337.api.shop.entity.ShopMainResponse;
import com.gk337.api.shop.entity.ShopMyProductResponse;

@Mapper
public interface ShopMapper {
	
	/**
	 * 요청서 상세보기.
	 * @param seq
	 * @return
	 */
	public RequestResponse selectReqeustInfo(int seq);
	
	/**
	 * 회원시퀀스 상점 정보 조회.
	 * @param seq
	 * @return
	 */
	public ShopInfoResponse selectShopInfo(int seq);
	
	/**
	 * 회원시퀀스 타인상점 정보 조회.
	 * @param seq
	 * @return
	 */
	public ShopInfoResponse selectDealerShopInfo(int seq);

	public List<PackpageShopArea> selectPackageShopArea(int seq);
	/**
	 * 
	 * @param ShopSeq
	 * @return
	 */
	public ShopInfoResponse selectShop(int ShopSeq);
	
	/**
	 * 
	 * @param request
	 */
	public void updateShopInfo(ShopInfoRequest request);
	
	/**
	 * 내 상점 메인페이지 정보 조회. - 딜러.
	 * @param memSeq
	 * @return
	 */
	public List<ShopMainResponse> selectShopMainInfoDealer(int memSeq);
	public List<ShopMainResponse> selectShopMainInfoDealerNew(int memSeq);
	
	/**
	 * 내 상점 메인페이지 정보 조회. - 유저.
	 * @param memSeq
	 * @return
	 */
	public List<ShopMainResponse> selectShopMainInfoUser(int memSeq);
	public List<ShopMainResponse> selectShopMainInfoUserNew(int memSeq);
	
	/**
	 * 내물품리스트 조회.
	 * @param memSeq
	 * @return
	 */
	public List<ShopMyProductResponse> selectMyProductList(int memSeq);
	public List<ShopMyProductResponse> selectMyProductListNew(int memSeq);
	public List<ShopMyProductResponse> selectDealerListNew(int memSeq);
	
	
	/**
	 * 메인 회원수,딜러수,물품수카운트
	 * @return
	 */
	public List<ShopMainResponse> selectMainInfoCountList();
	
	/**
	 * 사업자등록번호 중복조회.
	 * @param compRegNum
	 * @return
	 */
	public String bizNumDupCheck(String compRegNum);
	
	/**
	 * 상점등록여부 조회.
	 * @param hashMap
	 * @return
	 */
	public Integer bizExistCheck(int memSeq);
	
	public String selectProfilImage(int fileSeq);
	
	

}
