package com.gk337.api.product.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.gk337.api.product.entity.ProductAllResponse;
import com.gk337.api.product.entity.ProductEntity;
import com.gk337.api.product.entity.ProductRequest;
import com.gk337.api.product.service.ProductService;
import com.gk337.api.shop.entity.ShopMyProductResponse;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.exception.JGWServiceException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 상품 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 상품관련 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.01.26
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/products")
@Api(tags= {"[기준정보] - 상품 API"}, protocols="http", produces="application/json", consumes="application/json")
public class ProductRestController {

	@Autowired
	private ProductService productService;

	@ApiOperation(value = "상품저장 API", notes = "상품저장 API")
	@RequestMapping(method = RequestMethod.POST, path = "/", produces = "application/json")
	public ResponseEntity<ProductEntity> save(@RequestBody @Valid ProductRequest request) {
		return BaseResponse.ok(productService.saveOrUpdate(request));
	}

	@ApiOperation(value = "상품수정 API", notes = "상품수정 API")
	@RequestMapping(method = RequestMethod.PUT, path = "/{prodSeq}", produces = "application/json")
	public ResponseEntity<ProductEntity> update(
			@ApiParam(value = "상품시퀀스", required = true, defaultValue = "1") @PathVariable(value = "prodSeq", required = true) int prodSeq,
			@RequestBody @Valid ProductRequest request) {
		request.setSeq(prodSeq);
		return BaseResponse.ok(productService.saveOrUpdate(request));
	}

	@ApiOperation(value = "상품삭제 API.", notes = "상품삭제 API")
	@RequestMapping(method = RequestMethod.DELETE, path = "/{prodSeq}", produces = "application/json")
	public ResponseEntity<ProductEntity> deleteBySeq(
			@ApiParam(value = "요청서시퀀스", required = true, defaultValue = "1") @PathVariable(value = "prodSeq", required = true) int prodSeq) {
		return BaseResponse.ok(productService.deleteBySeq(prodSeq));
	}

	@ApiOperation(value = "상품목록 조회 API.", notes = "상품목록 조회 API.")
	@RequestMapping(method = RequestMethod.GET, path = "/", produces = "application/json")
	public ResponseEntity<List<ProductEntity>> findAll() throws JGWServiceException {
		return BaseResponse.ok(productService.findAll());
	}

	@ApiOperation(value = "상품정보 상세조회 API", notes = "상품정보 상세조회 API")
	@RequestMapping(method = RequestMethod.GET, path = "/{seq}", produces = "application/json")
	public ResponseEntity<List<ProductEntity>> findBySeq(
			@ApiParam(value = "상품Seq", required = true, defaultValue = "1") @PathVariable(value = "seq", required = true) int seq) {
		return BaseResponse.ok(productService.findBySeq(seq));
	}

	@ApiOperation(value = "상품목록조회 페이징.", notes = "페이징으로 상품목록조회.")
	@RequestMapping(method = RequestMethod.GET, path = "/{page_no}/{page_size}", produces = "application/json")
	public Page<ProductEntity> findAllPaging(
			@ApiParam(value = "페이지번호", required = true) @PathVariable(value = "page_no", required = true) int pNo,
			@ApiParam(value = "사이즈", required = true) @PathVariable(value = "page_size", required = true) int size
	) {
		Direction dir = Direction.DESC;
		@SuppressWarnings("deprecation")
		PageRequest pageable = new PageRequest(pNo - 1, size, dir, "seq");
		return productService.findAllPaging(pageable);
	}

	@ApiOperation(value = "검색조건으로 상품목록조회", notes = "검색조건으로 상품목록조회.")
	@RequestMapping(method = RequestMethod.POST, path = "/search", produces = "application/json")
	public ResponseEntity<List<ProductEntity>> findBySearchKey(@RequestBody @Valid ProductRequest request) {
		return BaseResponse.ok(productService.findBySearchKey(request));
	}	
	
	@ApiOperation(value = "상점 전체 물품 목록 조회 API", notes = "상점 전체 물품 목록 조회 API", response=ShopMyProductResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/all/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<ProductAllResponse>> selectAllProductList(
			Pageable pageable,
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="검색어", required=false, example="검색") @RequestParam(value = "search", required = false, defaultValue = "") String search,
			@ApiParam(value="정렬 조건[높은 가격순, 낮은 가격순,최신순]", required=false, example="regDate") @RequestParam(value = "orderBy", required = false, defaultValue = "") String orderBy,
			@ApiParam(value="카테고리 검색[1dp]", required=false, example="1") @RequestParam(value = "category", required = false, defaultValue = "")  String category,			
			@ApiParam(value="유저 형태 검색", required=false, example="P") @RequestParam(value = "memKind", required = false, defaultValue = "")  String memKind,
			@ApiParam(value="거래 형태 검색", required=false, example="S") @RequestParam(value = "tradeType", required = false, defaultValue = "")  String tradeType,
			@ApiParam(value="회원 번호", required=true, example="1") @RequestParam(value = "memSeq", required = false, defaultValue = "")  Integer memSeq
			
			){	
		
		
		Map<String,Object> map = new HashMap<>();
		map.put("search", search);
		map.put("orderBy", orderBy);
		map.put("category", category);		
		map.put("memKind", memKind);
		map.put("tradeType", tradeType);
		map.put("memSeq", memSeq);
		
		pageable = PageRequest.of(pageable.getPageNumber(), pageSize);
		
		
		return BaseResponse.ok(productService.selectAllProductList(pageNo, pageSize,map)); 
	}
	
	@ApiOperation(value = "물품 거래완료 ", notes = "물품 거래완료")
	@RequestMapping(method = RequestMethod.PUT, path="/status/complete/{prodSeq}", produces = "application/json")
	public ResponseEntity<ProductEntity> updateProductCompleteYn(
			@ApiParam(value="물품 시퀀스", required=true, example="1") @PathVariable(value = "prodSeq", required = true) int prodSeq
			){	
		
		
		return BaseResponse.ok(productService.updateTradeComplete(prodSeq,"Y")); 
	}
	
	@ApiOperation(value = "물품 보이기 숨기기 ", notes = "물품 보이기 숨기기")
	@RequestMapping(method = RequestMethod.PUT, path="/status/show/{prodSeq}", produces = "application/json")
	public ResponseEntity<ProductEntity> updateProductShowYn(
			@ApiParam(value="물품 시퀀스", required=true, example="1") @PathVariable(value = "prodSeq", required = true) int prodSeq,
			@ApiParam(value="보이기/숨기기", required=false, example="Y") @RequestParam(value = "showType", required = false, defaultValue = "Y")  String showType
			){	
		
		
		return BaseResponse.ok(productService.updateShowStatus(prodSeq,showType)); 
	}
	
	@ApiOperation(value = "물품 보관 ", notes = "물품 보관")
	@RequestMapping(method = RequestMethod.PUT, path="/status/temp/{prodSeq}", produces = "application/json")
	public ResponseEntity<ProductEntity> updateProductTempYn(
			@ApiParam(value="물품 시퀀스", required=true, example="1") @PathVariable(value = "prodSeq", required = true) int prodSeq,
			@ApiParam(value="보관여부", required=false, example="Y") @RequestParam(value = "tmepType", required = false, defaultValue = "Y")  String tempType
			){	
		
		
		return BaseResponse.ok(productService.updateTempStatus(prodSeq,tempType)); 
	}
	
	
}


/*
{
  "seq": 4,
  "cstatus": "Y",
  "memSeq": 1,
  "cateSeq1dp": 1,
  "cateSeq2dp": 1001,
  "cateSeq3dp": 1001015,
  "prodName": "알록달록커튼2",
  "price": 250000,
  "prodDesc": "커튼팔아요 봉은 서비스222222222",
  "asYn": "Y",
  "deliveryPriceYn": "Y",
  "prodStatus": "A",
  "keyWords": "산지이틀,급전필요,직거래가능",
  "readCnt": 0,
  "recommendYn": null
}
*/
