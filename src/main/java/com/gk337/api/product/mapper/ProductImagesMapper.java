package com.gk337.api.product.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.product.entity.ProductRequest;
import com.gk337.api.product.entity.ProductEntity;
import com.gk337.api.product.entity.ProductImagesResponse;

@Mapper
public interface ProductImagesMapper {

	List<ProductImagesResponse> selectProductImagesListBySeq(int prodSeq);
}
