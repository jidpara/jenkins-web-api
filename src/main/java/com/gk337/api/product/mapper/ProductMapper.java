package com.gk337.api.product.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.product.entity.ProductAllResponse;
import com.gk337.api.product.entity.ProductEntity;
import com.gk337.api.product.entity.ProductRequest;

@Mapper
public interface ProductMapper {

	public List<ProductEntity> findBySearchKey(ProductRequest request);
	
	public List<ProductAllResponse> selectAllProductList(Map<String,Object> params);
}
