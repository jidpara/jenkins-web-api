package com.gk337.api.product.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gk337.api.product.entity.ProductAllResponse;
import com.gk337.api.product.entity.ProductAttachFileEntity;
import com.gk337.api.product.entity.ProductEntity;
import com.gk337.api.product.entity.ProductRequest;
import com.gk337.api.product.entity.ProductResponse;
import com.gk337.api.product.mapper.ProductMapper;
import com.gk337.api.product.repository.ProductAttachFileRepository;
import com.gk337.api.product.repository.ProductRepository;
import com.gk337.api.request.entity.RequestProductAtfilRequest;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductMapper productMapper;
	
	@Autowired
	private ProductAttachFileRepository productAttachFileRepository;
	
	/**
	 * 상품정보 저장 또는 수정.
	 * @param request
	 * @return
	 */
	@Transactional
	public ProductResponse saveOrUpdate(ProductRequest request) {
		ProductResponse response = new ProductResponse();
		try {
			ProductEntity entity = new ProductEntity();
			BeanUtils.copyProperties(request, entity);
			entity = productRepository.save(entity);
			
			int prodSeq = entity.getSeq();

			// 상품이미지저장.
			List<RequestProductAtfilRequest> attachFiles = request.getAttachFiles();
			if(!"".equals(attachFiles)){
				productAttachFileRepository.deleteByprodSeq(prodSeq);

			if(attachFiles.size() > 0) {
				for(int i=0; i<attachFiles.size(); i++) {
					RequestProductAtfilRequest atfilRequest = (RequestProductAtfilRequest) attachFiles.get(i);
					ProductAttachFileEntity productAtfilEntity = new ProductAttachFileEntity();
					productAtfilEntity.setFileSeq(atfilRequest.getFileSeq());
					productAtfilEntity.setProdSeq(prodSeq);
					productAtfilEntity.setOrderBy(i+1);
					productAtfilEntity.setMemSeq(request.getMemSeq());
					productAtfilEntity.setMainYn(atfilRequest.getMainYn());
					if(i == 0) {
						productAtfilEntity.setMainYn("Y");
					}
					insertProductAttachFile(productAtfilEntity);
				}
			}
			}
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.0001", "저장 또는 수정");
		}
		return response;
	}
	
	/**
	 * 상품정보이미지 저장 또는 수정.
	 * @param request
	 * @return
	 */
	public ProductAttachFileEntity insertProductAttachFile(ProductAttachFileEntity request) {
		ProductAttachFileEntity entity = new ProductAttachFileEntity();
		try {
			entity = productAttachFileRepository.saveAndFlush(request);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return entity;
	}
	
	
	/**
	 * 시퀀스로 상품정보 삭제.
	 * @param seq
	 * @return
	 */
	public ErrorVo deleteBySeq(int prodSeq) {
		ErrorVo result = new ErrorVo();
		try {
			productRepository.deleteBySeq(prodSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.0001", "저장 또는 수정");
		}
		return result;
	}
	
	/**
	 * 상품전제조회.
	 * @return
	 */
	public ListVo<ProductEntity> findAll() {
		ListVo<ProductEntity> listVo = null;
		try {
			listVo = new ListVo<ProductEntity>(productRepository.findAll());
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.0001", "목록조회");
		}
		return listVo;
	}
	
	/**
	 * 상품정보 상세조회.
	 * @param seq
	 * @return
	 */
	public ProductEntity findBySeq(int seq) {
		ProductEntity response = new ProductEntity();
		try {
			response = productRepository.findBySeq(seq);
			response.setRegDateR(response.getRegDate());
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.0001", "Seq로 조회");
		}
		return response;
	}

	/**
	 * 상품정보 페이징 리스트.
	 * @param pageable
	 * @return
	 */
	public Page<ProductEntity> findAllPaging(Pageable pageable) {
		Page<ProductEntity> pageList = null;
		try {
			pageList = productRepository.findAll(pageable);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.0001", "페이징조회");
		}
		return pageList;
	}
	
	/**
	 * 검색조건으로 상품목록 조회.
	 * @param requestDto
	 * @return
	 */
	public ListVo<ProductEntity> findBySearchKey(ProductRequest request) {
		ListVo<ProductEntity> listVo = null;
		try {
			listVo = new ListVo<ProductEntity>(productMapper.findBySearchKey(request));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.0001", "조건조회");
		}
		return listVo;
	}
	
	/**
	 * 전체 물품 리스트 조회 [페이징]
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public PageInfo<ProductAllResponse> selectAllProductList(int pageNo, int pageSize,Map<String,Object> params) {
		
		
		PageInfo<ProductAllResponse> pageInfo = new PageInfo<ProductAllResponse>();
		try {
			
			int totalSize = productMapper.selectAllProductList(params).size();
			PageHelper.startPage(pageNo, pageSize); 
			List<ProductAllResponse> list = productMapper.selectAllProductList(params);
			pageInfo = new PageInfo<>(list);
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.SHOP.0001", "전체 물품 조회");
		}
		
		return pageInfo;
	}
	
	public Page<ProductEntity> getAllProductList(	Pageable pageable,	String search,	String orderBy, String category, String memKind, String tradeType){
		
		@SuppressWarnings("serial")
		Specification<ProductEntity> specification = new Specification<ProductEntity>() {
			@Override
			public Predicate toPredicate(Root<ProductEntity> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicates = new ArrayList<>();
				
				if(!category.equals("all")) {
					predicates.add(builder.equal(root.get("cateSeq1dp"), category));
				}
				
				/*
				 * Join<ProductEntity,MemberEntity> join = root.join("member");
				 * if(!memKind.equals("")) { predicates.add(builder.equal(join.get("memKind"),
				 * memKind)); }
				 */
				
				if(!search.equals("")) {
					predicates.add(builder.like(root.get("prodName"), '%' + search+'%'));
				}
				
				if(!tradeType.equals("")) {
					predicates.add(builder.equal(root.get("tradeType"), tradeType));
				}
				
				predicates.add(builder.equal(root.get("showYn"), "Y"));
				predicates.add(builder.equal(root.get("tempYn"), "N"));
				predicates.add(builder.notEqual(root.get("cstatus"), "D"));
				
				query.where(predicates.toArray(new Predicate[] {}));
				
				
				if(!orderBy.equals("")) {
					if(orderBy.equals("lowPrice")) {
						query.orderBy(builder.desc(root.get("price")));						
					}else if(orderBy.equals("highPrice")) {
						query.orderBy(builder.desc(root.get("price")));						
					}else if(orderBy.equals("regDate")) {
						query.orderBy(builder.desc(root.get("regDate")));						
					}
					
				}
				
				
				return builder.and(predicates.toArray(new Predicate[] {}));
			}		
			
		};
		return productRepository.findAll(specification,pageable);
		
	}
	
	public ProductResponse updateTradeComplete(Integer prodSeq,String completeYn) {
		
		ProductResponse response = new ProductResponse();
		try {
			
			productRepository.updateCompleteStatus(prodSeq, completeYn, LocalDateTime.now());
			
		}catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.0001", "저장 또는 수정");
		}
		return response;
		
	}
	
	public ProductResponse updateShowStatus(Integer prodSeq,String showYn) {
		
		ProductResponse response = new ProductResponse();
		try {
			
			productRepository.updateShowStatus(prodSeq, LocalDateTime.now());			
						
		}catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.0001", "저장 또는 수정");
		}
		return response;
		
	}
	
	public ProductResponse updateTempStatus(Integer prodSeq,String tempYn) {
		
		ProductResponse response = new ProductResponse();
		try {
			
			productRepository.updateTempStatus(prodSeq, tempYn, LocalDateTime.now());			
						
		}catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.0001", "저장 또는 수정");
		}
		return response;
		
	}
	
	

}
