package com.gk337.api.product.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gk337.api.product.entity.ProductEntity;
import com.gk337.api.product.entity.ProductImagesEntity;
import com.gk337.api.product.entity.ProductImagesRequest;
import com.gk337.api.product.entity.ProductImagesResponse;
import com.gk337.api.product.entity.ProductRequest;
import com.gk337.api.product.entity.ProductResponse;
import com.gk337.api.product.mapper.ProductImagesMapper;
import com.gk337.api.product.repository.ProductImagesRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProductImagesService {

	@Autowired
	private ProductImagesRepository productImagesRepository;
	
	@Autowired
	private ProductImagesMapper productImagesMapper;
	
	/**
	 * 상품 이미지 디비 저장 또는 수정.
	 * @param request
	 * @return
	 */
	public ProductImagesResponse saveOrUpdate(ProductImagesRequest request) {
		ProductImagesResponse response = new ProductImagesResponse();
		try {
			ProductImagesEntity entity = new ProductImagesEntity();
			BeanUtils.copyProperties(request, entity);
			entity = productImagesRepository.save(entity);
			BeanUtils.copyProperties(entity, response);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.IMAGES.0001");
		}
		return response;
	}
	
	/**
	 * 상품이미지 디비 목록조회.
	 * @param seq
	 * @return
	 */
	public ListVo<ProductImagesResponse> selectProductImagesListBySeq(int prodSeq) {
		ListVo<ProductImagesResponse> listVo = null;
		try {
			listVo = new ListVo<ProductImagesResponse>(productImagesMapper.selectProductImagesListBySeq(prodSeq));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.IMAGES.0001");
		}
		return listVo;
	}

	/**
	 * 시퀀스로 상품정보 삭제.
	 * @param seq
	 * @return
	 */
//	public ErrorVo deleteBySeq(int prodSeq) {
//		ErrorVo result = new ErrorVo();
//		try {
//			productRepository.deleteBySeq(prodSeq);
//		} catch (BizException be) {
//			throw new JGWServiceException(be.getMessage());
//		} catch(Exception e) {
//			e.printStackTrace();
//			throw new JGWServiceException("ERROR.PRODUCT.0001", "저장 또는 수정");
//		}
//		return result;
//	}

}
