package com.gk337.api.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gk337.api.product.entity.ProductAttachFileEntity;
import com.gk337.api.product.entity.ProductEntity;

public interface ProductAttachFileRepository  extends PagingAndSortingRepository<ProductAttachFileEntity, Integer>,  JpaRepository<ProductAttachFileEntity, Integer>{
	void deleteByprodSeq(Integer prodSeq);
}
