package com.gk337.api.product.repository;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gk337.api.product.entity.ProductEntity;

@Repository
@SuppressWarnings("unchecked")
public interface ProductRepository extends JpaSpecificationExecutor<ProductEntity>,  JpaRepository<ProductEntity, Integer> {

	ProductEntity save(ProductEntity entity);

	@Transactional
	void deleteBySeq(int seq);
	
	ProductEntity findBySeq(Integer seq);
	
	List<ProductEntity> findAll();
	
	Page<ProductEntity> findAll(Pageable pageable);
	

	
	// 상태값 (I:거래중, Y:거래완료, C:거래취소 D:삭제)
	@Modifying
	@Transactional
	@Query("update ProductEntity q set q.completeYn = :completeYn , q.chgDate = :chgDate where q.seq = :seq")
	void updateCompleteStatus(@Param("seq") int seq, @Param("completeYn") String completeYn,LocalDateTime chgDate);
	
	/*
	@Modifying
	@Transactional
	@Query("update ProductEntity q set q.showYn =:showYn , q.chgDate =:chgDate where q.seq = :seq")
	void updateShowStatus(@Param("seq") int seq, @Param("showYn") String showYn,LocalDateTime chgDate);
	*/
	
	@Modifying
	@Transactional
	@Query("update ProductEntity q set q.showYn =CASE WHEN q.showYn = 'Y' Then 'N' WHEN q.showYn = 'N' THEN 'Y' END , q.chgDate =:chgDate where q.seq = :seq")
	void updateShowStatus(@Param("seq") int seq ,LocalDateTime chgDate);
	
	@Modifying
	@Transactional
	@Query("update ProductEntity q set q.tempYn =:tempYn , q.chgDate =:chgDate where q.seq = :seq")
	void updateTempStatus(@Param("seq") int seq, @Param("tempYn") String tempYn,LocalDateTime chgDate);
	

	// 상태값 (I:거래중, Y:거래완료, C:거래취소 D:삭제)
	@Modifying
	@Query("update ProductEntity q set q.completeYn = :reqStatus where q.seq = :reqSeq")
	void updateRequestStatus(@Param("reqSeq") int reqSeq, @Param("reqStatus") String reqStatus);

}