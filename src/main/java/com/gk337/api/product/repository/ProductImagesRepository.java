package com.gk337.api.product.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.product.entity.ProductImagesEntity;

@Repository
@SuppressWarnings("unchecked")
public interface ProductImagesRepository extends PagingAndSortingRepository<ProductImagesEntity, Integer>,  JpaRepository<ProductImagesEntity, Integer> {

	ProductImagesEntity save(ProductImagesEntity entity);
	
	void deleteBySeq(int seq);
	
}