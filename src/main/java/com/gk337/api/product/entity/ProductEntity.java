package com.gk337.api.product.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.gk337.api.base.entity.MemberEntity;
import com.gk337.api.shop.entity.ShopInfoEntity;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상품
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_product")
@ApiModel(description = "상품 정보 Entity")
public class ProductEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @Column(name = "cstatus", nullable = false)
    @ApiModelProperty(notes = "상태값 (Y:판매중, H:대기, E:마감, N:차단, D:삭제)")
    private String cstatus;

    @Column(name = "mem_seq", nullable = false)
    @ApiModelProperty(notes = "회원시퀀스")
    private Integer memSeq;

    @Column(name = "cate_seq_1dp", nullable = false)
    @ApiModelProperty(notes = "1차카테고리시퀀스")
    private Integer cateSeq1dp;

    @Column(name = "cate_seq_2dp")
    @ApiModelProperty(notes = "2차카테고리시퀀스")
    private Integer cateSeq2dp;

    @Column(name = "cate_seq_3dp")
    @ApiModelProperty(notes = "3차카테고리시퀀스")
    private Integer cateSeq3dp;

    @Column(name = "prod_name", nullable = false)
    @ApiModelProperty(notes = "상품명")
    private String prodName;

    @Column(name = "price", nullable = false)
    @ApiModelProperty(notes = "가격")
    private Integer price;

    @Column(name = "prod_desc", nullable = false)
    @ApiModelProperty(notes = "상품정보")
    private String prodDesc;

    @Column(name = "as_yn", nullable = false)
    @ApiModelProperty(notes = "AS가능여부")
    private String asYn;

    @Column(name = "delivery_price_yn", nullable = false)
    @ApiModelProperty(notes = "택배비포함여부")
    private String deliveryPriceYn;

    @Column(name = "prod_status", nullable = false)
    @ApiModelProperty(notes = "물품상태 (S:미사용, A:거의새것, B:중고, C:하자)")
    private String prodStatus;

    @Column(name = "key_words")
    @ApiModelProperty(notes = "상품검색 키워드 (총5개까지 콤마로 구분)")
    private String keyWords;

    @Column(name = "read_cnt")
    @ApiModelProperty(notes = "조회수")
    private Integer readCnt;

    @Column(name = "recommend_yn")
    @ApiModelProperty(notes = "추천구분")
    private String recommendYn;
    
    @Column(name = "trade_type")
    @ApiModelProperty(notes = "거래 타입")
    private String tradeType;

    @Column(name = "address")
    @ApiModelProperty(notes = "전체주소")
    private String address;

    @Column(name = "address_div1")
    @ApiModelProperty(notes = "도/시 이름")
    private String addressDiv1;

    @Column(name = "address_div2")
    @ApiModelProperty(notes = "시/군/구 이름")
    private String addressDiv2;

    @Column(name = "address_div3")
    @ApiModelProperty(notes = "법정동/법정리 이름")
    private String addressDiv3;
    

    @Column(name = "complete_yn")
    @ApiModelProperty(notes = "구매 완료 구분(I:거래중, Y:거래완료, C:거래취소 D:삭제) ")
    private String completeYn;

    @Column(name = "show_yn")
    @ApiModelProperty(notes = "노출 여부")
    private String showYn;
    
    @Column(name = "temp_yn")
    @ApiModelProperty(notes = "보관 여부")
    private String tempYn;
    
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="prod_seq" , referencedColumnName = "seq" ,insertable = false, updatable = false)
    private List<ProductAttachFileEntity> attachFileList;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="mem_seq" , referencedColumnName = "seq" ,insertable = false, updatable = false)
    private MemberEntity member;
    
    @OneToOne(fetch = FetchType.EAGER)
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name="mem_seq" , referencedColumnName = "mem_seq" ,insertable = false, updatable = false)
    private ShopInfoEntity shopInfo;
    
    @Transient
    private LocalDateTime regDateR;

    
}