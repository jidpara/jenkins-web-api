package com.gk337.api.product.entity;

import java.time.LocalDateTime;
import java.util.List;

import com.gk337.api.request.entity.RequestProductAtfilResponse;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "모든 물품 Entity")
public class ProductAllResponse {
	
	@ApiModelProperty(notes = "상품 구분", example = "요청서")
	private String prodType;
	
	@ApiModelProperty(notes = "상품 시퀀스", example = "12")
	private Integer seq;
	
	@ApiModelProperty(notes = "상품명", example = "냉장고")
	private String prodName;
	
	@ApiModelProperty(notes = "[미사용]상품 상태", example = "S")
	private String prodStatus;
	
	@ApiModelProperty(notes = "상품 설명", example = "상품 설명")
	private String prodDesc;
	
	@ApiModelProperty(notes = "등록일", example = "2020-12-12")
	private String regDate;
	
	private List<RequestProductAtfilResponse> requestProductAtfilResponse; 
	
	@ApiModelProperty(notes = "상품 가격", example = "12000")
	private Integer price;
	
	@ApiModelProperty(notes = "상품 상태", example = "S")	
	private String cstatus;
	
	@ApiModelProperty(notes = "거래 타입", example = "판매")
	private String tradeType;
	
	@ApiModelProperty(notes = "카테고리 1dp", example = "대분류")
	private Integer cateSeq1dp;
	
	@ApiModelProperty(notes = "카테고리 2dp", example = "중분류")
	private Integer cateSeq2dp;
	
	@ApiModelProperty(notes = "카테고리 3dp", example = "소분류")
	private Integer cateSeq3dp;
	
	@ApiModelProperty(notes = "멤버 시퀀스", example = "1")
	private Integer memSeq;
	
	@ApiModelProperty(notes = "멤버 종류", example = "P")
	private String memKind;
	
	@ApiModelProperty(notes = "날짜 변환", example = "2020-01-01")
	private String regDateT;
	
	@ApiModelProperty(notes = "as 유무", example = "Y")
	private String asYn;
	
	
	

}
