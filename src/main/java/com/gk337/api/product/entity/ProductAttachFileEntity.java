package com.gk337.api.product.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.gk337.api.base.entity.BaseUploadS3Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_product_attach_file")
@ApiModel(description = "요청상품 이미지 Entity")
public class ProductAttachFileEntity extends AuditEntity implements Serializable{

	private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @NotNull
    @Column(name = "cstatus")
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";
    
    @NotNull
    @Column(name = "prod_seq")
    @ApiModelProperty(notes = "상품시퀀스")
    private Integer prodSeq;
    
    @NotNull
    @Column(name = "mem_seq")
    @ApiModelProperty(notes = "등록한 회원시퀀스")
    private Integer memSeq;
    
    @Column(name = "main_yn")
    @ApiModelProperty(notes = "메인여부")
    private String mainYn;

    @NotNull
    @Column(name = "file_seq")
    @ApiModelProperty(notes = "이미지시퀀스")
    private Integer fileSeq;

    @NotNull
    @Column(name = "order_by")
    @ApiModelProperty(notes = "정렬순번")
    private Integer orderBy;
    
    @OneToOne
    @JoinColumn(name="file_seq" ,referencedColumnName = "seq" ,insertable = false, updatable = false)
    private BaseUploadS3Entity file;
    
}
