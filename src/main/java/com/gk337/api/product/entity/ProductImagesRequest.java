package com.gk337.api.product.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "상품 이미지 REQUEST VO")
public class ProductImagesRequest implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "상품시퀀스")
    private Integer prodSeq;

    @ApiModelProperty(notes = "메인이미지여부")
    private String mainYn = "N";

    @ApiModelProperty(notes = "상품이미지 파일명")
    private String filename1;

    @ApiModelProperty(notes = "상품이미지 배경색")
    private String filename1Hex;

    @ApiModelProperty(notes = "순서")
    private Integer orderBy;

    @ApiModelProperty(notes = "원본파일명")
    private Integer orgFilename;
        
}