package com.gk337.api.product.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "전체 물품 REQUEST VO")
public class ProductAllRequest {
	
	@ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
	private String search;
	@ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
	private String orderBy;
	@ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
	private String category;
	@ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
	private String memKind;
	
	@ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
	private String tradeType;
	
	
	

}
