package com.gk337.api.product.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 상품이미지
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_product_images")
@ApiModel(description = "상품 이미지 Entity")
public class ProductImagesEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @Column(name = "cstatus", nullable = false)
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";

    @Column(name = "prod_seq", nullable = false)
    @ApiModelProperty(notes = "상품시퀀스")
    private Integer prodSeq;

    @Column(name = "main_yn")
    @ApiModelProperty(notes = "메인이미지여부")
    private String mainYn = "N";

    @Column(name = "filename1", nullable = false)
    @ApiModelProperty(notes = "상품이미지 파일명")
    private String filename1;

    @Column(name = "filename1_hex", nullable = false)
    @ApiModelProperty(notes = "상품이미지 배경색")
    private String filename1Hex;

    @Column(name = "order_by", nullable = false)
    @ApiModelProperty(notes = "순서")
    private Integer orderBy;

    @Column(name = "org_filename", nullable = false)
    @ApiModelProperty(notes = "원본파일명")
    private Integer orgFilename;
        
}