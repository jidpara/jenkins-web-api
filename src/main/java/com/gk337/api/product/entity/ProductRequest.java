package com.gk337.api.product.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.api.product.repository.ProductAttachFileRepository;
import com.gk337.api.request.entity.RequestProductAtfilRequest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "상품정보 REQUEST VO")
public class ProductRequest {

	@JsonIgnore
    @ApiModelProperty(notes = "SEQ", example = "1")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:판매중, H:대기, E:마감, N:차단, D:삭제)", example = "Y")
    private String cstatus= "Y";

    @ApiModelProperty(notes = "회원시퀀스", example = "1")
    private Integer memSeq;
    
    @ApiModelProperty(notes = "1차카테고리시퀀스")
    private Integer cateSeq1dp;

    @ApiModelProperty(notes = "2차카테고리시퀀스")
    private Integer cateSeq2dp;

    @ApiModelProperty(notes = "3차카테고리시퀀스")
    private Integer cateSeq3dp;

    @ApiModelProperty(notes = "상품명")
    private String prodName;

    @ApiModelProperty(notes = "가격")
    private Integer price;

    @ApiModelProperty(notes = "상품정보")
    private String prodDesc;

    @ApiModelProperty(notes = "AS가능여부")
    private String asYn;

    @ApiModelProperty(notes = "택배비포함여부")
    private String deliveryPriceYn;
    
    @ApiModelProperty(notes = "조회수")
    private Integer readCnt = 0;

    @ApiModelProperty(notes = "물품상태 (S:미사용, A:거의새것, B:중고, C:하자)")
    private String prodStatus;

    @ApiModelProperty(notes = "상품검색 키워드 (총5개까지 콤마로 구분)")
    private String keyWords;
    
    @ApiModelProperty(notes = "카테고리 1단게", example = "[\"1\",\"2\",\"3\"]")
    List<String> category1DptList = new ArrayList<String>();
    
    @ApiModelProperty(notes = "카테고리 2단계", example = "[\"10001\",\"1002\",\"1003\"]")
    List<String> category2DptList = new ArrayList<String>();
    
    @ApiModelProperty(notes = "카테고리 3단계", example = "[\"10001015\",\"1001016\",\"1001017\"]")
    List<String> category3DptList = new ArrayList<String>();
    
    @ApiModelProperty(notes = "보관 여부")
    private String tempYn;
    
    @ApiModelProperty(notes = "거래타입")
    private String tradeType;
    
    @ApiModelProperty(notes = "업로드 파일")
    private List<RequestProductAtfilRequest> attachFiles;

    @ApiModelProperty(notes = "주소 - 전체", example = "경기도 이천시 부발읍 아미리 726-5")
    private String address;

    @ApiModelProperty(notes = "주소 - 시도", example = "경기도")
    private String addressDiv1;

    @ApiModelProperty(notes = "주소 - 구군", example = "이천시")
    private String addressDiv2;

    @ApiModelProperty(notes = "주소 - 읍면동", example = "부발읍")
    private String addressDiv3;

    @ApiModelProperty(notes = "거래 완료 여부(Y:완료 N:미완료)", example = "N")
    private String completeYn;

    @ApiModelProperty(notes = "상품보이기(Y:보이기, N:숨기기)", example = "Y")
    private String showYn;

}
