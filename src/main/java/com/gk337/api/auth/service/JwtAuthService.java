package com.gk337.api.auth.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.gk337.api.auth.model.JwtAuthEntity;
import com.gk337.api.auth.repository.JwtAuthRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class JwtAuthService implements UserDetailsService {
	
	@Autowired
	private JwtAuthRepository authRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@SuppressWarnings("unused")
	@Override
	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		JwtAuthEntity user = authRepository.findByUserId(userId);
		log.debug("===== :: " + userId);
		log.debug("===== :: " + user.getMemPwd());
		if (user == null) {
			throw new UsernameNotFoundException("User not found with userId: " + userId);
		}
		return new org.springframework.security.core.userdetails.User(user.getUserId(), user.getMemPwd(), new ArrayList<>());
	}
	
	/*
	public AuthEntity save(AuthRequest auth) {
		AuthEntity newUser = new AuthEntity();
		newUser.setUsername(auth.getUserId());
		newUser.setPassword(bcryptEncoder.encode(auth.getMemPwd());
		return userDao.save(newUser);
	}
	*/
}