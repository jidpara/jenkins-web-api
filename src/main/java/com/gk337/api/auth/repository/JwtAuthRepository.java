package com.gk337.api.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.auth.model.JwtAuthEntity;

@Repository
public interface JwtAuthRepository extends PagingAndSortingRepository<JwtAuthEntity, Integer>,  JpaRepository<JwtAuthEntity, Integer> {

	JwtAuthEntity findByUserId(String userId);

}