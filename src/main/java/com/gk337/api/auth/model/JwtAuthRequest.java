package com.gk337.api.auth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "API 인증 Request Vo")
public class JwtAuthRequest {
	
    @ApiModelProperty(notes = "아이디", example="dealer2")
	private String userId;
	
    @ApiModelProperty(notes = "패스워드", example="1234")
	private String memPwd;
	
}