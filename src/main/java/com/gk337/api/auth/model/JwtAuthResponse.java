package com.gk337.api.auth.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "API 인증 Response Vo")
public class JwtAuthResponse {
	
	@ApiModelProperty(notes = "Json Web Token", example="alksjdfDSKLJ.FekjfslWekjfalkflwkdjalfkfllsdf")
	private String jwttoken;

}