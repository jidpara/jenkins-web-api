package com.gk337.api.auth.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_member")
public class JwtAuthEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
	private long seq;
	
	@Column(name = "user_id")
    @ApiModelProperty(notes = "아이디")
	private String userId;
	
	@Column(name = "mem_pwd")
    @ApiModelProperty(notes = "패스워드")
	private String memPwd;

}