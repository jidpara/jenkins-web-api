package com.gk337.api.area.controller;

import java.util.List;

import javax.validation.Valid;

import com.gk337.api.area.entity.AreaGroupMappingResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.gk337.api.area.entity.AreaEntity;
import com.gk337.api.area.entity.AreaRequest;
import com.gk337.api.area.entity.AreaResponse;
import com.gk337.api.area.repository.AreaRepository;
import com.gk337.api.area.service.AreaService;
import com.gk337.api.product.entity.ProductRequest;
import com.gk337.api.product.entity.ProductEntity;
import com.gk337.api.product.service.ProductService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.exception.JGWServiceException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 상품 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 상품관련 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.01.26
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

/**
 * 
 * 관련테이블 : jw_area
 * Q. 사용하는 테이블인지 확인필요.
 * Q. area_level 필드 의미는??
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/areas")
@Api(tags= {"[기준정보] - 지역정보 API"}, protocols="http", produces="application/json", consumes="application/json")
public class AreaRestController {
	
	@Autowired
	private AreaService areaService;
	
	/**
	 * 지역정보 등록.
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "지역정보 저장 API", notes = "지역정보 저장")
	@RequestMapping(method = RequestMethod.POST, path="/", produces = "application/json")
	public ResponseEntity<AreaResponse> saveArea(@RequestBody @Valid AreaRequest request) {
		return BaseResponse.ok(areaService.saveOrUpdateArea(request));
	}
	
	/**
	 * 지역정보 수정.
	 * @param seq
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "지역정보 수정 API", notes = "시퀀스로 지역정보 수정")
	@RequestMapping(method = RequestMethod.PUT, path="/{seq}", produces = "application/json")
	public ResponseEntity<AreaResponse> updateArea(
			@ApiParam(value="지역정보 시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq,
			@RequestBody @Valid AreaRequest request) {
		request.setSeq(seq);
		return BaseResponse.ok(areaService.saveOrUpdateArea(request));
	}
	
	/**
	 * 지역정보 삭제.
	 * @param reqSeq
	 * @return
	 */
	@ApiOperation(value = "지역정보 삭제 API", notes = "시퀀스로 지역정보 삭제 API")
	@RequestMapping(method = RequestMethod.DELETE, path="/{seq}", produces = "application/json")
	public ResponseEntity<ErrorVo> deleteArea(
			@ApiParam(value="지역정보 시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq) {
		return BaseResponse.ok(areaService.deleteBySeq(seq));
	}

	/**
	 * 지역조회 API
	 * @return
	 */
	@ApiOperation(value = "지역 조회 API", notes = "지역 조회")
	@RequestMapping(method = RequestMethod.GET, path="/{parentSeq}", produces = "application/json")
	public ResponseEntity<List<AreaResponse>> area(
			@ApiParam(value="부모 시퀀스", required=true, defaultValue = "1") @PathVariable(value ="parentSeq", required = true) int parentSeq ) {
		return BaseResponse.ok(areaService.selectAreaByParentSeq(parentSeq));
	}

	/**
	 * 지역그룹 목록 조회 API
	 * @return
	 */
	@ApiOperation(value = "지역그룹 목록 조회 API", notes = "미리 정의된 지역그룹 리스트 조회")
	@RequestMapping(method = RequestMethod.GET, path="/groups", produces = "application/json")
	public ResponseEntity<List<AreaResponse>> areaGroup() {
		return BaseResponse.ok(areaService.selectAreaGroups());
	}


	/**
	 * 지역그룹매핑 목록 조회 API
	 * @return
	 */
	@ApiOperation(value = "지역그룹매핑 목록 조회 API", notes = "미리 정의된 지역그룹 리스트 조회")
	@RequestMapping(method = RequestMethod.GET, path="/groups/mapping", produces = "application/json")
	public ResponseEntity<List<AreaGroupMappingResponse>> areaGroupMapping(
			@ApiParam(value="지역그룹시퀀스", required=false, defaultValue = "1") @RequestParam(value = "areaGroupSeq", required = false) Integer areaGroupSeq,
			@ApiParam(value="자식포함여부(Y:자식지역포함, N:미포함)", required=false, defaultValue = "N") @RequestParam(value = "includeChild", required = false) String includeChild
	) {
		return BaseResponse.ok(areaService.selectAreaGroupMappingList(areaGroupSeq, includeChild));
	}
}
