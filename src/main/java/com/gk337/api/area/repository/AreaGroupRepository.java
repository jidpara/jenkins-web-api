package com.gk337.api.area.repository;

import com.gk337.api.area.entity.AreaGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public interface AreaGroupRepository extends JpaRepository<AreaGroupEntity, Integer>, JpaSpecificationExecutor<AreaGroupEntity> {

    @Override
    List<AreaGroupEntity> findAll();
}