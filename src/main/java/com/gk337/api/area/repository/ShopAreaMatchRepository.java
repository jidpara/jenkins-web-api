package com.gk337.api.area.repository;

import com.gk337.api.area.entity.ShopAreaMatchEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ShopAreaMatchRepository extends JpaRepository<ShopAreaMatchEntity, Integer>, JpaSpecificationExecutor<ShopAreaMatchEntity> {
}