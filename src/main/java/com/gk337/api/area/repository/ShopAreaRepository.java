package com.gk337.api.area.repository;

import com.gk337.api.area.entity.ShopAreaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ShopAreaRepository extends JpaRepository<ShopAreaEntity, Integer>, JpaSpecificationExecutor<ShopAreaEntity> {
	
	void deleteByShopSeq(int shopSeq);
}