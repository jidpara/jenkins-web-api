package com.gk337.api.area.repository;

import com.gk337.api.area.entity.AreaGroupMappingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AreaGroupMappingRepository extends JpaRepository<AreaGroupMappingEntity, Integer>, JpaSpecificationExecutor<AreaGroupMappingEntity> {

}