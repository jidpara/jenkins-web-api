package com.gk337.api.area.repository;

import com.gk337.api.code.model.CodeInfoGrpEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.area.entity.AreaEntity;

import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public interface AreaRepository extends PagingAndSortingRepository<AreaEntity, Integer>, JpaRepository<AreaEntity, Integer> {

	AreaEntity save(AreaEntity entity);
	
	void deleteBySeq(int seq);

	List<AreaEntity> findByParentSeq(Integer parentSeq);
	// JPA 페이징 샘플 주석 풀고 확인...
	// Page<AreaEntity> findAll(Pageable pageable);
	
}
