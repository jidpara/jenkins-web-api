package com.gk337.api.area.mapper;

import java.util.List;

import com.gk337.api.area.entity.AreaGroupMappingResponse;
import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.area.entity.AreaResponse;
import org.apache.ibatis.annotations.Param;

/***
 * 
 * @author maroojjang
 *
 */
@Mapper
public interface AreaMapper {
	
	/**
	 * 그룹시퀀스 지역목록 조회
	 * @param groupSeq
	 * @return
	 */
	public List<AreaResponse> selectAreaByGroupSeq(int groupSeq);

	/**
	 * 지역그룹매핑목록 조회
	 * @param areaGroupSeq
	 * @return
	 */
	public List<AreaGroupMappingResponse> selectAreaGroupMappingList(@Param("areaGroupSeq") Integer areaGroupSeq, @Param("includeChild") String includeChild);
}
