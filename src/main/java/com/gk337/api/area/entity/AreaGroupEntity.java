package com.gk337.api.area.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 지역그룹
 */
@Table(name = "jw_area_group")
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class AreaGroupEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 시퀀스
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "seq", nullable = false)
    private Integer seq;

    /**
     * 상태값 (Y:정상, N:비노출, D:삭제)
     */
    @Column(name = "cstatus", nullable = false)
    private String cstatus;

    /**
     * 지역그룹명
     */
    @Column(name = "group_name", nullable = false)
    private String groupName;

    /**
     * 순서
     */
    @Column(name = "order_by", nullable = false)
    private Integer orderBy;

    
}