package com.gk337.api.area.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 상점지역매칭
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "jw_shop_area_match")
@Entity
@IdClass(ShopAreaMatchEntityPK.class)
public class ShopAreaMatchEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 상점시퀀스
     */
    @Id
    @Column(name = "shop_seq", insertable = false, nullable = false)
    private Integer shopSeq;

    /**
     * 지역시퀀스
     */
    @Id
    @Column(name = "area_seq", insertable = false, nullable = false)
    private Integer areaSeq;

    /**
     * 지역그룹시퀀스
     */
    @Column(name = "area_group_seq")
    private Integer areaGroupSeq;

    
}