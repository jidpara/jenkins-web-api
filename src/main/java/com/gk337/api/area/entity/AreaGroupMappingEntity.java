package com.gk337.api.area.entity;

import com.gk337.common.model.AuditEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 지역그룹매핑
 */
@Entity
@Table(name = "jw_area_group_mapping")
@Data
@EqualsAndHashCode(callSuper = true)
@IdClass(AreaGroupMappingEntityPK.class)
public class AreaGroupMappingEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 지역그룹시퀀스
     */
    @Id
    @Column(insertable = false, name = "area_group_seq", nullable = false)
    private Integer areaGroupSeq;

    /**
     * 지역시퀀스
     */
    @Id
    @Column(name = "area_seq", insertable = false, nullable = false)
    private Integer areaSeq;

    /**
     * 상태값 (Y:정상, N:비노출, D:삭제)
     */
    @Column(name = "cstatus", nullable = false)
    private String cstatus;

    
}