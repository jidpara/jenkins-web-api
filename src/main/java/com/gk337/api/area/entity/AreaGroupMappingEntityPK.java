package com.gk337.api.area.entity;

import com.gk337.common.model.AuditEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class AreaGroupMappingEntityPK extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EqualsAndHashCode.Include
    private Integer areaGroupSeq;

    @EqualsAndHashCode.Include
    private Integer areaSeq;
    
}