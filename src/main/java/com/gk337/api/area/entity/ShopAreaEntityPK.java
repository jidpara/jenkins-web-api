package com.gk337.api.area.entity;

import com.gk337.common.model.AuditEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class ShopAreaEntityPK extends AuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @EqualsAndHashCode.Include
    private Integer shopSeq;

    @EqualsAndHashCode.Include
    private Integer areaSeq;
    
}