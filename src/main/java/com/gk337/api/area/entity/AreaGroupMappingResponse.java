package com.gk337.api.area.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.model.AuditEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 지역그룹매핑 + 지역정보
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "지역그룹매핑정보 Response Vo")
public class AreaGroupMappingResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "지역그룹시퀀스")
    private Integer areaGroupSeq;

    @ApiModelProperty(notes = "지역그룹명")
    private String areaGroupName;

    @ApiModelProperty(notes = "지역시퀀스")
    private Integer areaSeq;

    @ApiModelProperty(notes = "지역그룹명")
    private String areaName;

}