package com.gk337.api.area.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 지역
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_area")
@ApiModel(description = "지역정보 Entity")
public class AreaEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @Column(name = "cstatus", nullable = false)
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus;

    @Column(name = "area_name", nullable = false)
    @ApiModelProperty(notes = "지역명")
    private String areaName;

    @Column(name = "group_seq", nullable = false)
    @ApiModelProperty(notes = "그룹시퀀스")
    private Integer groupSeq;

    @Column(name = "parent_seq")
    @ApiModelProperty(notes = "부모시퀀스")
    private Integer parentSeq;

    @Column(name = "area_level", nullable = false)
    @ApiModelProperty(notes = "레벨")
    private Integer areaLevel;

    @Column(name = "order_by", nullable = false)
    @ApiModelProperty(notes = "순서")
    private Integer orderBy;
    
}