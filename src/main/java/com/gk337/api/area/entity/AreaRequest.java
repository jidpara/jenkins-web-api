package com.gk337.api.area.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 지역
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "지역정보 Request Vo")
public class AreaRequest implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "지역명")
    private String areaName;

    @ApiModelProperty(notes = "그룹시퀀스")
    private Integer groupSeq;

    @ApiModelProperty(notes = "부모시퀀스")
    private Integer parentSeq;

    @ApiModelProperty(notes = "레벨")
    private Integer areaLevel;

    @ApiModelProperty(notes = "순서")
    private Integer orderBy;
    
}