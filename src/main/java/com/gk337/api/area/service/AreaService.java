package com.gk337.api.area.service;

import com.gk337.common.util.CollectionUtils;
import com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gk337.api.area.entity.*;
import com.gk337.api.area.mapper.*;
import com.gk337.api.area.repository.*;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Service
public class AreaService {

	@Autowired
	private AreaRepository areaRepository;

	@Autowired
	private AreaGroupRepository areaGroupRepository;

	@Autowired
	private AreaMapper areaMapper;
	
	/**
	 * 지역정보 저장 또는 수정.
	 * @param request
	 * @return
	 */
	public AreaResponse saveOrUpdateArea(AreaRequest request) {
		log.debug("========AreaService.saveOrUpdateArea========");
		AreaResponse response = new AreaResponse();
		try {
			AreaEntity entity = new AreaEntity();
			BeanUtils.copyProperties(request, entity);
			entity = areaRepository.save(entity);
			BeanUtils.copyProperties(entity, response);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.AREA.0001", "저장 또는 수정");
		}
		return response;
	}
	
	/**
	 * 해당 지역정보 시퀀스로 지역정보 삭제.
	 * @param seq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteBySeq(int seq) {
		log.debug("========AreaService.deleteBySeq========");
		ErrorVo result = new ErrorVo();
		try {
			areaRepository.deleteBySeq(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.AREA.0001", "지역정보 삭제");
		}
		return result;
	}


	/**
	 * 지역그룹 전체목록 조회
	 * @return
	 */
	public ListVo<AreaResponse> selectAreaByParentSeq(Integer parentSeq) {
		log.debug("========AreaService.selectAreaByGroupSeq========");

		ListVo<AreaResponse> listVo = null;
		try {
			List<AreaEntity> list = areaRepository.findByParentSeq(parentSeq);
			listVo = new ListVo<>(CollectionUtils.convert(list, AreaResponse::new));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.AREA.0001", "지역조회");
		}
		return listVo;
	}

	/**
	 * 지역그룹 전체목록 조회
	 * @return
	 */
	public ListVo<AreaGroupEntity> selectAreaGroups() {
		log.debug("========AreaService.selectAreaByGroupSeq========");
		ListVo<AreaGroupEntity> listVo = null;
		try {
			listVo = new ListVo<>(areaGroupRepository.findAll());
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.AREA.0001", "지역그룹조회");
		}
		return listVo;
	}

	/**
	 * 그룹시퀀스로 지역목록 조회
	 * @param groupSeq
	 * @return
	 */
	public ListVo<AreaResponse> selectAreaByGroupSeq(int groupSeq) {
		log.debug("========AreaService.selectAreaByGroupSeq========");
		ListVo<AreaResponse> listVo = null;
		try {
			listVo = new ListVo<AreaResponse>(areaMapper.selectAreaByGroupSeq(groupSeq));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.AREA.0001", "그룹조회");
		}
		return listVo;
	}

	/**
	 * 그룹시퀀스로 지역목록 조회
	 * @param areaGroupSeq
	 * @return
	 */
	public ListVo<AreaGroupMappingResponse> selectAreaGroupMappingList(Integer areaGroupSeq, String includeChild) {
		ListVo<AreaGroupMappingResponse> listVo = null;
		try {
			listVo = new ListVo<AreaGroupMappingResponse>(areaMapper.selectAreaGroupMappingList(areaGroupSeq, includeChild));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.AREA.0001", "지역그룹매핑조회");
		}
		return listVo;
	}

}
