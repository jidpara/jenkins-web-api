package com.gk337.api.screen.controller;

import com.gk337.api.screen.component.ScreenComponent;
import com.gk337.api.screen.entity.*;
import com.gk337.api.screen.service.ScreenService;
import com.gk337.common.core.mvc.BaseResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 *
 *
 * @author
 * @since
 * @see
 *  <pre>
 *      수정이력
 *          xxxxx
 * </pre>
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/screen")
@Api(tags= {"[메인] - 화면 정보 API"}, protocols="http", produces="application/json", consumes="application/json")
public class ScreenRestController {


	@Autowired
	private ScreenService screenService;

	@Autowired
	private org.springframework.core.env.Environment environment;

	@Autowired
	private ScreenComponent screenComponent;

	@Value("${jgw.chat-read-interval}")
	private Integer interval;

	/**
	 * 채팅방 화면정보 조회 API
	 * @param Integer memSeq - 본인회원시퀀스
	 * @param Integer resSeq - 요청서응답시퀀스
	 * @return
	 */
	@ApiOperation(value = "채팅방 화면 조회", notes = "채팅방 화면 조회 채팅방시퀀스 혹은 요청서응답시퀀스 둘중 하나 필수")
	@RequestMapping(method = RequestMethod.GET, path="/chat", produces = "application/json")
	public ResponseEntity<ChatScreenResponse> chatScreen(
			@ApiParam(value="본인회원시퀀스", required=true, defaultValue = "7") @RequestParam("memSeq") Integer memSeq,
			@ApiParam(value="미사용(삭제예정)", required=false, defaultValue = "96") @RequestParam(value = "roomSeq", required = false) Integer roomSeq,
			@ApiParam(value="요청서응답시퀀스", required=true, defaultValue = "30") @RequestParam(value = "resSeq", required = true) Integer resSeq ) {

		ChatScreenResponse response = screenService.getChatScreenData(memSeq, roomSeq, resSeq);
		response.setChatReadInterval(interval);
		return BaseResponse.ok(response);
	}

	/**
	 * 채팅방(물품) 화면정보 조회 API
	 * @param Integer memSeq - 본인회원시퀀스
	 * @param Integer prodSeq - 물품시퀀스
	 * @return
	 */
	@ApiOperation(value = "채팅방(물품) 화면 조회", notes = "채팅방(물품) 화면 조회 채팅방시퀀스 혹은 물품시퀀스 둘중 하나 필수")
	@RequestMapping(method = RequestMethod.GET, path="/v2/chat", produces = "application/json")
	public ResponseEntity<ChatScreenResponse> chatProductScreen(
			@ApiParam(value="본인회원시퀀스", required=true, defaultValue = "7") @RequestParam("memSeq") Integer memSeq,
			@ApiParam(value="미사용(삭제예정)", required=false, defaultValue = "96") @RequestParam(value = "roomSeq", required = false) Integer roomSeq,
			@ApiParam(value="요청서응답시퀀스", required=true, defaultValue = "30") @RequestParam(value = "prodSeq", required = true) Integer resSeq ) {

		ChatScreenResponse response = screenService.getChatProductScreenData(memSeq, roomSeq, resSeq);
		if(response == null){
			response = new ChatScreenResponse();
		}
		response.setChatReadInterval(interval);
		return BaseResponse.ok(response);
	}

	/**
	 * 메인 화면 조회 API
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "메인화면 데이터 조회", notes = "메인화면 데이터 조회")
	@RequestMapping(method = RequestMethod.GET, path="/main", produces = "application/json")
	public ResponseEntity<MainResponse> main(@RequestParam("memSeq") Integer memSeq ) {
		MainRequest request = new MainRequest();
		request.setMemSeq(memSeq);

		// 시연용 예외 처리
		MainResponse response = screenService.getMain(request);
		String[] activeProfiles = environment.getActiveProfiles();      // it will return String Array of all active profile.
		for(String profile:activeProfiles) {
//			if("dev".equals(profile)){
				response.setProductCnt(response.getProductCnt() + 367200);
				response.setPUserCnt(response.getPUserCnt() + 37021);
				response.setTotUserCnt(response.getTotUserCnt() + 182365);
//			}
		}

		return BaseResponse.ok(response);
	}

	/**
	 *
	 * @param memSeq
	 * @return
	 */
	@ApiOperation(value = "메인화면 요청서 조회", notes = "메인화면 데이터 조회", response = MainReqResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/main/request", produces = "application/json")
	public ResponseEntity<List<MainReqResponse>> mainRequest(@RequestParam("memSeq") Integer memSeq ) {
		return BaseResponse.ok(screenService.selectMainReqeustList(memSeq));
	}

	/**
	 *
	 * @param memSeq
	 * @return
	 */
	@ApiOperation(value = "결제상품 화면 조회", notes = "결제상품 화면 데이터 조회", response = PaymentScreenResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/mypage/{memSeq}/payment", produces = "application/json")
	public ResponseEntity<PaymentScreenResponse> getPaymentScreen(@PathVariable("memSeq") Integer memSeq ) {

//		PaymentScreenResponse response = new PaymentScreenResponse();
		PaymentScreenResponse response = screenComponent.getPaymentScreenInfo(memSeq);

//		response.setMemSeq(memSeq);
//		response.setAreaInfo("강원도(2) - 강원도A, 강원도B\n대전광역시(1) - 대전A");
//		response.setCategoryInfo("IT, 휴대폰, 의료, 비행/선박, 이륜바이크");

//		List<PaymentResult> paymentResultList = new ArrayList<>();
//
//		PaymentResult result1 = new PaymentResult();
//		result1.setAmount(10000000);
//		result1.setPaymentDate(LocalDateTime.of(2020, 6, 27, 11, 15, 10));
//		result1.setPaymentDateStr(LocalDateTime.of(2020, 6, 27, 11, 15, 10).toString());
//		result1.setPaymentMethod("card");
//		result1.setPaymentMethodName("신용카드");
//		result1.setShopSeq(1);
//		result1.setPaymentStatus("paid");
//		result1.setPaymentStatusName("결제완료");
//
//		PaymentResult result2 = new PaymentResult();
//		result2.setAmount(4000000);
//		result2.setPaymentDate(LocalDateTime.of(2020, 5, 27, 11, 15, 10));
//		result2.setPaymentDateStr(LocalDateTime.of(2020, 5, 27, 11, 15, 10).toString());
//		result2.setPaymentMethod("trans");
//		result2.setPaymentMethodName("실시간계좌이체");
//		result2.setShopSeq(1);
//		result2.setPaymentStatus("cancelled");
//		result2.setPaymentStatusName("결제취소");
//
//		PaymentResult result3 = new PaymentResult();
//		result3.setAmount(7000000);
//		result3.setPaymentDate(LocalDateTime.of(2020, 5, 27, 11, 15, 10));
//		result3.setPaymentDateStr(LocalDateTime.of(2020, 5, 27, 11, 15, 10).toString());
//		result3.setPaymentMethod("bank");
//		result3.setPaymentMethodName("무통장입금");
//		result3.setShopSeq(1);
//		result3.setPaymentStatus("ready");
//		result3.setPaymentStatusName("미결제");
//
//		paymentResultList.add(result1);
//		paymentResultList.add(result2);
//		paymentResultList.add(result3);
//
//		response.setPaymentResultList(paymentResultList);

		return BaseResponse.ok(response);
	}

	/**
	 * 결제내역 상세 조회
	 * @param memSeq
	 * @param paymentCode
	 * @return
	 */
	@ApiOperation(value = "[결제상세내역화면 조회] 상세내역화면 조회 API",
			notes = "[결제상세내역화면 조회] 결제번호로 상세내역화면 데이터 조회",
			response= PaymentDetailScreenResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/mypage/{memSeq}/payment/{paymentCode}", produces = "application/json")
	public ResponseEntity<PaymentDetailScreenResponse> paymentDetail(
			@ApiParam(value="회원시퀀스", required=true, defaultValue = "") @PathVariable(value = "memSeq", required = true) Integer memSeq,
			@ApiParam(value="결제번호", required=true, defaultValue = "") @PathVariable(value = "paymentCode", required = true) String paymentCode) {

		PaymentDetailScreenResponse response = screenComponent.getPaymentDetailScreenInfo(memSeq, paymentCode);

//		PaymentDetailScreenResponse response = new PaymentDetailScreenResponse();
//		response.setMemSeq(memSeq);
//		response.setPaymentCode(paymentCode);
//		response.setMerchantUid("f58484ac6f9aa39cf41f11f17180f2bbb546ffe7c312842bafe9135906a0b99d");
//		response.setStartDate("2020-06-27");
//		response.setEndDate("2020-07-26");
//		response.setAreaInfo("강원도(2) - 강원도A, 강원도B\n대전광역시(1) - 대전A");
//		response.setCategoryInfo("IT, 휴대폰, 의료, 비행/선박, 이륜바이크");
//		response.setAmount(10000000);
//		response.setPaymentDate(LocalDateTime.of(2020, 6, 27, 11, 15, 10));
//		response.setPaymentDateStr(LocalDateTime.of(2020, 6, 27, 11, 15, 10).toString());
//		response.setPaymentMethod("card");
//		response.setPaymentMethodName("신용카드");
//		response.setShopSeq(1);
//		response.setPaymentStatus("paid");
//		response.setPaymentStatusName("결제완료");
//		response.setApplyDate(LocalDateTime.of(2020, 6, 27, 11, 15, 10));
//		response.setCancelDate(LocalDateTime.of(2020, 6, 27, 11, 15, 10));
//		response.setPgTid("20200617965620");
//		response.setCardCode("041");
//		response.setCardName("기업은행");
//		response.setCardQuota("0");
//		response.setCardNumber("1234-56**-****-1234");
//		response.setReceiptUrl("https://admin8.kcp.co.kr/assist/bill.BillActionNew.do?cmd=acnt_bill&tno=20200617965620&order_no=imp_976795004399&trade_mony=1000");
//		response.setCashReceiptIssued("true");

		return BaseResponse.ok(response);
	}
}
