package com.gk337.api.screen.mapper;

import com.gk337.api.screen.entity.ChatScreenResponse;
import com.gk337.api.screen.entity.MainReqResponse;
import com.gk337.api.screen.entity.MainRequest;
import com.gk337.api.screen.entity.MainResponse;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface ScreenMapper {

	/**
	 * 채팅방 화면 데이터 조회
	 * @param HashMap request
	 * @return MainResponse
	 */
	public ChatScreenResponse selectChatScreenInfo(HashMap request);

	/**
	 * 채팅방(물품) 화면 데이터 조회
	 * @param HashMap request
	 * @return MainResponse
	 */
	public ChatScreenResponse selectChatProductScreenInfo(HashMap request);

	/**
	 * 메인화면 데이터 조회
	 * @param MainRequest request
	 * @return MainResponse
	 */
	public MainResponse selectMainInfo(MainRequest request);
	
	/**
	 * 메인 요청 최근 한건씩 조회 
	 * @param memSeq
	 * @return
	 */
	public List<MainReqResponse> selectMainReqeustList(int memSeq);

	/**
	 * 결제정보 - 지역
	 * @param Map request
	 * @return Map
	 */
	public List<Map<String, Object>> selectShopAreaInfo(Map request);

	/**
	 * 결제정보 - 카테고리
	 * @param Map request
	 * @return Map
	 */
	public Map<String, String> selectShopCategoryInfo(Map request);



}
