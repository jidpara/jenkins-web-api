package com.gk337.api.screen.entity;

import com.gk337.api.chat.entity.ChatRoomResponse;
import com.gk337.api.request.entity.RequestResponse;
import com.gk337.common.core.mvc.model.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "채팅화면 데이터 Response Vo")
public class ChatScreenResponse extends BaseVo {

	@ApiModelProperty(notes = "채팅방시퀀스", example = "55")
	private Integer roomSeq;

	@ApiModelProperty(notes = "요청서 시퀀스", example = "41")
	private Integer reqSeq;

	@ApiModelProperty(notes = "요청서 상위타입(REQ001 - BASIC:기본, BASIC_EMERGENCY:긴급요청, GMIDAS:지마이다스)", example = "GMIDAS")
    private String parentType = "GMIDAS";

	@ApiModelProperty(notes = "요청서 상품 시퀀스", example = "41")
	private Integer reqProdSeq;

	@ApiModelProperty(notes = "요청타입(S:판매, ?:구매)", example = "41")
	private String reqType;

	@ApiModelProperty(notes = "요청타입명", example = "판매")
	private String reqTypeName;

	@ApiModelProperty(notes = "요청서 회원 타입(P:딜러, G:유저)", example = "G")
	private String reqMemKind;

	@ApiModelProperty(notes = "요청서 회원 타입명", example = "유저")
	private String reqMemKindName;

	@ApiModelProperty(notes = "요청서 회원명 (닉네임 있을경우 닉네임 우선)", example = "라이언")
	private String reqMemName;

	@ApiModelProperty(notes = "요청서 회원 주소", example = "경기도용인시 수지구")
	private String reqAddress;

	@ApiModelProperty(notes = "요청서 작성일", example = "2020-03-09 03:14:22")
	private String reqDate;

	@ApiModelProperty(notes = "요청서 상품명", example = "세탁기중고")
	private String reqProdName;

	@ApiModelProperty(notes = "요청서 상품설명", example = "세탁기 싸게파라요..")
	private String reqProdDesc;

	@ApiModelProperty(notes = "요청서 상품 가격", example = "150000")
	private Integer reqProdPrice;

	@ApiModelProperty(notes = "요청서 메인이미지 URL", example = "http://d1n8fbd6e2ut61.cloudfront.net/assets/upload/product/20200309/def27e2f-fe88-4a9b-976e-5b86a36ce5b8")
	private String reqProdFileUrl;

	@ApiModelProperty(notes = "요청서답변 시퀀스", example = "48")
	private Integer resSeq;

	@ApiModelProperty(notes = "요청서답변 회원시퀀스", example = "1")
	private Integer resMemSeq;

	@ApiModelProperty(notes = "요청서답변 상품가격", example = "150000")
	private Integer resProdPrice;

	@ApiModelProperty(notes = "요청서 답변 구매포인트", example = "150000")
	private Integer resBuyPoint;

	@ApiModelProperty(notes = "요청서답변 내용", example = "같은제품 있어요")
	private String resComments;

	@ApiModelProperty(notes = "요청서답변 상품이미지", example = "http://d1n8fbd6e2ut61.cloudfront.net/assets/upload/product/20200316/e34383f2-53d2-4e9e-8f53-962b8365bce2")
	private String resProdFileUrl;

	@ApiModelProperty(notes = "대화상대 읽지 않은 메세지 수", example = "10")
	private Integer recvUnreadCnt;

	@ApiModelProperty(notes = "대화상대 전화번호(딜러일 경우만)", example = "01071178214")
	private String recvShopPhone;

	@ApiModelProperty(notes = "대화상대 프로필 이미지", example = "01071178214")
	private String recvProfileImage;

	@ApiModelProperty(notes = "대화상대 seq", example = "10")
	private Integer recvMemSeq;

	@ApiModelProperty(notes = "대화상대 이름", example = "중고왕 김태복 딜러")
	private String recvName;

	@ApiModelProperty(notes = "대화상대 이름", example = "중고왕 김태복 딜러")
	private String mergeName;

	@ApiModelProperty(notes = "대화상대 회원 타입(P:딜러, G:유저)", example = "중고왕 김태복 딜러")
	private String recvMemkind;

	@ApiModelProperty(notes = "대화상대 회원 타입명", example = "딜러")
	private String recvMemkindName;

	@ApiModelProperty(notes = "메세지 조회 Interval (ms)", example = "1000")
	private Integer chatReadInterval;

	@ApiModelProperty(notes = "상대방 방 나감 여부", example="N")
    private String recvOutYn = "N";
}
