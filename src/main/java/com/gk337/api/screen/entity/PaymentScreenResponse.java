package com.gk337.api.screen.entity;

import com.gk337.api.payment.entity.PaymentResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "결제상품 화면 Response Vo")
public class PaymentScreenResponse {

    @ApiModelProperty(notes = "회원SEQ", example = "1")
    private Integer memSeq;

    @ApiModelProperty(notes = "상점SEQ", example = "1")
    private Integer shopSeq;

    @ApiModelProperty(notes = "결제지역정보", example = "")
    private String areaInfo;

    @ApiModelProperty(notes = "결제카테고리정보", example = "")
    private String categoryInfo;


    @ApiModelProperty(notes = "시작일", example = "2020-06-27")
    String startDate;

    @ApiModelProperty(notes = "종료일", example = "2020-07-27")
    String endDate;

    @ApiModelProperty(notes = "결제정보목록", example = "")
    List<PaymentResult> paymentResultList;
}
