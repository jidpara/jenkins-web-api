package com.gk337.api.screen.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "메인 최근 요청서 Response Vo")
public class MainReqResponse {
	
	@ApiModelProperty(notes = "요청서시퀀스", example = "10")
    private Integer reqSeq;

    @ApiModelProperty(notes = "[REQ001] 요청서 상위 타입(BASIC : 기본, GMIDAS : 지마이다스)")
    private String parentType;

	@ApiModelProperty(notes = "회원시퀀스", example = "1")
    private Integer memSeq;
	
	@ApiModelProperty(notes = "상품시퀀스", example = "12")
    private Integer prodSeq;
	
	@ApiModelProperty(notes = "요청서타입", example = "B")
    private String reqType;

    @ApiModelProperty(notes = "새로운 문의내용 체크", example = "Y")
    private String isNew;

	@ApiModelProperty(notes = "요청서타입명", example = "구매")
    private String reqTypeName;
	
	@ApiModelProperty(notes = "회원종류", example = "P")
    private String memKind;
	
	@ApiModelProperty(notes = "회원이름", example = "홍길동")
    private String memName;
	
	@ApiModelProperty(notes = "닉네임", example = "닉네임")
    private String memNickname;
	
	@ApiModelProperty(notes = "memEmail", example = "abc@abc.com")
    private String memEmail;
	
	@ApiModelProperty(notes = "회원휴대폰번호", example = "01012349876")
    private String memHp;
	
	@ApiModelProperty(notes = "상품명", example = "선풍기")
    private String prodName;
	
	@ApiModelProperty(notes = "가격", example = "15000")
    private String price;
	
	@ApiModelProperty(notes = "등록일자", example = "2020-02-28 11:12:13")
    private String regDate;

}
