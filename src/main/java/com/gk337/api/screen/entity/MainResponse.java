package com.gk337.api.screen.entity;

import com.gk337.common.core.mvc.model.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "메인화면 데이터 Response Vo")
public class MainResponse extends BaseVo {

	@ApiModelProperty(notes = "새로운 알림 수", example = "10")
	private Integer alarmCnt;

	@ApiModelProperty(notes = "새로운 대화 수", example = "2")
	private Integer chatCnt;

	@ApiModelProperty(notes = "사업자 승인 여부", example = "Y")
	private String isApproved;

	@ApiModelProperty(notes = "중고전문가 수", example = "2")
	private Integer pUserCnt;

	@ApiModelProperty(notes = "누적물품 수", example = "2")
	private Integer productCnt;

	@ApiModelProperty(notes = "회원 수", example = "2")
	private Integer totUserCnt;

	@ApiModelProperty(notes = "메인비디오URL", example = "2")
	private String videoUrl;
	//recvRequestList

}
