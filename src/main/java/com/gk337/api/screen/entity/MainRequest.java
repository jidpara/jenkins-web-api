package com.gk337.api.screen.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "메인 Request Vo")
public class MainRequest {

    @ApiModelProperty(notes = "회원SEQ", example = "1")
    private Integer memSeq;
}
