package com.gk337.api.screen.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "결제상품 화면 Request Vo")
public class PaymentScreenRequest {

    @ApiModelProperty(notes = "회원SEQ", example = "1")
    private Integer memSeq;
}
