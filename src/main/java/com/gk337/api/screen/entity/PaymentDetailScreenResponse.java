package com.gk337.api.screen.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "결제상세정보 화면 Response Vo")
public class PaymentDetailScreenResponse {

    @ApiModelProperty(notes = "회원SEQ", example = "1")
    private Integer memSeq;

    @ApiModelProperty(notes = "상점SEQ", example = "1")
    private Integer shopSeq;

    @ApiModelProperty(notes = "결제UID", example = "f58484ac6f9aa39cf41f11f17180f2bbb546ffe7c312842bafe9135906a0b99d")
    private String merchantUid = "f58484ac6f9aa39cf41f11f17180f2bbb546ffe7c312842bafe9135906a0b99d";

    @ApiModelProperty(notes = "결제상태. ready:미결제, paid:결제완료, cancelled:결제취소, failed:결제실패 = ['ready', 'paid', 'cancelled', 'failed']", example = "paid")
    private String paymentStatus;

    @ApiModelProperty(notes = "결제상태명", example = "결제완료")
    private String paymentStatusName;

    @ApiModelProperty(notes = "결제번호", example = "20200627-123456-1123")
    private String paymentCode;

    @ApiModelProperty(notes = "주문 접수일", example = "2020-06-25 22:15:00")
    private LocalDateTime paymentDate;

    @ApiModelProperty(notes = "주문 접수일 포멧팅", example = "06.25 22:15")
    private String paymentDateStr;

    @ApiModelProperty(notes = "서비스기간 시작일", example = "2020.06.27")
    String startDate;

    @ApiModelProperty(notes = "서비스기간 종료일", example = "2020.07.27")
    String endDate;

    @ApiModelProperty(notes = "결제지역정보", example = "")
    private String areaInfo;

    @ApiModelProperty(notes = "결제카테고리정보", example = "")
    private String categoryInfo;

    @ApiModelProperty(notes = "승인일시(PG)", example = "2020.06.25 22:15:00")
    private LocalDateTime applyDate;

    @ApiModelProperty(notes = "취소일시(PG)", example = "2020.06.25 22:15:00")
    private LocalDateTime cancelDate;

    @ApiModelProperty(notes = "승인번호(PG)", example = "20200617965620")
    private String pgTid;

	@ApiModelProperty(notes = "결제종류코드 - samsung:삼성페이, card:신용카드, trans:계좌이체, vbank:가상계좌", example = "1")
    private String paymentMethod;

	@ApiModelProperty(notes = "결제종류이름", example = "신용카드")
    private String paymentMethodName;

    @ApiModelProperty(notes = "금액", example = "10000")
    private Integer amount;

    @ApiModelProperty(notes = "카드사 코드번호(금융결제원 표준코드번호) = ['361(BC카드)', '364(광주카드)', '365(삼성카드)', '366(신한카드)', '367(현대카드)', '368(롯데카드)', '369(수협카드)', '370(씨티카드)', '371(NH카드)', '372(전북카드)', '373(제주카드)', '374(하나SK카드)', '381(KB국민카드)', '041(우리카드)', '071(우체국)']", example = "041")
    private String cardCode;

    @ApiModelProperty(notes = "카드사 명칭 - (신용카드 결제 건의 경우)", example = "기업은행")
    private String cardName;

    @ApiModelProperty(notes = "할부개월 수(0이면 일시불)", example = "0")
    private String cardQuota;

    @ApiModelProperty(notes = "결제에 사용된 마스킹된 카드번호. 7~12번째 자리를 마스킹하는 것이 일반적이지만, PG사의 정책/설정에 따라 다소 차이가 있을 수 있음", example = "041")
    private String cardNumber;

    @ApiModelProperty(notes = "영수증URL", example = "https://admin8.kcp.co.kr/assist/bill.BillActionNew.do?cmd=acnt_bill&tno=20200617965620&order_no=imp_976795004399&trade_mony=1000")
    private String receiptUrl;

    @ApiModelProperty(notes = "현금영수증발급 여부", example = "true")
    private String cashReceiptIssued;



}
