package com.gk337.api.screen.component;

import com.github.pagehelper.PageInfo;
import com.gk337.api.base.entity.DeviceInfoSetting;
import com.gk337.api.base.entity.FmcMessageRequest;
import com.gk337.api.base.service.NotificationService;
import com.gk337.api.payment.entity.PaymentHistoryResponse;
import com.gk337.api.payment.entity.PaymentResult;
import com.gk337.api.payment.mapper.PaymentMapper;
import com.gk337.api.payment.service.PaymentService;
import com.gk337.api.request.entity.*;
import com.gk337.api.request.repository.ResponseProductAtfilRepository;
import com.gk337.api.request.repository.ResponseProductRepository;
import com.gk337.api.request.service.RequestService;
import com.gk337.api.screen.entity.PaymentDetailScreenResponse;
import com.gk337.api.screen.entity.PaymentScreenResponse;
import com.gk337.api.screen.mapper.ScreenMapper;
import com.gk337.api.verify.entity.UserMatchingResponse;
import com.gk337.api.verify.service.VerifyService;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.exception.JGWCommunicationException;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.CollectionUtils;
import com.gk337.common.util.MessageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ScreenComponent {

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private ScreenMapper screenMapper;

	@Autowired
	private PaymentMapper paymentMapper;
	
	/**
	 * 결제상품 화면정보를 조회한다.
	 * @param memSeq Integer - 본인회원시퀀스
	 * @return
	 */
	@javax.transaction.Transactional
	public PaymentScreenResponse getPaymentScreenInfo(Integer memSeq) {

		PaymentScreenResponse response = new PaymentScreenResponse();

		HashMap<String, Object> requestMap = new HashMap<>();
		requestMap.put("memSeq", memSeq);

		List<Map<String, Object>> areaInfoList = screenMapper.selectShopAreaInfo(requestMap);
		Map<String, String> cateInfo = screenMapper.selectShopCategoryInfo(requestMap);

		StringBuilder sb = new StringBuilder();

		if(areaInfoList != null && !areaInfoList.isEmpty()) {
			// 지역정보 화면표시용 문자열 생성 - 서울특별시(2) - 종로구, 서초구
			for(Map<String, Object> data : areaInfoList) {
				String areaInfo = (String) data.get("area_info");
				String areaName = (String) data.get("area_name");
				Integer areaCount = areaInfo != null ? areaInfo.split(",").length : 0;
				sb.append(areaName);
				sb.append("(").append(areaCount).append(")");
				sb.append(" - ").append(areaInfo);
				sb.append("\n");
			}
		} else {
			// 지역정보 가져오지 못하는 케이스 존재 함 -> 전국
			sb.append("전국");
		}

		response.setAreaInfo(sb.toString());
		response.setCategoryInfo(cateInfo.get("category_info"));


		// 최근결제내역 목록
		PageInfo<PaymentHistoryResponse> paymentHistoryListPage = paymentService.getPaymentHistoryListPaging(1,5, "A", memSeq);
		List<PaymentHistoryResponse> list = paymentHistoryListPage.getList();
		// 가장 최근 데이터에서 기본정보 추출
		PaymentHistoryResponse recentData = !list.isEmpty() ? list.get(0) : new PaymentHistoryResponse();
		response.setStartDate(recentData.getStartDate());
		response.setEndDate(recentData.getEndDate());
		response.setMemSeq(recentData.getMemSeq());
		response.setShopSeq(recentData.getShopSeq());

		// List Object 변환
		List<PaymentResult> paymentResultList = CollectionUtils.convert(list, PaymentResult::new);
		response.setPaymentResultList(paymentResultList);

		return response;
	}

	/**
	 * 결제상품 화면정보를 조회한다.
	 * @param memSeq Integer - 본인회원시퀀스
	 * @param paymentCode String - 주문번호
	 * @return
	 */
	@javax.transaction.Transactional
	public PaymentDetailScreenResponse getPaymentDetailScreenInfo(Integer memSeq, String paymentCode) {

		PaymentDetailScreenResponse response = new PaymentDetailScreenResponse();
		HashMap<String, Object> requestMap = new HashMap<>();
		requestMap.put("memSeq", memSeq);
		requestMap.put("paymentCode", paymentCode);

		PaymentHistoryResponse detail = paymentMapper.selectPaymentHistoryDetail(requestMap);

		BeanUtils.copyProperties(detail, response);
		response.setApplyDate(detail.getPaymentDate());

		List<Map<String, Object>> areaInfoList = paymentMapper.selectPaymentAreaInfo(requestMap);
		Map<String, String> cateInfo = paymentMapper.selectPaymentCategoryInfo(requestMap);

		StringBuilder sb = new StringBuilder();

		if(areaInfoList != null && !areaInfoList.isEmpty()) {
			for(Map<String, Object> data : areaInfoList) {
				String areaInfo = (String) data.get("area_info");
				String areaName = (String) data.get("area_name");
				Integer areaCount = areaInfo != null ? areaInfo.split(",").length : 0;
				sb.append(areaName);
				sb.append("(").append(areaCount).append(")");
				sb.append(" - ").append(areaInfo);
				sb.append("\n");
			}
		} else {
			// 지역정보 가져오지 못하는 케이스 존재 함 -> 전국
			sb.append("전국");
		}


		response.setAreaInfo(sb.toString());
		response.setCategoryInfo(cateInfo.get("category_info"));

		return response;
	}
}
