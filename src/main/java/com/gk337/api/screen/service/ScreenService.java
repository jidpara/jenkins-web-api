package com.gk337.api.screen.service;

import com.gk337.api.chat.entity.ChatRoomEntity;
import com.gk337.api.request.entity.RequestEntity;
import com.gk337.api.screen.entity.ChatScreenResponse;
import com.gk337.api.screen.entity.MainReqResponse;
import com.gk337.api.screen.entity.MainRequest;
import com.gk337.api.screen.entity.MainResponse;
import com.gk337.api.screen.mapper.ScreenMapper;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;


@Slf4j
@Service
public class ScreenService {
	
	@Autowired
	private ScreenMapper screenMapper;

	/**
	 * 채팅방 화면정보를 조회한다.
	 * @param Integer memSeq - 본인회원시퀀스
	 * @param Integer resSeq - 요청서응답시퀀스
	 * @return
	 */
	@javax.transaction.Transactional
	public ChatScreenResponse getChatScreenData(Integer memSeq, Integer roomSeq, Integer resSeq) {

		HashMap<String, Object> requestMap = new HashMap<>();
		requestMap.put("memSeq", memSeq);
		requestMap.put("roomSeq", roomSeq);
		requestMap.put("resSeq", resSeq);

		return screenMapper.selectChatScreenInfo(requestMap);
	}

	/**
	 * 채팅방 화면정보를 조회한다.
	 * @param Integer memSeq - 본인회원시퀀스
	 * @param Integer roomSeq - 채팅방시퀀스
	 * @param Integer prodSeq - 요청서응답시퀀스
	 * @return
	 */
	@javax.transaction.Transactional
	public ChatScreenResponse getChatProductScreenData(Integer memSeq, Integer roomSeq, Integer prodSeq) {

		HashMap<String, Object> requestMap = new HashMap<>();
		requestMap.put("memSeq", memSeq);
		requestMap.put("roomSeq", roomSeq); // TODO : 사용 ?
		requestMap.put("prodSeq", prodSeq);

		return screenMapper.selectChatProductScreenInfo(requestMap);
	}
	
	/**
	 * 요청서 - 메인 등록
	 * @param request
	 * @return
	 */
	@Transactional
	public MainResponse getMain(MainRequest request) {

		try {
			return screenMapper.selectMainInfo(request);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
	}
	
	/**
	 * 메인 최근 요청서 조회.
	 * @param memSeq
	 * @return
	 */
	public ListVo<MainReqResponse> selectMainReqeustList(int memSeq) {
		ListVo<MainReqResponse> listVo = null;
		try {
			listVo = new ListVo<MainReqResponse>(screenMapper.selectMainReqeustList(memSeq));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "조회");
		}
		return listVo;
	}


}


