package com.gk337.api.async.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AsyncService {
	
	@Async
	public void onAsync() {
		try {
			Thread.sleep(3000);
			log.info("onAsync");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
	public void onSync() {
		try {
			Thread.sleep(10000);
			log.info("onSync");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
