package com.gk337.api.async.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.async.service.AsyncService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class AsyncController {

	@Autowired
	private AsyncService asyncService;

	@GetMapping("/async")
	public String goAsync() {
		asyncService.onAsync();
		String str = "Hello Spring Boot Async!!";
		log.info(str);
		log.info("==================================");
		return str;
	}
	
	@GetMapping("/sync")
	public String goSync() {
		asyncService.onSync();
		String str = "Hello Spring Boot Sync!!";
		log.info(str);
		log.info("==================================");
		return str;
	}
}
