package com.gk337.api.request.component;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gk337.api.base.entity.DeviceInfoSetting;
import com.gk337.api.base.entity.FmcMessageRequest;
import com.gk337.api.base.service.NotificationService;
import com.gk337.api.product.entity.ProductAttachFileEntity;
import com.gk337.api.product.entity.ProductEntity;
import com.gk337.api.request.entity.RequestEntity;
import com.gk337.api.request.entity.RequestMatchMember;
import com.gk337.api.request.entity.RequestProductAtfilRequest;
import com.gk337.api.request.entity.RequestRequest;
import com.gk337.api.request.entity.RequestResendRequest;
import com.gk337.api.request.entity.RequestResendResponse;
import com.gk337.api.request.entity.RequestResponse;
import com.gk337.api.request.entity.RequestStatusRequest;
import com.gk337.api.request.entity.ResponseEntity;
import com.gk337.api.request.entity.ResponseProductAtfilEntity;
import com.gk337.api.request.entity.ResponseProductAtfilRequest;
import com.gk337.api.request.entity.ResponseProductEntity;
import com.gk337.api.request.entity.ResponseRequest;
import com.gk337.api.request.entity.ResponseResponse;
import com.gk337.api.request.repository.ResponseProductAtfilRepository;
import com.gk337.api.request.repository.ResponseProductRepository;
import com.gk337.api.request.service.RequestService;
import com.gk337.api.verify.entity.UserMatchingResponse;
import com.gk337.api.verify.service.VerifyService;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.exception.JGWCommunicationException;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.CollectionUtils;
import com.gk337.common.util.MessageUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class RequestComponent {

	@Autowired
	private RequestService requestService;

	@Autowired
	private VerifyService verifyService;

	@Autowired
	private NotificationService notificationService;
	
	/**
	 * 판매/구매 요청서 등록 또는 수정.
	 * @param request
	 * @return
	 */
	@Transactional
	public RequestResponse insertRequest(RequestRequest request) {
		
		RequestResponse requestResponse = new RequestResponse();
		try {
			
			// 상품저장.
			ProductEntity productEntity = new ProductEntity();
			BeanUtils.copyProperties(request, productEntity);
			productEntity.setTradeType(request.getReqType());
			productEntity.setCompleteYn("N");
			productEntity.setShowYn("Y");
			productEntity.setTempYn("N");

			productEntity = requestService.insertProduct(productEntity);
			int prodSeq = productEntity.getSeq();
			
			// 상품이미지저장.
			List<RequestProductAtfilRequest> attachFiles = request.getAttachFiles();
			if(attachFiles.size() > 0) {
				for(int i=0; i<attachFiles.size(); i++) {
					RequestProductAtfilRequest atfilRequest = (RequestProductAtfilRequest) attachFiles.get(i);
					ProductAttachFileEntity productAtfilEntity = new ProductAttachFileEntity();
					productAtfilEntity.setFileSeq(atfilRequest.getFileSeq());
					productAtfilEntity.setProdSeq(prodSeq);
					productAtfilEntity.setOrderBy(i+1);
					productAtfilEntity.setMemSeq(request.getMemSeq());
					productAtfilEntity.setMainYn(atfilRequest.getMainYn());
					if(i == 0) {
						productAtfilEntity.setMainYn("Y");
					}
					requestService.insertProductAttachFile(productAtfilEntity);
				}
			}
			
			// 지역정보 Match 코드 조회 후 거래정보 저장 시 Insert
			Integer areaSeq = requestService.selectAreaSeq(request.getAddressDiv1(), request.getAddressDiv2());
			request.setAreaSeq(areaSeq);
			
			// 거래정보 저장 또는 수정.
			request.setProdSeq(prodSeq);
			RequestEntity entity = requestService.insertRequest(request);

			// 저장완료 후 저장정보 리턴.
			requestResponse = requestService.selectRequestInfo(entity.getSeq());
			
			// 매칭 유저 정보 조회
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("seq", entity.getSeq());
			paramMap.put("reqType", request.getTradeType());
			paramMap.put("memSeq", request.getMemSeq());
			paramMap.put("reqSeq", request.getSeq());
			List<RequestMatchMember> matchMemberList = requestService.selectRequestMatchMember(paramMap);
			requestResponse.setRequestMatchMemberList(matchMemberList);
			requestResponse.setMatchMemberCnt(matchMemberList != null ? matchMemberList.size(): 0);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return requestResponse;
		
	}
	
	/*
	 * 	@Transactional
	public RequestResponse insertRequest(RequestRequest request) {
		
		RequestResponse requestResponse = new RequestResponse();
		try {
			
			// 상품저장.
			RequestProductEntity productEntity = new RequestProductEntity();
			BeanUtils.copyProperties(request, productEntity);
			productEntity.setTradeType(request.getReqType());
			productEntity = requestService.insertRequestProduct(productEntity);
			int prodSeq = productEntity.getSeq();
			
			// 상품이미지저장.
			List<RequestProductAtfilRequest> attachFiles = request.getAttachFiles();
			if(attachFiles.size() > 0) {
				for(int i=0; i<attachFiles.size(); i++) {
					RequestProductAtfilRequest atfilRequest = (RequestProductAtfilRequest) attachFiles.get(i);
					RequestProductAtfilEntity productAtfilEntity = new RequestProductAtfilEntity();
					productAtfilEntity.setFileSeq(atfilRequest.getFileSeq());
					productAtfilEntity.setProdSeq(prodSeq);
					productAtfilEntity.setOrderBy(i+1);
					productAtfilEntity.setMemSeq(request.getMemSeq());
					productAtfilEntity.setMainYn(atfilRequest.getMainYn());
					if(i == 0) {
						productAtfilEntity.setMainYn("Y");
					}
					requestService.insertRequestProductAtfil(productAtfilEntity);
				}
			}
			
			// 지역정보 Match 코드 조회 후 거래정보 저장 시 Insert
			Integer areaSeq = requestService.selectAreaSeq(request.getAddressDiv1(), request.getAddressDiv2());
			request.setAreaSeq(areaSeq);
			
			// 거래정보 저장 또는 수정.
			request.setProdSeq(prodSeq);
			RequestEntity entity = requestService.insertRequest(request);

			// 저장완료 후 저장정보 리턴.
			requestResponse = requestService.selectRequestInfo(entity.getSeq());
			
			// 매칭 유저 정보 조회
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("memSeq", entity.getSeq());
			paramMap.put("reqType", request.getReqType());
			List<RequestMatchMember> matchMemberList = requestService.selectRequestMatchMember(paramMap);
			requestResponse.setRequestMatchMemberList(matchMemberList);
			requestResponse.setMatchMemberCnt(matchMemberList != null ? matchMemberList.size(): 0);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return requestResponse;
		
	}*/

	/**
	 * 판매/구매 요청서 재전송.
	 * @param request
	 * @return
	 */
	@Transactional
	public RequestResendResponse resendRequest(RequestResendRequest request) throws JGWCommunicationException {

		RequestResendResponse requestResendResponse = new RequestResendResponse();
		RequestResponse requestResponse = requestService.selectRequestInfo(request.getReqSeq());
		requestResendResponse.setRequestResponse(requestResponse);

		// 요청서 등록자에게 발송 메세지
		FmcMessageRequest message1 = new FmcMessageRequest();
		message1.setName("중고왕의 알림 메세지");
		message1.setNotificationTitle("[긴급요청서 조정 알림]");
		message1.setNotificationBody(String.format("%s 을\n관리자가 확인 후 정상발송 했습니다.\n%s",
				requestResponse.getProdName(), request.getAdminMessage()));

		try {
			CompletableFuture<String> pushNotification = notificationService.send(requestResponse.getMemSeq(), message1);
			CompletableFuture.allOf(pushNotification).join();

			try {
				String firebaseResponse = pushNotification.get();
				System.out.println("firebaseResponse : " + firebaseResponse.toString());
			} catch (InterruptedException e) {
				log.debug("got interrupted!");
				e.printStackTrace();
				throw new JGWCommunicationException("네트워크 오류");
			} catch (ExecutionException e) {
				log.debug("execution error!");
				e.printStackTrace();
				throw new JGWCommunicationException(" 오류");
			}
		} catch (UnsupportedEncodingException e) {
			log.debug("encoding error!");
			e.printStackTrace();
			throw new JGWCommunicationException("인코딩 오류");
		}

		// 카테고리 정상일 경우만 처리
		if(!"N".equals(requestResponse.getValidCateYn())) {
			// Push 메세지 처리 - 요청서에 해당되는 딜러의 모든 기기에 전송
			FmcMessageRequest message = new FmcMessageRequest();
			message.setName("중고왕의 알림 메세지");
			message.setNotificationTitle(String.format("[%s요청서 알림]", requestResponse.getReqTypeName()));
			message.setNotificationBody(String.format("%s님이 %s에\n대한 %s요청을 하였습니다.",
					requestResponse.getMemName(), requestResponse.getProdName(), requestResponse.getReqTypeName()));
			message.setAppData("{\"url\":\"request_recieve_item\", \"param\":{\"reqSeq\":"+requestResponse.getSeq()+"}}");

			// 대상 사용자 목록
			List<UserMatchingResponse> list = verifyService.selectVerifyUserList(requestResponse.getSeq(), requestResponse.getReqType());
			// 본인 제외
			List<UserMatchingResponse> filterResult = list.stream() .filter(a -> !a.getMemSeq().equals(request.getMemSeq())) .collect(Collectors.toList());
			// 대상 기기목록으로 변환
			List<DeviceInfoSetting> recipientList = CollectionUtils.convert(filterResult, DeviceInfoSetting::new);

			try {
				CompletableFuture<String> pushNotification = notificationService.sendBulk(recipientList, message);
				CompletableFuture.allOf(pushNotification).join();

				try {
					String firebaseResponse = pushNotification.get();
					requestResendResponse.setNotificationResult(firebaseResponse);
					System.out.println("firebaseResponse : " + firebaseResponse.toString());
				} catch (InterruptedException e) {
					log.debug("got interrupted!");
					e.printStackTrace();
					throw new JGWCommunicationException("네트워크 오류");
				} catch (ExecutionException e) {
					log.debug("execution error!");
					e.printStackTrace();
					throw new JGWCommunicationException(" 오류");
				}
			} catch (UnsupportedEncodingException e) {
				log.debug("encoding error!");
				e.printStackTrace();
				throw new JGWCommunicationException("인코딩 오류");
			}
		}

		return requestResendResponse;
	}
	
	/**
	 * 판매/구매/기부 요청서 삭제.
	 * 실제로는 cstatus 값을 D로 업데이트.
	 * @param reqSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteRequest(int reqSeq) {
		
		ErrorVo result = new ErrorVo();
		try {
			
			RequestResponse requestResponse = requestService.selectRequestInfo(reqSeq);
			
			// 상품이미지 정보 삭제
			requestService.deleteProductAtfilBySeq(requestResponse.getProdSeq());
			
			// 상품정보 삭제
			requestService.deleteProductBySeq(requestResponse.getProdSeq());
			
			// 문의내용 삭제
			// requestService.deleteResponseBySeq(reqSeq);
			
			// 요청상태 정보 삭제.
			// requestService.deleteRequestStatusBySeq(reqSeq);
			
			// 요청서 삭제
			requestService.deleteRequestBySeq(reqSeq);
			
			result.setMessage(MessageUtil.get("MESSAGE.RESULT.0001"));
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "삭제");
		}
		return result;
		
	}
	
	/**
	 * 요청서 상세보기 
	 * @param reqSeq
	 * @return
	 */
	public RequestResponse selectRequestInfo(int reqSeq) {
		// 신규문의내용여부를 업데이트.
		requestService.updateIsNew(reqSeq, "N");
		RequestResponse response = requestService.selectRequestInfo(reqSeq);
		return response;
	}
	
	
	/**
	 * 응답정보 저장
	 * @param request
	 * @return
	 */
	@Transactional
	public ResponseResponse insertResponse(ResponseRequest request) {
		ResponseResponse response = new ResponseResponse();
		try {
			
			RequestResponse reqeustResponse = requestService.selectRequestInfo(request.getReqSeq());
			if(reqeustResponse == null) {
				throw new JGWServiceException("MESSAGE.REQUEST.0001");
			}

			// 답변 상품저장.
			ResponseProductEntity productEntity = new ResponseProductEntity();
			// 요청서 정보에서 기본정보 입력
			productEntity.setMemSeq(request.getMemSeq());
			productEntity.setProdName(reqeustResponse.getProdName());
			// 거래타입은 요청서의 반대로 등록 (판매요청서에 답변하면 구매이므로)
			productEntity.setTradeType("S".equals(reqeustResponse.getReqType()) ? "B" : "S");
			if(reqeustResponse.getCateSeq1dp() != null) {
				productEntity.setCateSeq1dp(Integer.parseInt(reqeustResponse.getCateSeq1dp()));
			}
			if(reqeustResponse.getCateSeq2dp() != null) {
				productEntity.setCateSeq2dp(Integer.parseInt(reqeustResponse.getCateSeq1dp()));
			}
			if(reqeustResponse.getCateSeq3dp() != null) {
				productEntity.setCateSeq3dp(Integer.parseInt(reqeustResponse.getCateSeq1dp()));
			}
			// 답변 정보로 부가정보 입력
			productEntity.setPrice(request.getPrice());
			productEntity.setProdDesc(request.getComments());
			productEntity = requestService.insertResponseProduct(productEntity);
			int prodSeq = productEntity.getSeq();
			request.setProdSeq(prodSeq);

			// 응답의 상품 이미지저장.
			List<ResponseProductAtfilRequest> attachFiles = request.getAttachFiles();
			if(attachFiles != null && attachFiles.size() > 0) {
				for(int i=0; i<attachFiles.size(); i++) {
					ResponseProductAtfilRequest atfilRequest = (ResponseProductAtfilRequest) attachFiles.get(i);
					ResponseProductAtfilEntity productAtfilEntity = new ResponseProductAtfilEntity();
					productAtfilEntity.setFileSeq(atfilRequest.getFileSeq());
					productAtfilEntity.setProdSeq(request.getProdSeq());
					productAtfilEntity.setOrderBy(i+1);
					productAtfilEntity.setMemSeq(request.getMemSeq());
					productAtfilEntity.setMainYn(atfilRequest.getMainYn());
					if(i == 0) {
						productAtfilEntity.setMainYn("Y");
					}
					requestService.insertResponseProductAtfil(productAtfilEntity);
				}
			}
			
			// 응답정보 저장
			ResponseEntity entity = requestService.insertResponse(request);
			
			// isNew 업데이트 
			requestService.updateIsNew(request.getReqSeq(), "Y");
			
			// 요청서 상태정보 등록 - 거래중..
			RequestStatusRequest status = new RequestStatusRequest();
			status.setMemSeq(entity.getMemSeq());
			status.setReqSeq(request.getReqSeq());
			status.setReqStatus("I");
			requestService.insertRequestStatus(status);
			
			// 저장완료 후 저장정보 리턴.
			response = requestService.selectResponseInfo(entity.getSeq(), request.getReqSeq());
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0002", "저장");
		}
		return response;
		
	}
	
	
	
	@ Autowired
	private ResponseProductAtfilRepository responseAtfilRepository;
	
	@ Autowired
	private ResponseProductRepository responseProductRepository;
	
	/**
	 * 응답정보 삭제
	 * @param reqSeq
	 * @param memSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteResponse(int resSeq, int reqSeq) {
		
		ErrorVo result = new ErrorVo();
		try {
			ResponseResponse response = requestService.selectResponseInfo(resSeq, reqSeq);
			
			// 상품이미지 테이블 cstatus 업데이트 
			responseAtfilRepository.updateCstatus(response.getSendProdSeq());
			
			// 상품테이블 cstatus 업데이트 
			responseProductRepository.updateCstatus(response.getSendProdSeq());
			
			// 문의(Response) 테이블 cstatus 업데이트 
			requestService.deleteResponseBySeq(resSeq);
			
			result.setMessage(MessageUtil.get("MESSAGE.RESULT.0001"));
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0002", "삭제");
		}
		return result;
		
	}

}
