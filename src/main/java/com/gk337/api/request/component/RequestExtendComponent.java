package com.gk337.api.request.component;

import com.gk337.api.base.entity.DeviceInfoSetting;
import com.gk337.api.base.entity.FmcMessageRequest;
import com.gk337.api.base.service.NotificationService;
import com.gk337.api.request.entity.*;
import com.gk337.api.request.mapper.RequestExtendMapper;
import com.gk337.api.request.repository.*;
import com.gk337.api.request.service.RequestExtendService;
import com.gk337.api.request.service.RequestService;
import com.gk337.api.verify.entity.UserMatchingResponse;
import com.gk337.api.verify.service.VerifyService;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.exception.JGWCommunicationException;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.BeanUtilsExt;
import com.gk337.common.util.CollectionUtils;
import com.gk337.common.util.MessageUtil;
import com.gk337.v2api.point.entity.PointDetailEntity;
import com.gk337.v2api.point.entity.PointMasterEntity;
import com.gk337.v2api.point.mapper.PointMapper;
import com.gk337.v2api.point.repository.PointMasterEntityRepository;
import com.gk337.v2api.point.service.PointService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Slf4j
@Component
public class RequestExtendComponent {

    @Autowired
	private RequestComponent requestComponent;

    @Autowired
	private RequestService requestService;

	@Autowired
	private RequestExtendService requestExtendService;

	@Autowired
	private RequestExtendMapper requestExtendMapper;

	@Autowired
	private VerifyService verifyService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private PointService pointService;

	@Autowired
	private RequestRepository requestRepository;

	@Autowired
	private ResponseRepository responseRepository;

	@Autowired
	private ResponseGmidasEntityRepository responseGmidasEntityRepository;

	@Autowired
	private PointMasterEntityRepository pointMasterEntityRepository;

	@Autowired
	private PointMapper pointMapper;

	@Autowired
	private EntityManager em;

	/**
	 * 판매/구매 요청서 등록 또는 수정.
	 * @param request
	 * @return
	 */
	@Transactional
	public RequestExtendResponse insertRequest(RequestExtendRequest request) {
		
		RequestExtendResponse requestExtendResponse = new RequestExtendResponse();
		RequestResponse requestResponse = new RequestResponse();

		try {
			
			// 상품저장.
			RequestProductEntity productEntity = new RequestProductEntity();
			BeanUtils.copyProperties(request, productEntity);
			productEntity.setTradeType(request.getReqType());
			productEntity.setCompleteYn("N");
			productEntity.setShowYn("Y");
			productEntity.setTempYn("N");
			productEntity = requestService.insertRequestProduct(productEntity);
			int prodSeq = productEntity.getSeq();
			
			// 상품이미지저장.
			List<RequestProductAtfilRequest> attachFiles = request.getAttachFiles();
			if(attachFiles.size() > 0) {
				for(int i=0; i<attachFiles.size(); i++) {
					RequestProductAtfilRequest atfilRequest = (RequestProductAtfilRequest) attachFiles.get(i);
					RequestProductAtfilEntity productAtfilEntity = new RequestProductAtfilEntity();
					productAtfilEntity.setFileSeq(atfilRequest.getFileSeq());
					productAtfilEntity.setProdSeq(prodSeq);
					productAtfilEntity.setOrderBy(i+1);
					productAtfilEntity.setMemSeq(request.getMemSeq());
					productAtfilEntity.setMainYn(atfilRequest.getMainYn());
					if(i == 0) {
						productAtfilEntity.setMainYn("Y");
					}
					requestService.insertRequestProductAtfil(productAtfilEntity);
				}
			}
			
			// 지역정보 Match 코드 조회 후 거래정보 저장 시 Insert
			Integer areaSeq = requestService.selectAreaSeq(request.getAddressDiv1(), request.getAddressDiv2());
			request.setAreaSeq(areaSeq);
			
			// 기본정보로 거래정보 저장 또는 수정.
			request.setProdSeq(prodSeq);
			RequestEntity entity = requestService.insertRequest(request.toRequestBasic());

			//기본정보 저장
			requestResponse = requestService.selectRequestInfo(entity.getSeq());
			BeanUtilsExt.copyNonNullProperties(requestResponse, requestExtendResponse);
			requestExtendResponse.setPickupDateStr(request.getPickupDateStr());

			// 확장정보 저장
			request.setReqSeq(requestResponse.getSeq());
			requestExtendService.insertRequestExtend(request);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return requestExtendResponse;
		
	}

	/**
	 * 판매/구매 요청서 재전송.
	 * @param request
	 * @return
	 */
	@Transactional
	public RequestResendResponse resendRequest(RequestResendRequest request) throws JGWCommunicationException {

		RequestResendResponse requestResendResponse = new RequestResendResponse();
		RequestResponse requestResponse = requestService.selectRequestInfo(request.getReqSeq());
		requestResendResponse.setRequestResponse(requestResponse);

		// 요청서 등록자에게 발송 메세지
		FmcMessageRequest message1 = new FmcMessageRequest();
		message1.setName("중고왕의 알림 메세지");
		message1.setNotificationTitle("[긴급요청서 조정 알림]");
		message1.setNotificationBody(String.format("%s 을\n관리자가 확인 후 정상발송 했습니다.\n%s",
				requestResponse.getProdName(), request.getAdminMessage()));

		try {
			CompletableFuture<String> pushNotification = notificationService.send(requestResponse.getMemSeq(), message1);
			CompletableFuture.allOf(pushNotification).join();

			try {
				String firebaseResponse = pushNotification.get();
				System.out.println("firebaseResponse : " + firebaseResponse.toString());
			} catch (InterruptedException e) {
				log.debug("got interrupted!");
				e.printStackTrace();
				throw new JGWCommunicationException("네트워크 오류");
			} catch (ExecutionException e) {
				log.debug("execution error!");
				e.printStackTrace();
				throw new JGWCommunicationException(" 오류");
			}
		} catch (UnsupportedEncodingException e) {
			log.debug("encoding error!");
			e.printStackTrace();
			throw new JGWCommunicationException("인코딩 오류");
		}

		// 카테고리 정상일 경우만 처리
		if(!"N".equals(requestResponse.getValidCateYn())) {
			// Push 메세지 처리 - 요청서에 해당되는 딜러의 모든 기기에 전송
			FmcMessageRequest message = new FmcMessageRequest();
			message.setName("중고왕의 알림 메세지");
			message.setNotificationTitle(String.format("[%s요청서 알림]", requestResponse.getReqTypeName()));
			message.setNotificationBody(String.format("%s님이 %s에\n대한 %s요청을 하였습니다.",
					requestResponse.getMemName(), requestResponse.getProdName(), requestResponse.getReqTypeName()));
			message.setAppData("{\"url\":\"request_recieve_item\", \"param\":{\"reqSeq\":"+requestResponse.getSeq()+"}}");

			// 대상 사용자 목록
			List<UserMatchingResponse> list = verifyService.selectVerifyUserList(requestResponse.getSeq(), requestResponse.getReqType());
			// 본인 제외
			List<UserMatchingResponse> filterResult = list.stream() .filter(a -> !a.getMemSeq().equals(request.getMemSeq())) .collect(Collectors.toList());
			// 대상 기기목록으로 변환
			List<DeviceInfoSetting> recipientList = CollectionUtils.convert(filterResult, DeviceInfoSetting::new);

			try {
				CompletableFuture<String> pushNotification = notificationService.sendBulk(recipientList, message);
				CompletableFuture.allOf(pushNotification).join();

				try {
					String firebaseResponse = pushNotification.get();
					requestResendResponse.setNotificationResult(firebaseResponse);
					System.out.println("firebaseResponse : " + firebaseResponse.toString());
				} catch (InterruptedException e) {
					log.debug("got interrupted!");
					e.printStackTrace();
					throw new JGWCommunicationException("네트워크 오류");
				} catch (ExecutionException e) {
					log.debug("execution error!");
					e.printStackTrace();
					throw new JGWCommunicationException(" 오류");
				}
			} catch (UnsupportedEncodingException e) {
				log.debug("encoding error!");
				e.printStackTrace();
				throw new JGWCommunicationException("인코딩 오류");
			}
		}

		return requestResendResponse;
	}
	
	/**
	 * 판매/구매/기부 요청서 삭제.
	 * 실제로는 cstatus 값을 D로 업데이트.
	 * @param reqSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteRequest(int reqSeq) {
		
		ErrorVo result = new ErrorVo();
		try {
			
			RequestResponse requestResponse = requestService.selectRequestInfo(reqSeq);
			
			// 상품이미지 정보 삭제
			requestService.deleteProductAtfilBySeq(requestResponse.getProdSeq());
			
			// 상품정보 삭제
			requestService.deleteProductBySeq(requestResponse.getProdSeq());
			
			// 문의내용 삭제
			// requestService.deleteResponseBySeq(reqSeq);
			
			// 요청상태 정보 삭제.
			// requestService.deleteRequestStatusBySeq(reqSeq);
			
			// 요청서 삭제
			requestService.deleteRequestBySeq(reqSeq);
			
			result.setMessage(MessageUtil.get("MESSAGE.RESULT.0001"));
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "삭제");
		}
		return result;
		
	}
	
	/**
	 * 요청서 상세보기 
	 * @param reqSeq
	 * @return
	 */
	public RequestExtendResponse selectRequestInfo(Integer reqSeq) {

		RequestExtendResponse response = new RequestExtendResponse();

		// 기존 로직 그대로 처리
		RequestResponse responseBasic = requestComponent.selectRequestInfo(reqSeq);
		BeanUtilsExt.copyNonNullProperties(responseBasic, response);

		// 지마이다스 확장정보 조회
		Optional<RequestGmidasEntity> entity = requestExtendService.getRequestGmidasEntity(reqSeq);
		if(entity.isPresent()) {
			response.setPickupDate(entity.get().getPickupDate());
			response.setPickupDateStr(entity.get().getPickupDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		} else {
			throw new JGWServiceException("ERROR.REQUEST_EXTEND.0003", Integer.toString(reqSeq));
		}

		return response;
	}
	
	
	/**
	 * 응답정보 저장
	 * @param request
	 * @return
	 */
	@Transactional
	public ResponseExtendResponse insertResponse(ResponseExtendRequest request) {

		ResponseExtendResponse response = new ResponseExtendResponse();

	    // 기본정보 저장 - 기존 답변 프로세스 그대로 처리
        ResponseResponse responseBasic = requestComponent.insertResponse(request.toResponseBasic());
		BeanUtilsExt.copyNonNullProperties(responseBasic, response);

		try {
			request.setResSeq(responseBasic.getResSeq());
			requestExtendService.insertResponseExtend(request);
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.RESPONSE_EXTEND.0002", "저장");
		}

		response.setBuyPoint(request.getBuyPoint());
		return response;
		
	}
	
	/**
	 * 답변 정보 조회
	 * @param resSeq
	 * @param reqSeq
	 * @return
	 */
	public ResponseExtendResponse selectResponseInfo(Integer resSeq, Integer reqSeq) {
		ResponseExtendResponse response = new ResponseExtendResponse();

		// 기본정보 조회
		ResponseResponse responseBasic = requestService.selectResponseInfo(resSeq, reqSeq);
		BeanUtilsExt.copyNonNullProperties(responseBasic, response);

		// 확장정보 조회
		Optional<ResponseGmidasEntity> entity = requestExtendService.getResponseGmidasEntity(resSeq, reqSeq);
		if(entity.isPresent()) {
			response.setBuyPoint(entity.get().getBuyPoint());
			response.setResStatus(entity.get().getResStatus());
		} else {
			throw new JGWServiceException("ERROR.RESPONSE_EXTEND.0003", Integer.toString(reqSeq));
		}

		// 이전에 확정 된 목록
        Optional<ResponseGmidasEntity> existsEntity = responseGmidasEntityRepository.findByReqSeqAndResStatus(reqSeq, "S");
        response.setIsSelected(existsEntity.isPresent() ? "Y" : "N");

		return response;
	}

	/**
	 * 답변 상태 변경 및 포인트 차감 등록
	 * @param resSeq
	 * @param resStatus
	 * @return
	 */
	@Transactional
	public Map<String, Object> updateResStatusAndPoint(Integer resSeq, Integer reqSeq, String resStatus) {

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("reqSeq", reqSeq);
		paramMap.put("resSeq", resSeq);
		Map<String, Object> resultMap = requestExtendMapper.selectProductInfo(paramMap);

		requestExtendService.updateResStatus(resSeq, resStatus);
		// 요청서 번호가 있는 경우 포인트 차감 로직 수행
		if(reqSeq != null && reqSeq > 0) {

			// 요청서 조회
			// TODO : v2 적용 - 요청서 + 답변 + 지마이다스 조인 Entity 필요
			Optional<RequestEntity> requestEntityOption = requestRepository.findById(reqSeq);
			Optional<ResponseEntity> responseEntityOption = responseRepository.findById(resSeq);
			Optional<ResponseGmidasEntity> responseGmidasEntityOption = responseGmidasEntityRepository.findByResSeqAndReqSeq(resSeq, reqSeq);

			if(requestEntityOption.isPresent() && responseEntityOption.isPresent() && responseGmidasEntityOption.isPresent()) {
				RequestEntity requestEntity = requestEntityOption.get();
				ResponseEntity responseEntity= responseEntityOption.get();
				ResponseGmidasEntity responseGmidasEntity= responseGmidasEntityOption.get();

				Map<String, Object> data = pointMasterEntityRepository.getMyPoint(responseEntity.getMemSeq());
				if(data == null) {
				    throw new JGWServiceException("ERROR.RESPONSE_EXTEND.0004");
                }
				Integer availablePoint = ((BigDecimal) data.get("point")).intValue();
				if(availablePoint > responseGmidasEntity.getBuyPoint()) {
					PointMasterEntity master = PointMasterEntity.builder()
					.pointType("GMIDAS")
					.pointStatus("U")
					.memSeq(responseEntity.getMemSeq())
					.point(responseGmidasEntity.getBuyPoint()*-1)
					.reqDate(LocalDateTime.now())
					.pointExpireDate(LocalDateTime.of(9999,12,31,23,59,59))
					.build();

					master =pointService.insertPointMaster(master);

					em.flush();

					paramMap.put("memSeq", responseEntity.getMemSeq());
					paramMap.put("sellMemSeq", requestEntity.getMemSeq());
					paramMap.put("point", responseGmidasEntity.getBuyPoint());
					paramMap.put("orgSeq", master.getSeq());
					paramMap.put("pointStatus", "U");

					pointMapper.insertPointUsed(paramMap);
				} else {
					throw new JGWServiceException("ERROR.RESPONSE_EXTEND.0004");
				}
			}
		}

		return resultMap;
	}

}
