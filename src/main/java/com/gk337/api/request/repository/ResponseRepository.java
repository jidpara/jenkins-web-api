package com.gk337.api.request.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.request.entity.RequestEntity;
import com.gk337.api.request.entity.ResponseEntity;

import java.util.Optional;

/**
 * 
 * @author maroojjang
 *
 */
@Repository
public interface ResponseRepository extends PagingAndSortingRepository<ResponseEntity, Integer>, JpaRepository<ResponseEntity, Integer> {

	@SuppressWarnings("unchecked")
	ResponseEntity save(ResponseEntity entity);
	
	void deleteBySeq(int resSeq);
	
	// 요청자만 요청서 삭제 가능 = 상태값을 D로 업데이트.
	@Modifying
	@Query("update ResponseEntity q set q.cstatus = 'D' where q.seq = :resSeq")
	void updateCstatus(@Param("resSeq") int resSeq);
}