package com.gk337.api.request.repository;

import com.gk337.api.request.entity.RequestGmidasEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RequestGmidasEntityRepository extends JpaRepository<RequestGmidasEntity, Integer>, JpaSpecificationExecutor<RequestGmidasEntity> {

}