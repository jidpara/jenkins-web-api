package com.gk337.api.request.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.request.entity.RequestStatusEntity;

/**
 * 
 * @author maroojjang
 *
 */
@Repository
public interface RequestStatusRepository extends PagingAndSortingRepository<RequestStatusEntity, Integer>, JpaRepository<RequestStatusEntity, Integer> {
	
	@SuppressWarnings("unchecked")
	RequestStatusEntity save(RequestStatusEntity entity);
	
	void deleteBySeq(int seq);
	
	// 상태값 (I:거래중, Y:거래완료, C:거래취소 D:삭제)
	@Modifying
	@Query("update RequestStatusEntity q set q.reqStatus = :reqStatus where q.reqSeq = :reqSeq")
	void updateRequestStatus(@Param("reqSeq") int reqSeq, @Param("reqStatus") String reqStatus);
	
	// 요청자만 요청서 삭제 가능 = 상태값을 D로 업데이트.
	@Modifying
	@Query("update RequestStatusEntity q set q.cstatus = 'D' where q.reqSeq = :reqSeq")
	void updateCstatus(@Param("reqSeq") int reqSeq);
	
}