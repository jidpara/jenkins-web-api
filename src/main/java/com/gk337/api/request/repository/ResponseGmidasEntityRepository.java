package com.gk337.api.request.repository;

import com.gk337.api.request.entity.ResponseGmidasEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.Optional;

public interface ResponseGmidasEntityRepository extends JpaRepository<ResponseGmidasEntity, Integer>, JpaSpecificationExecutor<ResponseGmidasEntity> {

    Optional<ResponseGmidasEntity> findByResSeqAndReqSeq(Integer resSeq, Integer reqSeq);

    // 요청서 seq 와 상태로 조회
    Optional<ResponseGmidasEntity> findByReqSeqAndResStatus(Integer reqSeq, String resStatus);

    @Modifying
	@Query("update ResponseGmidasEntity r set r.resStatus = :resStatus, r.chgDate = :chgDate where r.resSeq = :resSeq")
	void updateResStatus(@Param("resSeq") Integer resSeq, @Param("resStatus") String resStatus, @Param("chgDate") LocalDateTime chgDate);

}