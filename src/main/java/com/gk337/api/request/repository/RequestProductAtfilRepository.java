package com.gk337.api.request.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.request.entity.RequestProductAtfilEntity;

/**
 * 
 * @author maroojjang
 *
 */
@Repository
public interface RequestProductAtfilRepository extends PagingAndSortingRepository<RequestProductAtfilEntity, Integer>, JpaRepository<RequestProductAtfilEntity, Integer> {
	
	@SuppressWarnings("unchecked")
	RequestProductAtfilEntity save(RequestProductAtfilEntity entity);
	
	void deleteByProdSeqAndMemSeq(int prodSeq, int memSeq);
	
	// 요청자만 요청서 삭제 가능 = 상태값을 D로 업데이트.
	@Modifying
	@Query("update RequestProductAtfilEntity q set q.cstatus = 'D' where q.prodSeq = :prodSeq")
	void updateCstatus(@Param("prodSeq") int prodSeq);
	
	// 요청서 등록시 첨부된 파일 단건 삭제
	Long deleteByFileSeq(int fileSeq);
	
	// 대표이미지 변경을 위해 상품이미지 그룹시퀀스 조회.
	RequestProductAtfilEntity findByFileSeq(int fileSeq);
	
	// 상품 그룹에 있는 모든 대표이미지 플레그 초기화.
	@Modifying
	@Query("update RequestProductAtfilEntity q set q.mainYn = '' where q.prodSeq = :prodSeq")
	void updateByProdSeq(int prodSeq);
	
	// 선택된 이미지시퀀스로 대표이미지 설정.
	@Modifying
	@Query("update RequestProductAtfilEntity q set q.mainYn = 'Y' where q.fileSeq = :fileSeq")
	int updateByFileSeq(int fileSeq);
	
	
}