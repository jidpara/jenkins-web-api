package com.gk337.api.request.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.request.entity.RequestEntity;

import java.util.Optional;

/**
 * 
 * @author maroojjang
 *
 */
@Repository
public interface RequestRepository extends PagingAndSortingRepository<RequestEntity, Integer>, JpaRepository<RequestEntity, Integer> {
	
	@SuppressWarnings("unchecked")
	RequestEntity save(RequestEntity entity);
	
	void deleteBySeq(int seq);

	Optional<RequestEntity> findById(Integer reqSeq);

	// 요청자만 요청서 삭제 가능 = 상태값을 D로 업데이트.
	@Modifying
	@Query("update RequestEntity q set q.cstatus = 'D' where q.seq = :reqSeq")
	void updateCstatus(@Param("reqSeq") int reqSeq);
	
	@Modifying
	@Query("UPDATE RequestEntity q set q.isNew = :isNew where q.seq = :seq")
	void updateIsNew(@Param("seq") int seq, @Param("isNew") String isNew);
	
}