package com.gk337.api.request.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.request.entity.RequestProductEntity;

/**
 * 
 * @author maroojjang
 *
 */
@Repository
public interface RequestProductRepository extends PagingAndSortingRepository<RequestProductEntity, Integer>, JpaRepository<RequestProductEntity, Integer> {
	
	@SuppressWarnings("unchecked")
	RequestProductEntity save(RequestProductEntity entity);
	
	void deleteBySeq(int seq);
	
	// 요청자만 요청서 삭제 가능 = 상태값을 D로 업데이트.
	@Modifying
	@Query("update RequestProductEntity q set q.cstatus = 'D' where q.seq = :prodSeq")
	void updateCstatus(@Param("prodSeq") int prodSeq);
	
}