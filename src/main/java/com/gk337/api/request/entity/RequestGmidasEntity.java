package com.gk337.api.request.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 요청서 확장 - 지마이다스
 */
@Entity
@Table(name = "jw_request_gmidas")
@Data
public class RequestGmidasEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 요청서 시퀀스
     */
    @Id
    @Column(name = "req_seq", insertable = false, nullable = false)
    private Integer reqSeq;

    /**
     * 희망 수거일
     */
    @Column(name = "pickup_date", nullable = false)
    private LocalDate pickupDate;

    
}