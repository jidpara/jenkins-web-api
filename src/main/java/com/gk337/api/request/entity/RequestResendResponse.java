package com.gk337.api.request.entity;

import com.gk337.common.core.mvc.model.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "판매/구매/기부 요청서 Response Vo")
public class RequestResendResponse extends BaseVo {
	
	@ApiModelProperty(notes = "요청서 정보")
    private RequestResponse requestResponse;
	
	@ApiModelProperty(notes = "전송결과", example = "")
	private String notificationResult;


}
