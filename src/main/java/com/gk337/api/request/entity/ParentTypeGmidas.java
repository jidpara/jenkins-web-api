package com.gk337.api.request.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "요청서 상위 타입 - GMIDAS")
public class ParentTypeGmidas {
	
	@ApiModelProperty(notes = "타입(REQ001 - BASIC:기본, BASIC_EMERGENCY:긴급요청, GMIDAS:지마이다스)", example = "GMIDAS")
    private String type = "GMIDAS";

    @ApiModelProperty(notes = "희망수거일문자열 (yyyy-MM-dd)", example = "2020-07-07")
    private String pickupDateStr;

	@ApiModelProperty(notes = "희망수거일문자열 (yyyy-MM-dd)", example = "2020-07-07")
    private LocalDateTime pickupDate;
}
