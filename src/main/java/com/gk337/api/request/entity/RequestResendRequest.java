package com.gk337.api.request.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "판매/구매 요청서 재발송 Request Vo")
public class RequestResendRequest {
	
    @ApiModelProperty(notes = "요청서 시퀀스", example = "1")
    private Integer reqSeq;

    @ApiModelProperty(notes = "발송자 회원시퀀스", example = "-1")
    private Integer memSeq;

    @ApiModelProperty(notes = "관리자 메세지 (요청서 등록자에게 보내는 메세지)", example = "카테고리 신규 추가 후 재발송 완료 ^.^")
    private String adminMessage;

}
