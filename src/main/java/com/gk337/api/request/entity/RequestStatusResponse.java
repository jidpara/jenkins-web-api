package com.gk337.api.request.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "구매/판매 요청서상태 Response Vo")
public class RequestStatusResponse {
	
	@ApiModelProperty(notes = "요청서시퀀스", example = "10")
	private Integer reqSeq;

	@ApiModelProperty(notes = "요청서답변 시퀀스", example = "10")
	private Integer resSeq;

	@ApiModelProperty(notes = "답변한 회원시퀀스", example = "1")
	private Integer sendMemSeq;
	
	@ApiModelProperty(notes = "답변한 회원명", example = "홍길동")
	private String sendMemName;
	
    @ApiModelProperty(notes = "요청상태[거래완료:Y 거래중:I 거래취소:C]", example="N")
    private String reqStatus;
    
    @ApiModelProperty(notes = "요청상태명", example="거래중")
    private String reqStatusName;

}
