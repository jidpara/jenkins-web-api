package com.gk337.api.request.entity;

import java.util.List;

import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "판매상품 Response Vo")
public class RequestProductResponse {
	
	@ApiModelProperty(notes = "요청서시퀀스", example = "12")
    private Integer seq;
	
	@ApiModelProperty(notes = "상품시퀀스", example = "12")
    private Integer prodSeq;
	
	@ApiModelProperty(notes = "회원시퀀스", example = "1")
	private Integer memSeq;
	
	@ApiModelProperty(notes = "요청서구분[판매:S / 구매:B]", example = "S")
    private String reqType;
	
	@ApiModelProperty(notes = "요청서구분명", example = "판매")
    private String reqTypeName;
	
	@ApiModelProperty(notes = "회원구분[유저:G / 딜러:P]", example = "P")
	private String memKind;
	
	@ApiModelProperty(notes = "회원구분명", example = "딜러")
	private String memKindName;
	
	@ApiModelProperty(notes = "이메일", example = "abc@abc.com")
	private String memEmail;
	
	@ApiModelProperty(notes = "회원닉네임", example = "코로나19")
	private String memNickname;
	
	@ApiModelProperty(notes = "회원이름", example = "홍길동")
	private String memName;
	
	@ApiModelProperty(notes = "회원휴대폰번호", example = "01012349876")
	private String memHp;

	@ApiModelProperty(notes = "카테고리명1", example = "생활/가전")
	private String cate1dpName;
	
	@ApiModelProperty(notes = "카테고리명2", example = "생활")
	private String cate2dpName;
	
	@ApiModelProperty(notes = "카테고리명3", example = "컴퓨터")
	private String cate3dpName;
	
	@ApiModelProperty(notes = "카테고리1", example = "1")
	private String cateSeq1dp;
	
	@ApiModelProperty(notes = "카테고리2", example = "11")
	private String cateSeq2dp;
	
	@ApiModelProperty(notes = "카테고리3", example = "1001015")
	private String cateSeq3dp;
	
	@ApiModelProperty(notes = "상품명", example = "게이밍노트북")
	private String prodName;
	
	@ApiModelProperty(notes = "상품상세", example = "게이밍노트북,게이밍노트북,게이밍노트북,게이밍노트북")
	private String prodDesc;
	
	@ApiModelProperty(notes = "가격", example = "12000")
	private String price;
	
	@ApiModelProperty(notes = "키워드", example = "게이밍노트북,게이밍노트북,게이밍노트북,게이밍노트북")
	private String keyWords;
	
	@ApiModelProperty(notes = "등록일자", example = "2020-02-22 12:11:12")
	private String regDate;
	
	@ApiModelProperty(notes = "첨부파일[멀티건]")
	private List<RequestProductAtfilResponse> requestProductAtfilResponse; 

}
