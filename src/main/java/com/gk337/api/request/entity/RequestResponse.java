package com.gk337.api.request.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "판매/구매/기부 요청서 Response Vo")
public class RequestResponse extends BaseVo {
	
	@ApiModelProperty(notes = "요청서시퀀스", example = "12")
    private Integer seq;

	@ApiModelProperty(notes = "[REQ001] 요청서 상위 타입(BASIC : 기본, GMIDAS : 지마이다스)")
    private String parentType;
	
	@ApiModelProperty(notes = "상품시퀀스", example = "12")
    private Integer prodSeq;
	
	@ApiModelProperty(notes = "회원시퀀스", example = "1")
	private Integer memSeq;
	
	@ApiModelProperty(notes = "요청서구분[판매:S / 구매:B]", example = "S")
    private String reqType;
	
	@ApiModelProperty(notes = "요청서구분명", example = "판매")
    private String reqTypeName;
	
	@ApiModelProperty(notes = "회원구분[유저:G / 딜러:P]", example = "P")
	private String memKind;
	
	@ApiModelProperty(notes = "회원구분명", example = "딜러")
	private String memKindName;

	@ApiModelProperty(notes = "거래상태 (I:거래중, Y:거래완료)", example = "Y")
	private String reqStatus;

	@ApiModelProperty(notes = "거래상태명", example = "거래완료")
	private String reqStatusName;
	
	@ApiModelProperty(notes = "시간경과", example = "24시간 지남")
	private String timeGap;
	
	@ApiModelProperty(notes = "판매/구매 주소", example = "경기도")
	private String address;
	
	@ApiModelProperty(notes = "판매/구매 주소1", example = "용인시")
	private String addressDiv1;
	
	@ApiModelProperty(notes = "판매/구매 주소2", example = "기흥구")
	private String addressDiv2;
	
	@ApiModelProperty(notes = "판매/구매 주소3", example = "보라동 용구대로 3")
	private String addressDiv3;
	
	@ApiModelProperty(notes = "이메일", example = "abc@abc.com")
	private String memEmail;
	
	@ApiModelProperty(notes = "회원닉네임", example = "코로나19")
	private String memNickname;
	
	@ApiModelProperty(notes = "회원이름", example = "홍길동")
	private String memName;
	
	@ApiModelProperty(notes = "회원주소", example = "경기도")
	private String memAddr;
	
	@ApiModelProperty(notes = "회원상세주소", example = "용인시 기흥구 보라동 용구대로 1323")
	private String memAddrDetail;
	
	@ApiModelProperty(notes = "회원휴대폰번호", example = "01012349876")
	private String memHp;

	@ApiModelProperty(notes = "카테고리명1", example = "생활/가전")
	private String cate1dpName;
	
	@ApiModelProperty(notes = "카테고리명2", example = "생활")
	private String cate2dpName;
	
	@ApiModelProperty(notes = "카테고리명3", example = "컴퓨터")
	private String cate3dpName;
	
	@ApiModelProperty(notes = "카테고리1", example = "1")
	private String cateSeq1dp;
	
	@ApiModelProperty(notes = "카테고리2", example = "11")
	private String cateSeq2dp;
	
	@ApiModelProperty(notes = "카테고리3", example = "1001015")
	private String cateSeq3dp;
	
	@ApiModelProperty(notes = "상품명", example = "게이밍노트북")
	private String prodName;
	
	@ApiModelProperty(notes = "상품상세", example = "게이밍노트북,게이밍노트북,게이밍노트북,게이밍노트북")
	private String prodDesc;
	
	@ApiModelProperty(notes = "가격", example = "12000")
	private String price;
	
	@ApiModelProperty(notes = "키워드", example = "게이밍노트북,게이밍노트북,게이밍노트북,게이밍노트북")
	private String keyWords;
	
	@ApiModelProperty(notes = "답변건수", example = "11")
	private String resCnt;
	
	@ApiModelProperty(notes = "새로운 문의내용 체크", example = "Y")
	private String isNew;

	@ApiModelProperty(notes = "딜러긴급요청 여부", example = "Y")
	private String dealerYn;

	@ApiModelProperty(notes = "카테고리 정상 여부", example = "Y")
	private String validCateYn;
	
	@ApiModelProperty(notes = "등록일자", example = "2020-02-22 12:11:12")
	private String regDate;

	@ApiModelProperty(notes = "등록일자 - 당일 데이터(시:분) - 13:20 \n" +
			"\n" +
			"당월 데이터(월-일 시:분) - 06-24 13:20\n" +
			"\n" +
			"당년 데이터(연-월-일 시:분) - 2020-06-24 13:20", example = "2020-02-28 11:12:13")
    private String regDateStr;
	
	@ApiModelProperty(notes = "프로필이미지", example = "https://item.kakaocdn.net/do/aaacfe7e3c6d0564fded2313c5397779f43ad912ad8dd55b04db6a64cddaf76d")
	private String profileImage;

	@ApiModelProperty(notes = "이전 요청서시퀀스", example = "12")
    private Integer prevReqSeq;

	@ApiModelProperty(notes = "다음 요청서시퀀스", example = "12")
    private Integer nextReqSeq;

	@ApiModelProperty(notes = "이전 요청서 상위타입", example = "BASIC")
    private String prevParentType;

	@ApiModelProperty(notes = "다음 요청서 상위타입", example = "BASIC")
    private String nextParentType;

	@ApiModelProperty(notes = "첨부파일[멀티건]")
	private List<RequestProductAtfilResponse> requestProductAtfilResponse; 

	private List<RequestMatchMember> requestMatchMemberList;

	@ApiModelProperty(notes = "매칭된 딜러 수")
	private Integer matchMemberCnt;
	
	@ApiModelProperty(notes = "보관 여부")
	private String tempYn;
}
