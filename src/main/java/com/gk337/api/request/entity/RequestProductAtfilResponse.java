package com.gk337.api.request.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 구매/판매/기부 요청 엔티티 
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "요청상품 이미지 Response")
public class RequestProductAtfilResponse implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;
    
    @ApiModelProperty(notes = "파일시퀀스", example = "14")
    private Integer fileSeq;

    @ApiModelProperty(notes = "파일 썸네일 URL", example = "http://d1n8fbd6e2ut61.cloudfront.net/asset/upload/product20200220b2e78cd7-d38c-417b-a5c4-1aa87a5e4b96")
    private String fileUrl;

    @ApiModelProperty(notes = "파일 원본 URL", example = "http://d1n8fbd6e2ut61.cloudfront.net/asset/upload/product20200220b2e78cd7-d38c-417b-a5c4-1aa87a5e4b96")
    private String fileUrlOrg;

    @ApiModelProperty(notes = "파일명", example = "tesgt.jpg")
    private String fileName;
    
}