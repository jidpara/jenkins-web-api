package com.gk337.api.request.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 구매/판매/기부 요청 엔티티 
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_response")
@ApiModel(description = "구매/판매요청 응답 Entity")
public class ResponseEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @NotNull
    @Column(name = "cstatus")
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";

    @Column(name = "parent_type")
    @ApiModelProperty(notes = "[REQ001] 요청서 상위 타입(BASIC : 기본, GMIDAS : 지마이다스)")
    private String parentType;

    @NotNull
    @Column(name = "req_seq")
    @ApiModelProperty(notes = "요청서 시퀀스")
    private Integer reqSeq;

    @NotNull
    @Column(name = "mem_seq")
    @ApiModelProperty(notes = "응답을 보낸 회원시퀀스")
    private Integer memSeq;

    @NotNull
    @Column(name = "prod_seq")
    @ApiModelProperty(notes = "답변 상품 시퀀스")
    private Integer prodSeq;

    @NotNull
    @Column(name = "comments")
    @ApiModelProperty(notes = "문의내용, 구매/판매의사 문의내용")
    private String comments;

    @Column(name = "price")
    @ApiModelProperty(notes = "금액")
    private Integer price;
    
}