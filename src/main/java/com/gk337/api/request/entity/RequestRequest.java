package com.gk337.api.request.entity;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.gk337.api.base.entity.BaseUploadS3Request;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "판매/구매/기부 요청서 Request Vo")
public class RequestRequest {
	
	@JsonIgnore
    @ApiModelProperty(notes = "요청서 시퀀스", example = "")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example = "Y")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "요청서 상위타입(REQ001 - BASIC:기본, BASIC_EMERGENCY:긴급요청, GMIDAS:지마이다스)", example = "BASIC")
    private String parentType = "BASIC";

    @ApiModelProperty(notes = "요청서 타입(B:구매/S:판매/D:기부)", example = "B")
    private String reqType;
    
    @NotNull
    @ApiModelProperty(notes = "회원시퀀스", example = "1")
    private Integer memSeq;

    @JsonIgnore
    @ApiModelProperty(notes = "상품시퀀스", example = "1")
    private Integer prodSeq;

    @ApiModelProperty(notes = "전체주소", example = "경기도 이천시 부발읍 아미리 726-5")
    private String address = "서울특별시 송파구 신천동 29 롯데월드타워앤드롯데월드몰";

    @ApiModelProperty(notes = "도/시 이름", example = "경기도")
    private String addressDiv1 = "서울특별시";
    
    @ApiModelProperty(notes = "시/군/구 이름", example = "이천시 부발읍")
    private String addressDiv2 = "송파구";
    
    @ApiModelProperty(notes = "법정동/법정리 이름", example = "아미리")
    private String addressDiv3 = "신천동";
    
    @JsonIgnore
    @ApiModelProperty(notes = "매칭에 필요한 지역 시퀀스", example = "3001")
    private Integer areaSeq;
    
    @ApiModelProperty(notes = "새로운 응답이 있는지 여부.", example = "Y")
    private String isNew ="N";

    @ApiModelProperty(notes = "딜러긴급요청 여부", example = "Y")
    private String dealerYn = "N";

    @ApiModelProperty(notes = "카테고리 정상 여부", example = "Y")
    private String validCateYn = "Y";
    
    @ApiModelProperty(notes = "카테고리", example = "1100001")
    private Integer cateSeq;
    
    // 아래는 상품정보.
    @ApiModelProperty(notes = "1차카테고리시퀀스", example = "1")
    private Integer cateSeq1dp;

    @ApiModelProperty(notes = "2차카테고리시퀀스", example = "1")
    private Integer cateSeq2dp;

    @ApiModelProperty(notes = "3차카테고리시퀀스", example = "1")
    private Integer cateSeq3dp;

    @ApiModelProperty(notes = "상품명", example = "그람17인치")
    private String prodName;

    @ApiModelProperty(notes = "가격", example = "100000")
    private Integer price;

    @ApiModelProperty(notes = "상품정보", example = "상품설명입니다.")
    private String prodDesc;

    @ApiModelProperty(notes = "AS가능여부", example = "Y")
    private String asYn;

    @ApiModelProperty(notes = "택배비포함여부", example = "Y")
    private String deliveryPriceYn;

    @ApiModelProperty(notes = "물품상태 (S:미사용, A:거의새것, B:중고, C:하자)", example = "S")
    private String prodStatus;

    @ApiModelProperty(notes = "상품검색 키워드 (총5개까지 콤마로 구분)", example = "키워드 흠")
    private String keyWords;

    @ApiModelProperty(notes = "업로드 파일")
    private List<RequestProductAtfilRequest> attachFiles;
    
    @ApiModelProperty(notes = "보관 여부")
    private String tempYn;
    
    @ApiModelProperty(notes = "거래 타입")
    private String tradeType;

}
