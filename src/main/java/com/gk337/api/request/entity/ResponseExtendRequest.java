package com.gk337.api.request.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.util.BeanUtilsExt;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 구매/판매/기부 요청 엔티티 
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "구매/판매요청 응답 Entity")
public class ResponseExtendRequest implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(notes = "요청 답변 시퀀스")
    private Integer resSeq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "요청서 상위타입(REQ001 - BASIC:기본, BASIC_EMERGENCY:긴급요청, GMIDAS:지마이다스)", example = "GMIDAS")
    private String parentType = "GMIDAS";

    @ApiModelProperty(notes = "요청서 시퀀스", example = "1")
    private Integer reqSeq;

    @ApiModelProperty(notes = "답변 보낸 회원시퀀스", example = "7")
    private Integer memSeq;

    @ApiModelProperty(notes = "답변내용, 구매/판매의사 답변", example = "요청에 대한 답변 내용")
    private String comments;

    @ApiModelProperty(notes = "구매포인트", example = "10000")
    private Integer buyPoint;

    @JsonIgnore
    public ResponseRequest toResponseBasic() {
        ResponseRequest response = new ResponseRequest();
        BeanUtilsExt.copyNonNullProperties(this, response);
        return response;
    }
    
}