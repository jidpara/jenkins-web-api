package com.gk337.api.request.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.util.BeanUtilsExt;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "판매/구매/기부 요청서 Request Vo")
public class RequestExtendRequest {

    @JsonIgnore
    @ApiModelProperty(notes = "요청서 시퀀스", example = "")
    private Integer reqSeq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example = "Y")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "요청서 상위타입(REQ001 - BASIC:기본, BASIC_EMERGENCY:긴급요청, GMIDAS:지마이다스)", example = "GMIDAS")
    private String parentType = "GMIDAS";

    @ApiModelProperty(notes = "희망수거일 (yyyy-MM-dd)", example = "2020-07-07")
    private String pickupDateStr;

    @ApiModelProperty(notes = "요청서 타입(B:구매/S:판매/D:기부)", example = "B")
    private String reqType;

    @NotNull
    @ApiModelProperty(notes = "회원시퀀스", example = "1")
    private Integer memSeq;

    @JsonIgnore
    @ApiModelProperty(notes = "상품시퀀스", example = "1")
    private Integer prodSeq;

    @ApiModelProperty(notes = "전체주소", example = "경기도 이천시 부발읍 아미리 726-5")
    private String address;

    @ApiModelProperty(notes = "도/시 이름", example = "경기도")
    private String addressDiv1;

    @ApiModelProperty(notes = "시/군/구 이름", example = "이천시 부발읍")
    private String addressDiv2;

    @ApiModelProperty(notes = "법정동/법정리 이름", example = "아미리")
    private String addressDiv3;

    @JsonIgnore
    @ApiModelProperty(notes = "매칭에 필요한 지역 시퀀스", example = "3001")
    private Integer areaSeq;

    @ApiModelProperty(notes = "상품명", example = "그람17인치")
    private String prodName;

    @ApiModelProperty(notes = "상품정보", example = "상품설명입니다.")
    private String prodDesc;

    @ApiModelProperty(notes = "업로드 파일")
    private List<RequestProductAtfilRequest> attachFiles;

    @JsonIgnore
    public RequestRequest toRequestBasic() {
        RequestRequest request = new RequestRequest();
        BeanUtilsExt.copyNonNullProperties(this, request);
        return request;
    }
}
