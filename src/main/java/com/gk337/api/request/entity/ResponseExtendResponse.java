package com.gk337.api.request.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gk337.common.core.mvc.model.BaseVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "응답하기 Response Vo")
public class ResponseExtendResponse extends BaseVo {
	
	@ApiModelProperty(notes = "보낸요청서시퀀스", example = "10")
    private Integer resSeq;
	
	@ApiModelProperty(notes = "요청서시퀀서", example = "10")
    private Integer reqSeq;
	
	@ApiModelProperty(notes = "회원시퀀스", example = "1")
    private Integer memSeq;
	
	@ApiModelProperty(notes = "상품시퀀스", example = "12")
    private Integer prodSeq;

	@ApiModelProperty(notes = "요청서 상위타입(REQ001 - BASIC:기본, BASIC_EMERGENCY:긴급요청, GMIDAS:지마이다스)", example = "GMIDAS")
    private String parentType = "GMIDAS";

	@ApiModelProperty(notes = "요청서타입", example = "B")
    private String reqType;
	
	@ApiModelProperty(notes = "응답을 보낸 회원시퀀스", example = "7")
    private Integer sendMemSeq;
	
	@ApiModelProperty(notes = "회원이름", example = "홍길동")
    private String memName;
	
	@ApiModelProperty(notes = "닉네임", example = "닉네임")
    private String memNickname;
	
	@ApiModelProperty(notes = "회원휴대폰번호", example = "01012349876")
    private String memHp;

	@ApiModelProperty(notes = "상품명", example = "선풍기")
    private String prodName;

	@ApiModelProperty(notes = "구매포인트", example = "15000")
    private Integer buyPoint;

	@ApiModelProperty(notes = "새로운 응답 여부", example = "N")
    private String isNew;

	@ApiModelProperty(notes = "보낸요청서 카운트", example = "12")
    private String resCnt;
	
	@ApiModelProperty(notes = "보낸회원의 주소1", example = "경기도 성남시 수정구 시흥동 2")
    private String shopAddr1;
	
	@ApiModelProperty(notes = "보낸회원의 주소2", example = "판교 제2테크노밸리 경기 기업성장센터 21")
    private String shopAddr2;
		
	@ApiModelProperty(notes = "등록일자", example = "2020-02-28 11:12:13")
    private String regDate;

	@ApiModelProperty(notes = "등록일자 포멧팅", example = "2020-02-28")
    private String regDateStr;
	
	@ApiModelProperty(notes = "문의내용", example = "문의내용입니다. 구매의사...")
    private String comments;

	@ApiModelProperty(notes = "채팅방시퀀스", example = "55")
	private Integer roomSeq;
	
	@ApiModelProperty(notes = "프로필이미지", example = "https://item.kakaocdn.net/do/aaacfe7e3c6d0564fded2313c5397779f43ad912ad8dd55b04db6a64cddaf76d")
	private String profileImage;

	@ApiModelProperty(notes = "이전 요청서응답 시퀀스", example = "12")
    private Integer prevResSeq;

	@ApiModelProperty(notes = "다음 요청서응답 시퀀스", example = "12")
    private Integer nextResSeq;

	@ApiModelProperty(notes = "희망수거일", example = "2020-07-01 11:12:13")
    private LocalDate pickupDate;

	@ApiModelProperty(notes = "희망수거일 포멧팅", example = "2020-07-01")
    private String pickupDateStr;

	@ApiModelProperty(notes = "[RES001] 답변 상태 ( I : 진행중, S: 사용자선택, C: 취소)", example = "I")
    private String resStatus;

	@ApiModelProperty(notes = "해당 요청서의 (다른)답변을 이미 선택했는지 여부 ( Y: 선택, N:미선택 ", example = "Y")
    private String isSelected;
}
