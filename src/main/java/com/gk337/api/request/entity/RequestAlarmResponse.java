package com.gk337.api.request.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "알림함 Response Vo")
public class RequestAlarmResponse {
	
	@ApiModelProperty(notes = "요청서시퀀스", example = "33")
    private Integer reqSeq;

    @ApiModelProperty(notes = "[REQ001] 요청서 상위 타입(BASIC : 기본, GMIDAS : 지마이다스)")
    private String parentType;

	@ApiModelProperty(notes = "받은요청서시퀀스", example = "31")
    private Integer resSeq;
	
	@ApiModelProperty(notes = "상품시퀀스", example = "49")
    private Integer prodSeq;
	
	@ApiModelProperty(notes = "회원시퀀스", example = "3")
    private Integer memSeq;
	
	@ApiModelProperty(notes = "요청서타입[판매:S/구매:B]", example = "S")
    private String reqType;
	
	@ApiModelProperty(notes = "회원종류[딜러:P/유저:G]", example = "P")
    private String memKind;
	
	@ApiModelProperty(notes = "요청서타입명", example = "판매요청")
    private String reqTypeName;
	
	@ApiModelProperty(notes = "회원종류명", example = "딜러")
    private String memKindName;
	
	@ApiModelProperty(notes = "닉네임", example = "중고신청자")
    private String memNickname;
	
	@ApiModelProperty(notes = "회원프로필이미지", example = "https://monthly.chosun.com/up_fd/Mdaily/2017-09/bimg_thumb/2017042000056_0.jpg")
    private String profileImage;
	
	@ApiModelProperty(notes = "상품명", example = "선풍기")
    private String prodName;
	
	@ApiModelProperty(notes = "응답의 상품이미지 URL", example = "https://monthly.chosun.com/up_fd/Mdaily/2017-09/bimg_thumb/2017042000056_0.jpg")
    private String resProdImage;
	
	@ApiModelProperty(notes = "신규알림여부", example = "N")
    private String isNew;
	
	@ApiModelProperty(notes = "상품명리스트", example = "선풍기에 대한 판매요청")
    private String prodNameTitle;

    @ApiModelProperty(notes = "상품메인원본이미지(대표이미지)", example = "http://d1n8fbd6e2ut61.cloudfront.net/assets/upload/product/20200326/d53b0a14-41f5-4c9c-9639-b18de97fffa7")
    private String prodImageOrg;

	@ApiModelProperty(notes = "상품메인썸네일이미지(대표이미지)", example = "http://d1n8fbd6e2ut61.cloudfront.net/assets/upload/product/20200326/d53b0a14-41f5-4c9c-9639-b18de97fffa7")
    private String prodImage;
			
	@ApiModelProperty(notes = "등록일자", example = "2020-02-28 11:12:13")
    private String regDate;

    @ApiModelProperty(notes = "등록일자 - 당일 데이터(시:분) - 13:20 \n" +
			"\n" +
			"당월 데이터(월-일 시:분) - 06-24 13:20\n" +
			"\n" +
			"당년 데이터(연-월-일 시:분) - 2020-06-24 13:20", example = "2020-02-28 11:12:13")
    private String regDateStr;

}
