package com.gk337.api.request.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 구매/판매/기부 요청 엔티티 
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_request")
@ApiModel(description = "구매/판매/기부 요청 Entity")
public class RequestEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @NotNull
    @Column(name = "cstatus")
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";

    @Column(name = "parent_type")
    @ApiModelProperty(notes = "[REQ001] 요청서 상위 타입(BASIC : 기본, GMIDAS : 지마이다스)")
    private String parentType;

    @NotNull
    @Column(name = "req_type")
    @ApiModelProperty(notes = "요청서타입(구매/판매/기부)")
    private String reqType;
    
    @NotNull
    @Column(name = "mem_seq")
    @ApiModelProperty(notes = "회원시퀀스")
    private Integer memSeq;

    @NotNull
    @Column(name = "prod_seq")
    @ApiModelProperty(notes = "상품시퀀스")
    private Integer prodSeq;
    
    @Column(name = "cate_seq")
    @ApiModelProperty(notes = "카테고리시퀀스", example = "1100001")
    private Integer cateSeq;

    @NotNull
    @Column(name = "address")
    @ApiModelProperty(notes = "전체주소")
    private String address;

    @Column(name = "address_div1")
    @ApiModelProperty(notes = "도/시 이름")
    private String addressDiv1;
    
    @Column(name = "address_div2")
    @ApiModelProperty(notes = "시/군/구 이름")
    private String addressDiv2;
    
    @Column(name = "address_div3")
    @ApiModelProperty(notes = "법정동/법정리 이름")
    private String addressDiv3;
    
    @Column(name = "area_seq")
    @ApiModelProperty(notes = "매칭에 필요한 지역 시퀀스")
    private Integer areaSeq;
    
    @Column(name = "is_new")
    @ApiModelProperty(notes = "새로운 응답이 있는지 여부.")
    private String isNew;

    @Column(name = "dealer_yn")
    @ApiModelProperty(notes = "딜러긴급요청 여부", example = "N")
    private String dealerYn = "N";

    @Column(name = "valid_cate_yn")
    @ApiModelProperty(notes = "카테고리 정상 여부", example = "Y")
    private String validCateYn = "Y";
    
}