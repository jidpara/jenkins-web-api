package com.gk337.api.request.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 구매/판매/기부 요청 엔티티 
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_product")
@ApiModel(description = "요청상품 Entity")
public class RequestProductEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @NotNull
    @Column(name = "cstatus")
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";

    @Column(name = "trade_type")
    @ApiModelProperty(notes = "[PR001] 거래타입 (S:판매/B:구매)")
    private String tradeType;

    @Column(name = "mem_seq", nullable = false)
    @ApiModelProperty(notes = "회원시퀀스")
    private Integer memSeq;

    @Column(name = "cate_seq_1dp")
    @ApiModelProperty(notes = "상품카테고리1")
    private Integer cateSeq1dp;

    @Column(name = "cate_seq_2dp")
    @ApiModelProperty(notes = "상품카테고리2")
    private Integer cateSeq2dp;

    @Column(name = "cate_seq_3dp")
    @ApiModelProperty(notes = "상품카테고리3")
    private Integer cateSeq3dp;

    @Column(name = "prod_name")
    @ApiModelProperty(notes = "상품명")
    private String prodName;

    @Column(name = "prod_desc")
    @ApiModelProperty(notes = "상품설명")
    private String prodDesc;

    @Column(name = "price")
    @ApiModelProperty(notes = "가격")
    private Integer price;

    @Column(name = "as_yn")
    @ApiModelProperty(notes = "AS가능여부")
    private String asYn;

    @Column(name = "delivery_price_yn")
    @ApiModelProperty(notes = "택배비포함여부")
    private String deliveryPriceYn;

    @Column(name = "prod_status")
    @ApiModelProperty(notes = "물품상태 (S:미사용, A:거의새것, B:중고, C:하자)")
    private String prodStatus;

    @Column(name = "key_words")
    @ApiModelProperty(notes = "상품검색 키워드 (총5개까지 콤마로 구분)")
    private String keyWords;

    @Column(name = "read_cnt")
    @ApiModelProperty(notes = "조회수")
    private Integer readCnt = 0;

    @Column(name = "recommend_yn")
    @ApiModelProperty(notes = "추천구분")
    private String recommendYn = "";

    @Column(name = "complete_yn")
    @ApiModelProperty(notes = "구매 완료 구분(I:거래중, Y:거래완료, C:거래취소 D:삭제) ")
    private String completeYn;

    @Column(name = "show_yn")
    @ApiModelProperty(notes = "노출 여부")
    private String showYn;

    @Column(name = "temp_yn")
    @ApiModelProperty(notes = "보관 여부")
    private String tempYn;
    
}