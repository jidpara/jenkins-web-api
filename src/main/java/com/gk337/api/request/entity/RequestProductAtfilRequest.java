package com.gk337.api.request.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 구매/판매/기부 요청 엔티티 
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "요청상품 이미지 Request")
public class RequestProductAtfilRequest implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";
    
    @JsonIgnore
    @ApiModelProperty(notes = "상품시퀀스", example = "1")
    private Integer prodSeq;
    
    @ApiModelProperty(notes = "메인여부", example = "Y")
    private String mainYn;

    @ApiModelProperty(notes = "이미지시퀀스", example = "1")
    private Integer fileSeq;

    @JsonIgnore
    @ApiModelProperty(notes = "정렬순번", example = "1")
    private Integer orderBy;
    
}