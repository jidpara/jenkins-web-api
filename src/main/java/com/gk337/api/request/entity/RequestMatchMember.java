package com.gk337.api.request.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "요청서 매칭 사용자 VO")
public class RequestMatchMember implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ApiModelProperty(notes = "요청서 매칭 되는 딜러사용자 시퀀스")
    private Integer memSeq;

    @JsonIgnore
    @ApiModelProperty(notes = "딜러사용자의 디바이스토큰")
    private String deviceToken;

}