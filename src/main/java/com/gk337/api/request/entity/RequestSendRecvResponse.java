package com.gk337.api.request.entity;

import java.util.List;

import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "보낸요청서, 받은요청서 Response Vo")
public class RequestSendRecvResponse extends BaseVo {
	
	@ApiModelProperty(notes = "요청서시퀀스", example = "10")
    private Integer reqSeq;
	
	@ApiModelProperty(notes = "응답보낸시퀀스", example = "10")
    private Integer resSeq;
	
	@ApiModelProperty(notes = "회원시퀀스", example = "1")
    private Integer memSeq;
	
	@ApiModelProperty(notes = "상품시퀀스", example = "12")
    private Integer prodSeq;
	
	@ApiModelProperty(notes = "요청서타입", example = "B")
    private String reqType;
	
	@ApiModelProperty(notes = "요청서타입명", example = "구매")
    private String reqTypeName;
	
	@ApiModelProperty(notes = "거래상태", example = "거래완료")
	private String reqStatusName;
	
	@ApiModelProperty(notes = "시간경과", example = "24시간 지남")
	private String timeGap;

	@ApiModelProperty(notes = "주소 - 전체", example = "경기도 이천시 부발읍 아미리 726-5")
	private String address;

	@ApiModelProperty(notes = "주소 - 시도", example = "경기도")
	private String addressDiv1;

	@ApiModelProperty(notes = "주소 - 구군", example = "이천시")
	private String addressDiv2;

	@ApiModelProperty(notes = "주소 - 읍면동", example = "부발읍")
	private String addressDiv3;

	@ApiModelProperty(notes = "회원종류", example = "P")
    private String memKind;
	
	@ApiModelProperty(notes = "회원종류명", example = "딜러")
    private String memKindName;
	
	@ApiModelProperty(notes = "회원이름", example = "홍길동")
    private String memName;
	
	@ApiModelProperty(notes = "닉네임", example = "닉네임")
    private String memNickname;
	
	@ApiModelProperty(notes = "memEmail", example = "abc@abc.com")
    private String memEmail;
	
	@ApiModelProperty(notes = "회원휴대폰번호", example = "01012349876")
    private String memHp;
	
	@ApiModelProperty(notes = "카테고리1", example = "1")
	private String cateSeq1dp;
	
	@ApiModelProperty(notes = "카테고리2", example = "11")
	private String cateSeq2dp;
	
	@ApiModelProperty(notes = "카테고리3", example = "1001015")
	private String cateSeq3dp;
	
	@ApiModelProperty(notes = "상품명", example = "선풍기")
    private String prodName;
	
	@ApiModelProperty(notes = "가격", example = "15000")
	private String price;
	
	@ApiModelProperty(notes = "새글여부", example = "Y")
	private String isNew;

	@ApiModelProperty(notes = "나의답변여부", example = "Y")
    private String isResponse;

	@ApiModelProperty(notes = "응답건수", example = "1")
    private Integer resCnt;
	
	@ApiModelProperty(notes = "등록일자", example = "2020-02-28 11:12:13")
    private String regDate;

	@ApiModelProperty(notes = "등록일자 - 당일 데이터(시:분) - 13:20 \n" +
			"\n" +
			"당월 데이터(월-일 시:분) - 06-24 13:20\n" +
			"\n" +
			"당년 데이터(연-월-일 시:분) - 2020-06-24 13:20", example = "2020-02-28 11:12:13")
    private String regDateStr;
	
	@ApiModelProperty(notes = "이미지파일 리스트")
	private List<RequestProductAtfilResponse> requestProductAtfilResponse;

	@ApiModelProperty(notes = "지마이다스 요청서 정보")
	private ParentTypeGmidas parentTypeGmidas;


}
