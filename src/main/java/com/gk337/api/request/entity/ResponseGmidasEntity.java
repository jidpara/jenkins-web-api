package com.gk337.api.request.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 요청서 답변 확장 - 지마이다스
 */
@Entity
@Table(name = "jw_response_gmidas")
@Data
@IdClass(ResponseGmidasPK.class)
public class ResponseGmidasEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 답변 시퀀스
     */
    @Id
    @Column(name = "res_seq", nullable = false)
    private Integer resSeq;

    /**
     * 요청서 시퀀스
     */
    @Id
    @Column(name = "req_seq", nullable = false)
    private Integer reqSeq;

    /**
     * 구매 포인트
     */
    @Column(name = "buy_point", nullable = false)
    private Integer buyPoint;


    /**
     * [RES001]답변 상태 ( I : 진행중, S: 사용자선택, C: 취소)
     */
    @Column(name = "res_status")
    private String resStatus;

    @CreatedDate
	@Column(name="reg_date", nullable=false, updatable=false)
	@JsonProperty(value = "reg_date", access = JsonProperty.Access.WRITE_ONLY)
	private LocalDateTime regDate;

	@LastModifiedDate
	@Column(name="chg_date", nullable=false)
	@JsonProperty(value = "chg_date", access = JsonProperty.Access.WRITE_ONLY)
	private LocalDateTime chgDate;
}