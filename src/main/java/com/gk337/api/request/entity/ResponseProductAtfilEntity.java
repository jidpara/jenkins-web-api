package com.gk337.api.request.entity;

import com.gk337.common.model.AuditEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 구매/판매/기부 요청 엔티티 
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_product_attach_file")
@ApiModel(description = "요청서 답변 상품 이미지 Entity")
public class ResponseProductAtfilEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @NotNull
    @Column(name = "cstatus")
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";
    
    @NotNull
    @Column(name = "prod_seq")
    @ApiModelProperty(notes = "상품시퀀스")
    private Integer prodSeq;
    
    @NotNull
    @Column(name = "mem_seq")
    @ApiModelProperty(notes = "등록한 회원시퀀스")
    private Integer memSeq;
    
    @Column(name = "main_yn")
    @ApiModelProperty(notes = "메인여부")
    private String mainYn;

    @NotNull
    @Column(name = "file_seq")
    @ApiModelProperty(notes = "이미지시퀀스")
    private Integer fileSeq;

    @NotNull
    @Column(name = "order_by")
    @ApiModelProperty(notes = "정렬순번")
    private Integer orderBy;
    
}