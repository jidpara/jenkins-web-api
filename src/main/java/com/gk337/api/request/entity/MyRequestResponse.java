package com.gk337.api.request.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "요청상태 Response Vo")
public class MyRequestResponse {
	
    @ApiModelProperty(notes = "금일 판매요청 횟수", example = "1")
    private Integer sellCount;

    @ApiModelProperty(notes = "금일 구매요청 횟수", example = "1")
    private Integer buyCount;

}
