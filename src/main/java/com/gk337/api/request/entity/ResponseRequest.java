package com.gk337.api.request.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 구매/판매/기부 요청 엔티티 
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "구매/판매요청 응답 Entity")
public class ResponseRequest implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "요청서 상위타입(REQ001 - BASIC:기본, BASIC_EMERGENCY:긴급요청, GMIDAS:지마이다스)", example = "GMIDAS")
    private String parentType = "GMIDAS";

    @ApiModelProperty(notes = "요청서 시퀀스", example = "1")
    private Integer reqSeq;

    @ApiModelProperty(notes = "답변 보낸 회원시퀀스", example = "7")
    private Integer memSeq;

    @JsonIgnore
    @ApiModelProperty(notes = "답변 상품 시퀀스", example = "7")
    private Integer prodSeq;

    @ApiModelProperty(notes = "답변내용, 구매/판매의사 답변", example = "요청에 대한 답변 내용")
    private String comments;

    @ApiModelProperty(notes = "금액", example = "10000")
    private Integer price;
    
    @ApiModelProperty(notes = "업로드 파일")
    private List<ResponseProductAtfilRequest> attachFiles;
    
}