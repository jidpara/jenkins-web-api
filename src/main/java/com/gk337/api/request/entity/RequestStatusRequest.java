package com.gk337.api.request.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 구매/판매/기부 요청 상태 엔티티 
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "구매/판매/기부 요청서 상태 Entity")
public class RequestStatusRequest implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus = "Y";
    
    @ApiModelProperty(notes = "요청서시퀀스", example="1")
    private Integer reqSeq;

    @JsonIgnore
    @ApiModelProperty(notes = "회원시퀀스", example="1")
    private Integer memSeq;

    @ApiModelProperty(notes = "요청상태[거래완료:Y 거래중:I 거래취소:C]", example="I")
    private String reqStatus;
    
}