package com.gk337.api.request.controller;

import com.github.pagehelper.PageInfo;
import com.gk337.api.base.entity.DeviceInfoSetting;
import com.gk337.api.base.entity.FmcMessageRequest;
import com.gk337.api.base.service.NotificationService;
import com.gk337.api.request.component.RequestComponent;
import com.gk337.api.request.component.RequestExtendComponent;
import com.gk337.api.request.entity.*;
import com.gk337.api.request.service.RequestExtendService;
import com.gk337.api.request.service.RequestService;
import com.gk337.api.verify.entity.UserMatchingResponse;
import com.gk337.api.verify.service.VerifyService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.exception.JGWCommunicationException;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.BeanUtilsExt;
import com.gk337.common.util.CollectionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * 
 *
 * @author 
 * @since  
 * @see
 *  <pre>
 *      수정이력
 *          xxxxx
 * </pre>
 */
@CrossOrigin("*")
@Slf4j
@RestController
@RequestMapping("/api/extend/request")
@Api(tags= {"[거래관리] - 요청서 확장타입 API"}, protocols="http", produces="application/json", consumes="application/json")
public class RequestExtendRestController {
	
	@Autowired
	private RequestComponent requestComponent;

	@Autowired
	private RequestExtendComponent requestExtendComponent;

	@Autowired
	private RequestService requestService;

	@Autowired
	private RequestExtendService requestExtendService;


	@Autowired
	private NotificationService notificationService;

	@Autowired
	private VerifyService verifyService;

	/**
	 * 요청서 등록 API
	 * @param request
	 * @return
	 * @throws InterruptedException 
	 */
	@ApiOperation(value = "[판매/구매요청서] 등록 API", notes = "판매/구매 요청서 등록 API", response = RequestExtendResponse.class)
	@RequestMapping(method = RequestMethod.POST, path="/", produces = "application/json")
	public ResponseEntity<RequestExtendResponse> insertRequest(@RequestBody @Valid RequestExtendRequest request) throws JGWCommunicationException, InterruptedException {

		RequestExtendResponse response = requestExtendComponent.insertRequest(request);

			// Push 메세지 처리 - 요청서에 해당되는 딜러의 모든 기기에 전송
			FmcMessageRequest message = new FmcMessageRequest();
			message.setName("중고왕의 알림 메세지");
			message.setNotificationTitle(String.format("[지마이다스 판매 요청서 알림]", response.getReqTypeName()));
			message.setNotificationBody(String.format("%s님이 %s에\n대한 %s요청을 하였습니다.",
					response.getMemName(), response.getProdName(), response.getReqTypeName()));
			message.setAppData("{\"url\":\"request_recieve_item_g\", \"param\":{\"reqSeq\":"+response.getSeq()+"}}");

			// 대상 사용자 목록
			List<UserMatchingResponse> list = verifyService.selectVerifyUserListForGmidas(response.getSeq(), request.getReqType());
			// 본인 제외
			List<UserMatchingResponse> filterResult = list.stream() .filter(a -> !a.getMemSeq().equals(request.getMemSeq())) .collect(Collectors.toList());
			// 대상 기기목록으로 변환
			List<DeviceInfoSetting> recipientList = CollectionUtils.convert(filterResult, DeviceInfoSetting::new);

			try {
				CompletableFuture<String> pushNotification = notificationService.sendBulk(recipientList, message);
				CompletableFuture.allOf(pushNotification).join();

				try {
					String firebaseResponse = pushNotification.get();
					System.out.println("firebaseResponse : " + firebaseResponse.toString());
				} catch (InterruptedException e) {
					log.debug("got interrupted!");
					e.printStackTrace();
					throw new JGWCommunicationException("네트워크 오류");
				} catch (ExecutionException e) {
					log.debug("execution error!");
					e.printStackTrace();
					throw new JGWCommunicationException(" 오류");
				}
			} catch (UnsupportedEncodingException e) {
				log.debug("encoding error!");
				e.printStackTrace();
				throw new JGWCommunicationException("인코딩 오류");
			}

		return BaseResponse.ok(response);
	
	}

	/**
	 /* 요청서 상세조회 API
	 * @param reqSeq
	 * @return
	 */
	@ApiOperation(value = "[판매/구매요청서] 상세조회 API", notes = "판매/구매 요청서 상세조회 API", response = RequestExtendResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/{reqSeq}", produces = "application/json")
	public ResponseEntity<RequestExtendResponse> selectRequestInfo(
			@ApiParam(value="요청서시퀀스", required=true, defaultValue = "6") @PathVariable(value = "reqSeq", required = true) Integer reqSeq) throws JGWServiceException {
		return BaseResponse.ok(requestExtendComponent.selectRequestInfo(reqSeq));
	}

	/**
	 * 판매/구매 요청서 문의하기 등록 API
	 * @param request
	 * @return
	 * @throws InterruptedException
	 */
	@ApiOperation(value = "[요청서] 판매/구매요청서 답변내용 등록 API", notes = "판매/구매 요청서 답변하기", response = ResponseExtendRequest.class)
	@RequestMapping(method = RequestMethod.POST, path="/response", produces = "application/json")
	public ResponseEntity<ResponseExtendResponse> insertResponse(@RequestBody @Valid ResponseExtendRequest request) throws JGWCommunicationException, InterruptedException {

		ResponseExtendResponse response = requestExtendComponent.insertResponse(request);

		// TODO : Async 처리, 다국어 처리, Bulk 처리?
		// Push 메세지 처리 - 답변을 수신하는 대상에게 전송
		FmcMessageRequest fmcMessageRequest = new FmcMessageRequest();
		fmcMessageRequest.setName("중고왕의 알림 메세지");
		fmcMessageRequest.setNotificationTitle("[요청서 답변수신]");
		fmcMessageRequest.setNotificationBody(String.format("%s님이 %s에\n대한 답변을 보냈습니다.", response.getMemName(), response.getProdName()));
		fmcMessageRequest.setAppData("{\"url\":\"request_send_response_item_g\", \"param\":{ \"reqSeq\": "+response.getReqSeq()+", \"resSeq\": "+response.getResSeq()+"}}");
		try {
			CompletableFuture<String> pushNotification = notificationService.sendSync(response.getMemSeq(), fmcMessageRequest);
			CompletableFuture.allOf(pushNotification).join();

			try {
				String firebaseResponse = pushNotification.get();
				System.out.println("firebaseResponse : " + firebaseResponse.toString());
			} catch (InterruptedException e) {
				log.debug("got interrupted!");
				e.printStackTrace();
				throw new JGWCommunicationException("네트워크 오류");
			} catch (ExecutionException e) {
				log.debug("execution error!");
				e.printStackTrace();
				throw new JGWCommunicationException(" 오류");
			}
		} catch (UnsupportedEncodingException | JGWCommunicationException e) {
			log.debug("encoding error!");
			e.printStackTrace();
			throw new JGWCommunicationException("인코딩 오류");
		}
		return BaseResponse.ok(response);
	}

	/**
	 * 문의하기 상세내용 조회 API
	 * @param resSeq
	 * @param reqSeq
	 * @return
	 */
	@ApiOperation(value = "[보낸요청서] 판매/구매요청서 답변내용 상세보기 API", notes = "판매/구매요청서 답변내용 상세보기 API", response = ResponseExtendResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/response/{resSeq}", produces = "application/json")
	public ResponseEntity<ResponseExtendResponse> selectResponseInfo(
			@ApiParam(value="응답(문의)요청서 시퀀스", required=true, defaultValue = "2") @PathVariable(value = "resSeq", required = true) Integer resSeq,
			@ApiParam(value="판매/구매 요청서 시퀀스", required=true, defaultValue = "2") @RequestParam(value = "reqSeq", required = true) Integer reqSeq)
	 		throws JGWServiceException
	{

		ResponseExtendResponse response = requestExtendComponent.selectResponseInfo(resSeq, reqSeq);
		return BaseResponse.ok(response);
	}

		/**
	 * 문의하기 목록 조회[페이징처리] API
	 * @param pageNo
	 * @param pageSize
	 * @param reqSeq
	 * @return
	 */
	@ApiOperation(value = "[보낸요청서] 판매/구매요청서 한건에 대한 답변리스트 조회 페이징처리 API", notes = "판매/구매요청서 한건에 대한 답변 목록조회 페이징처리", response = ResponseExtendResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/response/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<ResponseExtendResponse>> selectRequestListPaging(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) Integer pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) Integer pageSize,
			@ApiParam(value="판매/구매 요청서 시퀀스", required=true, defaultValue = "1") @RequestParam(value = "reqSeq", required = true) Integer reqSeq) {

	    PageInfo<ResponseExtendResponse> pageInfo = requestExtendService.selectResponseListPaging(pageNo, pageSize, reqSeq);

		return BaseResponse.ok(pageInfo);
	}

    /**
     * [요청서답변 상태관리] 거래상태 업데이트 API
     * @param resSeq
     * @param resStatus
     * @return
     */
	@ApiOperation(value = "[요청서답변 상태관리] 답변상태 업데이트 API", notes = "[요청서답변 상태관리] 상태 업데이트 API", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.PUT, path="/response/status/{resSeq}", produces = "application/json")
	public ResponseEntity<ErrorVo> updateRequestStatus(
	        @ApiParam(value="요청서답변 시퀀스", required=true, example="1") @PathVariable(value = "resSeq", required = true) Integer resSeq,
            @ApiParam(value="요청서 시퀀스", required=false, defaultValue = "1") @RequestParam(value = "reqSeq", required = false) Integer reqSeq,
            @ApiParam(value="답변 상태 ( I : 진행중 S: 사용자선택 C: 취소)", required=true, defaultValue = "I") @RequestParam(value = "resStatus", required = true) String resStatus
            ) throws JGWCommunicationException {

		ErrorVo response = new ErrorVo();
		Map<String, Object> resultMap = requestExtendComponent.updateResStatusAndPoint(resSeq, reqSeq, resStatus);

		// Push 메세지 처리 - 답변한 대상에게 선택 메세지 전송
		FmcMessageRequest fmcMessageRequest = new FmcMessageRequest();
		fmcMessageRequest.setName("중고왕의 알림 메세지");
		fmcMessageRequest.setNotificationTitle("[판매 확정 알림]");
		fmcMessageRequest.setNotificationBody(String.format("지마이다스 %s님이 %s에 대해서\n 판매 확정을 하였습니다.", resultMap.get("req_mem_name"), resultMap.get("prod_name")));
		fmcMessageRequest.setAppData("{\"url\":\"request_send_response_item_g\", \"param\":{ \"reqSeq\": "+ reqSeq +", \"resSeq\": "+ resSeq +"}}");
		try {
			CompletableFuture<String> pushNotification = notificationService.sendSync((Integer) resultMap.get("res_mem_seq"), fmcMessageRequest);
			CompletableFuture.allOf(pushNotification).join();

			try {
				String firebaseResponse = pushNotification.get();
				System.out.println("firebaseResponse : " + firebaseResponse.toString());
			} catch (InterruptedException e) {
				log.debug("got interrupted!");
				e.printStackTrace();
				throw new JGWCommunicationException("네트워크 오류");
			} catch (ExecutionException e) {
				log.debug("execution error!");
				e.printStackTrace();
				throw new JGWCommunicationException(" 오류");
			}
		} catch (UnsupportedEncodingException | JGWCommunicationException e) {
			log.debug("encoding error!");
			e.printStackTrace();
			throw new JGWCommunicationException("인코딩 오류");
		}

		return BaseResponse.ok(response);
	}

}
