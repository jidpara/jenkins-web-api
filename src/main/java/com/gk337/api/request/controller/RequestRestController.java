package com.gk337.api.request.controller;

import com.github.pagehelper.PageInfo;
import com.gk337.api.base.entity.DeviceInfoEntity;
import com.gk337.api.base.entity.DeviceInfoSetting;
import com.gk337.api.base.entity.FmcMessageRequest;
import com.gk337.api.base.service.NotificationService;
import com.gk337.api.request.component.RequestComponent;
import com.gk337.api.request.entity.*;
import com.gk337.api.request.service.RequestService;
import com.gk337.api.verify.entity.UserMatchingResponse;
import com.gk337.api.verify.service.VerifyService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWCommunicationException;
import com.gk337.common.util.CollectionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * 
 *
 * @author 
 * @since  
 * @see
 *  <pre>
 *      수정이력
 *          xxxxx
 * </pre>
 */
@CrossOrigin("*")
@Slf4j
@RestController
@RequestMapping("/api/request")
@Api(tags= {"[거래관리] - 요청서 API"}, protocols="http", produces="application/json", consumes="application/json")
public class RequestRestController {
	
	@Autowired
	private RequestComponent requestComponent;

	@Autowired
	private RequestService requestService;

	@Autowired
	private VerifyService verifyService;

	@Autowired
	private NotificationService notificationService;
	
	/**
	 * 요청서 등록 API
	 * @param request
	 * @return
	 * @throws InterruptedException 
	 */
	@ApiOperation(value = "[판매/구매요청서] 등록 API", notes = "판매/구매 요청서 등록 API")
	@RequestMapping(method = RequestMethod.POST, path="/", produces = "application/json")
	public ResponseEntity<RequestResponse> insertRequest(@RequestBody @Valid RequestRequest request) throws JGWCommunicationException, InterruptedException {

		RequestResponse response = requestComponent.insertRequest(request);

		// 카테고리 정상일 경우만 처리
		if(!"N".equals(request.getValidCateYn())) {
			// Push 메세지 처리 - 요청서에 해당되는 딜러의 모든 기기에 전송
			FmcMessageRequest message = new FmcMessageRequest();
			message.setName("중고왕의 알림 메세지");
			message.setNotificationTitle(String.format("[%s요청서 알림]", response.getReqTypeName()));
			message.setNotificationBody(String.format("%s님이 %s에\n대한 %s요청을 하였습니다.",
					response.getMemName(), response.getProdName(), response.getReqTypeName()));
			message.setAppData("{\"url\":\"request_recieve_item\", \"param\":{\"reqSeq\":"+response.getSeq()+"}}");

			// 대상 사용자 목록(테이블 수정 jw_request_product -> jw_product)
			List<UserMatchingResponse> list = verifyService.selectVerifyUserList(response.getSeq(), request.getTradeType());
			// 본인 제외
			List<UserMatchingResponse> filterResult = list.stream() .filter(a -> !a.getMemSeq().equals(request.getMemSeq())) .collect(Collectors.toList());
			// 대상 기기목록으로 변환
			List<DeviceInfoSetting> recipientList = CollectionUtils.convert(filterResult, DeviceInfoSetting::new);

			try {
				CompletableFuture<String> pushNotification = notificationService.sendBulk(recipientList, message);
				CompletableFuture.allOf(pushNotification).join();

				try {
					String firebaseResponse = pushNotification.get();
					System.out.println("firebaseResponse : " + firebaseResponse.toString());
				} catch (InterruptedException e) {
					log.debug("got interrupted!");
					e.printStackTrace();
					throw new JGWCommunicationException("네트워크 오류");
				} catch (ExecutionException e) {
					log.debug("execution error!");
					e.printStackTrace();
					throw new JGWCommunicationException(" 오류");
				}
			} catch (UnsupportedEncodingException e) {
				log.debug("encoding error!");
				e.printStackTrace();
				throw new JGWCommunicationException("인코딩 오류");
			}
		}

		return BaseResponse.ok(response);
	
	}

	/**
	 * 요청서 등록 API
	 * @param request - RequestResendRequest
	 * @return ResponseEntity
	 * @throws JGWCommunicationException
	 */
	@ApiOperation(value = "[판매/구매요청서] 재전송 API", notes = "판매/구매 요청서 등록 API")
	@RequestMapping(method = RequestMethod.POST, path="/resend", produces = "application/json")
	public ResponseEntity<RequestResendResponse> resendRequest(@RequestBody @Valid RequestResendRequest request) throws JGWCommunicationException {

	    RequestResendResponse response = requestComponent.resendRequest(request);
		return BaseResponse.ok(response);
	}

	/**
	 * 요청서 등록 API
	 * @param memSeq
	 * @return
	 * @throws InterruptedException
	 */
	@ApiOperation(value = "[판매/구매요청서] 딜러긴급요청 등록 여부 확인 API", notes = "딜러긴급요청 등록 여부 확인 API")
	@RequestMapping(method = RequestMethod.GET, path="/my/{memSeq}", produces = "application/json")
	public ResponseEntity<MyRequestResponse> checkMyRequest(
			@ApiParam(value="회원시퀀스", required=true) @PathVariable(value ="memSeq", required = true) Integer memSeq) {

		return BaseResponse.ok(requestService.selectMyRequestInfo(memSeq));

	}

	/**
	 * 요청서 삭제 API
	 * @param reqSeq
	 * @return
	 */
	@ApiOperation(value = "[판매/구매요청서] 삭제 API", notes = "판매/구매 요청서 삭제 API", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.DELETE, path="/{reqSeq}", produces = "application/json")
	public ResponseEntity<ErrorVo> deleteRequest(
			@ApiParam(value="요청서시퀀스", required=true, defaultValue = "1") @PathVariable(value ="reqSeq", required = true) int reqSeq) {
		return BaseResponse.ok(requestComponent.deleteRequest(reqSeq));
	}
	
	 /**
	 /* 요청서 상세조회 API
	 * @param reqSeq
	 * @return
	 */
	@ApiOperation(value = "[판매/구매요청서] 상세조회 API", notes = "판매/구매 요청서 상세조회 API", response = RequestResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/{reqSeq}", produces = "application/json")
	public ResponseEntity<RequestResponse> selectRequestInfo(
			@ApiParam(value="요청서시퀀스", required=true, defaultValue = "6") @PathVariable(value = "reqSeq", required = true) int reqSeq) {
		return BaseResponse.ok(requestComponent.selectRequestInfo(reqSeq));
	}
	
	/**
	 * 요청서 목록조회[페이징없음] API
	 * @param reqType
	 * @return
	 */
	// 추후 필요하면 추가함.
//	@ApiOperation(value = "[판매/구매요청서] 목록조회_페이징없음 API", notes = "[판매/구매요청서] 목록조회_페이징없음 [판매:S/구매:B]", response = RequestResponse.class)
//	@RequestMapping(method = RequestMethod.GET, path="/", produces = "application/json")
//	public ResponseEntity<List<RequestResponse>> selectRequestList(
//			@ApiParam(value="회원시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq,
//			@ApiParam(value="요청서타입 [판매:S/구매:B/전체:A]", required=true, defaultValue = "B") @RequestParam(value = "reqType", required = true) String reqType) {
//		return BaseResponse.ok(requestService.selectRequestList(memSeq, reqType));
//	}
	
	/**
	 * 요청서 목록조회[페이징처리] API
	 * @param pageNo
	 * @param pageSize
	 * @param reqType
	 * @return
	 */
	@ApiOperation(value = "[판매/구매요청서] 목록조회_페이징처리 API", notes = "[판매/구매요청서] 목록조회_페이징처리 [판매:S  구매:B]", response = RequestResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<RequestResponse>> selectRequestListPaging(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="회원시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq,
			@ApiParam(value="요청서타입 [판매:S/구매:B/전체:A]", required=true, defaultValue = "B") @RequestParam(value = "reqType", required = true) String reqType) {
		return BaseResponse.ok(requestService.selectRequestListPaging(pageNo, pageSize, memSeq, reqType));
	}
	
	
	/**
	 * 판매/구매 요청서 문의하기 등록 API
	 * @param request
	 * @return
	 * @throws InterruptedException 
	 */
	@ApiOperation(value = "[보낸요청서] 판매/구매요청서 문의내용 등록 API", notes = "판매/구매 요청서 문의하기", response = ResponseResponse.class)
	@RequestMapping(method = RequestMethod.POST, path="/response", produces = "application/json")
	public ResponseEntity<ResponseResponse> insertResponse(@RequestBody @Valid ResponseRequest request) throws JGWCommunicationException, InterruptedException {
		ResponseResponse response = requestComponent.insertResponse(request);

		// TODO : Async 처리, 다국어 처리, Bulk 처리?
		// Push 메세지 처리 - 요청서에 해당되는 딜러의 모든 기기에 전송
		FmcMessageRequest fmcMessageRequest = new FmcMessageRequest();
		fmcMessageRequest.setName("중고왕의 알림 메세지");
		fmcMessageRequest.setNotificationTitle("[요청서 답변수신]");
		fmcMessageRequest.setNotificationBody(String.format("%s님이 %s에\n대한 답변을 보냈습니다.", response.getMemName(), response.getProdName()));
		fmcMessageRequest.setAppData("{\"url\":\"request_send_response_item\", \"param\":{ \"reqSeq\": "+response.getReqSeq()+", \"resSeq\": "+response.getResSeq()+"}}");
		try {
			CompletableFuture<String> pushNotification = notificationService.sendSync(response.getMemSeq(), fmcMessageRequest);
			CompletableFuture.allOf(pushNotification).join();

			try {
				String firebaseResponse = pushNotification.get();
				System.out.println("firebaseResponse : " + firebaseResponse.toString());
			} catch (InterruptedException e) {
				log.debug("got interrupted!");
				e.printStackTrace();
				throw new JGWCommunicationException("네트워크 오류");
			} catch (ExecutionException e) {
				log.debug("execution error!");
				e.printStackTrace();
				throw new JGWCommunicationException(" 오류");
			}
		} catch (UnsupportedEncodingException | JGWCommunicationException e) {
			log.debug("encoding error!");
			e.printStackTrace();
			throw new JGWCommunicationException("인코딩 오류");
		}
		return BaseResponse.ok(response);
	}
	
	/**
	 * 판매/구매 요청서 문의하기 삭제 API
	 * @param resSeq
	 * @param reqSeq
	 * @return
	 */
	@ApiOperation(value = "[보낸요청서] 판매/구매요청서 문의내용 삭제 API", notes = "판매/구매 요청서 문의하기 삭제 API", response = ResponseResponse.class)
	@RequestMapping(method = RequestMethod.DELETE, path="/response/{resSeq}", produces = "application/json")
	public ResponseEntity<ErrorVo> deleteRequest(
			@ApiParam(value="문의 시퀀스(resSeq)", required=true, defaultValue = "90") @PathVariable(value ="resSeq", required = true) int resSeq,
			@ApiParam(value="판매/구매 시퀀스(reqSeq)", required=true, defaultValue = "141") @RequestParam(value ="reqSeq", required = true) int reqSeq) {
		return BaseResponse.ok(requestComponent.deleteResponse(resSeq, reqSeq));
	}
	
	/**
	 * 문의하기 상세내용 조회 API
	 * @param resSeq
	 * @param reqSeq
	 * @return
	 */
	@ApiOperation(value = "[보낸요청서] 판매/구매요청서 문의내용 상세보기 API", notes = "판매/구매요청서 문의하기 상세보기 API", response = ResponseResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/response/{resSeq}", produces = "application/json")
	public ResponseEntity<ResponseResponse> selectResponseInfo(
			@ApiParam(value="응답(문의)요청서 시퀀스", required=true, defaultValue = "2") @PathVariable(value = "resSeq", required = true) int resSeq,
			@ApiParam(value="판매/구매 요청서 시퀀스", required=true, defaultValue = "2") @RequestParam(value = "reqSeq", required = true) int reqSeq) {
		return BaseResponse.ok(requestService.selectResponseInfo(resSeq, reqSeq));
	}
	
	/**
	 * 문의하기 목록 조회[페이징없음] API
	 * @param memSeq
	 * @return
	 */
// 나중에 필요하면 추가함.	
//	@ApiOperation(value = "[보낸요청서] 판매/구매요청서건에 대한 문의리스트 조회_페이징없음 API", notes = "판매/구매요청서 한건에 대한 목록 조회_페이징없음", response = ResponseResponse.class)
//	@RequestMapping(method = RequestMethod.GET, path="/response", produces = "application/json")
//	public ResponseEntity<List<ResponseResponse>> selectResponseList(
//			@ApiParam(value="판매/구매 요청서 시퀀스", required=true, defaultValue = "1") @RequestParam(value = "reqSeq", required = true) int reqSeq) {
//		return BaseResponse.ok(requestService.selectResponseList(reqSeq));
//	}
	
	/**
	 * 문의하기 목록 조회[페이징처리] API
	 * @param pageNo
	 * @param pageSize
	 * @param reqSeq
	 * @return
	 */
	@ApiOperation(value = "[보낸요청서] 판매/구매요청서 한건에 대한 답변리스트 조회 페이징처리 API", notes = "판매/구매요청서 한건에 대한 답변 목록조회 페이징처리", response = ResponseResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/response/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<ResponseResponse>> selectRequestListPaging(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="판매/구매 요청서 시퀀스", required=true, defaultValue = "1") @RequestParam(value = "reqSeq", required = true) int reqSeq) {
		return BaseResponse.ok(requestService.selectResponseListPaging(pageNo, pageSize, reqSeq));
	}
	
	/**
	 * 받은요청서 리스트 [페이징]
	 * @param pageNo
	 * @param pageSize
	 * @param memSeq
	 * @param reqType
	 * @return
	 */
	@ApiOperation(value = "[받은요청서] 목록조회_페이징처리 API", notes = "[받은요청서] 회원구분에 따라 딜러 또는 전체로 조회 가능 목록조회_페이징처리", response = RequestSendRecvResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/receive/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<RequestSendRecvResponse>> selectReceiveListPaging(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="회원시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq,
			@ApiParam(value="요청서구분[판매:S / 구매:B / 전체:A]", required=true, defaultValue = "A") @RequestParam(value = "reqType", required = true) String reqType,
			@ApiParam(value="회원구분[유저:G / 딜러:P / 전체:A]", required=true, defaultValue = "A") @RequestParam(value = "memKind", required = true) String memKind,
			@ApiParam(value="딜러긴급요청여부[Y:딜러긴급요청 / N: 일반]", required=false, defaultValue = "N") @RequestParam(value = "dealerYn", required = false) String dealerYn) {
		return BaseResponse.ok(requestService.selectReceiveListPaging(pageNo, pageSize, memSeq, reqType, memKind, dealerYn));
	}
	
	/**
	 * 보낸요청서 리스트 [페이징]
	 * @param pageNo
	 * @param pageSize
	 * @param memSeq
	 * @return
	 */
	@ApiOperation(value = "[보낸요청서] 목록조회_페이징처리 API", notes = "[보낸요청서] 목록조회_페이징처리", response = RequestSendRecvResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/send/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<RequestSendRecvResponse>> selectSendListPaging(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="요청타입(A:전체, S:판매, B:구매)", required=false, defaultValue="A") @RequestParam(value = "reqType", required = false) String reqType,
			@ApiParam(value="회원시퀀스", required=true, defaultValue = "1") @RequestParam(value = "memSeq", required = true) int memSeq) {

		return BaseResponse.ok(requestService.selectSendListPaging(pageNo, pageSize, memSeq, reqType));
	}
	
	/**
	 * [요청서 상태관리] 등록 API
	 * 바로 호출은 안 할것 같아 임시주석처리.
	 * @param request
	 * @return
	 */
//	@ApiOperation(value = "[요청서 상태관리] 등록 API", notes = "[요청서 상태관리] 등록 API")
//	@RequestMapping(method = RequestMethod.POST, path="/status", produces = "application/json")
//	public ResponseEntity<RequestStatusResponse> insertRequestStatus(@RequestBody @Valid RequestStatusRequest request) {
//		return BaseResponse.ok(requestService.insertRequestStatus(request));
//	}
	
	/**
	 * [요청서 상태관리] 거래상태 업데이트 API
	 * @param request
	 * @return
	 */
	
	@ApiOperation(value = "[요청서 상태관리] 거래상태 업데이트 API", notes = "[요청서 상태관리] 거래완료 업데이트 API", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.PUT, path="/status", produces = "application/json")
	public ResponseEntity<ErrorVo> updateRequestStatus(@RequestBody @Valid RequestStatusRequest request) {
		return BaseResponse.ok(requestService.updateRequestStatus(request.getReqSeq(), request.getReqStatus()));
	}
	
	@ApiOperation(value = "[요청서 상태관리] 거래상태 New 업데이트 API", notes = "[요청서 상태관리] 거래완료 업데이트 API", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.PUT, path="/complete", produces = "application/json")
	public ResponseEntity<ErrorVo> updateCompleteStatus(@RequestBody @Valid RequestStatusRequest request) {
		return BaseResponse.ok(requestService.updateCompleteStatus(request.getReqSeq(), request.getReqStatus()));
	}
	
	/**
	 * [요청서 상태관리] 거래상태 조회 API
	 * @param reqSeq
	 * @return
	 */
	@ApiOperation(value = "[요청서 상태관리] 거래상태 조회 API", notes = "[요청서 상태관리] 거래상태 조회 API", response = RequestStatusResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/status", produces = "application/json")
	public ResponseEntity<List<RequestStatusResponse>> selectRequestStatus(
			@ApiParam(value="요청서시퀀스", required=true, defaultValue = "1") @RequestParam(value = "reqSeq", required = true) Integer reqSeq,
			@ApiParam(value="요청서 답변자 시퀀스", required=false, defaultValue = "1") @RequestParam(value = "sendMemSeq", required = false) Integer sendMemSeq) {
			Map<String, Integer> paramMap = new HashMap<>();
			paramMap.put("reqSeq", reqSeq);
			paramMap.put("sendMemSeq", sendMemSeq);
		return BaseResponse.ok(requestService.selectRequestStatus(paramMap));
	}
	
	/**
	 * 첨부파일(단건 이미지) 삭제.
	 * @param fileSeq
	 * @return
	 */
	@ApiOperation(value = "[요청하기] 첨부파일(단건 이미지) 삭제 API", 
			notes = "첨부파일 이미지 단건으로 삭제 API, 버킷에 올라간 물리적인 파일을 삭제 여부는 확인 필요.", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.DELETE, path="/atfil/{fileSeq}", produces = "application/json")
	public ResponseEntity<ErrorVo> deleteByFileSeq(
			@ApiParam(value="이미지파일 시퀀스(S3 파일업로드 후 리턴 받은 시퀀스)", required=true, defaultValue = "21") @PathVariable(value ="fileSeq", required = true) int fileSeq) {
		return BaseResponse.ok(requestService.deleteByFileSeq(fileSeq));
	}
	
	/**
	 * 첨부파일 대표 이미지 설정.
	 * @param fileSeq
	 * @return
	 */
	@ApiOperation(value = "[요청하기] 첨부파일 이미지 대표이미지 설정 API",  
			notes = "첨부파일 이미지 대표이미지 설정", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.PUT, path="/atfil/{fileSeq}", produces = "application/json")
	public ResponseEntity<ErrorVo> updateMainImage(
			@ApiParam(value="이미지파일 시퀀스(S3 파일업로드 후 리턴 받은 시퀀스)", required=true, defaultValue = "29") @PathVariable(value ="fileSeq", required = true) int fileSeq) {
		return BaseResponse.ok(requestService.updateMainImage(fileSeq));
	}
	
	/**
	 * [요청상품 목록조회] API
	 * @param pageNo
	 * @param pageSize
	 * @param reqType
	 * @return
	 */
	@ApiOperation(value = "[전체상품 둘러보기] 판매/구매 상품 목록 조회 API", notes = "[전체상품 둘러보기] 판매/구매 상품 목록 조회 ", response = RequestProductResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/products/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<RequestProductResponse>> selectReqeustProductList(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="요청서구분[판매:S / 구매:B / 전체:A]", required=true, defaultValue = "S") @RequestParam(value = "reqType", required = true) String reqType) {
		return BaseResponse.ok(requestService.selectReqeustProductList(pageNo, pageSize, reqType));
	}
	
	/**
	 * 알림함 목록조회.
	 * @param pageNo
	 * @param pageSize
	 * @param memSeq
	 * @param memKind
	 * @return
	 */
	@ApiOperation(value = "[알림함] 알림함 목록조회 API", notes = "[알림함] 알림함 목록조회 API ", response = RequestAlarmResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/alarm/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<RequestAlarmResponse>> selectAlarmList(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="30") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="회원시퀀스", required=true, defaultValue = "8") @RequestParam(value = "memSeq", required = true) int memSeq,
			@ApiParam(value="회원구분[딜러:P / 유저:G]", required=true, defaultValue = "P") @RequestParam(value = "memKind", required = true) String memKind) {
		return BaseResponse.ok(requestService.selectAlarmList(pageNo, pageSize, memSeq, memKind));
	}
	
}
