package com.gk337.api.request.service;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gk337.api.base.entity.FmcMessageRequest;
import com.gk337.api.base.service.NotificationService;
import com.gk337.api.product.entity.ProductAttachFileEntity;
import com.gk337.api.product.entity.ProductEntity;
import com.gk337.api.product.repository.ProductAttachFileRepository;
import com.gk337.api.product.repository.ProductRepository;
import com.gk337.api.request.entity.MyRequestResponse;
import com.gk337.api.request.entity.RequestAlarmResponse;
import com.gk337.api.request.entity.RequestEntity;
import com.gk337.api.request.entity.RequestMatchMember;
import com.gk337.api.request.entity.RequestProductAtfilEntity;
import com.gk337.api.request.entity.RequestProductEntity;
import com.gk337.api.request.entity.RequestProductResponse;
import com.gk337.api.request.entity.RequestRequest;
import com.gk337.api.request.entity.RequestResponse;
import com.gk337.api.request.entity.RequestSendRecvResponse;
import com.gk337.api.request.entity.RequestStatusEntity;
import com.gk337.api.request.entity.RequestStatusRequest;
import com.gk337.api.request.entity.RequestStatusResponse;
import com.gk337.api.request.entity.ResponseEntity;
import com.gk337.api.request.entity.ResponseProductAtfilEntity;
import com.gk337.api.request.entity.ResponseProductEntity;
import com.gk337.api.request.entity.ResponseRequest;
import com.gk337.api.request.entity.ResponseResponse;
import com.gk337.api.request.mapper.RequestMapper;
import com.gk337.api.request.repository.RequestProductAtfilRepository;
import com.gk337.api.request.repository.RequestProductRepository;
import com.gk337.api.request.repository.RequestRepository;
import com.gk337.api.request.repository.RequestStatusRepository;
import com.gk337.api.request.repository.ResponseProductAtfilRepository;
import com.gk337.api.request.repository.ResponseProductRepository;
import com.gk337.api.request.repository.ResponseRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWCommunicationException;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.MessageUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RequestService {
	
	@Autowired
	private RequestRepository requestRepository;
	
	@Autowired
	private RequestProductRepository requestProductRepository;

	@Autowired
	private RequestProductAtfilRepository requestProductAtfilRepository;

	@Autowired
	private ResponseProductRepository responseProductRepository;

	@Autowired
	private ResponseProductAtfilRepository responseProductAtfilRepository;

	@Autowired
	private ResponseRepository responseRepository;
	
	@Autowired
	private RequestStatusRepository requestStatusRepository;
	
	@Autowired
	private RequestMapper requestMapper;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductAttachFileRepository productAttachFileRepository;
	
	/**
	 * 요청서 - 메인 등록
	 * @param request
	 * @return
	 */
	@Transactional
	public RequestEntity insertRequest(RequestRequest request) {
		RequestEntity entity = new RequestEntity();
		try {
			BeanUtils.copyProperties(request, entity);
			entity.setCateSeq(request.getCateSeq3dp());
			entity = requestRepository.save(entity);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return entity;
	}
	
	/**
	 * 요청서 - 상품 등록
	 * @param request
	 * @return
	 */
	@Transactional
	public RequestProductEntity insertRequestProduct(RequestProductEntity request) {
		RequestProductEntity entity = new RequestProductEntity();
		try {
			entity = requestProductRepository.save(request);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return entity;
	}

	/**
	 * 요청서 답변 - 상품 등록
	 * @param request
	 * @return
	 */
	@Transactional
	public ResponseProductEntity insertResponseProduct(ResponseProductEntity request) {
		ResponseProductEntity entity = new ResponseProductEntity();
		try {
			entity = responseProductRepository.save(request);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return entity;
	}

	/**
	 * 요청서 - 상품 이미지 등록
	 * @param request
	 * @return
	 */
	public RequestProductAtfilEntity insertRequestProductAtfil(RequestProductAtfilEntity request) {
		RequestProductAtfilEntity entity = new RequestProductAtfilEntity();
		try {
			entity = requestProductAtfilRepository.saveAndFlush(request);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return entity;
	}
	
	public ProductAttachFileEntity insertProductAttachFile(ProductAttachFileEntity request) {
		ProductAttachFileEntity entity = new ProductAttachFileEntity();
		try {
			entity = productAttachFileRepository.saveAndFlush(request);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return entity;
	}

	/**
	 * 요청서 답변 - 상품 이미지 등록
	 * @param request
	 * @return
	 */
	public ResponseProductAtfilEntity insertResponseProductAtfil(ResponseProductAtfilEntity request) {
		ResponseProductAtfilEntity entity = new ResponseProductAtfilEntity();
		try {
			entity = responseProductAtfilRepository.saveAndFlush(request);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return entity;
	}

	/**
	 * 해당 요청서번호로 요청서 삭제. : cstatus = 'D'
	 * @param reqSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteRequestBySeq(int reqSeq) {
		ErrorVo result = new ErrorVo();
		try {
			requestRepository.updateCstatus(reqSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "삭제");
		}
		return result;
	}
	
	/**
	 * 해당 요청서번호로 요청상품 삭제. : cstatus = 'D'
	 * @param prodSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteProductBySeq(int prodSeq) {
		ErrorVo result = new ErrorVo();
		try {
			requestProductRepository.updateCstatus(prodSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "삭제");
		}
		return result;
	}
	
	/**
	 * 해당 요청서번호의 요청상품 이미지 삭제. : cstatus = 'D'
	 * @param prodSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteProductAtfilBySeq(int prodSeq) {
		ErrorVo result = new ErrorVo();
		try {
			requestProductAtfilRepository.updateCstatus(prodSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "삭제");
		}
		return result;
	}
	
	/**
	 * 해당 요청건의 응답메시지 삭제. = cstatus:D
	 * @param resSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteResponseBySeq(int resSeq) {
		ErrorVo result = new ErrorVo();
		try {
			responseRepository.updateCstatus(resSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "삭제");
		}
		return result;
	}
	
	/**
	 * 해당 요청건의 요청상태 삭제. = cstatus:D
	 * @param reqSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteRequestStatusBySeq(int reqSeq) {
		ErrorVo result = new ErrorVo();
		try {
			requestStatusRepository.updateCstatus(reqSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "삭제");
		}
		return result;
	}
	
	/**
	 * 요청서 - 상세보기(단건)
	 */
	public RequestResponse selectRequestInfo(int reqSeq) {
		RequestResponse response = new RequestResponse();
		try {
			response = requestMapper.selectRequestInfo(reqSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "상세조회");
		}
		return response;
	}

	/**
	 * 요청서 - 상세보기(단건)
	 */
	public MyRequestResponse selectMyRequestInfo(Integer memSeq) {
		MyRequestResponse response = new MyRequestResponse();
		try {
			response = requestMapper.selectMyRequestInfo(memSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "상세조회");
		}
		return response;
	}
	
	/**
	 * 요청서 - 목록조회(페이징없음)
	 * @param memSeq
	 * @param reqType
	 * @return
	 */
	public ListVo<RequestResponse> selectRequestList(int memSeq, String reqType) {
		ListVo<RequestResponse> listVo = null;
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("memSeq", memSeq);
			hashMap.put("reqType", reqType);
			
			listVo = new ListVo<RequestResponse>(requestMapper.selectRequestList(hashMap));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "목록조회");
		}
		return listVo;
	}
	
	/**
	 * 요청서 - 목록조회(페이징처리)
	 * @param pageNo
	 * @param pageSize
	 * @param reqType
	 * @return
	 */
	public PageInfo<RequestResponse> selectRequestListPaging(int pageNo, int pageSize, int memSeq, String reqType) {
		PageInfo<RequestResponse> pageInfo = new PageInfo<RequestResponse>();
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("memSeq", memSeq);
			hashMap.put("reqType", reqType);
			
			int totalSize = requestMapper.selectRequestList(hashMap).size();
			PageHelper.startPage(pageNo, pageSize); 
			List<RequestResponse> list = requestMapper.selectRequestList(hashMap);
			pageInfo = new PageInfo<RequestResponse>(list);
			pageInfo.setTotal(totalSize);
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "목록조회");
		}
		return pageInfo;
	};
		
	
	/**
	 * 문의하기 저장
	 * @param request
	 * @return
	 */
	@Transactional
	public ResponseEntity insertResponse(ResponseRequest request) {
		ResponseEntity entity = new ResponseEntity();
		try {
			BeanUtils.copyProperties(request, entity);
			entity = responseRepository.saveAndFlush(entity);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0002", "저장 또는 수정");
		}
		return entity;
	}
	

	/**
	 * 시퀀스로 문의내용 삭제
	 * @param seq
	 * @return
	 * 
	 * 일단막았음.
	 * 
	 */
//	@Transactional
//	public ErrorVo deleteResponseBySeq(int resSeq) {
//		ErrorVo result = new ErrorVo();
//		try {
//			responseRepository.deleteBySeq(resSeq);
//		} catch (BizException be) {
//			throw new JGWServiceException(be.getMessage());
//		} catch(Exception e) {
//			e.printStackTrace();
//			throw new JGWServiceException("ERROR.REQUEST.0002", "삭제");
//		}
//		return result;
//	}
	
	/**
	 * 시퀀스로 문의내용 삭제시 상품이미지 삭제
	 * @param prodSeq
	 * @param memSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteByProdSeqAndMemSeq(int prodSeq, int memSeq) {
		ErrorVo result = new ErrorVo();
		try {
			requestProductAtfilRepository.deleteByProdSeqAndMemSeq(prodSeq, memSeq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0002", "삭제");
		}
		return result;
	}
	
	/**
	 * 문의하기 상세정보 조회.
	 * @param resSeq
	 * @param reqSeq
	 * @return
	 */
	
	@Transactional
	public ResponseResponse selectResponseInfo(int resSeq, int reqSeq) {
		ResponseResponse response = new ResponseResponse();
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("resSeq", resSeq);
			hashMap.put("reqSeq", reqSeq);
			
			response = requestMapper.selectResponseInfo(hashMap);
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0002", "상세조회");
		}
		return response;
	}
	
	/**
	 * 문의하기 목록 조회[페이징없음].
	 * @param reqSeq
	 * @return
	 */
	@Transactional
	public ListVo<ResponseResponse> selectResponseList(int reqSeq) {
		
		ListVo<ResponseResponse> listVo = null;
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("reqSeq", reqSeq);
			
			this.updateIsNew(reqSeq, "N");
			listVo = new ListVo<ResponseResponse>(requestMapper.selectResponseList(hashMap));
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0002", "목록조회");
		}
		return listVo;
	}
	
	/**
	 * 문의하기 목록 조회[페이징처리].
	 * @param pageNo
	 * @param pageSize
	 * @param reqSeq
	 * @return
	 */
	@Transactional
	public PageInfo<ResponseResponse> selectResponseListPaging(int pageNo, int pageSize, int reqSeq) {
		PageInfo<ResponseResponse> pageInfo = new PageInfo<ResponseResponse>();
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("reqSeq", reqSeq);
			
			PageHelper.startPage(pageNo, pageSize); 
			List<ResponseResponse> list = requestMapper.selectResponseList(hashMap);
			
			this.updateIsNew(reqSeq, "N");
			
			pageInfo = new PageInfo<ResponseResponse>(list);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0002", "목록조회");
		}
		return pageInfo;
	}
	
	//
	/**
	 * 요청서 문의하기 시 - isNew 업데이트
	 * @param seq
	 * @param isNew
	 * @return
	 */
	@Transactional
	public ErrorVo updateIsNew(int seq, String isNew) {
		try {
			requestRepository.updateIsNew(seq, isNew);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0002", "신규 문의등록");
		}
		return new ErrorVo();
	}
	
	/**
	 * 받은요청서 리스트 [페이징]
	 * @param pageNo
	 * @param pageSize
	 * @param memSeq
	 * @param reqType
	 * @param dealerYn
	 * @return
	 */
	@Transactional
	public PageInfo<RequestSendRecvResponse> selectReceiveListPaging(int pageNo, int pageSize, int memSeq, String reqType, String memKind, String dealerYn) {
		PageInfo<RequestSendRecvResponse> pageInfo = new PageInfo<RequestSendRecvResponse>();
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("memSeq"	, memSeq);
			hashMap.put("reqType"	, reqType);
			hashMap.put("memKind"	, memKind);

			// 딜러 요청서 조회 일 경우 dealerYn 값 Y 강제 설정 (프론트 2.7.0 미만에서의 호출 방어 코드)
			// TODO : 임시코드 삭제
			if("P".equals(memKind)){
				dealerYn = "Y";
			}
			hashMap.put("dealerYn"	, dealerYn);

			int totalSize = requestMapper.selectReceiveList(hashMap).size();
			PageHelper.startPage(pageNo, pageSize); 
			List<RequestSendRecvResponse> list = requestMapper.selectReceiveList(hashMap);
			pageInfo = new PageInfo<RequestSendRecvResponse>(list);
			pageInfo.setTotal(totalSize);
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "받은요청서 목록조회");
		}
		return pageInfo;
	};
	
	/**
	 * 보낸요청서 리스트 [페이징]
	 * @param pageNo
	 * @param pageSize
	 * @param memSeq
	 * @return
	 */
	@Transactional
	public PageInfo<RequestSendRecvResponse> selectSendListPaging(int pageNo, int pageSize, int memSeq, String reqType) {
		PageInfo<RequestSendRecvResponse> pageInfo = new PageInfo<RequestSendRecvResponse>();

        if(reqType == null || "".equals(reqType)) {
            reqType = "A";
        }

		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("memSeq"	, memSeq);
			hashMap.put("reqType"	, reqType);

			// TODO : 전체조회 무엇... 카운트 쿼리에서 가져오도록 변경...:(
			int totalSize = requestMapper.selectSendList(hashMap).size();
			PageHelper.startPage(pageNo, pageSize); 
			List<RequestSendRecvResponse> list = requestMapper.selectSendList(hashMap);
			pageInfo = new PageInfo<RequestSendRecvResponse>(list);
			pageInfo.setTotal(totalSize);
			
			pageInfo = new PageInfo<RequestSendRecvResponse>(list);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "보낸요청서 목록조회");
		}
		return pageInfo;
	};
	
	/**
	 * 요청서 상태관리 저장.
	 * @param request
	 * @return
	 */
	@Transactional
	public RequestStatusEntity insertRequestStatus(RequestStatusRequest request) {
		RequestStatusEntity entity = new RequestStatusEntity();
		try {
			BeanUtils.copyProperties(request, entity);
			entity = requestStatusRepository.saveAndFlush(entity);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "요청서 상태관리 저장");
		}
		return entity;
	}
	
	/**
	 * 요청서 상태관리 업데이트.
	 * @param reqSeq
	 * @param reqStatus
	 * @return
	 */
	@Transactional
	public ErrorVo updateRequestStatus(int reqSeq, String reqStatus) {
		ErrorVo result = new ErrorVo();
		try {
			requestStatusRepository.updateRequestStatus(reqSeq, reqStatus);
			result.setMessage(MessageUtil.get("MESSAGE.RESULT.0001"));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "요청서 상태관리 업데이트");
		}
		return result;
	}
	
	@Transactional
	public ErrorVo updateCompleteStatus(int reqSeq, String reqStatus) {
		ErrorVo result = new ErrorVo();
		try {						
			productRepository.updateCompleteStatus(reqSeq, reqStatus,LocalDateTime.now());
			result.setMessage(MessageUtil.get("MESSAGE.RESULT.0001"));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "요청서 상태관리 업데이트");
		}
		return result;
	}
	
	
	
	/**
	 * 요청서 상태관리 조회
	 * @param paramMap
	 * @return
	 */
	@Transactional
	public ListVo<RequestStatusResponse> selectRequestStatus(Map paramMap) {
		ListVo<RequestStatusResponse> listVo = null;
		try {
			listVo = new ListVo<RequestStatusResponse>(requestMapper.selectRequestStatus(paramMap));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0002", "요청서 상태관리 조회");
		}
		return listVo;
	}
	
	/**
	 * 첨부파일 단건 삭제.
	 * @param fileSeq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteByFileSeq(int fileSeq) {
		ErrorVo result = new ErrorVo();
		try {
			Long l = requestProductAtfilRepository.deleteByFileSeq(fileSeq);
			if(l.intValue()>0) {
				result.setMessage(MessageUtil.get("MESSAGE.RESULT.0001"));
			} else {
				throw new JGWServiceException("ERROR.COMM.0001");
			}
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "삭제");
		}
		return result;
	}
	
	@Transactional
	public ErrorVo updateMainImage(int fileSeq) {
		ErrorVo result = new ErrorVo();
		try {
			
			// 그룹조회.
			RequestProductAtfilEntity entity = requestProductAtfilRepository.findByFileSeq(fileSeq);
			log.debug(":::"+ entity.getProdSeq());
			
			// 이전 대표 이미지초기화.
			requestProductAtfilRepository.updateByProdSeq(entity.getProdSeq());
			
			// 대표이미지 설정.
			int i = requestProductAtfilRepository.updateByFileSeq(fileSeq);
			
			if(i > 0) {
				result.setMessage(MessageUtil.get("MESSAGE.RESULT.0001"));
			} else {
				throw new JGWServiceException("ERROR.COMM.0001");
			}
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "삭제");
		}
		return result;
	}
	
	/**
	 * 상품목록조회
	 * @param pageNo
	 * @param pageSize
	 * @param reqType
	 * @return
	 */
	public PageInfo<RequestProductResponse> selectReqeustProductList(int pageNo, int pageSize, String reqType) {
		PageInfo<RequestProductResponse> pageInfo = new PageInfo<RequestProductResponse>();
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("reqType", reqType);
			
			int totalSize = requestMapper.selectReqeustProductList(hashMap).size();
			PageHelper.startPage(pageNo, pageSize); 
			List<RequestProductResponse> list = requestMapper.selectReqeustProductList(hashMap);
			pageInfo = new PageInfo<RequestProductResponse>(list);
			pageInfo.setTotal(totalSize);
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "상품목록조회");
		}
		return pageInfo;
	};
	
	
	/**
	 * 알림함 목록조회.
	 * @param pageNo
	 * @param pageSize
	 * @param memSeq
	 * @param memKind
	 * @return
	 */
	public PageInfo<RequestAlarmResponse> selectAlarmList(int pageNo, int pageSize, int memSeq, String memKind) {
		PageInfo<RequestAlarmResponse> pageInfo = new PageInfo<RequestAlarmResponse>();
		try {
			
			int totalSize =  0;
			String memAuthYn = "";
			if("P".equals(memKind)) {
				memAuthYn = requestMapper.selectMemAuthYn(memSeq);
			}

			if("P".equals(memKind) && "Y".equals(memAuthYn)) {
				// TODO : 삭제 - 업데이트 안한 앱 사용자를 위한 pageSize 조정 임시처리
				if(pageSize > 50) {
					pageSize =50;
				}
				// 사업자승인 딜러 알람리스트.
				totalSize = requestMapper.selectAlarmListByDealer(memSeq).size();
				PageHelper.startPage(pageNo, pageSize);
				List<RequestAlarmResponse> list = requestMapper.selectAlarmListByDealer(memSeq);
				pageInfo = new PageInfo<RequestAlarmResponse>(list);
				pageInfo.setTotal(totalSize);
			} else {
				// TODO : 삭제 - 업데이트 안한 앱 사용자를 위한 pageSize 조정 임시처리
				if(pageSize > 50) {
					pageSize =50;
				}
				// 미승인사업자 또는 유저의 알림리스트.
				totalSize = requestMapper.selectAlarmListByUser(memSeq).size();
				PageHelper.startPage(pageNo, pageSize);
				List<RequestAlarmResponse> list = requestMapper.selectAlarmListByUser(memSeq);
				pageInfo = new PageInfo<RequestAlarmResponse>(list);
				pageInfo.setTotal(totalSize);
			}
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "알림함 목록조회");
		}
		return pageInfo;
	};

	/**
	 * 요청서 매칭 딜러사용자 조회.
	 * @param reqSeq
	 * @return
	 */
	public List<RequestMatchMember> selectRequestMatchMember(Map paramMap) {

		List<RequestMatchMember> result = null;
		try {
			result = requestMapper.selectRequestMatchMember(paramMap);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "알림함 목록조회");
		}

		return result;
	};
	
	/**
	 * 지역정보 Match 코드 조회
	 * @param addr1
	 * @param addr2
	 * @return
	 */
	public Integer selectAreaSeq(String addr1, String addr2) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("addr1", addr1);
		map.put("addr2", addr2);
		return requestMapper.selectAreaSeq(map);
	}
	
	/**
	 * Notification Async Send
	 */
	@Autowired
	public NotificationService notificationService;
	
	@Async
	public void sendNotification(Integer memSeq, FmcMessageRequest message) throws JGWCommunicationException, InterruptedException {
		
//		Thread.sleep(10000);
//		log.debug("========================= sendNotification =========================");

		try {
			CompletableFuture<String> pushNotification = notificationService.send(memSeq, message);
			CompletableFuture.allOf(pushNotification).join();

			try {
				String firebaseResponse = pushNotification.get();
				System.out.println("firebaseResponse : " + firebaseResponse.toString());
			} catch (InterruptedException e) {
				log.debug("got interrupted!");
				e.printStackTrace();
				throw new JGWCommunicationException("네트워크 오류");
			} catch (ExecutionException e) {
				log.debug("execution error!");
				e.printStackTrace();
				throw new JGWCommunicationException(" 오류");
			}
		} catch (UnsupportedEncodingException e) {
			log.debug("encoding error!");
			e.printStackTrace();
			throw new JGWCommunicationException("인코딩 오류");
		}

	}
	
	/**
	 * 요청서 - 상품 등록
	 * @param request
	 * @return
	 */
	@Transactional
	public ProductEntity insertProduct(ProductEntity request) {
		ProductEntity entity = new ProductEntity();
		try {
			entity = productRepository.save(request);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "저장 또는 수정");
		}
		return entity;
	}
	

	
}


