package com.gk337.api.request.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gk337.api.base.entity.FmcMessageRequest;
import com.gk337.api.base.service.NotificationService;
import com.gk337.api.request.entity.*;
import com.gk337.api.request.mapper.RequestExtendMapper;
import com.gk337.api.request.mapper.RequestMapper;
import com.gk337.api.request.repository.*;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWCommunicationException;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.MessageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Slf4j
@Service
public class RequestExtendService {

	@Autowired
	RequestService requestService;

	@Autowired
	RequestExtendMapper requestExtendMapper;
	@Autowired
	RequestGmidasEntityRepository requestGmidasEntityRepository;

	@Autowired
	ResponseGmidasEntityRepository responseGmidasEntityRepository;

	/**
	 * 요청서 - 메인 등록
	 * @param request
	 * @return
	 */
	@Transactional
	public RequestGmidasEntity insertRequestExtend(RequestExtendRequest request) {
		RequestGmidasEntity entity = new RequestGmidasEntity();
		try {
			BeanUtils.copyProperties(request, entity);
			if(entity.getPickupDate() == null) {
				entity.setPickupDate(LocalDate.parse(request.getPickupDateStr(), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
			}
			entity = requestGmidasEntityRepository.save(entity);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST_EXTEND.0001", "저장 또는 수정");
		}
		return entity;
	}

	/**
	 * 요청서 - 메인 등록
	 * @param request
	 * @return
	 */
	@Transactional
	public ResponseGmidasEntity insertResponseExtend(ResponseExtendRequest request) {
		ResponseGmidasEntity entity = new ResponseGmidasEntity();
		try {
			BeanUtils.copyProperties(request, entity);

			entity = responseGmidasEntityRepository.save(entity);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.RESPONSE_EXTEND.0001", "저장 또는 수정");
		}
		return entity;
	}

	public Optional<RequestGmidasEntity> getRequestGmidasEntity(Integer reqSeq) {
		Optional<RequestGmidasEntity> entity = null;
		try {
			entity = requestGmidasEntityRepository.findById(reqSeq);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST_EXTEND.0001", "조회");
		}
		// test
		return entity;
	}

	public Optional<ResponseGmidasEntity> getResponseGmidasEntity(Integer resSeq, Integer reqSeq) {
		Optional<ResponseGmidasEntity> entity = Optional.ofNullable(null);
		try {
			entity = responseGmidasEntityRepository.findByResSeqAndReqSeq(resSeq, reqSeq);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.RESPONSE_EXTEND.0001", "조회");
		}
		return entity;
	}

	/**
	 * 답변 목록 조회[페이징처리].
	 * @param pageNo
	 * @param pageSize
	 * @param reqSeq
	 * @return
	 */
	@Transactional
	public PageInfo<ResponseExtendResponse> selectResponseListPaging(Integer pageNo, Integer pageSize, Integer reqSeq) {
		PageInfo<ResponseExtendResponse> pageInfo = new PageInfo<ResponseExtendResponse>();
		try {
			HashMap<String, Object> hashMap = new HashMap<String, Object>();
			hashMap.put("reqSeq", reqSeq);

			PageHelper.startPage(pageNo, pageSize);
			List<ResponseExtendResponse> list = requestExtendMapper.selectResponseList(hashMap);

			requestService.updateIsNew(reqSeq, "N");

			pageInfo = new PageInfo<ResponseExtendResponse>(list);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0002", "목록조회");
		}
		return pageInfo;
	}


	/**
	 * 답변 상태 변경
	 * @param resSeq
	 * @param resStatus
	 * @return
	 */
	@Transactional
	public ErrorVo updateResStatus(Integer resSeq, String resStatus) {
		ErrorVo result = new ErrorVo();
		try {
			responseGmidasEntityRepository.updateResStatus(resSeq, resStatus, LocalDateTime.now());
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.REQUEST.0001", "삭제");
		}
		return result;
	}
}


