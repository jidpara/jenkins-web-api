package com.gk337.api.request.mapper;

import com.gk337.api.request.entity.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 
 * @author maroojjang
 *
 */
@Mapper
public interface RequestExtendMapper {
	/**
	 * 판매/구매요청서 해당 한건에 대한 응답목록 조회
	 * @param map
	 * @return List<ResponseExtendResponse>
	 */
	public List<ResponseExtendResponse> selectResponseList(HashMap map);

	/**
	 * 지마이다스 요청서 판매 확정을 위한 상품 정보 조회
	 * @param map Map<String, Object>
	 * @return Map<String, Object>
	 */
	public Map<String, Object> selectProductInfo(Map map);

}
