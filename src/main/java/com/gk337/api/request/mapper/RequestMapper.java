package com.gk337.api.request.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gk337.api.request.entity.*;
import org.apache.ibatis.annotations.Mapper;

/***
 * 
 * @author maroojjang
 *
 */
@Mapper
public interface RequestMapper {
	
	/**
	 * 판매/구매 요청서 상제정보 조회
	 * @param int reqSeq
	 * @return RequestResponse
	 */
	public RequestResponse selectRequestInfo(int reqSeq);

	/**
	 * 나의 요청서 정보
	 * @param Integer memSeq
	 * @return MyRequestResponse
	 */
	public MyRequestResponse selectMyRequestInfo(Integer memSeq);

	/**
	 * 전체/판매/구매 요청서목록 조회
	 * @param map
	 * @return List<RequestResponse>
	 */
	public List<RequestResponse> selectRequestList(HashMap map);
	
	/**
	 * 판매/구매요청서에 대한 응답의 상세정보 조회
	 * @param map
	 * @return ResponseResponse
	 */
	public ResponseResponse selectResponseInfo(HashMap map);
	
	/**
	 * 판매/구매요청서 해당 한건에 대한 응답목록 조회
	 * @param map
	 * @return List<ResponseResponse>
	 */
	public List<ResponseResponse> selectResponseList(HashMap map);
	
	/**
	 * 받은요청서 리스트 조회
	 * @param map
	 * @return List<RequestSendRecvResponse>
	 */
	public List<RequestSendRecvResponse> selectReceiveList(HashMap map);
	

	/**
	 * 보낸요청서 리스트 조회
	 * @param map
	 * @return List<RequestSendRecvResponse>
	 */
	public List<RequestSendRecvResponse> selectSendList(HashMap map);
	
	/**
	 * 요청서 진행상태 조회.
	 * @param map
	 * @return
	 */
	public List<RequestStatusResponse> selectRequestStatus(Map map);
	
	/**
	 * 판매상품조회.
	 * @param map
	 * @return
	 */
	public List<RequestProductResponse> selectReqeustProductList(HashMap map);
	
	/**
	 * 사업자승인여부 조회
	 * @param memSeq
	 * @return
	 */
	public String selectMemAuthYn(int memSeq);
	
	/**
	 * 알림리스트 - 사업자등록을 한 딜러의 알림리스트
	 * @param memSeq
	 * @return
	 */
	public List<RequestAlarmResponse> selectAlarmListByDealer(int memSeq);
	
	/**
	 * 미승인사업자 또는 유저의 알림리스트
	 * @param memSeq
	 * @return
	 */
	public List<RequestAlarmResponse> selectAlarmListByUser(int memSeq);

	/**
	 * 요청서에 매칭되는 딜러사용자 목록 조회
	 * @param reqSeq
	 * @return
	 */
	public List<RequestMatchMember> selectRequestMatchMember(Map paramMap);
	
	/**
	 * 지역정보 Match 코드 조회
	 * @param map
	 * @return
	 */
	public Integer selectAreaSeq(HashMap map);
}
