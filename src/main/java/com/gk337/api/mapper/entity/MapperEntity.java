package com.gk337.api.mapper.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@ApiModel(description = "Mapper VO")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MapperEntity {
	private int id;
	private String name;
	private String phoneNumber;
}