package com.gk337.api.mapper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk337.api.mapper.dao.MapperDao;
import com.gk337.api.mapper.entity.MapperEntity;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.mvc.model.ListVo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MapperService {
	
	@Autowired
	private MapperDao mapper;

	public ListVo<MapperEntity>findAll(){
		log.debug("findAll");
		ListVo<MapperEntity> listVo = new ListVo<MapperEntity>();
		try {
			listVo.setContent(mapper.findAll());
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch (Exception e) {
			throw new BizException("ERROR.SYS.9999"); 
		}
		return listVo;
	}
	
	
	public MapperEntity findOne(int id){
		log.debug("findOne");
		MapperEntity entity = new MapperEntity();
		try {
			entity = mapper.findOne(id);
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch (Exception e) {
			throw new BizException("ERROR.SYS.9999"); 
		}
		return entity;
	}
}
