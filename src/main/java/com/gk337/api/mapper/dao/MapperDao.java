package com.gk337.api.mapper.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.mapper.entity.MapperEntity;

@Mapper
public interface MapperDao {
	
	public List<MapperEntity> findAll();
	
	public MapperEntity findOne(int id); 
	
}
