package com.gk337.api.mapper.controller;

import com.gk337.api.mapper.service.MapperService;
import com.gk337.common.annotation.AccessLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.gk337.api.mapper.entity.MapperEntity;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.core.mvc.model.ListVo;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/mapper")
@Slf4j
@Api(tags= {"[샘플] 마이바티스 샘플 API"}, protocols="http", produces="application/json", consumes="application/json")
public class MapperRestController {
//	
//	@Autowired
//	MapperService mapperService;
//
//	@AccessLog(name="test")
//	@GetMapping("/")
//	@ApiOperation(value="마이바티스 매퍼 샘플1", response = MapperEntity.class)
//	public ResponseEntity<ListVo<MapperEntity>> findAll() {
//		log.debug("-------");
//		return BaseResponse.ok(mapperService.findAll());
//	}
//	
//	@ApiOperation(value="마이바티스 매퍼 샘플2", response = MapperEntity.class)
//	@GetMapping("/{id}")
//	public ResponseEntity<MapperEntity> findOne(
//			@ApiParam(value="아이디", required=true, example="1") @PathVariable("id") int id) {
//		return BaseResponse.ok(mapperService.findOne(id));
//	}
}