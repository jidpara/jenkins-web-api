package com.gk337.api.mypage.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 회원정보
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "jw_member")
@ApiModel(description = "회원관리 엔티티 ")
public class ProfileEntity extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, name = "seq")
    @ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

    @Column(name = "cstatus")
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제, L:탈퇴)", example="Y")
    private String cstatus = "Y";

    @Column(name = "join_kind")
    @ApiModelProperty(notes = "가입구분 (E:이메일, N:네이버, G:구글, F:페이스북, K:카카오)", example="E")
    private String joinKind;

    @Column(name = "mem_kind")
    @ApiModelProperty(notes = "회원구분 (G:일반회원, S:중고전문가)", example="1")
    private String memKind;

    @Column(name = "pro_request_yn")
    @ApiModelProperty(notes = "중고전문가신청여부", example="1")
    private String proRequestYn;

    @Column(name = "pro_request_date")
    @ApiModelProperty(notes = "중고전문가신청일자", example="")
    private LocalDateTime proRequestDate = null;

    @Column(name = "pro_ok_date")
    @ApiModelProperty(notes = "중고전문가승인일자", example="")
    private LocalDateTime proOkDate = null;

    @Column(name = "mem_code")
    @ApiModelProperty(notes = "회원코드", example="1")
    private String memCode  = null;

    @Column(name = "mem_email")
    @ApiModelProperty(notes = "회원이메일", example="1")
    private String memEmail;

    @Column(name = "mem_pwd")
    @ApiModelProperty(notes = "비밀번호 (6~16자리, 영문,숫자혼합)", example="1")
    private String memPwd;
    
    @Column(name = "file_seq")
    @ApiModelProperty(notes = "이미지파일 시퀀스", example="38")
    private Integer fileSeq = null;

    @Column(name = "mem_nickname")
    @ApiModelProperty(notes = "닉네임 (2~10자리)", example="1")
    private String memNickname = null;

    @Column(name = "mem_name")
    @ApiModelProperty(notes = "회원이름", example="1")
    private String memName = null;

    @Column(name = "mem_hp")
    @ApiModelProperty(notes = "휴대전화", example="1")
    private String memHp = null;

    @Column(name = "mem_hp_corp")
    @ApiModelProperty(notes = "통신사 (SKT, KT, LG)", example="1")
    private String memHpCorp = null;

    @Column(name = "mem_auth_yn")
    @ApiModelProperty(notes = "휴대폰 본인인증 여부", example="1")
    private String memAuthYn = null;

    @Column(name = "mem_auth_date")
    @ApiModelProperty(notes = "휴대폰 본인인증 일자", example="")
    private LocalDateTime memAuthDate = null;

    @Column(name = "mem_birth")
    @ApiModelProperty(notes = "생년월일", example="1")
    private String memBirth = null;

    @Column(name = "mem_gender")
    @ApiModelProperty(notes = "성별 (F:여성, M:남성)", example="1")
    private String memGender = null;

    @Column(name = "mem_zip")
    @ApiModelProperty(notes = "우편번호", example="1")
    private String memZip = null;

    @Column(name = "mem_addr")
    @ApiModelProperty(notes = "주소", example="1")
    private String memAddr = null;

    @Column(name = "mem_addr_detail")
    @ApiModelProperty(notes = "상세주소", example="1")
    private String memAddrDetail = null;

    @Column(name = "mem_desc")
    @ApiModelProperty(notes = "소개글", example="1")
    private String memDesc = null;

    @Column(name = "bank_seq")
    @ApiModelProperty(notes = "은행시퀀스", example="1")
    private Integer bankSeq = null;

    @Column(name = "bank_name")
    @ApiModelProperty(notes = "은행명", example="1")
    private String bankName = null;

    @Column(name = "account_num")
    @ApiModelProperty(notes = "계좌번호", example="1")
    private String accountNum = null;

    @Column(name = "sms_yn")
    @ApiModelProperty(notes = "SMS수신여부 (Y:수신, N:수신안함)", example="1")
    private String smsYn = null;

    @Column(name = "email_yn")
    @ApiModelProperty(notes = "이메일수신여부 (Y:수신, N:수신안함)", example="1")
    private String emailYn = null;

    @Column(name = "memo")
    @ApiModelProperty(notes = "메모", example="1")
    private String memo = null;

    @Column(name = "room_open_yn")
    @ApiModelProperty(notes = "내방공개여부", example="1")
    private String roomOpenYn = null;

    @Column(name = "chat_open_yn")
    @ApiModelProperty(notes = "채팅공여여부", example="1")
    private String chatOpenYn = null;

    @Column(name = "safe_num_open_yn")
    @ApiModelProperty(notes = "안심번호공개여부", example="1")
    private String safeNumOpenYn = null;

    @Column(name = "safe_time_fr")
    @ApiModelProperty(notes = "안심번호통화가능시간fr (미인증회원은 비노출)", example="1")
    private String safeTimeFr = null;

    @Column(name = "safe_time_to")
    @ApiModelProperty(notes = "안심번호통화가능시간to (미인증회원은 비노출)", example="1")
    private String safeTimeTo = null;

    @Column(name = "noti_buy_yn")
    @ApiModelProperty(notes = "알림설정_구매요청", example="1")
    private String notiBuyYn = null;

    @Column(name = "noti_sell_yn")
    @ApiModelProperty(notes = "알림설정_판매요청", example="1")
    private String notiSellYn = null;

    @Column(name = "noti_buy_req_yn")
    @ApiModelProperty(notes = "알림설정_구매신청", example="1")
    private String notiBuyReqYn = null;

    @Column(name = "noti_notice_yn")
    @ApiModelProperty(notes = "알림설정_공지사항", example="1")
    private String notiNoticeYn = null;

    @Column(name = "noti_qna_yn")
    @ApiModelProperty(notes = "알림설정_1:1문의", example="1")
    private String notiQnaYn = null;

    @Column(name = "noti_chat_yn")
    @ApiModelProperty(notes = "알림설정_채팅대화", example="1")
    private String notiChatYn = null;

    @Column(name = "out_ip")
    @ApiModelProperty(notes = "탈퇴IP", example="1")
    private String outIp = null;

    @Column(name = "out_date")
    @ApiModelProperty(notes = "탈퇴일자", example="")
    private LocalDateTime outDate = null;

    @Column(name = "out_memo")
    @ApiModelProperty(notes = "탈퇴사유", example="1")
    private String outMemo = null;

    @Column(name = "deletion_scheduled_date")
    @ApiModelProperty(notes = "삭제예정일자", example="1")
    private String deletionScheduledDate = null;

    @Column(name = "login_ip")
    @ApiModelProperty(notes = "로그인", example="1")
    private String loginIp;

    @Column(name = "login_date")
    @ApiModelProperty(notes = "로그인일자", example="")
    private LocalDateTime loginDate;

    @Column(name = "mem_phone_safety")
    @ApiModelProperty(notes = "시퀀스", example="Y")
    private String memPhoneSafety = null;

    @Column(name = "noti_rent_yn")
    @ApiModelProperty(notes = "시퀀스", example="1")
    private String notiRentYn = null;

    @Column(name = "check_n")
    @ApiModelProperty(notes = "시퀀스", example="1")
    private String checkN = null;

    @Column(name = "check_g")
    @ApiModelProperty(notes = "시퀀스", example="1")
    private String checkG = null;

    @Column(name = "check_f")
    @ApiModelProperty(notes = "시퀀스", example="1")
    private String checkF = null;

    @Column(name = "check_k")
    @ApiModelProperty(notes = "시퀀스", example="1")
    private String checkK = null;

    @Column(name = "check_e")
    @ApiModelProperty(notes = "시퀀스", example="1")
    private String checkE = null;

    @Column(name = "profile_image")
    @ApiModelProperty(notes = "프로필 이미지 URL", example="http://d1n8fbd6e2ut61.cloudfront.net/asset/upload/product/20200220/2e91cd94-1cc3-48bc-84d7-f8171992276b")
    private String profileImage = null;
    
}