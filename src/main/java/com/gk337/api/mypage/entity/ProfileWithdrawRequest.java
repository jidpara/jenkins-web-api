package com.gk337.api.mypage.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "회원관리 탈퇴신청 Request Vo")
public class ProfileWithdrawRequest {

	@ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

	@JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제, L:탈퇴)", example="L")
    private String cstatus = "L";
	
	@JsonIgnore
    @ApiModelProperty(notes = "탈퇴IP", example="127.0.0.1")
    private String outIp = "127.0.0.1";

    @JsonIgnore
    @ApiModelProperty(notes = "탈퇴일자")
    private LocalDateTime outDate = LocalDateTime.now();

    @ApiModelProperty(notes = "탈퇴사유", example="탈퇴사유 입력하세요.")
    private String outMemo;

}