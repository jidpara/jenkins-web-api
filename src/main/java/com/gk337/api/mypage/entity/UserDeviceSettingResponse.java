package com.gk337.api.mypage.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 유저별 기기설정 Request Vo
 */
@Setter
@Getter
@NoArgsConstructor
@ApiModel(description = "유저별 기기설정 Request Vo")
public class UserDeviceSettingResponse {
	
    @ApiModelProperty(notes = "회원시퀀스", example="60")
    private Integer memSeq;
    
    @ApiModelProperty(notes = "기기고유ID (미입력 시 가장 최근로그인한 uuid로 처리 됨", example="6e5e5d8c5b215d3a")
    private String uuid;

    @JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제, L:탈퇴)", example="Y")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "구매요청 푸시 수신여부 (Y:수신, N:미수신)", example="Y")
    private String notiBuyYn;
    
    @ApiModelProperty(notes = "판매요청 푸시 수신여부 (Y:수신, N:미수신)", example="Y")
    private String notiSellYn;
    
    @ApiModelProperty(notes = "공지사항 푸시 수신여부 (Y:수신, N:미수신)", example="Y")
    private String notiNoticeYn;
    
    @ApiModelProperty(notes = "채팅대화 푸시 수신여부 (Y:수신, N:미수신)", example="Y")
    private String notiChatYn;
           
}