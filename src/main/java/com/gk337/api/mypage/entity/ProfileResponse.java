package com.gk337.api.mypage.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.api.shop.entity.ShopInfoResponse;
import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "회원관리 Response Vo")
public class ProfileResponse extends BaseVo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제, L:탈퇴)", example="Y")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "가입구분 (E:이메일, N:네이버, G:구글, F:페이스북, K:카카오)", example="E")
    private String joinKind;

    @ApiModelProperty(notes = "회원구분 (G:일반회원, S:중고전문가)", example="G")
    private String memKind;

    @ApiModelProperty(notes = "중고전문가신청여부", example="Y")
    private String proRequestYn;

    @ApiModelProperty(notes = "중고전문가신청일자", example="2020-01-31")
    private LocalDateTime proRequestDate;

    @ApiModelProperty(notes = "중고전문가승인일자", example="2020-01-31")
    private LocalDateTime proOkDate;

    @JsonIgnore
    @ApiModelProperty(notes = "회원코드", example="A")
    private String memCode;

    @ApiModelProperty(notes = "회원이메일", example="abc@gmail.com")
    private String memEmail;

    @JsonIgnore
    @ApiModelProperty(notes = "비밀번호 (6~16자리, 영문,숫자혼합)", example="123456")
    private String memPwd;

    @ApiModelProperty(notes = "닉네임 (2~10자리)", example="nickname")
    private String memNickname;

    @ApiModelProperty(notes = "회원이름", example="홍길동")
    private String memName;

    @ApiModelProperty(notes = "휴대전화", example="01012341234")
    private String memHp;

    @JsonIgnore
    @ApiModelProperty(notes = "통신사 (SKT, KT, LG)", example="KT")
    private String memHpCorp;

    @JsonIgnore
    @ApiModelProperty(notes = "휴대폰 본인인증 여부", example="Y")
    private String memAuthYn;

    @JsonIgnore
    @ApiModelProperty(notes = "휴대폰 본인인증 일자", example="2020-01-31")
    private LocalDateTime memAuthDate;

    @JsonIgnore
    @ApiModelProperty(notes = "생년월일", example="1999-01-01")
    private String memBirth;

    @JsonIgnore
    @ApiModelProperty(notes = "성별 (F:여성, M:남성)", example="M")
    private String memGender;

    @ApiModelProperty(notes = "우편번호", example="12708")
    private String memZip;

    @ApiModelProperty(notes = "주소", example="경기도 성남시 분당구 판교동")
    private String memAddr;

    @ApiModelProperty(notes = "상세주소", example="SKC&C")
    private String memAddrDetail;

    @ApiModelProperty(notes = "보유 포인트", example="150000")
    private Integer point;

    @JsonIgnore
    @ApiModelProperty(notes = "소개글", example="소개글")
    private String memDesc;

    @JsonIgnore
    @ApiModelProperty(notes = "은행시퀀스", example="1")
    private Integer bankSeq;

    @JsonIgnore
    @ApiModelProperty(notes = "은행명", example="우리은행")
    private String bankName;

    @JsonIgnore
    @ApiModelProperty(notes = "계좌번호", example="123-12-1234")
    private String accountNum;

    @JsonIgnore
    @ApiModelProperty(notes = "SMS수신여부 (Y:수신, N:수신안함)", example="N")
    private String smsYn;

    @JsonIgnore
    @ApiModelProperty(notes = "이메일수신여부 (Y:수신, N:수신안함)", example="N")
    private String emailYn = "N";

    @JsonIgnore
    @ApiModelProperty(notes = "메모", example="N")
    private String memo;

    @JsonIgnore
    @ApiModelProperty(notes = "내방공개여부", example="N")
    private String roomOpenYn ="N";

    @ApiModelProperty(notes = "채팅공여여부", example="N")
    private String chatOpenYn = "N";

    @ApiModelProperty(notes = "안심번호공개여부", example="N")
    private String safeNumOpenYn = "N";

    @JsonIgnore
    @ApiModelProperty(notes = "안심번호통화가능시간fr (미인증회원은 비노출)", example="18:00")
    private String safeTimeFr;

    @JsonIgnore
    @ApiModelProperty(notes = "안심번호통화가능시간to (미인증회원은 비노출)", example="20:00")
    private String safeTimeTo = "Y";

    @JsonIgnore
    @ApiModelProperty(notes = "알림설정_구매요청", example="N")
    private String notiBuyYn = "Y";

    @JsonIgnore
    @ApiModelProperty(notes = "알림설정_판매요청", example="N")
    private String notiSellYn = "Y";

    @JsonIgnore
    @ApiModelProperty(notes = "알림설정_구매신청", example="N")
    private String notiBuyReqYn = "Y";

    @JsonIgnore
    @ApiModelProperty(notes = "알림설정_공지사항", example="N")
    private String notiNoticeYn = "Y";

    @JsonIgnore
    @ApiModelProperty(notes = "알림설정_1:1문의", example="N")
    private String notiQnaYn = "Y";

    @JsonIgnore
    @ApiModelProperty(notes = "알림설정_채팅대화", example="N")
    private String notiChatYn = "Y";

    @JsonIgnore
    @ApiModelProperty(notes = "탈퇴IP", example="")
    private String outIp;

    @JsonIgnore
    @ApiModelProperty(notes = "탈퇴일자", example="")
    private LocalDateTime outDate;

    @JsonIgnore
    @ApiModelProperty(notes = "탈퇴사유", example="")
    private String outMemo;

    @JsonIgnore
    @ApiModelProperty(notes = "삭제예정일자", example="")
    private String deletionScheduledDate;

    @ApiModelProperty(notes = "로그인IP", example="")
    private String loginIp;

    @ApiModelProperty(notes = "로그인일자", example="")
    private LocalDateTime loginDate;

    @JsonIgnore
    @ApiModelProperty(notes = "회원안심번호", example="1234")
    private String memPhoneSafety;

    @JsonIgnore
    @ApiModelProperty(notes = "notiRentYn", example="N")
    private String notiRentYn = "Y";

    @JsonIgnore
    @ApiModelProperty(notes = "check_n", example="1")
    private String checkN = "";

    @JsonIgnore
    @ApiModelProperty(notes = "check_g", example="1")
    private String checkG = "";

    @JsonIgnore
    @ApiModelProperty(notes = "check_f", example="1")
    private String checkF = "";

    @JsonIgnore
    @ApiModelProperty(notes = "check_k", example="1")
    private String checkK = "";

    @JsonIgnore
    @ApiModelProperty(notes = "check_e", example="1")
    private String checkE = "";

    @ApiModelProperty(notes = "이미지파일 URL", example="http://d1n8fbd6e2ut61.cloudfront.net/asset/upload/product/20200220/2e91cd94-1cc3-48bc-84d7-f8171992276b")
    private String fileUrl = "";

    @JsonIgnore
    @ApiModelProperty(notes = "실제파일명", example="3333333.png")
    private String fileName = "";

    @ApiModelProperty(notes = "프로필 이미지 URL", example="http://d1n8fbd6e2ut61.cloudfront.net/asset/upload/product/20200220/2e91cd94-1cc3-48bc-84d7-f8171992276b")
    private String profileImage;

    @ApiModelProperty(notes = "딜러 상점정보")
    private ShopInfoResponse shopInfoResponse;
    
}