package com.gk337.api.mypage.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "회원관리 Request Vo")
public class ProfileRequest {
	
	@JsonIgnore
    @ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

	@JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제, L:탈퇴)", example="Y")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "가입구분 (E:이메일, N:네이버, G:구글, F:페이스북, K:카카오)", example="E")
    private String joinKind;

    @ApiModelProperty(notes = "회원구분 (G:일반회원, P:딜러[중고전문가])", example="G")
    private String memKind;

    @ApiModelProperty(notes = "중고전문가신청여부", example="Y")
    private String proRequestYn;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
    @ApiModelProperty(notes = "중고전문가신청일자", example="2020-04-01")
    private LocalDate proRequestDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
    @ApiModelProperty(notes = "중고전문가승인일자", example="2020-04-02")
    private LocalDate proOkDate;

    @ApiModelProperty(notes = "회원코드", example="A")
    private String memCode = null;

    @ApiModelProperty(notes = "회원이메일", example="abc@gmail.com")
    private String memEmail;

    @ApiModelProperty(notes = "비밀번호 (6~16자리, 영문,숫자혼합)", example="123456")
    private String memPwd;
    
    @ApiModelProperty(notes = "jw_attach_file에 등록된 이미지파일 시퀀스", example="38")
    private Integer fileSeq;

    @ApiModelProperty(notes = "닉네임 (2~10자리)", example="nickname")
    private String memNickname = null;

    @ApiModelProperty(notes = "회원이름", example="홍길동")
    private String memName = null;

    @ApiModelProperty(notes = "휴대전화", example="01012341234")
    private String memHp = null;

    @ApiModelProperty(notes = "통신사 (SKT, KT, LG)", example="KT")
    private String memHpCorp = null;

    @ApiModelProperty(notes = "휴대폰 본인인증 여부", example="Y")
    private String memAuthYn = null;

    @ApiModelProperty(notes = "휴대폰 본인인증 일자", example="")
    private LocalDateTime memAuthDate;

    @ApiModelProperty(notes = "생년월일", example="19992211")
    private String memBirth = null;

    @ApiModelProperty(notes = "성별 (F:여성, M:남성)", example="M")
    private String memGender = null;

    @ApiModelProperty(notes = "우편번호", example="12708")
    private String memZip = null;

    @ApiModelProperty(notes = "주소", example="경기도 성남시 분당구 판교동")
    private String memAddr = null;

    @ApiModelProperty(notes = "상세주소", example="SKC&C")
    private String memAddrDetail = null;

    @ApiModelProperty(notes = "소개글", example="소개글")
    private String memDesc = null;

    @ApiModelProperty(notes = "은행시퀀스", example="1")
    private Integer bankSeq = null;

    @ApiModelProperty(notes = "은행명", example="우리은행")
    private String bankName = null;

    @ApiModelProperty(notes = "계좌번호", example="123-12-1234")
    private String accountNum = null;

    @ApiModelProperty(notes = "SMS수신여부 (Y:수신, N:수신안함)", example="N")
    private String smsYn = null;

    @ApiModelProperty(notes = "이메일수신여부 (Y:수신, N:수신안함)", example="N")
    private String emailYn = null;

    @ApiModelProperty(notes = "메모", example="N")
    private String memo = null;

    @ApiModelProperty(notes = "내방공개여부", example="N")
    private String roomOpenYn = null;

    @ApiModelProperty(notes = "채팅공여여부", example="N")
    private String chatOpenYn = null;

    @ApiModelProperty(notes = "안심번호공개여부", example="N")
    private String safeNumOpenYn = null;

    @ApiModelProperty(notes = "안심번호통화가능시간fr (미인증회원은 비노출)", example="18")
    private String safeTimeFr = null;

    @ApiModelProperty(notes = "안심번호통화가능시간to (미인증회원은 비노출)", example="20")
    private String safeTimeTo = null;

    @ApiModelProperty(notes = "알림설정_구매요청", example="N")
    private String notiBuyYn = null;

    @ApiModelProperty(notes = "알림설정_판매요청", example="N")
    private String notiSellYn = null;

    @ApiModelProperty(notes = "알림설정_구매신청", example="N")
    private String notiBuyReqYn = null;

    @ApiModelProperty(notes = "알림설정_공지사항", example="N")
    private String notiNoticeYn = null;

    @ApiModelProperty(notes = "알림설정_1:1문의", example="N")
    private String notiQnaYn = null;

    @ApiModelProperty(notes = "알림설정_채팅대화", example="N")
    private String notiChatYn = null;

    @JsonIgnore
    @ApiModelProperty(notes = "탈퇴IP", example="")
    private String outIp = null;

    @JsonIgnore
    @ApiModelProperty(notes = "탈퇴일자", example="")
    private LocalDateTime outDate;

    @JsonIgnore
    @ApiModelProperty(notes = "탈퇴사유", example="")
    private String outMemo = null;

    @JsonIgnore
    @ApiModelProperty(notes = "삭제예정일자", example="2020-01-31")
    private String deletionScheduledDate = null;

    @JsonIgnore
    @ApiModelProperty(notes = "로그인IP", example="")
    private String loginIp = "127.0.0.1";

    @JsonIgnore
    @ApiModelProperty(notes = "로그인일자", example="")
    private LocalDateTime loginDate = LocalDateTime.now();

    @ApiModelProperty(notes = "회원안심번호", example="Y")
    private String memPhoneSafety = null;

    @ApiModelProperty(notes = "notiRentYn", example="N")
    private String notiRentYn = null;

    // ???
    @ApiModelProperty(notes = "check_n", example="1")
    private String checkN = null;

    @ApiModelProperty(notes = "check_g", example="1")
    private String checkG = null;

    @ApiModelProperty(notes = "check_f", example="1")
    private String checkF = null;

    @ApiModelProperty(notes = "check_k", example="1")
    private String checkK = null;

    @ApiModelProperty(notes = "check_e", example="1")
    private String checkE = null;

    @ApiModelProperty(notes = "프로필 이미지 URL", example="http://d1n8fbd6e2ut61.cloudfront.net/asset/upload/product/20200220/2e91cd94-1cc3-48bc-84d7-f8171992276b")
    private String profileImage = null;
}