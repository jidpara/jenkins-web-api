package com.gk337.api.mypage.entity;

import java.io.Serializable;

import com.gk337.common.core.domain.PKEntity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class UserDeviceSettingPK implements Serializable, PKEntity {

	private static final long serialVersionUID = 1L;

	@EqualsAndHashCode.Include
	private Integer memSeq;

	@EqualsAndHashCode.Include
	private String uuid;

}
