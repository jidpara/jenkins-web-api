package com.gk337.api.mypage.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gk337.api.code.model.CodeInfoPK;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 유저별 기기설정 엔티티
 */
@Entity
@Setter
@Getter
@NoArgsConstructor
@IdClass(UserDeviceSettingPK.class)
@Table(name = "jw_device_setting")
@ApiModel(description = "유저별 기기설정 엔티티")
@JsonNaming(PropertyNamingStrategy.LowerCaseWithUnderscoresStrategy.class)
public class UserDeviceSettingEntity extends AuditEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @Id
    @NotNull
    @Column(name = "mem_seq")
    @ApiModelProperty(notes = "회원시퀀스", example="60")
    private Integer memSeq;
    
    @Id
    @NotNull
    @Column(name = "uuid")
    @ApiModelProperty(notes = "기기고유ID", example="6e5e5d8c5b215d3a")
    private String uuid;

    @NotNull
    @Column(name = "cstatus")
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제, L:탈퇴)", example="Y")
    private String cstatus = "Y";

    @NotNull
    @Column(name = "noti_buy_yn")
    @ApiModelProperty(notes = "구매요청 푸시 수신여부 (Y:수신, N:미수신)", example="Y")
    private String notiBuyYn;
    
    @NotNull
    @Column(name = "noti_sell_yn")
    @ApiModelProperty(notes = "판매요청 푸시 수신여부 (Y:수신, N:미수신)", example="Y")
    private String notiSellYn;
    
    @NotNull
    @Column(name = "noti_notice_yn")
    @ApiModelProperty(notes = "공지사항 푸시 수신여부 (Y:수신, N:미수신)", example="Y")
    private String notiNoticeYn;
    
    @NotNull
    @Column(name = "noti_chat_yn")
    @ApiModelProperty(notes = "채팅대화 푸시 수신여부 (Y:수신, N:미수신)", example="Y")
    private String notiChatYn;
           
}