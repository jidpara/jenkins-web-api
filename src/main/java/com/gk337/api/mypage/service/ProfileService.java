package com.gk337.api.mypage.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gk337.api.base.entity.DeviceInfoEntity;
import com.gk337.api.base.repository.DeviceInfoRepository;
import com.gk337.api.mypage.entity.ProfileEntity;
import com.gk337.api.mypage.entity.ProfileRequest;
import com.gk337.api.mypage.entity.ProfileResponse;
import com.gk337.api.mypage.entity.ProfileWithdrawRequest;
import com.gk337.api.mypage.entity.UserDeviceSettingEntity;
import com.gk337.api.mypage.entity.UserDeviceSettingRequest;
import com.gk337.api.mypage.entity.UserDeviceSettingResponse;
import com.gk337.api.mypage.mapper.ProfileMapper;
import com.gk337.api.mypage.repository.ProfileRepository;
import com.gk337.api.mypage.repository.UserDeviceSettingRepository;
import com.gk337.api.shop.entity.ShopInfoEntity;
import com.gk337.api.shop.entity.ShopInfoResponse;
import com.gk337.api.shop.mapper.ShopMapper;
import com.gk337.api.shop.repository.ShopInfoRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.BeanUtilsExt;
import com.gk337.common.util.MessageUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProfileService {
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private ProfileMapper profileMapper;
	
	@Autowired
	private ShopMapper shopMapper;
	
	@Autowired
	private ShopInfoRepository shopInfoRepository;

	/**
	 * 마이페이지 - 프로필 저장 또는 수정.
	 * @param request
	 * @return
	 */
	@Transactional
	public ProfileResponse saveOrUpdate(ProfileRequest request) {
		log.debug("========ProfileService.saveOrUpdate========");
		ProfileResponse response = new ProfileResponse();
		try {
			ProfileEntity entity = new ProfileEntity();
			BeanUtils.copyProperties(request, entity);
			entity = profileRepository.saveAndFlush(entity);
			BeanUtils.copyProperties(entity, response);
			
			response = this.selectMemberBySeq(entity.getSeq());
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.MEMBER.0001", "저장 또는 수정");
		}
		return response;
	}
	
	@Transactional
	public ProfileResponse updateMyProfile(ProfileRequest request) {
		log.debug("========ProfileService.updateProfile========");
		ProfileResponse response = new ProfileResponse();
		try {
			
			profileMapper.updateProfile(request);
			response = this.selectMemberBySeq(request.getSeq());
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.MEMBER.0001", "저장 또는 수정");
		}
		return response;
	}
	
	/**
	 * 마이페이지 - 회원탈퇴.
	 * @param request
	 * @return
	 */
	@Transactional
	public ErrorVo updateWithdraw(ProfileWithdrawRequest request) {
		log.debug("========ProfileService.updateWithdraw========");
		ErrorVo response = new ErrorVo();
		try {
			int seq = request.getSeq();
			String outMemo = request.getOutMemo();
			String outIp = request.getOutIp();
			profileRepository.updateWithdraw(seq, outMemo, outIp);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.MEMBER.0001", "탈퇴신청");
		}
		return response;
	}
	
	/**
	 * 마이페이지 - 프로필 상세조회 
	 * @param seq
	 * @return
	 */
	public ProfileResponse selectMemberBySeq(int seq) {
		log.debug("========ProfileService.selectMemberBySeq========");
		ProfileResponse response = new ProfileResponse();
		ShopInfoEntity shop = new ShopInfoEntity();
		try {
			response = profileMapper.selectMemberBySeq(seq);
			
			if(!checkUserShopInfo(seq)) {
				insertUserShopInfo(response);
			}
			
			// 회원종류 일반/딜러의 구분으로 상점정보 조회.
			ShopInfoResponse shopInfoResponse = new ShopInfoResponse();
			shopInfoResponse = shopMapper.selectShopInfo(seq);
			response.setShopInfoResponse(shopInfoResponse);

			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.MEMBER.0001", "상세정보 조회");
		}
		return response;
	}
	
	public boolean checkUserShopInfo(int memSeq) {
		
		boolean result = true;
		
		
		Optional<ShopInfoEntity> shopInfo = shopInfoRepository.findByMemSeq(memSeq);
		
		result = shopInfo.isPresent();
		
		return result;
	}
	
	public void insertUserShopInfo(ProfileResponse profile) {
		ShopInfoEntity shopInfo = new ShopInfoEntity();
		shopInfo.setCstatus("Y");
		shopInfo.setOwnerName(profile.getMemName());
		shopInfo.setShopName(profile.getMemNickname());
		shopInfo.setShopPhone(profile.getMemHp());
		shopInfo.setZipCode(profile.getMemZip());
		shopInfo.setShopAddr1(profile.getMemAddr());
		shopInfo.setShopAddr2(profile.getMemAddrDetail());
		shopInfo.setMemSeq(profile.getSeq());
		
		shopInfoRepository.save(shopInfo);
	}
	
	/**
	 * 
	 */
	@Autowired
	private DeviceInfoRepository userDeviceInfoRepository;

	@Autowired
	private UserDeviceSettingRepository userDeviceSettingRepository;
	
	@Transactional
	public ErrorVo userDeviceSetting(UserDeviceSettingRequest request) {
		log.debug("========ProfileService.UserDeviceSettingRequest========");
		ErrorVo response = new ErrorVo();
		try {
			DeviceInfoEntity deviceInfoEntity = userDeviceInfoRepository.findTopByMemSeqAndLastLoginYn(request.getMemSeq(), "Y");

			UserDeviceSettingEntity entity = new UserDeviceSettingEntity();
			BeanUtilsExt.copyNonNullProperties(request, entity);
			// API에서 넘어온 값이 없는 경우 최근로그인된 기기의 UUID를 입력
			if(deviceInfoEntity != null && (entity.getUuid() == null || "".equals(entity.getUuid()))){
				entity.setUuid(deviceInfoEntity.getUuid());
			}
			entity = userDeviceSettingRepository.saveAndFlush(entity);
			
			response.setMessage(MessageUtil.get("MESSAGE.RESULT.0001"));
			
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.MEMBER.0001", "저장 또는 수정");
		}
		return response;
	}

	@Transactional
	public UserDeviceSettingResponse  userDeviceSetting(Integer memSeq) {
		log.debug("========ProfileService.UserDeviceSettingRequest========");
		UserDeviceSettingResponse response = new UserDeviceSettingResponse();
		try {
			DeviceInfoEntity deviceInfoEntity = userDeviceInfoRepository.findTopByMemSeqAndLastLoginYn(memSeq, "Y");

			UserDeviceSettingEntity entity = new UserDeviceSettingEntity();

			if(deviceInfoEntity != null){
				entity = userDeviceSettingRepository.findByMemSeqAndUuid(deviceInfoEntity.getMemSeq(), deviceInfoEntity.getUuid());
			} else {
				entity.setMemSeq(memSeq);
				entity.setNotiBuyYn("Y");
				entity.setNotiChatYn("Y");
				entity.setNotiNoticeYn("Y");
				entity.setNotiSellYn("Y");
			}

			BeanUtilsExt.copyNonNullProperties(entity, response);

		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.MEMBER.0001", "저장 또는 수정");
		}
		return response;
	}

}
