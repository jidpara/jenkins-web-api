package com.gk337.api.mypage.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.mypage.entity.ProfileEntity;
import com.gk337.api.mypage.entity.UserDeviceSettingEntity;

@Repository
public interface UserDeviceSettingRepository extends PagingAndSortingRepository<UserDeviceSettingEntity, Integer>, JpaRepository<UserDeviceSettingEntity, Integer> {
	
	UserDeviceSettingEntity findByMemSeqAndUuid(@Param("memSeq") int memSeq, @Param("uuid") String uuid);
}