package com.gk337.api.mypage.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.mypage.entity.ProfileEntity;

/**
 * 
 * @author maroojjang
 *
 */
@Repository
public interface ProfileRepository extends PagingAndSortingRepository<ProfileEntity, Integer>, JpaRepository<ProfileEntity, Integer> {
	
	@SuppressWarnings("unchecked")
	ProfileEntity save(ProfileEntity entity);
	
	@Modifying
	@Query("update ProfileEntity q set q.outDate = now(), q.outMemo = :outMemo, q.outIp = :outIp, q.cstatus = 'L' where q.seq = :seq")
	void updateWithdraw(@Param("seq") int seq, @Param("outMemo") String outMemo, @Param("outIp") String outIp);
	
	@Modifying
	@Query("update ProfileEntity q set q.profileImage = :profileImage where q.seq = :memSeq")
	void updateProfilImage(@Param("profileImage") String profileImage, @Param("memSeq") int memSeq);
	
}