package com.gk337.api.mypage.controller;

import javax.validation.Valid;

import com.gk337.api.mypage.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.mypage.service.ProfileService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 상품 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 회원관련 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.01.26
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/mypage/")
@Api(description="딜러/유저 프로필 조회 및 수정", tags= {"[마이페이지] - 프로필정보 API"}, protocols="http", produces="application/json", consumes="application/json")
public class ProfileRestController {
	
	@Autowired
	private ProfileService profileService;
	
	/**
	 * 마이페이지 프로필저장.
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "유저/딜러 프로필 저장 API", notes = "프로필 저장 ",	response = ProfileResponse.class)
	@RequestMapping(method = RequestMethod.POST, path="/profile", produces = "application/json")
	public ResponseEntity<ProfileResponse> insertMember(@RequestBody @Valid ProfileRequest request) {
		return BaseResponse.ok(profileService.saveOrUpdate(request));
	}
	
	/**
	 * 마이페이지 프로필수정.
	 * @param seq
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "유저/딜러 프로필 수정 API", notes = "프로필 수정 ", 	response = ProfileResponse.class)
	@RequestMapping(method = RequestMethod.PUT, path="/profile/{seq}", produces = "application/json")
	public ResponseEntity<ProfileResponse> updateMember(
			@ApiParam(value="회원시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq,
			@RequestBody @Valid ProfileRequest request) {
		request.setSeq(seq);
		return BaseResponse.ok(profileService.updateMyProfile(request));
	}
	
	/**
	 * 마이페이지 프로필 상세조회.
	 * @param seq
	 * @return
	 */
	@ApiOperation(value = "유저/딜러 프로필 상세조회 API", notes = "유저의 경우 회원테이블에서 조회", response = ProfileResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/profile/{seq}", produces = "application/json")
	public ResponseEntity<ProfileResponse> selectMemberBySeq(
			@ApiParam(value="회원시퀀스", required=true, defaultValue = "1") @PathVariable(value = "seq", required = true) int seq) {
		return BaseResponse.ok(profileService.selectMemberBySeq(seq));
	}
	
	/**
	 * 마이페이지 회원탈퇴.
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "유저/딜러 회원 탈퇴 API", notes = "마이페이지 회원정보 탈퇴", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.PUT, path="/profile/withdraw", produces = "application/json")
	public ResponseEntity<ErrorVo> updateWithdraw(@RequestBody @Valid ProfileWithdrawRequest request) {
		return BaseResponse.ok(profileService.updateWithdraw(request));
	}

	/**
	 * 마이페이지 프로필 상세조회.
	 * @param seq
	 * @return
	 */
	@ApiOperation(value = "마이페이지 설정 조회 API", notes = "현재 기기의 설정정보 조회", response = ProfileResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/device/setting/{memSeq}", produces = "application/json")
	public ResponseEntity<UserDeviceSettingResponse> selectDeviceSetting(
			@ApiParam(value="회원시퀀스", required=true, defaultValue = "1") @PathVariable(value = "memSeq", required = true) int memSeq) {
		return BaseResponse.ok(profileService.userDeviceSetting(memSeq));
	}

	/**
	 * 마이페이지 셋팅 설정.
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "마이페이지 설정 API", notes = "마이페이지 설정 API ", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.PUT, path="/device/setting", produces = "application/json")
	public ResponseEntity<ErrorVo> UserDeviceSetting(@RequestBody @Valid UserDeviceSettingRequest request) {
		return BaseResponse.ok(profileService.userDeviceSetting(request));
	}
	

}