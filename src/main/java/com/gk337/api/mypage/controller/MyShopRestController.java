package com.gk337.api.mypage.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.mypage.entity.ProfileRequest;
import com.gk337.api.mypage.entity.ProfileResponse;
import com.gk337.api.mypage.entity.ProfileWithdrawRequest;
import com.gk337.api.mypage.service.ProfileService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 상품 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 회원관련 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.01.26
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

//@CrossOrigin("*")
//@RestController
//@RequestMapping("/api/mypage/")
//@Api(description="", tags= {"[마이페이지] - 내상점정보 API"}, protocols="http", produces="application/json", consumes="application/json")
//public class MyShopRestController {
//	
//	@ApiOperation(value = "상점정보 조회 API", notes = "상점정보 조회 API", response = ProfileResponse.class)
//	@RequestMapping(method = RequestMethod.POST, path="/myshop", produces = "application/json")
//	public ResponseEntity<ErrorVo> saveMember() {
//		return null;
//	}
//	
//}
