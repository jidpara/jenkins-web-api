package com.gk337.api.mypage.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.mypage.entity.ProfileRequest;
import com.gk337.api.mypage.entity.ProfileResponse;

@Mapper
public interface ProfileMapper {
	
	public ProfileResponse selectMemberBySeq(int seq);
	
	public void updateProfile(ProfileRequest request);
	
}
