package com.gk337.api.info.service;

import java.util.HashMap;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gk337.api.info.entity.TermsEntity;
import com.gk337.api.info.entity.TermsRequest;
import com.gk337.api.info.entity.TermsResponse;
import com.gk337.api.info.mapper.TermsMapper;
import com.gk337.api.info.repository.TermsRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TermsService {

	@Autowired
	private TermsRepository termsRepository;
	
	@Autowired
	@Qualifier(value="termsMapper")
	private TermsMapper termsMapper;
	
	/**
	 * 약관정보 저장 또는 수정.
	 * @param entity
	 * @return
	 */
	@Transactional
	public TermsResponse saveOrUpdate(TermsRequest request) {
		log.debug("========TermsService.saveOrUpdate========");
		TermsResponse response = new TermsResponse();
		try {
			TermsEntity entity = new TermsEntity();
			BeanUtils.copyProperties(request, entity);
			entity = termsRepository.save(entity);
			BeanUtils.copyProperties(entity, response);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.TERMS.0001", "저장 또는 수정");
		}
		return response;
	}
	
	/**
	 * 약관정보 삭제.
	 * @param seq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteBySeq(int seq) {
		log.debug("========TermsService.deleteBySeq========");
		ErrorVo result = new ErrorVo();
		try {
			termsRepository.deleteBySeq(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.TERMS.0001", "삭제");
		}
		return result;
	}
	
	/**
	 * 약관정보 상세조회.
	 * @param seq
	 * @return TermsResponse
	 */
	public TermsResponse selectTermsBySeq(int seq) {
		TermsResponse response = new TermsResponse();
		try {
			response = termsMapper.selectTermsBySeq(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.TERMS.0001", "상세조회");
		}
		return response;
	}
	
	/**
	 * 약관종류별 목록조회.
	 * 약관종류코드 없는 경우 - 전체조회.
	 * @param termsKind
	 * @return ListVo<TermsResponse>
	 */
	public ListVo<TermsResponse> selectTermsByTermsKind(String termsKind) {
		ListVo<TermsResponse> listVo = null;
		try {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("termsKind", termsKind);
			listVo = new ListVo<TermsResponse>(termsMapper.selectTermsByTermsKind(hashMap));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.TERMS.0001", "목록조회");
		}
		return listVo;
	}
	
	
	
	
}
