package com.gk337.api.info.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gk337.api.info.entity.FaqEntity;
import com.gk337.api.info.entity.FaqRequest;
import com.gk337.api.info.entity.FaqResponse;
import com.gk337.api.info.entity.NoticeResponse;
import com.gk337.api.info.entity.TermsResponse;
import com.gk337.api.info.mapper.FaqMapper;
import com.gk337.api.info.repository.FaqRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FaqService {

	@Autowired
	private FaqRepository faqRepository;
	
	@Autowired
	@Qualifier(value="faqMapper")
	private FaqMapper faqMapper;
	
	/**
	 * 자주하는 질문 저장 또는 수정.
	 * @param FaqRequest
	 * @return
	 */
	@Transactional
	public FaqResponse saveOrUpdate(FaqRequest request) {
		log.debug("========FaqService.saveOrUpdate========");
		FaqResponse response = new FaqResponse();
		try {
			FaqEntity entity = new FaqEntity();
			BeanUtils.copyProperties(request, entity);
			entity = faqRepository.save(entity);
			BeanUtils.copyProperties(entity, response);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.FAQ.0001", "저장 또는 수정");
		}
		return response;
	}
	
	/**
	 * 자주하는 질문 삭제.
	 * @param seq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteBySeq(int seq) {
		log.debug("========FaqService.deleteBySeq========");
		ErrorVo result = new ErrorVo();
		try {
			faqRepository.deleteBySeq(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.FAQ.0001", "삭제");
		}
		return result;
	}
	
	/**
	 * 조회수 업데이트.
	 * @param seq
	 * @return
	 */
	@Transactional
	public ErrorVo updateReadCnt(int seq) {
		log.debug("========FaqService.updateReadCnt========");
		try {
			faqRepository.updateReadCnt(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.FAQ.0001", "조회수 업데이트");
		}
		return new ErrorVo();
	}
	
	/**
	 * 상세조회.
	 * @param seq
	 * @return
	 */
	public FaqResponse selectFaqBySeq(int seq) {
		log.debug("========FaqService.selectFaqBySeq========");
		FaqResponse response = new FaqResponse();
		try {
			response = faqMapper.selectFaqBySeq(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.FAQ.0001", "상세정보 조회");
		}
		return response;
	}
	
	/**
	 * 목록조회.
	 * @param faqKind
	 * @param title
	 * @return
	 */
	public ListVo<FaqResponse> selectFaqByCondition(String faqKind, String title) {
		log.debug("========FaqService.selectFaqByKeyword========");
		ListVo<FaqResponse> listVo = null;
		try {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("faqKind", faqKind);
			hashMap.put("title", title);
			listVo = new ListVo<FaqResponse>(faqMapper.selectFaqByCondition(hashMap));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.FAQ.0001", "조건조회");
		}
		return listVo;
	}
	
	/**
	 * 목록페이징.
	 * @param pageNo
	 * @param pageSize
	 * @param faqKind
	 * @param title
	 * @return
	 */
	public PageInfo<FaqResponse> selectFaqByConditionPaging(int pageNo, int pageSize, String faqKind, String title) {
		log.debug("========FaqService.selectFaqByConditionPaging========");
		PageInfo<FaqResponse> pageInfo = new PageInfo<FaqResponse>();
		try {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("faqKind", faqKind);
			hashMap.put("title", title);
			
			PageHelper.startPage(pageNo, pageSize); 
			List<FaqResponse> list = faqMapper.selectFaqByCondition(hashMap);
			pageInfo = new PageInfo<FaqResponse>(list);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.FAQ.0001", "조건조회");
		}
		return pageInfo;
	};
	
}
