package com.gk337.api.info.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gk337.api.info.entity.NoticeEntity;
import com.gk337.api.info.entity.NoticeRequest;
import com.gk337.api.info.entity.NoticeResponse;
import com.gk337.api.info.mapper.NoticeMapper;
import com.gk337.api.info.repository.NoticeRepository;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;
import com.gk337.common.util.BeanUtilsExt;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NoticeService {

	@Autowired
	private NoticeRepository noticeRepository;
	
	@Autowired
	@Qualifier(value="noticeMapper")
	private NoticeMapper noticeMapper;
	
	/**
	 * 공지사항 저장 또는 수정
	 * @param request
	 * @return
	 */
	@Transactional
	public NoticeResponse saveOrUpdate(NoticeRequest request) {
		log.debug("========NoticeService.saveOrUpdate========");
		NoticeResponse response = new NoticeResponse();
		try {
			NoticeEntity entity = new NoticeEntity();
			//BeanUtils.copyProperties(request, entity);
			BeanUtilsExt.copyNonNullProperties(request, entity);
			
			log.debug("{}", entity.getTitle());
			
			entity = noticeRepository.save(entity);
			
			
			log.debug("{}", entity.getTitle());
			
			BeanUtils.copyProperties(entity, response);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.NOTICE.0001", "저장 또는 수정");
		}
		return response;
	}
	
	/**
	 * 공지사항 삭제
	 * @param seq
	 * @return
	 */
	@Transactional
	public ErrorVo deleteBySeq(int seq) {
		log.debug("========NoticeService.deleteBySeq========");
		ErrorVo result = new ErrorVo();
		try {
			noticeRepository.deleteBySeq(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.NOTICE.0001", "삭제");
		}
		return result;
	}
	
	/**
	 * 공지사항 조회수 업데이트
	 * @param seq
	 * @return
	 */
	@Transactional
	public ErrorVo updateReadCnt(int seq) {
		log.debug("========NoticeService.updateReadCnt========");
		try {
			noticeRepository.updateReadCnt(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.NOTICE.0001", "조회수 업데이트");
		}
		return new ErrorVo();
	}
	
	/**
	 * 상세조회.
	 * @param seq
	 * @return
	 */
	public NoticeResponse selectNoticeBySeq(int seq) {
		log.debug("========NoticeService.selectNoticeBySeq========");
		NoticeResponse response = new NoticeResponse();
		try {
			response = noticeMapper.selectNoticeBySeq(seq);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.NOTICE.0001", "상세정보 조회");
		}
		return response;
	}
	
	/**
	 * 조건조회[목록전체].
	 * @param title
	 * @param regDate
	 * @return
	 */
	public ListVo<NoticeResponse> selectNoticeByCondition(String startDate, String endDate, String title) {
		log.debug("========NoticeService.selectNoticeByCondition========");
		ListVo<NoticeResponse> listVo = null;
		try {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("startDate"	, startDate);
			hashMap.put("endDate"	, endDate);
			hashMap.put("title"		, title);
			listVo = new ListVo<NoticeResponse>(noticeMapper.selectNoticeByCondition(hashMap));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.NOTICE.0001", "조건조회");
		}
		return listVo;
	}
	
	/**
	 * 조건조회[페이징]
	 * @param pageNo
	 * @param pageSize
	 * @param startDate
	 * @param endDate
	 * @param title
	 * @return
	 */
	public PageInfo<NoticeResponse> selectNoticeByConditionPaging(int pageNo, int pageSize, String startDate, String endDate, String title) {
		log.debug("========NoticeService.selectNoticeByConditionPaging========");
		PageInfo<NoticeResponse> pageInfo = new PageInfo<NoticeResponse>();
		try {
			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("startDate"	, startDate);
			hashMap.put("endDate"	, endDate);
			hashMap.put("title"		, title);
			
			PageHelper.startPage(pageNo, pageSize); 
			List<NoticeResponse> list = noticeMapper.selectNoticeByCondition(hashMap);
			pageInfo = new PageInfo<NoticeResponse>(list);
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.NOTICE.0001", "조건조회");
		}
		return pageInfo;
	};
	
}
