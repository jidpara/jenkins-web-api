package com.gk337.api.info.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.info.entity.NoticeResponse;
import com.gk337.api.info.entity.TermsEntity;
import com.gk337.api.info.entity.TermsRequest;
import com.gk337.api.info.entity.TermsResponse;
import com.gk337.api.info.service.TermsService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 상품 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 공지사항 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.01.26
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/info")
@Api(tags= {"[고객센터] - 운영정책 API"}, protocols="http", produces="application/json", consumes="application/json")
public class PolicyRestController {
	
	@ApiOperation(value = "운영정책 상세조회[단건] API - 테이블 확인중", notes = "운영정책 상세조회 한다", response = TermsResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/policy/{seq}", produces = "application/json")
	public ResponseEntity<ErrorVo> selectpolicyBySeq(
			@ApiParam(value="시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq) {
		return null;
	}
	
	@ApiOperation(value = "운영정책 목록조회 API - 테이블 확인중.", notes = "운영정책 목록조회 한다.", response = TermsResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/policy", produces = "application/json")
	public ResponseEntity<ErrorVo> selectPolicyByTermsKind() {
		return null;
	}
	
}

/*
{
  "termsKind": "L",
  "content": "약관정보입니다... 긴글이 들어갑니다. 수정도 가능한 ....",
}
*/
