package com.gk337.api.info.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.info.entity.NoticeResponse;
import com.gk337.api.info.entity.TermsEntity;
import com.gk337.api.info.entity.TermsRequest;
import com.gk337.api.info.entity.TermsResponse;
import com.gk337.api.info.service.TermsService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 상품 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 공지사항 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.01.26
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/info")
@Api(tags= {"[고객센터] - 약관정보 API"}, protocols="http", produces="application/json", consumes="application/json")
public class TermsRestController {
	
	@Autowired
	private TermsService termsService;
	
	@ApiOperation(value = "약관정보 저장 API", notes = "약관정보 저장한다.", response = TermsResponse.class)
	@RequestMapping(method = RequestMethod.POST, path="/terms", produces = "application/json")
	public ResponseEntity<TermsResponse> save(@RequestBody @Valid TermsRequest request) {
		return BaseResponse.ok(termsService.saveOrUpdate(request));
	}
	
	@ApiOperation(value = "약관정보 수정 API", notes = "약관정보 수정한다.", response = TermsResponse.class)
	@RequestMapping(method = RequestMethod.PUT, path="/terms/{seq}", produces = "application/json")
	public ResponseEntity<TermsResponse> update(
			@ApiParam(value="시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq,
			@RequestBody @Valid TermsRequest request) {
		request.setSeq(seq);
		return BaseResponse.ok(termsService.saveOrUpdate(request));
	}
	
	@ApiOperation(value = "약관정보 삭제 API", notes = "약관정보 삭제한다.", response = TermsResponse.class)
	@RequestMapping(method = RequestMethod.DELETE, path="/terms/{seq}", produces = "application/json")
	public ResponseEntity<ErrorVo> deleteArea(
			@ApiParam(value="시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq) {
		return BaseResponse.ok(termsService.deleteBySeq(seq));
	}
	
	@ApiOperation(value = "약관정보 상세조회[단건] API", notes = "약관정보 상세조회 한다", response = TermsResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/terms/{seq}", produces = "application/json")
	public ResponseEntity<TermsResponse> selectTermsBySeq(
			@ApiParam(value="시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq) {
		return BaseResponse.ok(termsService.selectTermsBySeq(seq));
	}
	
	@ApiOperation(value = "약관정보 조건조회[목록전체] API", notes = "약관정보 목록조회 한다.", response = TermsResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/terms", produces = "application/json")
	public ResponseEntity<List<TermsResponse>> selectTermsByTermsKind(
			@ApiParam(value="약관종류 - 미입력시 전체조회", required=false, defaultValue = "S") @RequestParam(value ="termsKind", required = false) String termsKind) {
		return BaseResponse.ok(termsService.selectTermsByTermsKind(termsKind));
	}
	
}

/*
{
  "termsKind": "L",
  "content": "약관정보입니다... 긴글이 들어갑니다. 수정도 가능한 ....",
}
*/
