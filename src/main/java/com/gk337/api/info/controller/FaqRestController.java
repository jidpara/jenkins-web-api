package com.gk337.api.info.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.info.entity.FaqRequest;
import com.gk337.api.info.entity.FaqResponse;
import com.gk337.api.info.entity.NoticeResponse;
import com.gk337.api.info.service.FaqService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 상품 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 자주하는질문 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.01.26
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/info")
@Api(tags= {"[고객센터] - 자주하는 질문 API"}, protocols="http", produces="application/json", consumes="application/json")
public class FaqRestController {
	
	@Autowired
	private FaqService faqService;
	
	@ApiOperation(value = "자주하는 질문 저장 API", notes = "자주하는 질문을 저장", response = FaqResponse.class)
	@RequestMapping(method = RequestMethod.POST, path="/faq", produces = "application/json")
	public ResponseEntity<FaqResponse> save(@RequestBody @Valid FaqRequest request) {
		return BaseResponse.ok(faqService.saveOrUpdate(request));
	}
	
	@ApiOperation(value = "자주하는 질문 수정 API", notes = "자주하는 질문을 수정한다", response = FaqResponse.class)
	@RequestMapping(method = RequestMethod.PUT, path="/faq/{seq}", produces = "application/json")
	public ResponseEntity<FaqResponse> update(
			@ApiParam(value="시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq,
			@RequestBody @Valid FaqRequest request) {
		request.setSeq(seq);
		return BaseResponse.ok(faqService.saveOrUpdate(request));
	}
	
	@ApiOperation(value = "자주하는 질문 삭제 API", notes = "자주하는 질문 삭제한다.", response = FaqResponse.class)
	@RequestMapping(method = RequestMethod.DELETE, path="/faq/{seq}", produces = "application/json")
	public ResponseEntity<ErrorVo> deleteArea(
			@ApiParam(value="시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq) {
		return BaseResponse.ok(faqService.deleteBySeq(seq));
	}
	
	@ApiOperation(value = "자주하는 질문 조회수 업데이트 API", notes = "자주하는 질문 조회수 업데이트한다", response = FaqResponse.class)
	@RequestMapping(method = RequestMethod.PUT, path="/faq/count/{seq}", produces = "application/json")
	public ResponseEntity<ErrorVo> updateReadCnt(
			@ApiParam(value="시퀀스", required=true, defaultValue = "1") @PathVariable(value = "seq", required = true) int seq) {
		return BaseResponse.ok(faqService.updateReadCnt(seq));
	}
	
	@ApiOperation(value = "자주하는 질문 상세조회[단건] API", notes = "자주하는 질문 상세조회 API", response = FaqResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/faq/{seq}", produces = "application/json")
	public ResponseEntity<FaqResponse> findBySeq(
			@ApiParam(value="시퀀스", required=true, defaultValue = "1") @PathVariable(value = "seq", required = true) int seq) {
		return BaseResponse.ok(faqService.selectFaqBySeq(seq));
	}
	
	@ApiOperation(value = "자주하는 질문 조건조회[목록전체] API", notes = "자주하는 질문 조건조회[목록전체] API", response = FaqResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/faq", produces = "application/json")
	public ResponseEntity<List<FaqResponse>> selectFaqByCondition(
			@ApiParam(value="FAQ종류 - 미입력시 전체조회", required=false, defaultValue = "L") @RequestParam(value ="faqKind", required = false) String faqKind,
			@ApiParam(value="제목검색 - 미입력시 전체조회", required=false, defaultValue = "자주") @RequestParam(value ="title", required = false) String title) {
		return BaseResponse.ok(faqService.selectFaqByCondition(faqKind, title));
	}
	
	@ApiOperation(value = "자주하는 질문 조건조회[목록페이징] API", notes = "자주하는 질문 조건조회[목록페이징] API", response = FaqResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/faq/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<List<FaqResponse>> selectFaqByCondition(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="FAQ종류 - 미입력시 전체조회", required=false, defaultValue = "L") @RequestParam(value ="faqKind", required = false) String faqKind,
			@ApiParam(value="제목검색 - 미입력시 전체조회", required=false, defaultValue = "자주") @RequestParam(value ="title", required = false) String title) {
		return BaseResponse.ok(faqService.selectFaqByConditionPaging(pageNo, pageSize, faqKind, title));
	}
}

/*
{
  "content": "자주하는 질문1",
  "cstatus": "Y",
  "faqKind": "L",
  "readCnt": 0,
  "title": "질문제목1"
}
 */

