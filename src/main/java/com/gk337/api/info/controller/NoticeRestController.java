package com.gk337.api.info.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.gk337.api.info.entity.NoticeEntity;
import com.gk337.api.info.entity.NoticeRequest;
import com.gk337.api.info.entity.NoticeResponse;
import com.gk337.api.info.service.NoticeService;
import com.gk337.common.core.exception.ErrorVo;
import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 상품 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 공지사항 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.01.26
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/info")
@Api(tags= {"[고객센터] - 공지사항 API"}, protocols="http", produces="application/json", consumes="application/json")
public class NoticeRestController {
	
	@Autowired
	private NoticeService noticeService;
	
	@ApiOperation(value = "공지사항 저장", notes = "공지사항내용을 저장", response = NoticeResponse.class)
	@RequestMapping(method = RequestMethod.POST, path="/notice", produces = "application/json")
	public ResponseEntity<NoticeResponse> save(@RequestBody @Valid NoticeRequest request) {
		return BaseResponse.ok(noticeService.saveOrUpdate(request));
	}
	
	@ApiOperation(value = "공지사항 수정", notes = "공지사항 내용을 수정한다", response = NoticeResponse.class)
	@RequestMapping(method = RequestMethod.PUT, path="/notice/{seq}", produces = "application/json")
	public ResponseEntity<NoticeResponse> update(
			@ApiParam(value="수정할 공지사항 시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq,
			@RequestBody @Valid NoticeRequest request) {
		request.setSeq(seq);
		return BaseResponse.ok(noticeService.saveOrUpdate(request));
	}
	
	@ApiOperation(value = "공지사항 삭제 API", notes = "공지사항 삭제한다.", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.DELETE, path="/notice/{seq}", produces = "application/json")
	public ResponseEntity<ErrorVo> deleteArea(
			@ApiParam(value="삭제할 공지사항 시퀀스", required=true, defaultValue = "1") @PathVariable(value ="seq", required = true) int seq) {
		return BaseResponse.ok(noticeService.deleteBySeq(seq));
	}
	
	@ApiOperation(value = "공지사항 조회수 업데이트", notes = "공지사항 공지사항 조회수 업데이트한다", response = ErrorVo.class)
	@RequestMapping(method = RequestMethod.PUT, path="/notice/count/{seq}", produces = "application/json")
	public ResponseEntity<ErrorVo> updateReadCnt(
			@ApiParam(value="공지사항 시퀀스", required=true, defaultValue = "1") @PathVariable(value = "seq", required = true) int seq) {
		return BaseResponse.ok(noticeService.updateReadCnt(seq));
	}
	
	@ApiOperation(value = "공지사항 상세조회[단건] API", notes = "공지사항 상세조회 API", response = NoticeResponse.class)
	@RequestMapping(method = RequestMethod.GET, path="/notice/{seq}", produces = "application/json")
	public ResponseEntity<NoticeResponse> selectNoticeBySeq(
			@ApiParam(value="시퀀스", required=true, defaultValue = "1") @PathVariable(value = "seq", required = true) int seq) {
		return BaseResponse.ok(noticeService.selectNoticeBySeq(seq));
	}
	
	@ApiOperation(value = "공지사랑 조건조회[목록전체]", notes = "공지사랑 조건조회[전체]", response = NoticeEntity.class)
	@RequestMapping(method = RequestMethod.GET, path="/notice", produces = "application/json")
	public ResponseEntity<List<NoticeResponse>> selectNoticeByCondition(
			@ApiParam(value="조회시작일시", required=true, defaultValue = "2020-01-01") @RequestParam(value ="startDate", required = true) String startDate,
			@ApiParam(value="조회종료일시", required=true, defaultValue = "2020-12-12") @RequestParam(value ="endDate", required = true) String endDate,
			@ApiParam(value="제목검색 - 미입력시 전체조회", required=false, defaultValue = "자주") @RequestParam(value ="title", required = false) String title) {
		return BaseResponse.ok(noticeService.selectNoticeByCondition(startDate, endDate, title));
	}
	
	@ApiOperation(value = "공지사랑 조건조회[목록페이징]", notes = "공지사랑 조건조회[목록페이징]", response = NoticeEntity.class)
	@RequestMapping(method = RequestMethod.GET, path="/notice/{pageNo}/{pageSize}", produces = "application/json")
	public ResponseEntity<PageInfo<NoticeResponse>> selectNoticeByConditionPaging(
			@ApiParam(value="페이지번호", required=true, example="1") @PathVariable(value = "pageNo", required = true) int pageNo,
			@ApiParam(value="페이지당 리스트 수", required=true, example="10") @PathVariable(value = "pageSize", required = true) int pageSize,
			@ApiParam(value="조회시작일시", required=true, defaultValue = "2020-01-01") @RequestParam(value ="startDate", required = true) String startDate,
			@ApiParam(value="조회종료일시", required=true, defaultValue = "2020-12-12") @RequestParam(value ="endDate", required = true) String endDate,
			@ApiParam(value="제목검색 - 미입력시 전체조회", required=false, defaultValue = "자주") @RequestParam(value ="title", required = false) String title) {
		return BaseResponse.ok(noticeService.selectNoticeByConditionPaging(pageNo, pageSize, startDate, endDate, title));
	}
	
}

/*
{
  "attachFile": "a.jpg",
  "content": "this is content",
  "cstatus": "Y",
  "readCnt": 0,
  "title": "title"
}
*/
