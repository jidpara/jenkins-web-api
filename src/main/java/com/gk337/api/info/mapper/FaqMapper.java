package com.gk337.api.info.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.info.entity.FaqResponse;

/***
 * 
 * @author maroojjang
 *
 */
@Mapper
public interface FaqMapper {
	
	/**
	 * 상세조회.
	 * @param seq
	 * @return
	 */
	public FaqResponse selectFaqBySeq(int seq);
	
	/**
	 * 목록조회 페이징.
	 * @param hashMap
	 * @return
	 */
	public List<FaqResponse> selectFaqByCondition(HashMap<String, String> hashMap);
	
}
