package com.gk337.api.info.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.info.entity.TermsResponse;

/***
 * 
 * @author maroojjang
 *
 */
@Mapper
public interface TermsMapper {
	
	/**
	 * 상세조회.
	 * @param seq
	 * @return
	 */
	public TermsResponse selectTermsBySeq(int seq);
	
	/**
	 * 목록조회.
	 * @param hashMap
	 * @return
	 */
	public List<TermsResponse> selectTermsByTermsKind(HashMap<String, String> hashMap);
	
}
