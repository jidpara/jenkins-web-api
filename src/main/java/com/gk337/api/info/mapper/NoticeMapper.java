package com.gk337.api.info.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.info.entity.NoticeResponse;

/***
 * 
 * @author maroojjang
 *
 */
@Mapper
public interface NoticeMapper {
	
	/**
	 * 상세조회.
	 * @param seq
	 * @return
	 */
	public NoticeResponse selectNoticeBySeq(int seq);
	
	/**
	 * 목록조회 페이징.
	 * @param hashMap
	 * @return
	 */
	public List<NoticeResponse> selectNoticeByCondition(HashMap<String, String> hashMap);
	
}
