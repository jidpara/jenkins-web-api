package com.gk337.api.info.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.info.entity.TermsEntity;

@Repository
@SuppressWarnings("unchecked")
public interface TermsRepository extends PagingAndSortingRepository<TermsEntity, Integer>,  JpaRepository<TermsEntity, Integer> {

	TermsEntity save(TermsEntity entity);
	
	void deleteBySeq(int seq);
	
}