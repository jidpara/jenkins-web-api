package com.gk337.api.info.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.info.entity.FaqEntity;

@Repository
@SuppressWarnings("unchecked")
public interface FaqRepository extends PagingAndSortingRepository<FaqEntity, Integer>, JpaRepository<FaqEntity, Integer> {

	FaqEntity save(FaqEntity entity);
	
	void deleteBySeq(int seq);
	
	@Modifying
	@Query("update FaqEntity q set q.readCnt = q.readCnt + 1 where q.seq = :seq")
	void updateReadCnt(@Param("seq") int seq);
	
//	FaqEntity findBySeq(Integer seq);
//	
//	//전체조회.
//	Page<FaqEntity> findByCstatusOrderBySeqDesc(String cstatus, Pageable pageable);
//	
//	//전체조회-모든카테고리내에 타이틀검색.
//	Page<FaqEntity> findByCstatusAndTitleLikeOrderBySeqDesc(String cstatus, String title, Pageable pageable);
//	
//	//전체조회-검색어없이 카테고리검색.
//	Page<FaqEntity> findByCstatusAndFaqKindOrderBySeqDesc(String cstatus, String faqKind, Pageable pageable);
//	
//	//전체조회-카테고리와 타이틀검색.
//	Page<FaqEntity> findByCstatusAndFaqKindAndTitleLikeOrderBySeqDesc(String cstatus, String faqKind, String title, Pageable pageable);
	
}