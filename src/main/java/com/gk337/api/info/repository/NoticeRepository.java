package com.gk337.api.info.repository;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.info.entity.NoticeEntity;

@Repository
@SuppressWarnings("unchecked")
public interface NoticeRepository extends PagingAndSortingRepository<NoticeEntity, Integer>,  JpaRepository<NoticeEntity, Integer> {

	NoticeEntity save(NoticeEntity entity);
	
	@Modifying
	@Query("UPDATE NoticeEntity q set q.readCnt = q.readCnt + 1 where q.seq = :seq")
	void updateReadCnt(@Param("seq") int seq);
	
	void deleteBySeq(int seq);

}