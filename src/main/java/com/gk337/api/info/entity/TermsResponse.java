package com.gk337.api.info.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "약관정보 Response Vo")
public class TermsResponse extends BaseVo implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus;

    @ApiModelProperty(notes = "약관종류 (U:이용약관, P:개인정보취급방침, L:위치기반 서비스 이용약관)", example="L")
    private String termsKind;

    @ApiModelProperty(notes = "내용", example="약관내용 입니다.")
    private String content;
    
    @ApiModelProperty(notes = "등록일자", example="2020-01-01")
    private String regDate;
    
}