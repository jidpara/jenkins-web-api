package com.gk337.api.info.entity;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "공지사항 Request Vo")
public class NoticeRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ApiModelProperty(value="시퀀스", example="1", hidden=true)
    private Integer seq;

    //@JsonIgnore
    @ApiModelProperty(value = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y", hidden=true)
    private String cstatus = "Y";

    @ApiModelProperty(value = "제목", example="제목을 입력합니다.")
    private String title;

    @ApiModelProperty(value = "내용", example="내용을 입력합니다.")
    private String content;
    
    @ApiModelProperty(value = "첨부파일", example="a.jpg")
    private String attachFile;

    @JsonIgnore
    @ApiModelProperty(value = "조회수", hidden=true)
    private Integer readCnt = 0;
    
}