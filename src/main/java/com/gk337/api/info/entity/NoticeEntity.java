package com.gk337.api.info.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 공지사항
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_notice")
@ApiModel(description = "상품조회 Entity")
public class NoticeEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @Column(name = "cstatus", nullable = false)
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus;

    @Column(name = "title", nullable = false)
    @ApiModelProperty(notes = "내용")
    private String title;

    @Column(name = "content", nullable = false)
    @ApiModelProperty(notes = "내용")
    private String content;
    
    @Column(name = "attach_file")
    @ApiModelProperty(notes = "첨부파일.")
    private String attachFile;

    @Column(name = "read_cnt")
    @ApiModelProperty(notes = "조회수")
    private Integer readCnt;
    
}