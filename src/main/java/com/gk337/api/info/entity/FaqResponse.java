package com.gk337.api.info.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 자주하는 질문 
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "자주하는 질문 Response Vo")
public class FaqResponse extends BaseVo implements Serializable {
	
	private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="D")
    private String cstatus;

    @ApiModelProperty(notes = "FAQ 종류", example="L")
    private String faqKind;
    
    @ApiModelProperty(notes = "제목", example="제목..")
    private String title;

    @ApiModelProperty(notes = "내용", example="내용..")
    private String content;

    @ApiModelProperty(notes = "조회수", example="1")
    private Integer readCnt;
    
    @ApiModelProperty(notes = "등록일자", example="2020-01-01")
    private String regDate;
    
}