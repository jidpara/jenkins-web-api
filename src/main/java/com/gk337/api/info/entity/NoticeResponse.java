package com.gk337.api.info.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.gk337.common.core.mvc.model.BaseVo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "공지사항 Response Vo")
public class NoticeResponse extends BaseVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus;

    @ApiModelProperty(notes = "내용", example="제목입니다.")
    private String title;

    @ApiModelProperty(notes = "내용", example="내용입니다.")
    private String content;
    
    @ApiModelProperty(notes = "첨부파일.", example="http://d1n8fbd6e2ut61.cloudfront.net/assets/upload/notice/20200328/236095b1-e76d-4609-a3b5-ab8cc6090d74")
    private String fileUrl;
    
    @ApiModelProperty(notes = "첨부파일.", example="doc1.pdf")
    private String attachFile;

    @ApiModelProperty(notes = "조회수", example="1")
    private Integer readCnt;
    
    @ApiModelProperty(notes = "등록일자", example="'2020-03-28 19:12:11'")
    private String regDate;
    
}