package com.gk337.api.info.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 자주하는 질문
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_faq")
@ApiModel(description = "자주하는 질문 Entity")
public class FaqEntity extends AuditEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @Column(name = "cstatus", nullable = false)
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus;

    @Column(name = "faq_kind")
    @ApiModelProperty(notes = "FAQ 종류")
    private String faqKind;
    
    @Column(name = "title", nullable = false)
    @ApiModelProperty(notes = "제목")
    private String title;

    @Column(name = "content", nullable = false)
    @ApiModelProperty(notes = "내용")
    private String content;

    @Column(name = "read_cnt")
    @ApiModelProperty(notes = "조회수")
    private int readCnt = 0;
    
}