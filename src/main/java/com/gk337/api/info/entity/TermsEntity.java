package com.gk337.api.info.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "jw_terms")
@ApiModel(description = "약관정보 Entity")
public class TermsEntity extends AuditEntity implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(insertable = false, name = "seq", nullable = false)
    @ApiModelProperty(notes = "시퀀스")
    private Integer seq;

    @Column(name = "cstatus", nullable = false)
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)")
    private String cstatus;

    @Column(name = "terms_kind")
    @ApiModelProperty(notes = "약관종류 (U:이용약관, P:개인정보취급방침, L:위치기반 서비스 이용약관)")
    private String termsKind;

    @Column(name = "content")
    @ApiModelProperty(notes = "내용")
    private String content;
    
}