package com.gk337.api.info.entity;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 자주하는 질문 
 */
@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "자주하는 질문 Request Vo")
public class FaqRequest {

	@JsonIgnore
    @ApiModelProperty(notes = "시퀀스", example="1")
    private Integer seq;

	@JsonIgnore
    @ApiModelProperty(notes = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus = "Y";

    @ApiModelProperty(notes = "FAQ 종류", example="L")
    private String faqKind;
    
    @NotNull
    @ApiModelProperty(notes = "제목", example="제목을 입력..")
    private String title;

    @NotNull
    @ApiModelProperty(notes = "내용", example="내용을 입력..")
    private String content;
    
    @JsonIgnore
    @ApiModelProperty(notes = "조회수")
    private int readCnt = 0;
    
}