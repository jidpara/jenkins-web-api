package com.gk337.api.info.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gk337.common.model.AuditEntity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(description = "약관정보 Request Vo")
public class TermsRequest implements Serializable {
	
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ApiModelProperty(value = "시퀀스", example="1")
    private Integer seq;

    @JsonIgnore
    @ApiModelProperty(value = "상태값 (Y:정상, N:비노출, D:삭제)", example="Y")
    private String cstatus = "Y";

    @ApiModelProperty(value = "약관종류 (U:이용약관, P:개인정보취급방침, L:위치기반 서비스 이용약관)", example="P")
    private String termsKind;

    @ApiModelProperty(value = "내용", example="내용을 입력..")
    private String content;
    
}