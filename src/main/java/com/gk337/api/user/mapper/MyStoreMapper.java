package com.gk337.api.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.gk337.api.user.entity.MyStoreRequestVO;
import com.gk337.api.user.entity.MyStoreResponseVO;

@Mapper
public interface MyStoreMapper {

	public List<MyStoreResponseVO> selectList(MyStoreRequestVO request);
}
