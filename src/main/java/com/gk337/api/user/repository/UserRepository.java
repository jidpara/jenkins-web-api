package com.gk337.api.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gk337.api.user.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	Optional<User> findByUid(String uid);
	
//	@Modifying
//	@Query("UPDATE User set u.name = :name "
//			+ " , u.sex = :sex"
//			+ " , u.introduce = :introduce"
//			+ " , u.avata_file_name =:avataFileName"
//			+ "where u.uid = :uid")
//	User modify(@Param("uid") String uid,
//			@Param("name") String name,
//			@Param("sex") String sex,
//			@Param("introduce") String introduce,
//			@Param("avataFileName") String avataFileName);

    //Optional<User> findByUidAndProvider(String uid, String provider);
}