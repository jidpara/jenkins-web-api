package com.gk337.api.user.controller;

import com.gk337.api.user.service.UserService;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.api.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.gk337.common.core.mvc.BaseResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

//@CrossOrigin("*")
@RestController
@RequestMapping("/api/user")
@Api(tags= {"[샘플] 회원정보 API"}, protocols="http", produces="application/json", consumes="application/json")
public class UserRestController {
//
//	@Autowired
//	private UserService userService;
//
//	@Value("${file.upload.directory}")
//	private String dir;
//
//	@ApiOperation(value = "가입", notes = "회원가입을 한다.")
//	@RequestMapping(method = RequestMethod.GET, path="/signup", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	public ResponseEntity<User> signup(
//			@ApiParam(value = "회원ID : 이메일", required = true) @RequestParam String uid,
//			@ApiParam(value = "비밀번호", required = true) @RequestParam String password,
//			@ApiParam(value = "이름", required = true) @RequestParam String name) {
//		return BaseResponse.ok(userService.insertUser(uid, password, name));
//	}
//
//	@ApiOperation(value = "로그인", notes = "이메일 회원 로그인을 한다.")
//	@RequestMapping(method = RequestMethod.GET, path="/signin", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	public ResponseEntity<User> signin(
//			@ApiParam(value = "회원ID : 이메일", required = true) @RequestParam String uid,
//			@ApiParam(value = "비밀번호", required = true) @RequestParam String password) {
//        return BaseResponse.ok(userService.findUser(uid, password));
//    }
//
//	@ApiOperation(value = "수정", notes = "회원정보를 수정한다.")
//	@RequestMapping(method = RequestMethod.POST, path="/modify", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	public ResponseEntity<User> update(
//			@RequestHeader(value="X-AUTH-TOKEN") String xAuthToken,
//			@RequestParam(value="uid") String uid,
//			@RequestParam(value="name") String name,
//			@RequestParam(value="sex", required = false) String sex,
//			@RequestParam(value="introduce", required = false) String introduce,
//			@RequestParam(value="file", required = false) MultipartFile multipartFile) {
//		return BaseResponse.ok(userService.modifyUser(uid, name, sex, introduce, multipartFile));
//	}
//
//	@ApiImplicitParams({
//        @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = true, dataType = "String", paramType = "header")
//	})
//	@ApiOperation(value = "회원 리스트 조회", notes = "모든 회원을 조회한다")
//	@RequestMapping(method = RequestMethod.GET, path="/users", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	public ListVo<User> findAllUser() {
//	    // 결과데이터가 여러건인경우 getListResult를 이용해서 결과를 출력한다.
//	    return BaseResponse.ok(userService.findAllUser());
//	}
//
//	@ApiImplicitParams({
//        @ApiImplicitParam(name = "X-AUTH-TOKEN", value = "로그인 성공 후 access_token", required = false, dataType = "String", paramType = "header")
//	})
//	@ApiOperation(value = "회원 단건 조회", notes = "회원번호(msrl)로 회원을 조회한다")
//	@RequestMapping(method = RequestMethod.GET, path="/user", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	public ResponseEntity<User> findUserById(@ApiParam(value = "언어", defaultValue = "ko") @RequestParam String lang) {
//	    return BaseResponse.ok(userService.findByUid());
//	}

}
