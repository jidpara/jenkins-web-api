package com.gk337.api.user.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gk337.api.user.entity.MyStoreRequestVO;
import com.gk337.api.user.entity.MyStoreResponseVO;
import com.gk337.api.user.service.MyStoreService;
import com.gk337.common.core.mvc.BaseResponse;
import com.gk337.common.exception.JGWServiceException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <PRE>
 * 시스템명   : 중고왕 서비스
 * 업무명     : 상품 RestAPI Controller
 * 프로그램명 : JGWServiceException.java
 *
 *  - 상품관련 RestAPI 컨트롤러 -
 *
 * </PRE>
 *
 * @since 2020.01.26
 * @author sejoonhwang
 * @version 1.0
 * <PRE>
 * 수정 내역:
 * 수정자    수정일자           수정내역
 * ------  ----------  ----------------------------
 *
 * </PRE>
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/api/user/mystore")
@Api(tags= {"[임시] - 내상점 API"}, protocols="http", produces="application/json", consumes="application/json")
public class MyStoreRestController {
	
	@Autowired
	private MyStoreService myStoreService;
//	
//	@ApiOperation(value = "상품저장.", notes = "상품저장.")
//	@RequestMapping(method = RequestMethod.POST, path="/", produces = "application/json")
//	public ResponseEntity<ProductSearchResponse> save(@RequestBody @Valid ProductSearchResponse entity) {
//		return BaseResponse.ok(productService.save(entity));
//	}
//	
//	@ApiOperation(value = "상품수정.", notes = "상품수정.")
//	@RequestMapping(method = RequestMethod.PUT, path="/", produces = "application/json")
//	public ResponseEntity<ProductSearchResponse> update(@RequestBody @Valid ProductSearchResponse entity) {
//		return BaseResponse.ok(productService.save(entity));
//	}
//	
//	@ApiOperation(value = "상품목록조회.", notes = "상품목록조회.")
//	@RequestMapping(method = RequestMethod.GET, path="/", produces = "application/json")
//	public ResponseEntity<List<ProductSearchResponse>> findAll() throws JGWServiceException {
//		return BaseResponse.ok(productService.findAll());
//	}
//	
//	@ApiOperation(value = "SEQ로 상품목록조회", notes = "상품목록조회.")
//	@RequestMapping(method = RequestMethod.GET, path="/{seq}", produces = "application/json")
//	public ResponseEntity<List<ProductSearchResponse>> findBySeq(
//			@ApiParam(value="상품Seq", required=true, defaultValue = "1") @PathVariable(value = "seq", required = true) int seq) {
//		return BaseResponse.ok(productService.findBySeq(seq));
//	}
//	
//	@ApiOperation(value = "상품목록조회 페이징.", notes = "페이징으로 상품목록조회.")
//	@RequestMapping(method = RequestMethod.GET, path="/{page_no}/{page_size}", produces = "application/json")
//	public Page<ProductSearchResponse> findAllPaging(
//			@ApiParam(value="페이지번호", required=true) @PathVariable(value = "page_no", required = true) int pNo,
//			@ApiParam(value="사이즈", required=true) @PathVariable(value = "page_size", required = true) int size
//			) {
//		Direction dir = Sort.Direction.DESC;
//		@SuppressWarnings("deprecation")
//		PageRequest pageable = new PageRequest(pNo-1, size, dir, "id");
//		return productService.findAllPaging(pageable);
//	}
//	
	@ApiOperation(value = "검색조건으로 상품목록조회", notes = "검색조건으로 상품목록조회.")
	@RequestMapping(method = RequestMethod.POST, path="/mystore/search", produces = "application/json")
	public ResponseEntity<List<MyStoreResponseVO>> selectList(@RequestBody @Valid MyStoreRequestVO requestDto) {
		return BaseResponse.ok(myStoreService.selectList(requestDto));
	}
	
}

/*
{
  "seq": 4,
  "cstatus": "Y",
  "memSeq": 1,
  "cateSeq1dp": 1,
  "cateSeq2dp": 1001,
  "cateSeq3dp": 1001015,
  "prodName": "알록달록커튼2",
  "price": 250000,
  "prodDesc": "커튼팔아요 봉은 서비스222222222",
  "asYn": "Y",
  "deliveryPriceYn": "Y",
  "prodStatus": "A",
  "keyWords": "산지이틀,급전필요,직거래가능",
  "readCnt": 0,
  "recommendYn": null
}
*/
