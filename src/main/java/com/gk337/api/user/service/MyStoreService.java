package com.gk337.api.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gk337.api.user.entity.MyStoreRequestVO;
import com.gk337.api.user.entity.MyStoreResponseVO;
import com.gk337.api.user.mapper.MyStoreMapper;
import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.exception.JGWServiceException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MyStoreService {

	
	@Autowired
	private MyStoreMapper myStoreMapper;
	
	public ListVo<MyStoreResponseVO> selectList(MyStoreRequestVO requestDto) {
		ListVo<MyStoreResponseVO> listVo = null;
		try {
			listVo = new ListVo<MyStoreResponseVO>(myStoreMapper.selectList(requestDto));
		} catch (BizException be) {
			throw new JGWServiceException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new JGWServiceException("ERROR.PRODUCT.0001", "조건조회");
		}
		return listVo;
	}

}
