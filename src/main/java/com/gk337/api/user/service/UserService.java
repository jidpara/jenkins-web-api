package com.gk337.api.user.service;

import java.util.Collections;

import com.gk337.api.user.repository.UserRepository;
import com.gk337.api.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.gk337.common.core.exception.BizException;
import com.gk337.common.core.mvc.model.ListVo;
import com.gk337.common.core.security.JwtTokenProvider;
import com.gk337.common.util.FileUtil;

//@RequiredArgsConstructor
@Service
public class UserService { //implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Value("${file.upload.directory}")
	private String directory;

	@Transactional
	public User insertUser(String uid, String password, String name) {
		User user = new User();
		try {
			if(userRepository.findByUid(uid).isPresent()) {
				throw new BizException("ERROR.USER.0001");
			}

			user = userRepository.save(User.builder()
	                .uid(uid)
	                .password(passwordEncoder.encode(password))
	                .name(name)
	                .roles(Collections.singletonList("ROLE_USER"))
	            .build());
			if(user != null) {
				user = this.findUser(uid, password);
			} else {
				throw new BizException("ERROR.COMM.0001");
			}
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("ERROR.COMM.0001");
		}
		return user;
	}

	@Transactional
	public User findUser(String uid, String password) {
		User user = new User();
		String token = "";
		try {
			user = userRepository.findByUid(uid).orElseThrow(() -> new BizException("ERROR.USER.0002"));
			if (!passwordEncoder.matches(password, user.getPassword()))
				throw new BizException("ERROR.USER.0003");
			token = jwtTokenProvider.createToken(String.valueOf(user.getMsrl()), user.getRoles());
			user.setToken(token);
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("ERROR.COMM.0001");
		}
		return user;
	}

	public User modifyUser(String uid, String name, String sex, String introduce, MultipartFile avataFile) {
		User user = new User();
		try {
			// 파일업로드
			String fileName = "";
			if(avataFile != null) {
				FileUtil.fileUpload(directory+"avatars/", avataFile);
				fileName = avataFile.getOriginalFilename();
			}

			user = userRepository.findByUid(uid).orElseThrow(() -> new BizException("ERROR.USER.0002"));
			user.setName(name);
			user.setSex(sex);
			user.setIntroduce(introduce);
			if(!"".equals(fileName)) {
				user.setAvatar(fileName);
			}
			userRepository.save(user);

		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("ERROR.COMM.0001");
		}
		return user;
	}

	@Transactional
	public ListVo<User> findAllUser() {
		ListVo<User> listVo = null;
		try {
			listVo = new ListVo<User>(userRepository.findAll());
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("ERROR.COMM.0001");
		}
		return listVo;
	}

	@Transactional
	public User findByUid() {
		User user = null;
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		    String uid = authentication.getName();
		    user = userRepository.findByUid(uid).get();
		} catch (BizException be) {
			throw new BizException(be.getMessage());
		} catch(Exception e) {
			e.printStackTrace();
			throw new BizException("ERROR.COMM.0001");
		}
		return user;
	}

//	@Override
//	public UserDetails loadUserByUsername(String userPk) throws UsernameNotFoundException {
//		return userRepository.findById(Long.valueOf(userPk)).orElseThrow(null);
//	}

}
