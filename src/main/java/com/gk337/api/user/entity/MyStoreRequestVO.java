package com.gk337.api.user.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "상품조회 REQUEST VO")
public class MyStoreRequestVO {

    @ApiModelProperty(notes = "조회모드", example = "buy, sell")
    private String mode;

    @ApiModelProperty(notes = "회원 SEQ목록", example = "1")
    private Integer memSeq;
    
    @ApiModelProperty(notes = "정렬기준", example = "price, latest")
    private String orderBy;
}
