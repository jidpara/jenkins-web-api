package com.gk337.api.user.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "상품조회 RESPONSE VO")
public class MyStoreResponseVO {

    @ApiModelProperty(notes = "SEQ", example = "1")
    private Integer seq;
    
    @ApiModelProperty(notes = "상품명", example = "안마기")
    private String prodName;
    
    @ApiModelProperty(notes = "가격", example = "250000")
    private Integer price;

    @ApiModelProperty(notes = "상태값 (Y:판매중, H:대기, E:마감, N:차단, D:삭제)", example = "Y")
    private String cstatus;

    @ApiModelProperty(notes = "등록시간", example = "9분전, 2020-10-23")
    private String timeDiff;
    
    @ApiModelProperty(notes = "메인이미지", example = "123.jpg")
    private String firstImg;
    
    @ApiModelProperty(notes = "조회모드", example = "buy, sell")
    private String mode;

}
