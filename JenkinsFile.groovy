

node{


    stage('Git'){
        git credentialsId: 'BitBucket', url: 'https://jidpara@bitbucket.org/jidpara/jenkins-web-api.git'
    }
    stage('setting'){
        configFileProvider([configFile('maven_settings_xml')]) {
            echo 'setting'
        }
    }
    stage('build'){
        def mvnHome = tool 'MAVEN_HOME'
        sh "${mvnHome}/bin/mvn -P local -Dmaven.test.skip=true clean install"
    }


    stage('Send'){
        sshPublisher(
                publishers:
                        [sshPublisherDesc(configName: 'oh', transfers: [sshTransfer(cleanRemote: false, excludes: '', execCommand: '', execTimeout: 120000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: '', remoteDirectorySDF: false, removePrefix: '/target', sourceFiles: '/target/*.jar')], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false)])
    }
}





